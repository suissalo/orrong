$(document).on('click', '#header [href^="#"], .btn-travel', function (event) {
      event.preventDefault();

      $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top - $('#header').height()
      }, 500);
  });
