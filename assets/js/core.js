/**
 * Add ECMA262-5 Array methods if not supported natively
 */
//extend for Array filter
if (!Array.prototype.filter) {
	Array.prototype.filter = function (fun /*, thisp */ ) {
		"use strict";
		if (this == null)
			throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun != "function")
			throw new TypeError();

		var res = [];
		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) {
				var val = t[i]; // in case fun mutates this
				if (fun.call(thisp, val, i, t))
					res.push(val);
			}
		}
		return res;
	};
}
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
	Object.defineProperty(Array.prototype, 'find', {
		value: function (predicate) {
			// 1. Let O be ? ToObject(this value).
			if (this == null) {
				throw new TypeError('"this" is null or not defined');
			}

			var o = Object(this);

			// 2. Let len be ? ToLength(? Get(O, "length")).
			var len = o.length >>> 0;

			// 3. If IsCallable(predicate) is false, throw a TypeError exception.
			if (typeof predicate !== 'function') {
				throw new TypeError('predicate must be a function');
			}

			// 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
			var thisArg = arguments[1];

			// 5. Let k be 0.
			var k = 0;

			// 6. Repeat, while k < len
			while (k < len) {
				// a. Let Pk be ! ToString(k).
				// b. Let kValue be ? Get(O, Pk).
				// c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
				// d. If testResult is true, return kValue.
				var kValue = o[k];
				if (predicate.call(thisArg, kValue, k, o)) {
					return kValue;
				}
				// e. Increase k by 1.
				k++;
			}

			// 7. Return undefined.
			return undefined;
		},
		configurable: true,
		writable: true
	});
}
// extend for Array indexOf
if (!('indexOf' in Array.prototype)) {
	Array.prototype.indexOf = function (find, i /*opt*/ ) {
		if (i === undefined) i = 0;
		if (i < 0) i += this.length;
		if (i < 0) i = 0;
		for (var n = this.length; i < n; i++)
			if (i in this && this[i] === find)
				return i;
		return -1;
	};
}

var QueryString = function () {
	// This function is anonymous, is executed immediately and
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [query_string[pair[0]], pair[1]];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	}
	return query_string;
}();


var animations_completed = false;

//WOW.js
new WOW().init();

//For Mobile View
function updateContainer() {

    var $containerWidth = $(window).width();
    var $containerHeight = $(window).height();
    if ($containerWidth <= 768) {
        $(".main-wrapper").addClass("mobile");
        $(".mobile-wrapper h1").html("You <i>only</i><br> Live once, <br><i>so live</i> exceptionally.");
        $(".mobile-wrapper .success p").html("One of our agents will be in touch with you promptly. If however you would prefer to contact us directly, our details are above.");
    }
    if ($containerWidth > 768 ) {
        $(".main-wrapper").removeClass("mobile");
        $("#home").css("display", "table-cell");
    }
}

//Check active section
function checkActives() {
	var home = document.getElementById("home");
     var regform = document.getElementById("regform");
     var thankyou = document.getElementById("thankyou");
     var form = document.getElementById("regform").getElementsByClassName("reg-form");
	  if (window.getComputedStyle(regform).display === "block") {
	    home.style.display = "none";
	  } 
	  if (window.getComputedStyle(thankyou).display === "block") {
	    home.style.display = "none";
	  } 

}



$(document).ready(function () {
	updateContainer();

	$(".regform-caption, .reg-form, .success").clone().appendTo(".mobile-wrapper");
	$(".mobile-wrapper .success p").html("One of our agents will be in touch with you promptly. If however you would prefer to contact us directly, our details are above.")
	

	$(window).trigger('resize');
	$(window).bind('resize', function () { 
	     updateContainer();
	     checkActives();
	});

	
	var ini_auto_google_anchor_events = auto_google_anchor_events();
	

	
	if ($('.form-validation-errors').length) {
		var pos = $('.form-validation-errors').offset().top - $('header').outerHeight() - 150;

		$('html, body').animate({
			'scrollTop': pos
		}, 600, 'swing', function () {
			$('.form-validation-errors').fadeOut(500, function () {
				$('.form-validation-errors').fadeIn(500);
			});
		});
	}



	// Scroll to
	if($('.js-scroll-to').length) {
		$('.js-scroll-to').on('click tap touch', function(e) {
			e.preventDefault();
			var el = $(this).attr('href').slice(1); //remove # from href
			scroll_to(el);
		});
	}

	

	//Notifications
	if ($('#notifications').children().length) {
		var ini_notifications = init_notifications();
	}


	/** Helpers fn */
	function offset(el) {
		var rect = el.getBoundingClientRect(),
			scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
			scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return {
			top: rect.top + scrollTop,
			left: rect.left + scrollLeft
		}
	}

	
	//Click register button
	$('.register').on('click',function(){
        $(".logo1, .logo2").addClass("smaller");
        $(".mobile-wrapper .right-mobile, .mobile-wrapper,.mobile-wrapper .home-caption, .mobile-wrapper .regform-caption").hide();
        $(".regform-caption, .reg-form").show();
        $(".main-wrapper .right").css("z-index", "7");
        //updateContainer();
        checkActives();
        $(".home-caption").hide();
    });

	//Click Button
    $(".mobile-wrapper .button").click(function() {
        $(".main-wrapper .regform-caption").hide();
        $(".main-wrapper .reg-form").hide();
        $(".right-mobile, .mobile-wrapper,.mobile-wrapper .home-caption").show();

        //Scroll to Form
        $('html,body').animate({
            scrollTop: $(".mobile-wrapper .reg-form").show().offset().top},
            'slow');
    }); 

    //Click Submit button
    $('.submits').on('click',function(e){
    	e.preventDefault();
    	var form = $(this).closest("form");
    	var data = form.serialize();
    	console.debug(data);
    	$.post(form.attr('action'), data, function(response){
    		if(response){
    			$(".validation-error").show().html(response);
    		} else {
    			$(".validation-error").hide();


    			$(".home-caption").hide();
		        $(".main-wrapper .reg-form form").hide();
		        $(".mobile-wrapper .reg-form form").show();
		        $(".reg-form .blank-space").show();
		        $(".thankyou-caption").show();
		        $(".regform-caption").hide();
		        $(".success").show();
		        //updateContainer();
		        checkActives();
    		}
    	});
    });




}); /* END DOCUMENT READY */


function scroll_to(el)
{
	if($('#' + el).length) {
		$('html, body').stop();
		var scrollPos = ($('#' + el).offset().top - ($('header').outerHeight() + $('.mobile-register').outerHeight()) ) +2;

		$('html, body').animate({
	        scrollTop: scrollPos
	    }, 500);
	}
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}


function auto_google_anchor_events() {
	$('a').on('click tap touch', function () {
		var el = $(this);
		var params = {},
			event_action = '';
		params.event_category = '';
		params.event_label = '';

		if (el.data('eventcategory')) {
			params.eventCategory = el.data('eventcategory');
		} else if (el.attr('href')) {
			event_action = 'click';
			if (el.attr('href').indexOf('mailto:') >= 0) {
				params.event_category = 'email';
				params.event_label = el.attr('href');
			} else if (el.attr('href').indexOf('tel:') >= 0) {
				params.event_category = 'phone';
				params.event_label = el.attr('href');
			}
		}
		if (el.data('eventaction')) {
			event_action = el.data('eventaction');
		}

		if (el.data('eventlabel')) {
			params.event_label = el.data('eventlabel');
		} else if (typeof el.attr('title') !== 'undefined') {
			params.event_label = el.attr('title');
		}

		if (params.event_category.length && typeof gtag !== 'undefined') {
			gtag('event', event_action, params);
		}
	});
}

function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this,
			args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function init_notifications() {

	$('#notifications').children().on('click tap touch', '.notification-close', function () {
		var $currElem = $(this).closest('#info-box');
		$currElem.fadeOut(function () {
			$currElem.remove();
		});
	});

	$('#notifications').children().each(function () {
		var $currElem = $(this);
		$currElem.fadeIn();
		if ($currElem.data('timeout')) {
			var waitSecs = $currElem.data('timeout') * 1000;
			var tmp = setTimeout(function () {
				$currElem.fadeOut(500, function () {
					$currElem.remove();
				});
			}, waitSecs);
		}
	});

	$('#notifications').delay(750).fadeIn(400);

	$('#notifications .additional').delay(1200).fadeIn(400);

	$('#notifications .additional .close-additional').on('click tap touch', function () {
		$('#notifications .additional').fadeOut(600);
	});
}

function formSubmit() {
	$('form').on('submit', function (e) {
		var el = $(this);
		var $button = el.find('[type="submit"]');
		var loading = '<i class="fal fa-spinner fa-spin"></i>';
		$button.html(loading);
		$button.prop('disabled', true);
	});
}
