//update an element with the geo location from google maps
function update_geo(target, lat, long) {
    if($(target).length) {
        $(target).val(lat + ', '+long);
    }
}