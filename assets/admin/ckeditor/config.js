/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.contentsCss = ['/assets/css/bootstrap.css','/assets/css/library.css','/assets/css/main.css','/assets/admin/ckeditor/contents.css'];
	config.extraAllowedContent = 'div[*]{*}(*); article[*]{*}(*); aside[*]{*}(*); details[*]{*}(*); figcaption[*]{*}(*); figure[*]{*}(*); footer[*]{*}(*); header[*]{*}(*); hgroup[*]{*}(*); main[*]{*}(*); nav[*]{*}(*); section[*]{*}(*); summary[*]{*}(*); h1[*]{*}(*); h2[*]{*}(*); h3[*]{*}(*); h4[*]{*}(*); h5[*]{*}(*); h6[*]{*}(*); p[*]{*}(*); a[*]{*}(*); span[*]{*}(*); table[*]{*}(*); tr[*]{*}(*); td[*]{*}(*); img[*]{*}(*); body[*]{*}(*); html[*]{*}(*); head[*]{*}(*); style[*]{*}(*); meta[*]{*}(*);';

	config.line_height="0.75em1em;1.1em;1.2em;1.3em;1.4em;1.5em;1.75em;2em;2.5em;3em;10px;12px;14px;16px;18px;20px;22px;24px;28px;32px;36px;";

};
