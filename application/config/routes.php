<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

## ADMIN SECTION ##

//$route['admin/process-login'] 					= 'admin/admin/process_login';
$route['admin/login'] 							= 'admin/admin/login';
$route['admin/logout'] 							= 'admin/admin/logout';
//$route['admin/update-dashboard'] 				= 'admin/admin/update_dashboard';
$route['admin/ajax/(.+)'] 						= 'admin/ajax/$1';
$route['admin/forgot_password'] 				= 'admin/admin/forgot_password';
$route['admin/enter_reset_password/(.+)'] 		= 'admin/admin/enter_reset_password/$1';
$route['admin/change_password/(.+)'] 			= 'admin/admin/change_password/$1';

$route['admin/dashboard'] 						= 'admin/admin/index'; 

$route['admin/pages/(.+)'] 						= 'admin/pages/$1';
$route['admin/pages'] 							= 'admin/pages/index';

$route['admin/sections/(.+)'] 					= 'admin/sections/$1';
$route['admin/sections'] 						= 'admin/sections/index';

$route['admin/galleries/(.+)'] 					= 'admin/galleries/$1';
$route['admin/galleries'] 						= 'admin/galleries/index';

$route['admin/settings/(.+)'] 					= 'admin/settings/$1';
$route['admin/settings'] 						= 'admin/settings/index';

$route['admin/blog/(.+)'] 						= 'admin/blog/$1';
$route['admin/blog'] 							= 'admin/blog/index';

$route['admin/users/(.+)'] 						= 'admin/users/$1';
$route['admin/users'] 							= 'admin/users/index';

$route['admin/projects/(.+)'] 					= 'admin/projects/$1';
$route['admin/projects'] 						= 'admin/projects/index';

$route['admin/leads/(.+)'] 						= 'admin/leads/$1';
$route['admin/leads'] 							= 'admin/leads/index';

$route['admin'] 								= 'admin/admin/index'; 

## FRONTEND ##
$route['default_controller'] 					= "pages";
$route['sitemap\.xml'] 							= "pages/sitemap";

$route['ajax/(.+)'] 							= 'ajax/$1';


$route['(.+)'] 									= 'pages/index';
$route['404_override'] 							= '';
$route['translate_uri_dashes'] 					= FALSE;

