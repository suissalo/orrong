<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// hr_title is the name of the section shown in the admin
// main is the info that is one off to the section - think the main title and description above the row
// items are the multiple "columns" inside the row
// item_class is applied to items to make them span full width or horizontally stack
// field types - text, textarea, ckeditor, image, file
// on the same level as the field type, 'class' can be specified to limit the width - similar to item_class above.
// image_sizes - specifies the name of a diffrent image sizing array to use ie. 'section_sizes'
// file_path - specify a different save path for uploaded images
// information - a piece of explananatory text to show for the admin user.
// limit - limits the amount of items/columns that can be added.

$config['section_sizes'] = array(
    'full' 		=> array('2400', '2400'),
    'desktop' 	=> array('1200', '1200'),
    'tablet' 	=> array('1000', '1000'),
    'mobile' 	=> array('800', '800'),
    'thumb' 	=> array('400', '400'),
);

$config['section_path'] = 'assets/media/sections/';


$config['section_type']['contact_form'] = array(
	'hr_title' => 'Contact Form',
	'main' => array(
        'form_id' => array(
            'field' => 'text',
            'class' => 'col-sm-30',
        ),
        'form_name' => array(
            'field' => 'text',
            'class' => 'col-sm-30',
        ),

        'section_ID' => array(
    			'field' => 'text',
    		)
    ),
	'limit' => '',
	'image_sizes' => '',
	'file_path' => '',
);
$config['section_type']['register_your_interest'] = array(
	'hr_title' => 'Register Your Interest',
	'limit' => '',
	'image_sizes' => '',
	'file_path' => '',
);
$config['section_type']['image_article'] = array(
	'hr_title' => 'Article with Images',

	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'text_position' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'left' => 'Left',
				'right' => 'Right',
			)
		),
		'background_color' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'white' => 'White',
				'grey' => 'Grey',
			)
		),
		'title' => array(
			'field' => 'text',
		),
		'description' => array(
			'field' => 'textarea',
		),
		'link_text' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'link_url' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),

	),

	'items' => array(
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60'
		),
		'show_artists_impression' => array(
			'field' => 'select',
			'class' => 'col-sm-60',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
    'artist_impression_additional_text' => array(
			'field' => 'text',
		),
	),

	'limit' => '',
	'item_class' => 'col-sm-20',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['full_image'] = array(
	'hr_title' => 'Full Image',


	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
	),

	'items' => array(
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60'
		),
		'show_artists_impression' => array(
			'field' => 'select',
			'class' => 'col-sm-60',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
    'artist_impression_additional_text' => array(
			'field' => 'text',
		),
	),

	'limit' => '',
	'item_class' => 'col-sm-20',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['masonry_gallery'] = array(
	'hr_title' => 'Masonry Gallery',


	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
	),

	'items' => array(
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60'
		),
		'show_artists_impression' => array(
			'field' => 'select',
			'class' => 'col-sm-60',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
    'artist_impression_additional_text' => array(
			'field' => 'text',
		),
	),

	'limit' => '',
	'item_class' => 'col-sm-20',
	'image_sizes' => '',
	'file_path' => '',

);


$config['section_type']['3_column_article'] = array(
	'hr_title' => '3 Column Article',

	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
	),

	'items' => array(
		'title' => array(
			'field' => 'text',
		),
		'description' => array(
			'field' => 'ckeditor',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-20'
		),

	),

	'limit' => '3',
	'item_class' => 'col-sm-60',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['text_section'] = array(
	'hr_title' => 'Text Section',

	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
    'extra_padding' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
        'no' => 'No',
				'yes' => 'Yes',
			)
		),
    'gradient_bg' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
        'no' => 'No',
				'yes' => 'Yes',
			)
		),
    'section_ID' => array(
			'field' => 'text',
		),
    'title' => array(
			'field' => 'text',
		),
    'subtitle' => array(
			'field' => 'text',
		),
		'text' => array(
			'field' => 'ckeditor',
		),
	),

	'limit' => '',
	'item_class' => 'col-sm-60',
	'image_sizes' => '',
	'file_path' => '',

);
