<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//width, height
$config['hero_sizes'] = array(
    'full' 		=> array('2400', '2400'),
    'desktop' 	=> array('1200', '1200'),
    'tablet' 	=> array('1000', '1000'),
    'mobile' 	=> array('800', '800'),
    'thumb' 	=> array('400', '400'),
);

$config['gallery_path'] = 'assets/media/gallery/';
$config['gallery_sizes'] = array(
    'full' 		=> array('2400', '2400'),
    'desktop' 	=> array('1200', '1200'),
    'tablet' 	=> array('1000', '1000'),
    'mobile' 	=> array('800', '800'),
    'thumb' 	=> array('400', '400'),
);

$config['user_image'] = array(
    'desktop' 	=> array('1200', '1200'),
    'tablet' 	=> array('1000', '1000'),
    'mobile' 	=> array('800', '800'),
    'thumb' 	=> array('400', '400'),
);

$config['blog_file_path'] 	= 'assets/media/';

$config['page_templates'] = array(
	// 'default' 		=> 'Page',
	// 'home' 			=> 'Home',
	// 'contact' 		=> 'Contact',
	'roi' 		=> 'ROI',
	// 'blog' 			=> 'Blog',
	//'thankyou' 			=> 'Thank You',
	'404' 			=> '404',
	// 'projects' 		=> 'Projects',

);

$config['site_author'] 					= "Barking Bird";
$config['site_name'] 					= "700 Orrong Road, Toorak";

$config['process_file_path'] 			= 'assets/admin/imports/';
$config['export_file_path'] 			= 'assets/admin/exports/';

$config['form_path'] = 'assets/media/forms';
$config['css'] = array(
	//'/assets/css/reset.css',
	//'/assets/css/normalize.css',
	'//use.typekit.net/dey2ytt.css', //Adobe Fonts: Miller Banner & Brassey
	'//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css', //Animate css
	'/assets/css/styles.css',
);
$config['js'] = array(
	'//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
	'/assets/js/wow.min.js',
	'/assets/js/core.js?v=1',
);

$config['admin_js'] = array(
	'/assets/admin/js/library.js',
	'/assets/admin/js/bootstrap-switch.js',
	'/assets/admin/js/bootstrap-select.js',
	'/assets/admin/ckeditor/ckeditor.js',
	'/assets/admin/ckfinder/ckfinder.js',
	'/assets/admin/js/dropzone.js',
	'/assets/admin/js/main.js'
);
$config['admin_css'] = array(
	'/assets/admin/styles/bootstrap.css',
	'/assets/admin/styles/flat-ui.css',
	'/assets/admin/styles/main.css',
	'/assets/admin/styles/jquery.fancybox.css',
	'/assets/admin/styles/dropzone.css'
);

$config['admin_menu'] = array(
	array(
		'title' => 'Dashboard',
		'url'	=> 'admin/dashboard',
	),
	array(
		'title' => 'Pages',
		'url'	=> 'admin/pages',
	),

	array(
		'title' => 'Leads',
		'url'	=> 'admin/leads',
	),

);

$config['access_permissions'] = array(
	1 => 'ALL',
	2 => array(
			'admin/dashboard',
			'admin/users/edit',
			'admin/users/save_edit',
			'admin/leads/detail',
		),
);

$config['icon_image'] = '<img onerror="this.src=\'/assets/admin/images/icons.png\'" src="/assets/admin/images/icons.svg" />';
