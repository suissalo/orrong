<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="" lang="en"> <!--<![endif]-->

<head>
	<title><?php echo (($meta_title)? $meta_title  : config_item('site_name')).' - '.config_item('site_name'); ?></title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="<?php echo $meta_keywords; ?>">
	<meta name="description" content="<?php echo $meta_description; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="canonical" href="<?php echo base_url(substr($_SERVER['REQUEST_URI'], 1)); ?>">

	<link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon.ico/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon.ico/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon.ico/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon.ico/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon.ico/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon.ico/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon.ico/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon.ico/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon.ico/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon.ico/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon.ico/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon.ico/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.ico/favicon-16x16.png">
	<link rel="manifest" href="/assets/images/favicon.ico/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/images/favicon.ico/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/fonts.css'); ?>" /> -->
	
	<?php if(isset($css) && count($css) > 0) : 
		foreach ($css as $key => $file) : ?>
			<link type="text/css" rel="stylesheet" href="<?php echo base_url($file); ?>" />
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if (isset($og) && is_array($og)): ?>
		<?php foreach ($og as $og_key => $og_value): ?>
			<meta property="og:<?php echo $og_key; ?>" content="<?php echo $og_value; ?>" />  
		<?php endforeach ?>
	<?php endif ?>

	<?php if(isset($js) && count($js) > 0) : ?>
		<?php  foreach ($js as $key => $file): ?>
			<script type="text/javascript" src="<?php echo base_url($file); ?>"></script>
		<?php endforeach ?>
	<?php endif; ?>

	<?php if(isset($map['js']) && $map['js']) : ?>
		<?php echo $map['js']; ?>
	<?php endif; ?>

	<!--[if lt IE 9]>
	<script src="<?php echo base_url('assets/js/lib/html5shiv.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/lib/respond.js'); ?>"></script>
	<![endif]-->

</head>
<body class="template-roi <?php echo 'page-'.url_title(uri_string()); ?>">

	<div id="wrap">
		<header id="header" class="header">
		<a class="menu-toggle" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
		<a href="<?php echo base_url(); ?>" class="menu__mobile-logo">	
			<img class="img-responsive" src="<?php echo base_url('assets/images/logos/your_logo.png'); ?>" alt="<?php echo config_item('site_name'); ?>" />
		</a>
		<nav class="menu">
			<a class="menu-close" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
			<div class="menu__inner container">
				<div class="menu__inner-wrapper">
					<a class="visible-xs top-lvl-item menu-item menu__apartments" href="/">Home</a>
					<?php echo main_page_navigation(1); ?>
				</div>
			</div>
			
		</nav>
	</header>
		<main id="main">
			<?php echo get_message(); ?>
			<?php echo $page_content; ?>
		</main>
	</div>

	<footer>
	
	</footer>
	<script type="text/javascript">
		var base_url = '<?php echo base_url() ?>';
	</script>

	<script type="text/javascript">
	<?php
	if (isset($_POST) && isset($_POST['ref']) && $_POST['ref']) :
		foreach ($this->form_validation->form_errors_array() as $error_key => $error_value) :
	?>
	$('form[name="<?php echo $_POST['ref']; ?>"]').find('[name="<?php echo $error_key; ?>"]').addClass('form-error');
	<?php			
		endforeach;
	endif; 
	?>
	</script>

</body>
</html>