



<?php if ($row->sub_heading || $row->detail) : ?>
<div class="page-content">
	<div class="container">

		<?php if ($row->sub_heading) : ?>
		<h1 class="heading"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php
if ($page_sections) :
	foreach ($page_sections as $section) :
		echo $section->html;
	endforeach;
endif;
?>
