<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="" lang="en"> <!--<![endif]-->

<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TX96BHW');</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160387029-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-160387029-1');
	</script>

	<title><?php echo (($meta_title)? $meta_title  : config_item('site_name')).' - '.config_item('site_name'); ?></title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="<?php echo $meta_keywords; ?>">
	<meta name="description" content="<?php echo $meta_description; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="canonical" href="<?php echo base_url(substr($_SERVER['REQUEST_URI'], 1)); ?>">

	<link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon.ico/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon.ico/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon.ico/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon.ico/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon.ico/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon.ico/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon.ico/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon.ico/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon.ico/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon.ico/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon.ico/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon.ico/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.ico/favicon-16x16.png">
	<link rel="manifest" href="/assets/images/favicon.ico/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/images/favicon.ico/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link rel="stylesheet" href="https://use.typekit.net/awp6awo.css">
	<?php if(isset($css) && count($css) > 0) :
		foreach ($css as $key => $file) : ?>
			<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if (isset($og) && is_array($og)): ?>
		<?php foreach ($og as $og_key => $og_value): ?>
			<meta property="og:<?php echo $og_key; ?>" content="<?php echo $og_value; ?>" />
		<?php endforeach ?>
	<?php endif ?>

	<?php if(isset($js) && count($js) > 0) : ?>
		<?php  foreach ($js as $key => $file): ?>
			<script type="text/javascript" src="<?php echo $file; ?>"></script>
		<?php endforeach ?>
	<?php endif; ?>

	<?php if(isset($map['js']) && $map['js']) : ?>
		<?php echo $map['js']; ?>
	<?php endif; ?>

	<!--[if lt IE 9]>
	<script src="<?php echo base_url('assets/js/lib/html5shiv.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/lib/respond.js'); ?>"></script>
	<![endif]-->

<!------------------- BUILD BY -----------------------
   ___           __    _             ___  _        __
  / _ )___ _____/ /__ (_)__  ___ _  / _ )(_)______/ /
 / _  / _ `/ __/  '_// / _ \/ _ `/ / _  / / __/ _  /
/____/\_,_/_/ /_/\_\/_/_//_/\_, / /____/_/_/  \_,_/
                           /___/
----------------------------------------------------->
</head>
<body class="<?php echo 'template-'.@$template; ?> <?php echo 'page-'.url_title(uri_string()); ?>">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TX96BHW"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
	<!-- START MAIN WRAPPER -->
   <div class="main-wrapper">
			<?php echo get_message(); ?>
			<?php echo $page_content; ?>
	</div>

	
	<?php /* need to render all possible recaptcha fields on the page */ ?>
	<?php echo $this->recaptcha->add_reCAPTCHA(array());?>

	<script type="text/javascript">
		var base_url = '<?php echo base_url() ?>';
	</script>

</body>
</html>
