<section class="page-thankyou">

	<div class="page-thankyou-middle">
    <?php echo file_get_contents(base_url('assets/images/brandmark.svg'));?>
    <h2>Thank you for registering</h2>
		<p>An agent will be in touch with you shortly.<br/> Alternatively, <strong>Russell Murphy</strong> is available to answer your immediate questions now on <a href="tel:0407 839 184">0407&nbsp;839&nbsp;184</a></p>
		<a class="btn return-home-btn" href="<?php echo base_url(); ?>">Return Home</a>
	</div>
</section>
