<div class="left">
	        	
        <div class="logo1">
            <div><img src="assets/images/orrong-logo.png" alt="Orrong logo"></div>
        </div>

        <!-- START REGISTRATION FORM -->
            <div id="regform" class="regform-caption">
        		<p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><i>expressions of</i> <span>Interest</span></p>
        	</div>
            <div class="reg-form">
            	<div class="validation-error">All fields are required.</div>
            	<form class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" id="first_form" method="post" action="">
            		<div class="reg-input">
            			<input type="text" name="db_first_name" id="fname" placeholder="First Name">
            		</div>
            		<div class="reg-input">
            			<input type="text" name="db_last_name" id="lname" placeholder="Surname">
            		</div>
            		<div class="reg-input">
            			<input type="text" name="db_email" id="email" placeholder="Email">
            		</div>
            		<div class="reg-input">
            			<input  type="tel" name="db_phone" id="phone" placeholder="Phone">
            		</div>
            		<div class="reg-input">
            			<select name="db_hear_about_us" id="hear">
            				<option value="">Where did you hear about us?</option>
            				<option value="Internet">Internet</option>
            				<option value="Social Media">Social Media</option>
            				<option value="Hotels">Hotels</option>

            			</select>
            		</div>
            		<input type="hidden" name="ref" value="register" />
					<input type="hidden" name="form" value="MQ" />
            		<input class="button submits" type="submit" value="submit">
            	</form>
            	<div class="blank-space"></div>
            	<div class="contact-details wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
            		<div class="left">
            			<img src="assets/images/marshall-white-logo.png" alt="">
            			<span>Marcus Chiminello<br><i>0411 411 271</i></span>
            			<span>Liron Selimi<br><i>0425 77 66 22</i></span>
            		</div>
            		<div class="right kay">
            			<img src="assets/images/kay-burton-logo.png" alt="">
            			<span>Daniel Bradd<br><i>0411 347 511</i></span>
            			<span>John Bradbury<br><i>0413 772 778</i></span>
            		</div>
            	</div>
            </div>
        <!-- END REGISTRATION FORM -->

        <!-- START SUCCESS FORM -->
        <div id="thankyou" class="thankyou-caption">
    		<p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><i>Thank you for</i> <span>registering</span></p>
    	</div>
    	<div class="success wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    		<p>One of our agents will be in touch with you promptly. If however you would prefer to contact us directly, our details are below:</p>
    		<!-- <div class="contact-details wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
        		<div class="left">
        			<img src="assets/images/marshall-white-logo.png" alt="">
        			<span>Marcus Chiminello<br><i>0411 411 271</i></span>
        			<span>Liron Selimi<br><i>0425 77 66 22</i></span>
        		</div>
        		<div class="right kay">
        			<img src="assets/images/kay-burton-logo.png" alt="">
        			<span>Daniel Bradd<br><i>0411 347 511</i></span>
        			<span>John Bradbury<br><i>0413 772 778</i></span>
        		</div>
        	</div> -->
    	</div>
    	 <!-- END SUCCESS FORM -->

        <div class="logo2">
        	<p>Architecture and Interiors by:</p>
            <img src="assets/images/nicholas-border.png" alt="Orrong logo">
        </div>
    </div>
    <div class="right">
    	<div class="home-wrapper">
            <div id="home" class="home-caption">
	        	<p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">For architectural mastermind Nicholas Day, comes five exceptionally appointed residences in Melbourne's exclusive Toorak</p>
	        	<h1 class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s">You <i>only</i> Live once, <br><i>so live</i> exceptionally.</h1>
	        	<a class="button register wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s">register</a>
	        </div>
	    </div>
    </div>

    <div class="right-mobile">
    	<div class="bg"></div>
    </div>
    <div class="mobile-wrapper">
    	<div class="home-caption">
        	<h1 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">You <i>only</i> Live once, <br><i>so live</i> exceptionally.</h1>
        	<p class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.6s">For architectural mastermind Nicholas Day, comes five exceptionally appointed residences in Melbourne's exclusive Toorak</p>
        	<a class="button register wow fadeIn" data-wow-duration="2s" data-wow-delay="0.8s">register</a>
        </div>
        <div class="logo2">
        	<p>Architecture and Interiors by:</p>
            <img src="assets/images/nicholas-border.png" alt="Orrong logo">
        </div>
	</div>