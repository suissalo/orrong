<?php 
if(isset($gallery_images) && $gallery_images) : 
	$this->load->view('pages/_partials/hero_gallery', array('images' => $gallery_images));
endif; 
?>

<?php if ($row->sub_heading || $row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php 
if ($page_sections) : 
	foreach ($page_sections as $section) : 
		echo $section->html; 
	endforeach;
endif;
?>

<!-- An example for your convenience -->
<div class="register__form">
		<?php
			$form_name = '';
			$form_id = '';
			$sent = FALSE;
			$errors = FALSE;

			if (isset($form_result) && $form_result) 
			{
				if (isset($form_result->form_name) && $form_result->form_name == $form_name) 
				{
					$errors = TRUE;
					if (isset($form_result->form_success) && $form_result->form_success == TRUE) 
					{
						$sent = TRUE;
						$errors = FALSE;
					}
				}
			} 
			?>
				<?php if ($sent) : ?>

				<div class="form-thankyou">
					<h1 class="h2">Thank you for your registration of interest.</h1>
				</div>

				<?php else : ?>

				<?php if ($errors && validation_errors()) : ?>
				<div class="form-validation-errors">
					<p>Please fill in the required fields.</p>
				</div>
				<?php endif; ?>


				<form class="" method="POST" action="" name="<?php echo $form_name; ?>" id="enquiry">
					<input type="hidden" name="ref" value="<?php echo $form_name; ?>" />
					<input type="hidden" name="form" value="<?php echo $form_id;?>" />
					<input type="hidden" name="db_lead_source" value="Web" />

					<input type="text" name="db_first_name" placeholder="First name*" value="<?php echo set_value('db_first_name'); ?>" required />
					<input type="text" name="db_last_name" placeholder="Surname*" value="<?php echo set_value('db_last_name'); ?>" required />
					<input type="email" name="db_email" placeholder="Email*" value="<?php echo set_value('db_email'); ?>" required />
					<input type="text" name="db_phone" placeholder="Phone*" value="<?php echo set_value('db_phone'); ?>" required />
					<div class="custom-select">
						<select name="db_source" required >
							<option selected disabled>How did you hear about us?*</option>
							<option value="Google Search" <?php echo set_select('db_source', 'Google Search'); ?> >Google Search</option>
							<option value="Realestate.com.au" <?php echo set_select('db_source', 'Realestate.com.au'); ?> >Realestate.com.au</option>
							<option value="Signage" <?php echo set_select('db_source', 'Signage'); ?> >Signage</option>
							<option value="Walk-in" <?php echo set_select('db_source', 'Walk-in'); ?> >Walk-in</option>
							<option value="Word of month" <?php echo set_select('db_source', 'Word of month'); ?> >Word of month</option>
						</select>
					</div>
					
					<button type="submit" class="button button--secondary">Submit</button>
				</form>
			<?php endif; ?>
		</div>