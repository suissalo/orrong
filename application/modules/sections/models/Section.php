<?php 
class Section extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_sections($page_id = FALSE, $data = FALSE) 
	{
		if (!$page_id) return FALSE;

		$this->db->where('page_id', $page_id);
		$this->db->where('published', 1);
		$this->db->order_by('display_order');
		$query = $this->db->get('sections');

		if ($query->num_rows() > 0)
		{
			$sections = $query->result();

			$count = 0; 
			foreach ($sections as $key => $section)
			{
				$count++; 
				if ($section->content)
				{
					$section->content = json_decode($section->content);
				}

				$this->db->where('section_id', $section->id);
				$this->db->order_by('display_order');
				$item_query = $this->db->get('section_items');

				$section->items = FALSE;

				if ($item_query->num_rows() > 0)
				{
					$section->items = array();
					$items = $item_query->result();

					foreach ($items as $ikey => $item)
					{
						$section->items[$ikey] = json_decode($item->content);
					}
				}
				$section->data = $data; // pass through of data from page controller for stuff like form submissions
				$section->row_number = $count;
				$section->html = $this->load->view('sections/'.$section->type, $section, TRUE);

				$sections[$key] = $section; 
			}

			return $sections;

		}

		return FALSE;
	} 

}