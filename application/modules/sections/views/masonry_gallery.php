
<?php
if ( (isset($content) && $content) && (isset($items) && $items) ) :
$image = $items[0];
?>

<section class="masonry-gallery-section full-width-<?php echo $content->full_width; ?> ">
	<?php if($content->full_width != 'yes') : ?>
	 <div class="container">
	<?php endif; ?>


		<div class="masonry-wrapper">

				<ul class="masonry-items">
          <?php $fancyboxid = 975; ?>
					<?php foreach ($items as $image) : ?>
						<li>
							<a class="js-fancybox <?php echo ($image->show_artists_impression == 'yes') ? 'fancybox-with-ai' : ''; ?>"<?php if(property_exists($image, 'artist_impression_additional_text')) {if($image->artist_impression_additional_text) { echo ' data-artist-impression-additional=" - ' .  $image->artist_impression_additional_text . '"';}}?> data-fancybox="group-<?php echo $fancyboxid; ?>" href="<?php echo display_image($image->image, 'full_'); ?>" style="background-image: url('<?php echo display_image($image->image, 'desktop_'); ?>');">
								<?php if ($image->show_artists_impression == 'yes') : ?>
									<span class="artist-impression">Artist's Impression<?php if(property_exists($image, 'artist_impression_additional_text')) {if($image->artist_impression_additional_text) { echo ' - ' .  $image->artist_impression_additional_text;}} ?></span>
								<?php endif; ?>
							</a>
						</li>
				<?php endforeach; ?>
			</ul>

		</div>



		<?php if($content->full_width != 'yes') : ?>
		 </div>
		<?php endif; ?>

</section>

<?php endif; ?>
