<section class="section section-register-your-interest">
    <div class="container">
        <div class="row std-pd">
            <button class="btn btn-arrow btn-travel" href="#register">Register Your Interest<i class="fas fa-chevron-right"></i></button>
        </div>
    </div>
</section>
