<?php
if ( (isset($content) && $content)  ) :
?>
<section class="article-section article-section--text-section full-width-<?php echo $content->full_width; ?><?php if($content->extra_padding == 'yes') { echo ' extra-padding'; } ?><?php if($content->gradient_bg == 'yes') { echo ' gradient-bg'; } ?>"<?php if((isset($content->section_ID) && $content->section_ID)) { echo ' id="' . $content->section_ID . '"';}?>>
	<div class="container">
		<div class="row std-pd">
			<?php if($content->gradient_bg == 'yes') : ?>
      	<?php echo file_get_contents(base_url('assets/images/brandmark.svg'));?>
			<?php endif; ?>
			<div class="<?php if($content->extra_padding == 'yes') { echo 'col-lg-35 col-md-40 col-sm-50 col-xs-60'; } else { echo 'col-md-50 col-sm-50 col-xs-60'; } ?> std-pd center-block title">
				<h2><?= $content->title; ?></h2>
			</div>
			<?php if($content->subtitle) : ?>
	      <div class="col-md-50 col-sm-50 col-xs-60 std-pd center-block subtitle">
	        <strong><?= $content->subtitle; ?></strong>
	      </div>
			<?php endif; ?>
			<?php if($content->gradient_bg == 'yes') : ?>
				<?php if($content->extra_padding == 'yes') : ?>
					<a href="http://www.addarc.com.au/" target="_blank">
						<img style="max-width: 100px;" src="<?php echo base_url('assets/images/addarc-logo.png'); ?>" />
					</a>
				<?php endif; ?>
			<?php endif; ?>
			<?php if($content->gradient_bg == 'no') : ?>
      	<?php echo file_get_contents(base_url('assets/images/brandmark.svg'));?>
			<?php endif; ?>
			<?php if($content->text) : ?>
				<div class="<?php if($content->extra_padding == 'yes' && $content->gradient_bg == 'yes') { echo 'col-md-26 col-sm-50 col-xs-60'; } elseif($content->extra_padding == 'yes') { echo 'col-md-44 col-sm-50 col-xs-60'; } else { echo 'col-md-50 col-sm-50 col-xs-60'; } ?> std-pd center-block content">
					<?= $content->text ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php endif; ?>
