

<section class="contact-section" <?php if((isset($content->section_ID) && $content->section_ID)) { echo ' id="' . $content->section_ID . '"';}?>>

	<div class="container">

<?php
	$form_name = @$content->form_name;
	$form_id = @$content->form_id;
	$sent = FALSE;
	$errors = FALSE;

	if (isset($form_result) && $form_result)
	{
		if (isset($form_result->form_name) && $form_result->form_name == $form_name)
		{
			$errors = TRUE;
			if (isset($form_result->form_success) && $form_result->form_success == TRUE)
			{
				$sent = TRUE;
				$errors = FALSE;
			}
		}
	}
	?>
		<?php if ($sent) : ?>

		<div class="form-thankyou">
			<h1 class="h2">Thank you for your registration of interest.</h1>
		</div>

		<?php else : ?>

		<?php if ($errors && validation_errors()) : ?>
		<div class="form-validation-errors">
			<p>Please fill in the required fields.</p>
		</div>
		<?php endif; ?>
		<h2>Register your interest</h2>
		<h3>To Receive floorplans &amp; Pricing</h3>

		<form class="register__form" method="POST" action="" name="<?php echo $form_name; ?>">
			<div class="input-elements">
				<input type="hidden" name="ref" value="<?php echo $form_name; ?>" />
				<input type="hidden" name="form" value="<?php echo $form_id;?>" />
				<input type="hidden" name="db_lead_source" value="Web" />
				<input type="text" name="db_first_name" placeholder="Name*" value="<?php echo set_value('db_first_name'); ?>" required />
				<input type="text" name="db_last_name" placeholder="Surname*" value="<?php echo set_value('db_last_name'); ?>" required />
				<input type="text" name="db_phone" placeholder="Phone*" value="<?php echo set_value('db_phone'); ?>" required />
				<input type="email" name="db_email" placeholder="Email*" value="<?php echo set_value('db_email'); ?>" required />
			</div>
			<div class="text-center recaptcha__notice">
				<small>This site is protected by reCAPTCHA and the Google
				    <a href="https://policies.google.com/privacy" target="_blank">Privacy Policy</a> and
				    <a href="https://policies.google.com/terms" target="_blank">Terms of Service</a> apply.
				</small>
			</div>
			<button type="submit" class="btn register__submit">Submit</button>
		</form>

		<script>


    $('.register__form').submit(function() {
			var yVal = $('.register__form').find('[name="g-recaptcha-response-y"]').val();
			if(yVal === 'y') {
		
			} else {
				event.preventDefault();

				grecaptcha.ready(function() {


						grecaptcha.execute('6LcWzLkUAAAAAAZegsGEOFObS4W_wS7licjOghLq', {action: 'register'}).then(function(token) {

								$('.register__form').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
										$.post("form.php",{token: token}, function(result) {
														console.log(result);
														if(result.success) {
															 $('.register__form').prepend('<input type="hidden" name="g-recaptcha-response-y" value="y">');
															 $('.register__form').trigger('submit');
														} else {

														}
										});
						});;
				});
			}
		});
  </script>

		<h3>TO RESERVE A PRIVATE APPOINTMENT WITH OUR SALES TEAM, CONTACT</h3>
		<strong>Russell Murphy <a href="tel:0407 839 184">0407 839 184</a></strong>

	<?php endif; ?>

</div>

</section>
