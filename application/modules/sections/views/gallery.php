
<section class="section gallery-section">
    <div class="gallery-section__container">
        <div class="flexslider">
           
            <?php if ($items):?>
            <ul class="slides">    
                <?php foreach($items as $item): ?>
                <li>
                    <div class="flex-row">
                        <div class="gallery-section__image-wrap">
                            <figure class="gallery-section__image">
                                <div class="image-center byheight <?php echo ($item->show_artists_impression === 'yes') ? 'ai' : '';?>" style="background-image: url('<?php echo $item->image; ?>');">
                                    <img src="<?php echo $item->image; ?>" alt="Apartments" />
                                </div>
                            </figure>
                        </div>
                        <div class="gallery-section__content-wrap">
                            <div class="article-section__content flex-row flex-row--center">
                                <div class="article-section__content-center">
                                    <?php if ($item->show_brandmark === 'yes'): ?>
                                    <img class="article-section__content-logo" src="<?php echo base_url('assets/images/brandmark.png');?>" alt="">
                                    <?php endif?>
                                    <?php if ($item->title): ?>
                                    <h2 class="article-section__title"><?php echo $item->title; ?></h2>
                                    <?php endif?>
                                    <?php if ($item->description): ?>
                                    <div class="article-section__para"><?php echo $item->description; ?></div>
                                    <?php endif?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <?php endforeach?>
            </ul>     
            <?php endif; ?>
        </div>
    </div>
</section>