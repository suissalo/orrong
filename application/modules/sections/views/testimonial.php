<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section class="article-section article-section--testimonial full-width-<?php echo $content->full_width; ?> ">
	<div class="container">

			<div class="testimonial-wrapper">
				<div class="flexslider testimonial-gallery">
					<ul class="slides">
						<?php foreach ($items as $item) : ?>
						<li>
							<div class="wysiwyg-content h1">
								<?php echo $item->quote; ?>
							</div>
							<div class="testimonial_author">
								<?php echo $item->author; ?>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>

	</div>
</section>

<?php endif; ?>
