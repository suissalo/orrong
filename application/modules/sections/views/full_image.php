
<?php
if ( (isset($content) && $content) && (isset($items) && $items) ) :
$image = $items[0];
?>

<section class="article-section article-section--full-image full-width-<?php echo $content->full_width; ?> ">
	<?php if($content->full_width != 'yes') : ?>
	 <div class="container">
	<?php endif; ?>
		<?php if (count($items) > 1) : ?>

		<div class="flexslider-wrapper">
				<div class="flexslider hero-gallery">
					<ul class="slides">
						<?php foreach ($items as $image) : ?>
							<li>
								<a class="js-fancybox-flexslider <?php echo ($image->show_artists_impression == 'yes') ? 'fancybox-with-ai' : ''; ?>"<?php if(property_exists($image, 'artist_impression_additional_text')) {if($image->artist_impression_additional_text) { echo ' data-artist-impression-additional=" - ' .  $image->artist_impression_additional_text . '"';}}?> data-fancybox="group-<?php echo $id; ?>" href="<?php echo display_image($image->image, 'full_'); ?>" style="background-image: url('<?php echo display_image($image->image, 'desktop_'); ?>');">
									<img src="<?php echo display_image($image->image, 'desktop_'); ?>" alt="<?php echo @$content->title; ?>" class="" />
									<?php if ($image->show_artists_impression == 'yes') : ?>
										<span class="artist-impression">Artist's Impression<?php if(property_exists($image, 'artist_impression_additional_text')) {if($image->artist_impression_additional_text) { echo ' - ' .  $image->artist_impression_additional_text;}}?></span>
									<?php endif; ?>
								</a>
							</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>

		<?php elseif (count($items) == 1) : ?>

		<a class="full-image js-fancybox <?php echo ($image->show_artists_impression == 'yes') ? 'fancybox-with-ai' : ''; ?>"<?php if(property_exists($image, 'artist_impression_additional_text')) {if($image->artist_impression_additional_text) { echo ' data-artist-impression-additional=" - ' .  $image->artist_impression_additional_text . '"';}}?> data-fancybox="group-<?php echo $id; ?>" href="<?php echo display_image($image->image, 'full_'); ?>">
			<img src="<?php echo display_image($image->image, 'full_'); ?>" alt="<?php echo @$content->title; ?>" class="" />
			<?php if ($image->show_artists_impression == 'yes') : ?>
				<span class="artist-impression">Artist's Impression<?php if(property_exists($image, 'artist_impression_additional_text')) {if($image->artist_impression_additional_text) { echo ' - ' .  $image->artist_impression_additional_text;}}?></span>
			<?php endif; ?>
		</a>

		<?php endif; ?>

		<?php if($content->full_width != 'yes') : ?>
		 </div>
		<?php endif; ?>

</section>

<?php endif; ?>
