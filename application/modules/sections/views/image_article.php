<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
$image = $items[0];
?>

<section class="article-image article-image--text-<?php echo $content->text_position; ?> article-image--bg-<?php echo $content->background_color; ?> ">
	<div class="flex-row">
		<div class="article-image__gallery">
			<div class="flexslider-wrapper">
					<div class="flexslider hero-gallery">
						<ul class="slides">
							<?php foreach ($items as $image) : ?>
								<li>
									<a class="image-center js-fancybox <?php echo ($image->show_artists_impression == 'yes') ? 'ai' : ''; ?>" data-fancybox="group-<?php echo $id; ?>" href="<?php echo display_image($image->image, 'full_'); ?>" style="background-image: url('<?php echo display_image($image->image, 'desktop_'); ?>');"> 
										<img src="<?php echo display_image($image->image, 'desktop_'); ?>" alt="<?php echo @$content->title; ?>" class="" />
										<span class=" fancybox-zoom"><i class="fal fa-search-plus"></i></span>
									</a>
								</li>
							<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="article-image__content flex-row flex-row--center">
			<div class="article-image__content-wrap">
				<?php if ($content->title) : ?>
				<h1 class="article-image__title"><?php echo $content->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($content->description) : ?>
				<div class="article-image__description"><?php echo $content->description; ?></div>
				<?php endif; ?>
				
				<?php if ($content->link_1_url) : ?>
				<a class="article-image__link button button--primary" href="<?php echo $content->link_1_url; ?>"> <?php echo ($content->link_1_text) ? $content->link_1_text : 'Find out more'; ?> &nbsp;<i class="fal fa-long-arrow-right"></i></a>
				<?php endif; ?>
		</div>
	</div>
</section>

<?php endif; ?>
