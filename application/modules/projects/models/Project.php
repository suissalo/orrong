<?php 
class Project extends CI_Model {

	var $data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function fetch($id = FALSE, $args = FALSE)
	{
		if(!$id) return FALSE;

		$this->db->where('id', $id);
		if($args) $this->db->where($args);

		$query = $this->db->get('projects', 1);
		
		return ($query->num_rows() == 1)? $query->row() : FALSE;
	}

	public function fetch_all( $args = FALSE)
	{

		if($args) $this->db->where($args);
		$this->db->select('projects.*, relation.category_id as category_id');
		$this->db->order_by('display_order');
		$this->db->where('status', 1);
		$this->db->join('project_categories_relation relation', 'relation.project_id = projects.id', 'left');
		$query = $this->db->get('projects');
		
		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}

	public function fetch_by($args = FALSE, $single_row = FALSE)
	{
		if($args) $this->db->where($args);

		if($single_row) $this->db->limit(1);
		$this->db->order_by('display_order','asc');

		//$this->db->select('projects.*, gallery_images.image AS image');
		$query = $this->db->get('projects');
		
		if($single_row)
		{
			return ($query->num_rows() == 1)? $query->row() : FALSE;
		}
		return ($query->num_rows() > 0)? $query->result() : FALSE;
	}

	public function fetch_by_category($category_id = FALSE, $limit = FALSE, $project_id = FALSE)
	{
		if(!$category_id) return FALSE;

		$querystring = '
		SELECT `projects`.*
		FROM (
			SELECT `projects`.*, `gallery_images`.`image`
			FROM `projects` 
			LEFT JOIN `gallery_images` on `projects`.`gallery_id` = `gallery_images`.`gallery_id`
			ORDER BY `gallery_images`.`display_order` ASC
		) AS projects
		LEFT JOIN `project_categories_relation` ON `project_categories_relation`.`project_id` = `projects`.`id`
		WHERE `project_categories_relation`.`category_id` = ?
		';

		if ($project_id)
		{
			$querystring .= 'AND `projects`.`id` <> '.$project_id;
		}

		$querystring .= '
		GROUP BY `projects`.`id`
		ORDER BY `projects`.`display_order` ASC
		';

		if ($limit)
		{
			$querystring .= ' LIMIT '.(int) $limit;
		}

		$query = $this->db->query($querystring, $category_id);

		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}

	public function get_featured_projects()
	{
		$this->db->where('status', 1);
		$this->db->where('featured', 1);
		$this->db->order_by('display_order','asc');
		$this->db->limit(3);
		$query = $this->db->get('projects');
		return ($query->num_rows() > 0)? $query->result() : FALSE;
	}

	public function get_image($gallery_id, $single_row = TRUE)
	{
		if(!$gallery_id) return FALSE;

		$this->db->order_by('display_order');
		$this->db->where('gallery_id', $gallery_id);

		if($single_row)
		{
			$this->db->limit(1);
		}

		//$this->db->select('projects.*, gallery_images.image AS image');
		$query = $this->db->get('gallery_images');
		
		if($single_row)
		{
			return ($query->num_rows() == 1)? $query->row() : FALSE;
		}
		return ($query->num_rows() > 0)? $query->result() : FALSE;
	}

	public function get_next($current_project)
	{
		if(!$current_project) return FALSE;

		$this->db->where(
			array(
				'display_order >' => $current_project->display_order,
				'status ' => 1,
			)
		);
		$this->db->order_by('display_order ASC');
		$query = $this->db->get('projects', 1);
		
		return ($query->num_rows() == 1)? $query->row() : FALSE;
	}

	public function get_prev($current_project)
	{
		if(!$current_project) return FALSE;

		$this->db->where(
			array(
				'display_order <' => $current_project->display_order,
				'status ' => 1,
			)
		);
		$this->db->order_by('display_order DESC');
		$query = $this->db->get('projects', 1);
		
		return ($query->num_rows() == 1)? $query->row() : FALSE;
	}

	function categories()
    {
        $this->db->order_by('display_order');
        $query = $this->db->get('project_categories');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function get_relations()
    {
        $this->db->order_by('project_id','category_id');
        $query = $this->db->get('project_categories_relation');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function get_relations_for_project($project_id = FALSE)
    {
    	if(!$project_id) return FALSE;

    	$this->db->select('project_categories.*');
    	$this->db->where('project_id', $project_id);
        $this->db->join('project_categories', 'project_categories_relation.category_id = project_categories.id', 'LEFT');
        $this->db->order_by('project_id','category_id');

        $query = $this->db->get('project_categories_relation');
        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    function sitemap()
    {
    	$return = array();

    	$this->db->where('status', 1);
		$this->db->order_by('display_order','asc');
		$query = $this->db->get('projects');
		
		foreach ($query->result() as $row) 
		{
            $return['project'.$row->id] = base_url('/projects/detail/'.$row->safe_name); 
		}

        return $return;
    }

}