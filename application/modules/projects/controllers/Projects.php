<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends MX_Controller {
    
    var $data;
    var $permalink;
    function __construct() {
        parent::__construct();
        
        $this->load->model('project');
        //$this->load->model('page');
        $this->load->model('gallery');
        $this->permalink = rtrim(uri_string(),'/').'/';

        // $this->data['project_status']               = config_item('development_status');
        // $this->data['project_status_safe_names']    = config_item('development_safe_names');
        //$this->data['template']                     = 'projects';
    }

    function index($data = FALSE)
    {
        $project_url            = project_url();
        $segments               = $this->uri->segment_array();

        if (count($segments) > 1 && end($segments) != rtrim($project_url, '/'))
        {
            $html =  $this->detail(end($segments));
            $this->data['page_content'] = $html;
            return $this->data;
        }

        $this->data = $data;

        if($this->data['row']->gallery_id) 
        {
            $this->data['gallery']          = $this->gallery->get_gallery($this->data['row']->gallery_id);
            $this->data['gallery_images']   = $this->gallery->get_images($this->data['row']->gallery_id);
        }
        
        $categories = $this->project->categories();

        if ($categories )
        {
            // foreach ($categories as $key => $category) 
            // {
            //     $categories[$key]->projects = $this->project->fetch_by_category($category->id);
            // }
            $this->data['categories'] = $categories;
        }
        
        $projects = $this->project->fetch_all();
        if ($projects)
        {
            $this->data['projects'] = $projects;
        }

        
    	$html = $this->load->view('listing', $this->data, TRUE);
        $this->data['page_content'] = $html;
        return $this->data;
    }

    function detail($safe_name = FALSE)
    {
       
        if(!$safe_name) return false;

        $this->data['project'] = $this->project->fetch_by(array('status' => 1, 'safe_name' => $safe_name), TRUE);

        if (!$this->data['project']) return false;

        $category = $this->project->get_relations_for_project($this->data['project']->id);
        
        if ($category)
        {
            $category = reset($category);
            $this->data['other_projects']   = $this->project->fetch_by_category($category->id, 3, $this->data['project']->id);
        }

        $this->data['category']         = $category;
        $this->data['gallery_images']   = $this->gallery->get_images($this->data['project']->gallery_id, FALSE);
        $this->data['next_project']     = $this->project->get_next($this->data['project']);
        $this->data['prev_project']     = $this->project->get_prev($this->data['project']);
        
        return $this->load->view('details', $this->data, TRUE);

    }

    function featured_projects()
    {
        $featured = $this->project->get_featured_projects();

        if ($featured) 
        {
            foreach ($featured as $key => $project)
            {
                $image = $this->project->get_image($project->gallery_id);
                if ($image) 
                {
                    $image = $image->image;
                }
                $featured[$key]->image = $image;
            }
        }

        return $featured;
    }

}