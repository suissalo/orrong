<div class="header-placeholder"></div>

<?php if (@$row->sub_heading || @$row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php if ($projects) : ?>

<?php 
$project_url = project_url();
$filter = $this->input->get('service');
$filter_valid = FALSE;
?>
<div class="projects">
	<div class="projects__filters js-project-categories">		
		<div class="hidden-xs">
			<a class="js-project-category projects__filter" href="all" >All</a>
			<?php foreach ($categories as $category) : ?>
			<a class="js-project-category projects__filter <?php echo ($filter && $filter == $category->safe_name) ? 'active' : ''; ?>" href="category-<?php echo encode($category->id); ?>" ><?php echo $category->name; ?></a>
			<?php endforeach; ?>
		</div>
		<div class="visible-xs">
			<div class="custom-select js-project-category-select">
				<select>
					<option selected disabled>- Filter by -</option>
					<option value="all">All</option>
					<?php foreach ($categories as $category) : ?>
						<option value="category-<?php echo encode($category->id); ?>" <?php echo ($filter && $filter == $category->safe_name) ? 'selected="selected"' : ''; ?> ><?php echo $category->name; ?></option>
						<?php if ($filter && $filter == $category->safe_name) { $filter_valid = TRUE; } ?>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</div>
	

	<?php if (!$filter_valid) { $filter = FALSE; } ?>

	<?php if (@$projects) : ?>
	<div class="projects__listing flex-row">
		<?php foreach ($projects as $project) : ?>
			<a class="js-project-listing projects__project <?php echo $project->width ? 'projects__project--fullwidth' : '';?>" href="<?php echo base_url($project_url.$project->safe_name); ?>" data-category-id="<?php echo 'category-'.encode($project->category_id);?>">
				<figure class="image-center" style="background-image: url('<?php echo display_image($project->hero_image, 'full_'); ?>');"></figure>
				<div class="flex-row flex-row--center projects__project-overlay-wrap">
					<div class="projects__project-overlay">
						<h1 class="projects__project-title"><?php echo $project->title; ?></h1>
						<?php if (@$project->subtitle):?>
						<h3 class="projects__project-subtitle"><?php echo $project->subtitle;?></h3>
						<?php endif;?>
						<div class="projects__project-details">
							<?php if (@$project->intro): ?>
							<div class="projects__project-detail"><?php echo $project->intro;?></div>
							<?php endif;?>
							<div class="text-center" style="margin-top: 1em;"><i class="fal fa-long-arrow-right"></i></div>
						</div>
					</div>
				</div>
			</a>
		<?php endforeach; ?>
	</div>
	<?php else: ?>
	<p>Currently no projects listing</p>
	<?php endif; ?>
	
</div>
<?php endif; ?>

<?php 
if (isset($page_sections) && $page_sections) : 
	foreach ($page_sections as $section) : 
		echo $section->html; 
	endforeach;
endif;
?>
