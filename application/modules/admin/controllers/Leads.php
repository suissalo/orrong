<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leads extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('auth_helper');

		$this->load->library('leads_submission');
		$this->load->model('lead');
	}

	function index()
	{
		access_area('admin/leads');

		$data = FALSE;
		$data['forms'] = $this->lead->get_forms();
		$data['leads'] = array();
		
		if ($data['forms']) {
			foreach($data['forms'] as $form) 
			{
				$data['leads'][$form->id] = $this->lead->get_leads($form->id);
			}
		}
		
		$this->layout->view('admin/leads/manage', $data);
	}

	function create_form()
	{
		access_area('admin/leads');

		$form_id = $this->lead->create_form();
		
		redirect('admin/leads/edit_form/'.$form_id);
	}

	function edit_form($form_id = FALSE)
	{
		access_area('admin/leads');

		$data = FALSE;

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('notification_active', 'Notification Active', 'trim');
		$this->form_validation->set_rules('responder_active', 'Responder Active', 'trim');
		$this->form_validation->set_rules('notification_recipients[]', 'Recipients', 'trim');

		if ($form_id && $this->form_validation->run())
		{
			$_POST['notification_active'] = (isset($_POST['notification_active'])) ? $_POST['notification_active'] : 0;
			$_POST['responder_active'] = (isset($_POST['responder_active'])) ? $_POST['responder_active'] : 0;
			
			if ($this->input->post('notification_recipients'))
			{
				$_POST['notification_recipients'] = implode(',', $_POST['notification_recipients']);
			}
			else
			{
				$_POST['notification_recipients'] = '';
			}

			$this->lead->update_form($form_id, $_POST);
			set_message('success', 'Form has been updated');
		}

		$data['form'] = $this->lead->get_form($form_id);

		if (!$data['form'])
		{
			set_message('error', 'Form not found');
			redirect('admin/leads');
		}
		
		$data['form']->secret_id = $this->leads_submission->form_id_encode($data['form']->id);
		$new_id = $this->leads_submission->form_id_decode($data['form']->secret_id);
		// view(array($data['form']->id, $data['form']->secret_id, $new_id ));
		$data['form']->notification_recipients = explode(',', $data['form']->notification_recipients);

		$this->layout->view('admin/leads/form', $data);
	}

	function delete_form($form_id = FALSE)
	{
		access_area('admin/leads');

		if (!$form_id) 
		{
			set_message('error', 'Form not found');
		}
		else
		{
			$result = $this->lead->remove_form($form_id);
			
			if ($result)
			{
				set_message('success', 'Form has been deleted');
			}
			else
			{
				set_message('error', 'Form not found');
			}
			
		}

		redirect('admin/leads');
	}

	public function publish_form($form_id = FALSE, $status = FALSE)
	{
		access_area('admin/leads');
		
		if (!$form_id || $status === FALSE) return FALSE; 

		$success = $this->lead->update_form($form_id, array('published' => $status));
		
		if ($success) 
		{
			set_message('success', 'Form status updated.');
		} 
		else
		{
			set_message('error', 'Form not be found');
		}

		redirect('admin/leads');
	}

	function detail($lead_id = FALSE)
	{
		access_area('admin/leads/detail');

		$data = FALSE;
		$lead_id = decode($this->uri->segment(4));
		$lead = $this->lead->get_lead($lead_id);
		$lead->content = (isset($lead->content)) ? json_decode($lead->content) : FALSE;
		$data['lead'] = $lead;
		// view($data);
		$this->layout->view('admin/leads/detail', $data);
	}

	public function upload_autoresponder_attachment()
	{
		$this->load->library('leads_submission');
		$form_id    		= $this->leads_submission->form_id_decode($this->uri->segment(4));

		if(!$form_id) return FALSE;

		$this->load->model('admin/lead');
		$data['form'] = $this->lead->get_form($form_id);
		$data['form']->secret_id = $this->leads_submission->form_id_encode($data['form']->id);

		//view(array("form_id" => $form_id, "form" => $form, "FILES" => $_FILES));

        if($data['form'] && $_FILES["file"]["name"] != '') 
        {
        	$path = config_item('form_path').$form_id;

            make_folder(ROOT.$path);
            $config['upload_path']      = ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG|pdf|PDF|docx|DOCX|xlsx|XLSX';

            $this->load->library('upload');
            $this->upload->initialize($config);
            if (! $this->upload->do_multi_upload('file'))
            {
                respond('error: '.$this->upload->display_errors());
                return;
            }
            $file = $this->upload->data();

            $form_data['file'] 		= $path.'/'.$file['file_name'];
            $form_data['file_name'] = $file['file_name'];
            $form_data['form_id'] 	= $form_id;
            $form_data['file_type'] = trim($file['file_ext'],'.');

            $uploaded_file = $this->lead->save_lead_form_attachment($form_data, TRUE);

            $html = '<li class="col-xs-60 col-sm-30 col-md-20 std-pd" id="sort_new" >'.$this->load->view('admin/_partials/autoresponder_attachment_media', array('form_id' => $data['form']->secret_id, 'attachment_id' => $uploaded_file->new_attachment_id, 'attachment' => $form_data), TRUE).'</li>';
            
            respond(json_encode(array('html' => $html)));
            return;
        }
        respond('error: Could not find the item');
	}

	function remove_attachment()
	{
		$form_id = decode($this->uri->segment(4));
		$attachment_id = decode($this->uri->segment(5));
		$redirect_path = 'admin/leads/edit_form/'.$form_id.'#autoresponder';

		$form = $this->lead->get_form($form_id);
		$attachments = json_decode($form->attachments, true);

		unset($attachments[$attachment_id]);

		if(count($attachments) > 0)
		{
			$form->attachments = json_encode($attachments);
		}
		else
		{
			$form->attachments = NULL;
		}

		$this->lead->update_form($form_id, $form);

		redirect($redirect_path);
	}

	function delete_lead()
	{
		$lead_id = decode($this->uri->segment(4));
		$this->lead->delete_lead($lead_id);

		redirect('admin/leads');
	}

	public function emails()
	{
		$this->load->library('lead_reader');
		$this->lead_reader->get_email();
	}
}