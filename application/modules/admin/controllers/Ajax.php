<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		redirect_if_not_logged_in();
	}

	function validate_url()
	{
	    $data['success'] = 0;
	    $data['message'] = 0;
	    $data['parent'] = '';
	    $data['uri'] = '';
	    $data['segment'] = '';
	    $parent_url = '';

	    $this->form_validation->set_rules('parent_rel', 'Parent', 'trim');
	    $this->form_validation->set_rules('permalink_uri', 'Permalink', 'trim|required');
	    if($this->form_validation->run())
	    {
		    $this->load->model('admin/page');
		    $base = base_url();
		    $uri = url_title($_POST['permalink_uri'], '-', TRUE);
		    $data['segment'] = $uri;

		    if(@$_POST['parent_rel']) {
			    if($parent = $this->page->get_page($_POST['parent_rel'])) {
				    $parent_url .= $parent->permalink;
				    $data['parent'] = $parent->permalink;
			    }
		    } elseif(@$_POST['parent_segment']) {
			    $parent_url .= $_POST['parent_segment'];
			    $data['parent'] =$_POST['parent_segment'];
		    }

		    $url = $base.$parent_url.$uri;
		    $data['uri'] = $url;
		    $data['permalink'] = trim($parent_url.$uri, '/').'/';
		    // $curl = curl_init();
		    // curl_setopt($curl, CURLOPT_URL, $url);
		    // curl_setopt($curl, CURLOPT_FILETIME, TRUE);
		    // curl_setopt($curl, CURLOPT_NOBODY, TRUE);
		    // curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		    // $header = curl_exec($curl);
		    // curl_close($curl);
		    $header = FALSE;
		    if($header == FALSE)
		    {
			    $data['success'] = 1;
		    }
		    else
			    $data['messsage'] = 'That Permalink is already taken.';

	    }
	    else
	    {
		    $data['messsage'] = validation_errors();
	    }
	    respond(json_encode($data));
	}
    
    function validate_blog_url()
	{
		//view($prep_date);
	    $data['success'] = 0;
	    $data['message'] = 0;
	    $data['parent'] = '';
	    $data['uri'] = '';
	    $data['segment'] = '';
	    $parent_url = '';


	    $this->form_validation->set_rules('date', 'Date', 'trim');
	    $this->form_validation->set_rules('permalink_uri', 'Permalink', 'trim');
	    if($this->form_validation->run())
	    {
            $this->load->helper('date');
		    $base = base_url();
		    $uri = url_title($_POST['permalink_uri'], '-', TRUE);
		    $data['segment'] = $uri;

		    if(@$_POST['parent_rel']) {
			    if($parent = $this->page->get_page($_POST['parent_rel'])) {
				    $parent_url .= $parent->permalink;
				    $data['parent'] = $parent->permalink;
			    }
		    } elseif(@$_POST['parent_segment']) {
			    $parent_url .= $_POST['parent_segment'];
			    $data['parent'] =$_POST['parent_segment'];
		    }

		    $parent_url = '';
            $date = strtotime(str_replace('/', '-', $_POST['date']));
            
            $prep_date = date('Y/m/', $date);//formate_date('Y/m/', $date);
            //view(array($prep_date, $_POST['date']));
            $parent_url .=$prep_date;
            $data['parent'] .= $parent_url;
		    $url = $base.$parent_url.$uri;
		    $data['uri'] = $url;
		    $data['permalink'] = trim($parent_url.$uri, '/').'/';

		    $handle = curl_init($url);
		    // curl_setopt($curl, CURLOPT_URL, $url);
		    // curl_setopt($curl, CURLOPT_FILETIME, TRUE);
		    // curl_setopt($curl, CURLOPT_NOBODY, TRUE);
		    // curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		    // $header = curl_exec($curl);
		    // curl_close($curl);
		    

		    curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($handle, CURLOPT_HEADER, false);
			curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
			curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15") ); // request as if Firefox
			curl_setopt($handle, CURLOPT_NOBODY, true);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
			$connectable = curl_exec($handle);
			$info = curl_getinfo($handle);
			
			curl_close($handle);
			// Force success after url check
			$data['success'] = 1;

		 //    if($info['http_code'] == '404' || $info['http_code'] == '401')
		 //    {
			//     $data['success'] = 1;
		 //    }
		 //    else
		 //    {
			//     $data['messsage'] = 'That Permalink is already taken.';
			// }
	    }
	    else
	    {
		    $data['messsage'] = validation_errors();
	    }
	    respond(json_encode($data));
	}

	function update_page_order()
	{
	    //$this->form_validation->set_rules('sort', 'Order', 'is_array');
	    //$this->form_validation->set_rules('testimonialsort', 'Order', 'is_array');
	    $this->form_validation->set_rules('rel', 'Owner', 'trim|required');
	    if($this->form_validation->run()) {
		    if($this->input->post('rel') == 'manage-pages') 
		    {
			    $this->load->model('admin/page');
			    $this->page->re_order($this->input->post('sort'));
			    //view($this->db->last_query());
		    } 
		    elseif($this->input->post('rel') == 'gallery_images') 
		    {
			    $this->load->model('admin/gallery');
			    $this->gallery->re_order_images($this->input->post('sort'));
		    } 
		    elseif($this->input->post('rel') == 'manage-galleries') 
		    {
			    $this->load->model('admin/gallery');
			    $this->gallery->re_order($this->input->post('sort'));
		    } 
		    elseif($this->input->post('rel') == 'service-galleries') 
		    {
			    $this->load->model('admin/service_gallery');
			    $this->service_gallery->re_order($this->input->post('sort'));
		    } 
		    elseif($this->input->post('rel') == 'service_gallery_images') 
		    {
			    $this->load->model('admin/service_gallery');
			    $this->service_gallery->re_order_image($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'service-packages') 
			{
			    $this->load->model('admin/service_package');
			    $this->service_package->re_order($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'service-testimonials') 
			{
			    $this->load->model('admin/service_testimonial');
			    $this->service_testimonial->re_order($this->input->post('testimonialsort'));
			} 
			elseif($this->input->post('rel') == 'project_house') 
			{
			    $this->load->model('admin/house');
			    $this->house->re_order_project_house($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'project-galleries') 
			{
			    $this->load->model('admin/project');
			    $this->project->re_order_project_galleries($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'project_gallery_images') 
			{
			    $this->load->model('admin/project');
			    $this->project->re_order_project_gallery_images($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'house') 
			{
			    $this->load->model('admin/house');
			    $this->house->re_order_house($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'house_images') 
			{
			    $this->load->model('admin/house');
			    $this->house->re_order_house_images($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'office_images') 
			{
			    $this->load->model('admin/office');
			    $this->office->re_order_office_images($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'agent-testimonials') 
			{
			    $this->load->model('admin/agent', 'my_agent');
			    $this->my_agent->re_order_agent_testimonials($this->input->post('testimonialsort'));
			} 
			elseif($this->input->post('rel') == 'agent-group') 
			{
			    $this->load->model('admin/agent_group');
			    $this->agent_group->re_order_agent_groups($this->input->post('sort'));
			} 
			elseif($this->input->post('rel') == 'office') 
			{
			    $this->load->model('admin/office');
			    $this->office->re_order_office($this->input->post('sort'));
			}
			elseif($this->input->post('rel') == 'projects') 
			{
			    $this->load->model('admin/project');
			    $this->project->re_order_projects($this->input->post('sort'));
			}    
	    }
	    else
	    {
		    respond(validation_errors());
	    }
	}



	function update_user_order()
	{
		if(is_array($_POST['sort']))
		{
			$this->load->model('admin/display_homes');
			$this->display_homes->reorder($this->input->post());
		}
	}
    
    /**
     * takes a querystring and searchs for a post code
     */
    function postcode() 
    {
        $this->form_validation->set_rules('address', 'Address query', 'trim|xss_clean|required');
        if($this->form_validation->run())
        {
            $this->load->library('auspost');
            $query_string = $this->input->post('address');
            
            $responce = $this->auspost->get_post_code($query_string, 'VIC');
            if($responce) {
                $prep = $this->auspost->decode_response($responce, TRUE);
                respond(json_encode($prep));
            }
        }
    }
    
    /*
    function load_map()
    {
        $address = '2000 Grattan St Prahran VIC';
        
        $this->load->library('googlemaps');
        $config['center']   = $address;//'-37.8481570, 144.9909740';
        $config['zoom']     = '18';
        $config['styles']   = array(
            array("name"=>"Red Parks", "definition"=>array(
              array("featureType"=>"all", "stylers"=>array(array("saturation"=>"-30"))),
              array("featureType"=>"poi.park", "stylers"=>array(array("saturation"=>"10"), array("hue"=>"#990000")))
            )),
            array("name"=>"Black Roads", "definition"=>array(
              array("featureType"=>"all", "stylers"=>array(array("saturation"=>"-70"))),
              array("featureType"=>"road.arterial", "elementType"=>"geometry", "stylers"=>array(array("hue"=>"#000000")))
            )),
            array("name"=>"No Businesses", "definition"=>array(
              array("featureType"=>"poi.business", "elementType"=>"labels", "stylers"=>array(array("visibility"=>"off")))
            ))
        );
        $config['stylesAsMapTypes']         = true;
        $config['stylesAsMapTypesDefault']  = "Black Roads"; 
        
        $marker                             = array();
        $marker['position']                 = $address;//'-37.8481570, 144.9909740';
        $marker['draggable']                = true;
        $marker['ondragend']                = 'update_geo("#geo_location", event.latLng.lat(), event.latLng.lng());';
        
        $this->googlemaps->initialize($config);
        $this->googlemaps->add_marker($marker);
        
        $data['map']        = $this->googlemaps->create_map();
        $this->layout->light_view('admin/properties/map', $data);
    }
    */

    /*
    function upload_image()
    {
    	$relation 	= $this->uri->segment('4');
    	$id 		= decode($this->uri->segment('5'));

    	if ($relation == 'gallery') 
    	{
    		$path = config_item('gallery_path').$id.'/';

    		make_folder(ROOT.$path);
            $config['upload_path']		= ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|png|GIF|JPG|PNG';
            $config['max_size']         = '2048'; // No more that 2 mb please

            $this->load->library('upload');
            $this->upload->initialize($config);

            if (! $this->upload->do_multi_upload('file'))
            {
                respond('error: '.$this->upload->display_errors());
                return;
            }
            
            $file = $this->upload->data();
            $this->gallery->make_image($file);

            $page_data['title'] = $file['raw_name'];
            $page_data['image'] = $path.$file['file_name'];
            $page_data['thumb'] = $path.$file['raw_name'].'_thumb'.$file['file_ext'];
            $page_data['main'] = $path.$file['raw_name'].'_main'.$file['file_ext'];
            $page_data['gallery_id'] = $gallery_id;

            $this->gallery->save_gallery_image($page_data);
            
            respond('1');
            return;
    	}
    }
    */

}