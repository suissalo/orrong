<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galleries extends MX_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('auth_helper');
		redirect_if_not_logged_in();

        $this->load->helper('ckeditor');
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
        $this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/'); 

        $this->load->model('admin/gallery');       
    }

    function index()
    {
        $data['galleries'] = $this->gallery->get_all();

        $data['page_content'] = $this->load->view('admin/galleries/manage', $data, TRUE);
        $this->load->view('admin/template', $data);
    }

    function remove()
    {
        $gallery_id = decode($this->uri->segment('4'));
        if(!$gallery_id) {
            set_message('error', 'Could not find that Gallery.');
            redirect(base_url('admin/galleries'));
        }
        if($this->gallery->remove_row($gallery_id))
        {
            set_message('success', 'Gallery has been removed.');
        }
        else
        {
            set_message('error', 'Could not find that Gallery.');
        }
        redirect(base_url('admin/galleries'));
    }

    /**
     * new Gallery
     *
     * @return void
     * 
     **/
    function new_gallery()
    {
        $this->layout->view('admin/galleries/new', array());
    }

    /**
     * Save an new
     *
     * @return void
     * 
     **/
    function save_new()
    {
        $data = array();
        $this->form_validation->set_rules('status', 'Active', 'trim|is_natural');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('content', 'Content', 'trim|required');
        $this->form_validation->set_rules('arrows', 'Arrows', 'trim|is_natural');
        $this->form_validation->set_rules('hover', 'Hover', 'trim|is_natural');
        $this->form_validation->set_rules('bg_colour', 'Hover Colour', 'trim');    
        $this->form_validation->set_rules('thumbs', 'Thumbs', 'trim');

         /// check if the editing of gallery is coming from  module editing ///
        $moduleId = $moduleTypeName = '';
        if(isset($_POST['fromModuleId']) && isset($_POST['fromModuleTypeName']))
        {
            $moduleId = trim($_POST['fromModuleId']);
            $moduleTypeName = trim($_POST['fromModuleTypeName']);
            $moduleTypeName = ($moduleTypeName === false || trim($moduleTypeName) === '') ? 'gallery' : trim($moduleTypeName);
            unset($_POST['fromModuleId']);
            unset($_POST['fromModuleTypeName']);
        }   
         
         /// check if the adding of gallery is coming from  assigning a hero gallery from service edit ///
        $assignToService = false;
        $serviceId = $service = '';
        if(isset($_POST['assign_to_service']) && isset($_POST['service_id']) && trim($_POST['assign_to_service']) == '1')
        {
            $serviceId = trim($_POST['service_id']);
            $assignToService = true;
            $this->load->model('admin/service');
            $service = $this->service->get_service($serviceId);
            
            unset($_POST['assign_to_service']);
            unset($_POST['service_id']);
        }     

        if ($this->form_validation->run()) 
        {
            $Gallery_data   = $_POST;
            $gallery_id     = $this->gallery->new_row($Gallery_data);
            if($gallery_id) 
            {
                set_message('success', 'Gallery has been created.');
                
                /// update the module body with the new gallery id just being added ///
                if($moduleId !== '')
                {
                    $moduleBody = json_encode(array('gallery_id' => $gallery_id));
                    $this->load->model('admin/module');
                    $this->module->update_module($moduleId, $moduleBody); 
                    redirect(base_url('admin/pages/gallery/edit/'.encode($moduleId)));    
                }
                else if($assignToService === true)
                {   
                    $this->service->update_service(array('hero_gallery_id' => $gallery_id), array('id' => $serviceId));
                    redirect(base_url('admin/services/edit/'.encode($serviceId).'?dy=1'));
                }   
                else
                    redirect(base_url('admin/galleries/edit/'.encode($gallery_id)));
            }
            else 
                set_message('error', 'There was an error saving the Gallery, please try again.');
        }
        else if($error = validation_errors())
            set_message('error', $error);

        /// redirect back to the module edit page ///
        if($moduleId !== '')
            redirect(base_url('admin/pages/gallery/edit/'.encode($moduleId)));
        else if($assignToService === true)
        {
            $data['service'] = $service;
            $data['assign_gallery_to_service'] = 1;
        } 

        /// else normal gallery add page ///
        $data['galleries']      = $this->gallery->get_all();
        $data['page_content']   = $this->load->view('admin/galleries/new', $data, TRUE); 
        $this->load->view('admin/template', $data); 
    }

    /**
     * edit a Gallery
     *
     * @return void
     * 
     **/
    function edit()
    {
        $gallery_id = decode($this->uri->segment('4'));
        $data['gallery'] = $this->gallery->get_gallery($gallery_id);
        $data['images'] = $this->gallery->get_images($gallery_id);

        if(!$data['gallery']) 
        {
            set_message('error', 'Selected Gallery could not be found');
            redirect(base_url('admin/galleries'));
        }
        $this->layout->view('admin/galleries/edit', $data);
    }

    /**
     * save an edit
     *
     * @return void
     * 
     **/
    function save_edit()
    {
        $gallery_id = decode($this->uri->segment('4'));

        $data['gallery'] = $this->gallery->get_gallery($gallery_id);
        $data['images'] = $this->gallery->get_images($gallery_id);

        $this->form_validation->set_rules('status', 'Active', 'trim|is_natural');
        $this->form_validation->set_rules('title', 'Title', 'trim');
        $this->form_validation->set_rules('content', 'Content', 'trim');
        $this->form_validation->set_rules('arrows', 'Arrows', 'trim|is_natural');
        $this->form_validation->set_rules('hover', 'Hover', 'trim|is_natural');
        $this->form_validation->set_rules('bg_colour', 'Hover Colour', 'trim');    
        $this->form_validation->set_rules('thumbs', 'Thumbs', 'trim');    
        $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
        //$this->form_validation->set_rules('images', 'Images', 'trim|required');

        /// check if the editing of gallery is coming from  module editing ///
        $moduleId = $moduleTypeName = '';
        if(isset($_POST['fromModuleId']) && isset($_POST['fromModuleTypeName']))
        {
            $moduleId = trim($_POST['fromModuleId']);
            $moduleTypeName = trim($_POST['fromModuleTypeName']);
            $moduleTypeName = ($moduleTypeName === false || trim($moduleTypeName) === '') ? 'gallery' : trim($moduleTypeName);
            unset($_POST['fromModuleId']);
            unset($_POST['fromModuleTypeName']);
        }
        
        /// check if the editing of gallery is coming from  module editing ///
        $serviceId = $service =  '';
        $fromService = false;
        if(isset($_POST['fromServiceId']))
        {
            $serviceId = trim($_POST['fromServiceId']);
            $this->load->model('admin/service');
            $service = $this->service->get_service($serviceId);
            $fromService = true;
            unset($_POST['fromServiceId']);
        }

        /// check if we have custom redirection from the view , if yes we have to redirect to the appropriate url specified when we are finished editing ///
        $customRedirect = false;
        $redirectionURL = '';
        if(isset($_POST['custom_redirection']))
        {
            $redirectionURL = trim($_POST['custom_redirection']);
            unset($_POST['custom_redirection']);
            $customRedirect  = true;
        }    

        if ($data['gallery'] && $this->form_validation->run()) 
        {
            //$save           = TRUE;
            $gallery_images = @$_POST['images'];
            unset($_POST['images']);
            $gallery_data   = $_POST;

            //update the gallery images
            if ($gallery_images) 
            {
                foreach ($gallery_images as $image_id => $row_data)
                    $this->gallery->update_image_row($image_id, $row_data);
            }

            //update the gallery
            $success = $this->gallery->update_row($gallery_id, $gallery_data);
            if($success)
            {
                set_message('success', 'Gallery has been updated.');
                if($moduleId === '' && $fromService === false && $customRedirect === false)
                    redirect(base_url('admin/galleries'));
                else if($fromService === true)
                    redirect(base_url('admin/services/edit/'.encode($serviceId).'?dy=1&cw=1'));/// giving special instruction to edit function on services controller to close off the fancy box ///
                else if($moduleId !== '')
                    redirect(base_url('admin/pages/gallery/view/'.encode($moduleId)));
                else if($customRedirect === true && $redirectionURL !== '')
                    redirect(base_url($redirectionURL));
            }
            else
            {
                set_message('error', 'Selected Gallery could not be found.');
            }
        }
        elseif($error = validation_errors())
            set_message('error', $error);
        else
        {
            if(!$data['gallery'])
                set_message('error', 'Gallery could not be found');
            
            if($moduleId === '' && $fromService === false && $customRedirect === false)
                redirect(base_url('admin/galleries'));
            else if($fromService === true)
                redirect(base_url('admin/services/assign_slider/'.encode($serviceId)));
            else if($moduleId !== '')
                redirect('admin/pages/gallery/edit/'.encode($moduleId));
            else if($customRedirect === true && $redirectionURL !== '')
                redirect(base_url($redirectionURL));
        }

        if($moduleId === '' && $fromService === false && $customRedirect === false)
        {
            $data['page_content'] = $this->load->view('admin/galleries/edit', $data, TRUE);
            $this->load->view('admin/template', $data);
        }
        else if($fromService === true)
             redirect(base_url('admin/services/assign_slider/'.encode($serviceId)));
        else if($moduleId !== '')
            redirect(base_url('admin/pages/gallery/edit/'.encode($moduleId)));
        else if($customRedirect === true && $redirectionURL !== '')
        {
            $data['custom_redirect_url'] = $redirectionURL;
            $this->layout->view('admin/galleries/edit', $data);
        }    
    }

    function upload_image() 
    {
        $gallery_id     = $this->uri->segment('4');
        $from_modules   = $this->uri->segment('5');
        $module_id      = decode($this->uri->segment('6'));

        if($gallery_id && $_FILES["file"]["name"] != '') 
        {

            $path = config_item('gallery_path').$gallery_id.'/';

            make_folder(ROOT.$path);
            $config['upload_path']      = ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|png|GIF|JPG|PNG';

            $this->load->library('upload');
            $this->upload->initialize($config);
            if (! $this->upload->do_multi_upload('file'))
            {
                respond('error: '.$this->upload->display_errors());
                return;
            }
            $file = $this->upload->data();
            $this->gallery->make_image($file);
            $page_data['image'] = $path.$file['file_name'];
            $page_data['thumb'] = $path.$file['raw_name'].'_thumb'.$file['file_ext'];
            $page_data['main'] = $path.$file['raw_name'].'_main'.$file['file_ext'];
            $page_data['gallery_id'] = $gallery_id;

            $this->gallery->save_gallery_image($page_data);
            
            respond('1');
            return;
        }
        respond('error: Could not find the item');

    }
    
    function remove_image() {
        $image_id = decode($this->uri->segment('4'));

        $gallery_id = $this->gallery->remove_image($image_id);
        if($gallery_id) 
        {
            if(is_ajax())
                respond('1');
            else
            {
                set_message('success', 'Image removed.');
                if($this->input->get('redirect'))
                {
                    redirect(decode($this->input->get('redirect')));
                }
                else
                {
                    redirect('admin/galleries/edit/'.encode($gallery_id));
                }                
            }
        }
        else
        {
            if(is_ajax())
                respond('0');
            set_message('error', 'There was an issue finding that Image. Please try again.');    
        }
    }

    function publish()
    {
        $Gallery_id = $this->uri->segment('4');
        $Gallery_status = (int)$this->uri->segment('5');
        $success = $this->gallery->update_row($Gallery_id, array('status' => $Gallery_status));

        if($success) {
            set_message('success', 'Gallery status updated.');
        } else {
            set_message('error', 'Selected Gallery could not be found.');
        }
        redirect(base_url('admin/galleries'));
    }
}