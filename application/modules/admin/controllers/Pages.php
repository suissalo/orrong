<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		access_area('admin/pages');

		$this->load->helper('ckeditor');
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
		$this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/'); 

		$this->load->model('admin/page');
        $this->load->model('admin/gallery');
        $this->load->library('thumb');
        $this->load->helper('model');

        //$this->output->enable_profiler(TRUE);
	}

	public function index()
	{
		$data['pages'] = $this->page->build_list();
		$data['orphans'] = $this->page->build_list('-1');
		$this->layout->view('admin/pages/manage', $data);
	}

	public function remove()
	{
		$page_id = decode($this->uri->segment('4'));
		if(!$page_id) 
		{
			set_message('error', 'Could not find that page.');
			redirect(base_url('admin/pages'));
		}
		
		$pageIdsDeleted = array();
		if($this->page->remove_row($page_id, $pageIdsDeleted))
		{
			set_message('success', 'Page has been removed.');
		}
		else
		{
			set_message('error', 'Could not find that page.');
		}

		redirect(base_url('admin/pages'));
	}

	/**
	 * new page
	 *
	 * @return void
	 **/
	public function new_page()
	{
		$data['pages'] 			= $this->page->get_all();

		//used for the page templates
		$data['templates'] 		= config_item('page_templates');

		$this->layout->view('admin/pages/new', $data);
	}

	/**
	 * save an edit
	 *
	 * @return void
	 **/
	public function save_new()
	{
		$data = array();
		$this->form_validation->set_rules('parent_id', 'Parent', 'trim');
		$this->form_validation->set_rules('display', 'Display', 'trim');
        $this->form_validation->set_rules('galleri_id', 'Gallery', 'trim|is_natural');
		$this->form_validation->set_rules('status', 'Status', 'trim|is_natural');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('detail', 'Page Content', 'trim');
		$this->form_validation->set_rules('sidebar', 'Sidebar Content', 'trim');
		$this->form_validation->set_rules('category', 'Category', 'trim');
		$this->form_validation->set_rules('sub_heading', 'Sub-heading', 'trim');
		$this->form_validation->set_rules('template', 'Template', 'trim|required');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
		//$this->form_validation->set_rules('contact_form', 'Contact Form', 'trim');
		$this->form_validation->set_rules('permalink', 'Permalink', 'trim|required');
		$this->form_validation->set_rules('enable_link', 'Clickable', 'trim');

		if ($this->form_validation->run()) 
		{
            /// set default value as we do not want to use them from the DB ///	
            $_POST['contact_form'] = '';
            $_POST['contact_embed'] = 0;

			$success	= $this->page->new_row($_POST);

			if($success) 
			{
				set_message('success', 'Page has been created.');
				redirect(base_url('admin/pages'));
			} 
			else 
			{
				set_message('error', 'There was an error saving the page, please try again.');
			}
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
		}
		$data['pages'] = $this->page->get_all();
		$this->layout->view('admin/pages/new', $data);
	}

	/**
	 * edit a page
	 *
	 * @return void
	 **/
	public function edit()
	{

		$page_id 			= decode($this->uri->segment('4'));
		$data['page'] 		= $this->page->get_page($page_id);
		$data['pages'] 		= $this->page->get_all($page_id);
        //$data['galleries'] 	= $this->gallery->get_all();

		if(!$data['page']) 
		{
			set_message('error', 'Selected page could not be found');
			redirect(base_url('admin/pages'));
		}
		if(!$data['page']->gallery_id)
		{
			//create a Gallery
        	$this->db->insert('gallery', array('title' => $data['page']->title.' Gallery'));

			$data['page']->gallery_id = $this->db->insert_id();
			$this->page->update_row($page_id, (array)$data['page']);
			$data['page'] = $this->page->get_page($page_id);
		}
		
		$data['gallery_images']	= $this->gallery->get_all_images_for_gallery($data['page']->gallery_id);

		//used for the page templates
		$data['templates'] 		= config_item('page_templates');

		$this->layout->view('admin/pages/edit', $data);		
	}

	/**
	 * save an edit
	 *
	 * @return void
	 **/
	public function save_edit()
	{
		$page_id 		= decode($this->uri->segment('4'));
		$data['page'] 	= $this->page->get_page($page_id);

		if(!$data['page'])
		{
			set_message('error', 'Invalid Page id provided');
			redirect(base_url('admin/pages'));
		}

		$this->_page_edit_validate();
		if($this->form_validation->run()) 
		{


			$_POST['footer'] = (int)@$_POST['footer'];
			$_POST['enable_link'] = (int)@$_POST['enable_link'];
			
			/// as we don't need these properties, we are setting a default value for them in the POST ///
			$_POST['contact_form'] = '';
			$_POST['contact_embed'] = 0;

			/// update existing gallery image(s) for the page    ///
			/// also add new image for the gallery (if provided) ///
			$path = config_item('gallery_path').$_POST['permalink'];
            $this->_upload_gallery_file($_POST['gallery_id'], $path);
            $this->_update_existing_gallery_file($path);
            /////////////////////////////////////////////////////////
			unset($_POST['images']);

			//Have we changed the urls?
			//view(array($data['page']->permalink, $_POST['permalink']));
			if ($data['page']->permalink != $_POST['permalink']) 
			{
				$this->page->make_redirect($data['page']->permalink, $_POST['permalink']);
			}
			$success = $this->page->update_row($page_id, $_POST);
			if($success) 
            {
       			$redirect = '';
				if (isset($_FILES['image']['size']))
				{
					$redirect = '#hero-gallery';
				}

				set_message('success', 'Page has been saved.');
				redirect(base_url('admin/pages/edit/'.encode($page_id).$redirect));
			} 
			else 
				set_message('error', 'Selected page could not be found.....');
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
		}

		redirect(base_url('admin/pages/edit/'.encode($page_id)));
	}

	private function _update_existing_gallery_file($path)
	{
		if(isset($_POST['images']))
		{
			if(is_array($_POST['images']))
			{
				$files = $_FILES;
				foreach($_POST['images'] as $key => $value)
				{
					if(isset($files['hero_images']) && $files['hero_images']['name'][$key]['video_file'] != '')
			        {
			            $config['upload_path']      = $path;
			            $config['allowed_types']    = '*';
			            $config['remove_spaces']    = TRUE;
			            $config['detect_mime']    	= FALSE;

			            $_FILES['dummy'] = array(
			            	'name'		=> $files['hero_images']['name'][$key]['video_file'],
			            	'type'		=> $files['hero_images']['type'][$key]['video_file'],
			            	'tmp_name'	=> $files['hero_images']['tmp_name'][$key]['video_file'],
			            	'error'		=> $files['hero_images']['error'][$key]['video_file'],
			            	'size'		=> $files['hero_images']['size'][$key]['video_file'],
			            );
			            
			            $this->upload->initialize($config);

			            if (! $this->upload->do_upload('dummy')){
			                set_message('error', 'Sorry there was an issue uploading Video. '.$this->upload->display_errors());
			                die($this->upload->display_errors());
			            }
			            else
			            {
			                $file = $this->upload->data();
			                $value['mp4'] = str_replace(ROOT, '', $file['full_path']);
			            }
			        }
			        if(isset($files['hero_images']) && $files['hero_images']['name'][$key]['image'] != '')
			        {

			            $config['upload_path']      = $path;
			            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            			$config['remove_spaces']    = TRUE;

			            $_FILES['dummy'] = array(
			            	'name'		=> $files['hero_images']['name'][$key]['image'],
			            	'type'		=> $files['hero_images']['type'][$key]['image'],
			            	'tmp_name'	=> $files['hero_images']['tmp_name'][$key]['image'],
			            	'error'		=> $files['hero_images']['error'][$key]['image'],
			            	'size'		=> $files['hero_images']['size'][$key]['image'],
			            );
			            
			            $this->upload->initialize($config);

			            if (! $this->upload->do_upload('dummy')){
			                set_message('error', 'Sorry there was an issue uploading Video. '.$this->upload->display_errors());
			                die($this->upload->display_errors());
			            }
			            else
			            {
			                $file = $this->upload->data();
                			$value['image'] = str_replace(ROOT, '', $file['full_path']);				
							$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
			            }
			        }

					$this->gallery->update_image_row($key, $value);
				}					
			}
			unset($_POST['images']);
		}
	}

	public function _upload_gallery_file($gallery_id = FALSE, $path = FALSE)
	{
		if(!$gallery_id || !$path) return FALSE;

		make_folder(ROOT.$path);
		$this->load->library('upload');
		$this->load->library('thumb');
		$data = array();

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['image'] = str_replace(ROOT, '', $file['full_path']);				
				$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }
		if(isset($_FILES['image_hover']) && $_FILES['image_hover']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image_hover'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['image_hover'] = str_replace(ROOT, '', $file['full_path']);
				$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }
        if(isset($_FILES['webm']) && $_FILES['webm']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'webm';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('webm'))
                set_message('error', 'Sorry there was an issue uploading video. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['webm'] = str_replace(ROOT, '', $file['full_path']);
            }
        }
        if(isset($_FILES['mp4']) && $_FILES['mp4']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = '*';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('mp4'))
                set_message('error', 'Sorry there was an issue uploading video. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['mp4'] = str_replace(ROOT, '', $file['full_path']);
            }
        }
        if(isset($_FILES['new_image_video_file']) && $_FILES['new_image_video_file']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'mp4';
            $config['remove_spaces']    = TRUE;
            $config['detect_mime']    	= FALSE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('new_image_video_file'))
                set_message('error', 'Sorry there was an issue uploading video. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['mp4'] = str_replace(ROOT, '', $file['full_path']);
            }
        }

        if (count($data) > 0) 
        {
        	$data['gallery_id'] = $gallery_id;
        	//$data['width'] = @$_POST['new_image_width'];
        	//$data['height'] = @$_POST['new_image_height'];
        	$data['title'] = @$_POST['new_image_title'];
        	$data['description'] = @$_POST['new_image_description'];
        	$data['caption_position'] = @$_POST['new_image_caption_position'];
        	$data['link'] = @$_POST['new_image_link'];
        	$data['link_text'] = @$_POST['new_image_link_text'];
        	$this->db->insert('gallery_images', $data);
        }

        unset($_POST['new_image_width']);
        unset($_POST['new_image_height']);
        unset($_POST['new_image_title']);
        unset($_POST['new_image_description']);
        unset($_POST['new_image_caption_position']);
        unset($_POST['new_image_link']);
        unset($_POST['new_image_link_text']);
        
	}

	public function publish()
	{
		$page_id = $this->uri->segment('4');
		$page_status = (int)$this->uri->segment('5');
		$success = $this->page->update_row($page_id, array('status' => $page_status));
		
		if($success) {
			set_message('success', 'Page status updated.');
		} else {
			set_message('error', 'Selected page could not be found.');
		}
		redirect(base_url('admin/pages'));
	}



	public function footer()
	{
		$this->layout->page_title = 'Footer';
		$data['new'] 			= $this->uri->segment(4);
		$data['footer'] 		= $this->page->footer();
		for($i = 0; $i < 6; $i++)
		{
			$links[$i] = $this->page->footer_links($i);
		}
		$data['footerlinks'] = $links;
		$this->layout->view('admin/pages/footer', $data);
	}

	public function save_footer()
	{
		$this->page->update_footer($_POST);
		set_message('success', 'Footer has been updated');
		redirect('admin/pages/footer');
	}

	public function reorder_footer()
	{
		$this->page->reorder_footer($_POST);
	}

	public function remove_footer()
	{
		$footer_id = decode($this->uri->segment(4));
		
		$removed = $this->page->remove_footer($footer_id);

		if (!$footer_id) {
			set_message('error', 'Sorry that item could not be found.');
		} else {
			set_message('success', 'Item has been removed.');
		}
		redirect('admin/pages/footer');
	}

	public function new_footer()
	{
		$column = $this->uri->segment(4);

		$id = $this->page->new_footer($column);
		set_message('success', 'Footer created');
		redirect('admin/pages/footer/'.$id);
	}

	//DRAG-N-DROP - gallery_images
	public function upload_gallery_image()
	{

		$gallery_id    		= decode($this->uri->segment(4));
		$page_id    		= decode($this->uri->segment(5));
		$data['gallery'] 	= $this->gallery->get_gallery_by_id($gallery_id);
		$page 				= $this->page->get_page($page_id);
		//view($page);
        if($page && $data['gallery'] && $_FILES["file"]["name"] != '') 
        {
        	$path = config_item('gallery_path').$page->permalink;

            make_folder(ROOT.$path);
            $config['upload_path']      = ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';

            $this->load->library('upload');
            $this->upload->initialize($config);
            if (! $this->upload->do_multi_upload('file'))
            {
                respond('error: '.$this->upload->display_errors());
                return;
            }
            $file = $this->upload->data();
            $this->load->library('thumb');
            				
			$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');

            $page_data['image'] 		= $path.$file['file_name'];
            $page_data['thumb'] 		= str_replace(ROOT, '', $file['full_path']);
            $page_data['main'] 			= $path.$file['raw_name'].'_main'.$file['file_ext'];
            $page_data['gallery_id'] 	= $gallery_id;

            $image = $this->gallery->save_gallery_image($page_data, TRUE);
            // view($image);

            //$html .= $this->load->view('admin/_partials/blog_image_partial', array('image' => $image, 'redirect' => base_url('admin/blog/edit/'.encode($post_id))), TRUE);
            $html = '<li class="col-xs-60 col-sm-30 col-md-20 std-pd" id="sort_'. $image->id .'" >'.$this->load->view('admin/_partials/gallery_item_media', array('image' => $image, 'redirect' => 'admin/pages/edit/'.encode($page->id).'#hero-gallery'), TRUE).'</li>';
            
            respond(json_encode(array('html' => $html)));
            return;
        }
        respond('error: Could not find the item');
	}
	

	#####################################################################################################################################################################################
	#####################################################################################################################################################################################
	#																						validation
	#####################################################################################################################################################################################
	#####################################################################################################################################################################################

	private function _page_edit_validate()
	{
		$this->form_validation->set_rules('parent_id', 'Parent', 'trim');
		$this->form_validation->set_rules('display', 'Display', 'trim');
        $this->form_validation->set_rules('galleri_id', 'Gallery', 'trim|is_natural');
		$this->form_validation->set_rules('status', 'Status', 'trim|is_natural');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('detail', 'Page Content', 'trim');
		$this->form_validation->set_rules('sidebar', 'Sidebar Content', 'trim');
		$this->form_validation->set_rules('category', 'Category', 'trim');
		$this->form_validation->set_rules('sub_heading', 'Sub-heading', 'trim');
		$this->form_validation->set_rules('template', 'Template', 'trim|required');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
		//$this->form_validation->set_rules('contact_form', 'Contact Form', 'trim');
		$this->form_validation->set_rules('permalink', 'Permalink', 'trim|required');
		$this->form_validation->set_rules('enable_link', 'Clickable', 'trim');
		$this->form_validation->set_rules('new_image_width', 'New Image Width', 'trim');
		$this->form_validation->set_rules('new_image_height', 'New Image Height', 'trim');
		$this->form_validation->set_rules('new_image_title', 'New Image Title', 'trim');
		$this->form_validation->set_rules('new_image_description', 'New Image Description', 'trim');

		if(isset($_POST['existing_image']))
		{
			foreach($_POST['existing_image'] as $key => $value)
			{
				$valueKeyArray = array_keys($value);
				foreach($valueKeyArray as $k)
					$this->form_validation->set_rules('existing_image['.$key.']['.$k.']', 'Existing Image '.$k, 'trim');
			}
		}
	}
	
}