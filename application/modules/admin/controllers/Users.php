<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');

		$this->load->helper('ckeditor');
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
		$this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/'); 

		$this->load->model('admin/user');
	}
    
    /***************************************************************************
     * Users
     ***************************************************************************
     */
    
    /**
     * Manage Users
     */
	function index()
	{
		access_area('admin/users');

		$data['users'] = $this->user->get_users();
		$this->layout->view('admin/users/manage', $data);
	}

    /**
     * Remove a user
     */
	function remove()
	{
		access_area('admin/users/remove');

		$user_id = decode($this->uri->segment('4'));
		if(!$user_id) {
			set_message('error', 'Could not find that user.');
			redirect(base_url('admin/users'));
		}
		if($this->user->remove_row($user_id))
		{
			set_message('success', 'User has been removed.');
		}
		else
		{
			set_message('error', 'Could not find that User.');
		}
		redirect(base_url('admin/users'));
	}

	/**
	 * new page
	 *
	 * @return void
	 * 
	 **/
	function new_user()
	{
		access_area('admin/users/new_user');

		$data['groups'] = $this->user->get_groups();
		$this->layout->view('admin/users/new', $data);
	}

	/**
	 * save an edit
	 *
	 * @return void
	 * 
	 **/
	function save_new()
	{
		access_area('admin/users/save_new');

		$data = array();
		$this->_user_validation();

		if ($this->form_validation->run())
		{
			//profile image
			if(isset($_FILES['user_image']) && $_FILES['user_image']['name'] != '')
			{
				$path = config_item('user_path').url_title($_POST['name'], '-', TRUE).'/';
                make_folder($path);
                $config['upload_path']      = ROOT.$path;
                $config['allowed_types']    = 'jpg|jpeg|png|gif';

                $this->load->library('upload');
                $this->upload->initialize($config);

                if (! $this->upload->do_upload('user_image'))
                {
                    set_message('error', 'Sorry there was an issue uploading your Image.');
                }
                else
                {
                    $file              = $this->upload->data();                    
                    $_POST['image']    = $path.$file['file_name'];

                    //make thumbnails
                    $this->load->library('thumb');
                    $this->thumb->make_thumbs($file['full_path'], 'user_image');
                }
			}

			$success = $this->user->new_row($_POST);
			if($success) 
			{
				set_message('success', 'User has been created.');
				redirect(base_url('admin/users'));
			} 
			else 
			{
				set_message('error', 'There was an error saving the user, please try again.');
			}
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
			return self::new_user();
		}
		//$this->layout->view('admin/users/new', $data, TRUE);
		
	}

	/**
	 * edit a page
	 *
	 * @return void
	 * 
	 **/
	function edit()
	{
		access_area('admin/users/edit');

		$user_id 				= decode($this->uri->segment('4'));
		$data['user'] 			= $this->user->get_user($user_id);
		$data['master_users']	= FALSE;

		if(!$data['user'])
		{
			set_message('error', 'Selected user could not be found');
			redirect(base_url('admin/users'));
		}
        $data['groups'] 	= $this->user->get_groups();
		$this->layout->view('admin/users/edit', $data);
		
	}

	/**
	 * save an edit
	 *
	 * @return void
	 * 
	 **/
	function save_edit()
	{
		access_area('admin/users/save_edit');

		$data 				= array();
		$user_id 			= $_POST['row_id'];
        $data['groups'] 	= $this->user->get_groups();
		$data['user']		= $this->user->get_user($user_id);

		$this->_user_validation(TRUE, $data['user']);

		if ($data['user'] && $this->form_validation->run()) 
		{
			//profile image
			if(isset($_FILES['user_image']) && $_FILES['user_image']['name'] != '')
			{
				$path = config_item('user_path').url_title($_POST['name'], '-', TRUE).'/';
                make_folder($path);
                $config['upload_path']      = ROOT.$path;
                $config['allowed_types']    = 'jpg|jpeg|png|gif';

                $this->load->library('upload');
                $this->upload->initialize($config);

                if (! $this->upload->do_upload('user_image'))
                {
                    set_message('error', 'Sorry there was an issue uploading your Image.');
                }
                else
                {
                    $file              = $this->upload->data();                    
                    $_POST['image']    = $path.$file['file_name'];

                    //make thumbnails
                    $this->load->library('thumb');
                    $this->thumb->make_thumbs($file['full_path'], 'user_image');

                    if($data['user']->image)
                    {
                        $this->load->library('thumb');
                        $this->thumb->remove_thumbs($data['user']->image, 'user_image');
                    }
                }
			}

			$page_data = $_POST;
			unset($page_data['row_id']);
			$success = $this->user->update_row($user_id, $page_data);
			if($success) 
			{
				set_message('success', 'User has been updated.');
				if (!access_menu('admin/users'))
				{
					redirect(base_url('admin'));
				}
				redirect(base_url('admin/users'));
			} 
			else 
			{
				set_message('error', 'Selected User could not be found.');
			} 
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
		}
		else
		{
			set_message('error', 'User could not be found.');
			redirect(base_url('admin/users'));
		}
		$this->layout->view('admin/users/edit', $data);
	}

	function _user_validation($edit = FALSE, $user = FALSE)
	{
		if ($edit && $user) 
		{
			if ($user->username != $_POST['username'])
			{
				$this->form_validation->set_rules('username', 'Usernname', 'trim|required');
			}
			else
			{
				$this->form_validation->set_rules('username', 'Usernname', 'trim|required');
			}
		}
		else
		{
			$this->form_validation->set_rules('username', 'Usernname', 'trim|required');
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('title', 'Title', 'trim');
		$this->form_validation->set_rules('contact_email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('phone', 'Phone', 'trim');
		if (!$edit || $_POST['pass']) 
		{
			$this->form_validation->set_rules('pass', 'Password', 'trim|min_length[4]|required');
		}		
		//$this->form_validation->set_rules('group_id', 'Group', 'trim|required');
		$this->form_validation->set_rules('owner_id', 'Location', 'trim');
		$this->form_validation->set_rules('bio', 'Bio', 'trim');
		
	}
    
    /***************************************************************************
     * Groups
     ***************************************************************************
     */
    
    /**
     * Manage groups
     */
    function groups()
    {
    	access_area('admin/users/groups');

    	$this->layout->page_title = 'Groups';
        $data['groups'] = $this->user->get_groups();
        $data['areas'] = config_item('estate_area');
        $this->layout->view('admin/users/groups_manage', $data);
    }

    function edit_group()
    {
    	access_area('admin/users/edit_group');

    	$this->layout->page_title = 'Groups';
    	$group_id 		= decode($this->uri->segment(4));
        $data['group'] 	= $this->user->group($group_id);

        if(!$data['group'])
        {
        	set_message('error', 'This group could not be found');
        	redirect(base_url('admin/users/groups'));
        }
        $data['areas'] 	= config_item('estate_area');
        $data['themes']	= config_item('themes');
        
        $this->layout->view('admin/users/groups_edit', $data);
    }
    
    /**
     * New group
     */
    function new_group()
    {
    	access_area('admin/users/new_group');

    	$this->layout->page_title = 'Groups';
        $this->layout->view('admin/users/groups_new', array());
    }
    
    /**
     * save new group
     */
    function save_new_group()
    {
    	access_area('admin/users/save_new_group');

        $this->layout->page_title = 'Groups';
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('active', 'Active', 'is_natural');

        if($this->form_validation->run())
        {
            //$_POST['permissions'] = $data['permissions'];
            $group_id = $this->user->new_group($_POST);
            if($group_id)
            {
                set_message('success', 'Your Group has been created.');
                redirect(base_url().'admin/users/groups');
            }
        }
        else
        {
            set_message('error', validation_errors());
        }
        return $this->new_group();
    }

    function save_edit_group()
    {
    	access_area('admin/users/save_edit_group');

    	$this->layout->page_title = 'Groups';
    	$group_id = decode($this->uri->segment(4));
    	$this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('active', 'Active', 'is_natural');
        $this->form_validation->set_rules('area_id', 'Area', 'trim|is_natural');

        if ($this->form_validation->run()) 
        {
        	$success = $this->user->update_group($this->input->post(), $group_id);
        	if($success)
        	{
        		set_message('success', 'Group has been updated');
        		redirect('admin/users/groups');
        	}
        	set_message('error', 'There was an issue updating that group, Please try again.');
        }
        else
        {
        	set_message('error', validation_errors());
        }
        return $this->edit_group();
    }
    
    function remove_group()
    {
    	access_area('admin/users/remove_group');

        $group_id = decode($this->uri->segment(4));
        $user_group = $this->session->userdata('admin_group_id');
        
        if($group_id == $user_group) 
        {
            set_message('error', 'Sorry cannot remove your own group');
            redirect(base_url().'admin/users/groups');
        }
        // cannot remove master admin
        elseif($group_id == 1)
        {
        	set_message('error', 'Sorry that group cannot be removed.');
        	redirect(base_url().'admin/users/groups');
        }

        $success = $this->user->remove_group($group_id);
        
        if($success) {
            set_message('success', 'Your your group has been removed');
        } else {
            set_message('error', 'there was an error removing that group');
        }
        
        redirect(base_url().'admin/users/groups');
    }
	
    /***************************************************************************
     * Form Validation
     ***************************************************************************
     */
    
    /**
     * form validation callback to check username
     * @param type $username
     * @return boolean
     */
	function _username_check($username, $user_id) {
		$user_id = (@$_POST['row_id'])? $_POST['row_id'] : FALSE;
		if ($this->user->unique_username($username, $user_id)) {
			return TRUE;
		}
		
		$this->form_validation->set_message('_username_check', 'User %s allready exists.');
		return FALSE;
	}
}