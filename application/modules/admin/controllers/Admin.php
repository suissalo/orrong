<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		
		$this->load->helper('ckeditor');
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
		$this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/');

		$this->load->model('admin/user');
	
	}

	/**
	*-------------------------------------------------------------------- dashboard
	*/
	public function index()
	{
		redirect_if_not_logged_in();

		$data = array();

		$this->load->model('lead');
		$data['forms'] = $this->lead->get_forms();
		$data['leads'] = array();
		
		if ($data['forms']) {
			foreach($data['forms'] as $form) 
			{
				$data['leads'][$form->id] = $this->lead->get_leads($form->id);
			}
		}

	    $this->layout->page_title = 'Dashboard';

	    if ($this->config->item('site_id'))
	    {

			$this->load->library('piwik');
			$this->load->library('gcharts');

			$visits = $this->piwik->actions('day', 10);
			$unique = $this->piwik->unique_visitors('day', 10);

			$this->gcharts->DataTable('Analytics')
			              ->addColumn('number', 'Count', 'count')
			              ->addColumn('number', 'Total Visits', 'visits')
			              ->addColumn('number', 'Unique Visits', 'unique');

			$this_month = date("m");
			if ($visits)
			{
			    foreach($visits as $date => $visit)
			    { 
			        $date_arr = explode('-', $date);
			        $year = $date_arr[0];
			        $month = $date_arr[1];
			        $day = $date_arr[2];

			        if($month != $this_month) continue;
			        //print_r(array($month, $this_month)); die;
			        $utc = mktime(date('h') + 1, NULL, NULL, $month, $day, $year) * 1000;

			        $flot_visits[] = '[' . $utc . ',' . $visit . ']';
			        $flot_unique[] = '[' . $utc . ',' . $unique[$date] . ']';

			        $stats = array(
			        	$day,
			        	$visit,
			        	$unique[$date]
			        );

			        $this->gcharts->DataTable('Analytics')->addRow($stats);
			    }
			}

			$config = array(
	            'title' => 'Analytics'
	        );

			$this->gcharts->LineChart('Analytics')->setConfig($config);

		}      

	    $this->layout->view('admin/dashboard', $data);
	}


	/**
	*-------------------------------------------------------------------- user
	*/

	public function login()
	{

		$this->form_validation->set_rules('user', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run())
		{
			if ($user = $this->user->login($_POST['user'], $_POST['password'])) 
			{
				set_message('success', 'Welcome '.$user->name);
				redirect('admin');
			}
			else
			{
				set_message('error', 'Sorry, that Username and Password do not match.');
			}

		}

		$this->layout->page_title = 'Admin';
		$this->layout->view('admin/users/login');
	}

	// public function process_login()
	// {
	// 	$this->form_validation->set_rules('user', 'User Name', 'trim|required');
	// 	$this->form_validation->set_rules('password', 'Password', 'trim|required');
	// 	if($this->form_validation->run())
	// 	{
			
	// 		if ($user = $this->user->login($_POST['user'], $_POST['password'])) 
	// 		{
	// 			set_message('success', 'Welcome '.$user->name);
	// 			redirect('admin');
	// 		}
	// 		else
	// 		{
	// 			set_message('error', 'Sorry, that Username and Password did not match.');
	// 			redirect('admin/login');
	// 		}
	// 	}
	// 	$this->layout->view('admin/login', array());		
	// }

	public function logout()
	{
		$this->session->userdata = array();

		set_message('success', 'You are now logged out.');
		redirect('admin/login');
	}

	public function forgot_password()
	{
		if(isset($_POST['submit_password_reset']) && trim($_POST['submit_password_reset']) !== '')
		{
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
			if($this->form_validation->run() !== false)
			{
				$userDetails = $this->user->check_and_generate_hash_for_password_reset($_POST['email_address']);
				if($userDetails === false)
					set_message('error', 'Email address provided is not valid!!!');
				else
				{
					$message = $this->load->view('admin/email/reset_password', array('user' => $userDetails), TRUE);


					$this->load->library('email');
					$config['mailtype'] = 'html';

					$this->email->initialize($config);
					$this->email->from('support@barkingbird.com.au', 'Barking Bird');

					$this->email->to($userDetails->contact_email); 
					$this->email->subject('Forgotten Password');
					$this->email->message(trim($message));

					if($this->email->send())
					{
						set_message('success', 'Please check email '.$_POST['email_address'].' for further instructions');
						redirect('admin/login');
					}
					else
					{
						set_message('error', 'Please check email '.$_POST['email_address'].' for further instructions');
						redirect('admin/login');
					}
				}
			}
			else if($error = validation_errors())
				set_message('error', $error);
		}
		
		$this->layout->view('admin/users/forgot_password');
	}

	public function enter_reset_password()
	{
		$userId = decode($this->uri->segment(3));
		$hash = $this->uri->segment(4);

		$output = $this->user->validate_hash($userId, $hash);
		if($output['success'] === false)
		{
			set_message('error', $output['message'].'. Please Re-Submit the form');
			$this->forgot_password();
		}
		else
		{
			$userDetails = $output['user'];
			//view($userDetails);
			$this->layout->view('admin/users/enter_reset_password', array('user' => $userDetails));
		}
	}

	public function change_password()
	{
		$userId = decode($this->uri->segment(3));
		$userDetails = $this->user->get_user($userId);
		if($userDetails === false)
			redirect(base_url('admin/login'));

		if(isset($_POST['submit_new_password']) && trim($_POST['submit_new_password']) !== '')
		{
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
			$this->form_validation->set_rules('re_new_password', 'Re-entry of New Password', 'trim|required');

			if($this->form_validation->run() !== false)
			{
				$newPassword = $_POST['new_password'];
				$newPassword2 = $_POST['re_new_password'];

				if($newPassword2 != $newPassword)
					set_message('error', 'New Password and Re-entry of new password must be equal');
				else
				{
					$result = $this->user->change_password($userId, $newPassword);	
					if($result)
					{
						set_message('success', 'Password reset successful');	
						redirect(base_url('admin/login'));
					}
					else
						set_message('error', 'Password Reset Failed');
				}		
			}
			else if($error = validation_errors())	
				set_message('error', $error);	

			$this->layout->view('admin/users/enter_reset_password', array('user' => $userDetails));
		}
		else
			redirect(base_url('admin/forgot_password'));
	}

}




















