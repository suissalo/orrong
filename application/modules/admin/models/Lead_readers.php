<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lead_readers extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function fetch_last_source($args = FALSE)
    {

        if($args) $this->db->where($args);
        $this->db->order_by('email_date,id','DESC');
        $query = $this->db->get('lead_source', 1);
        
        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    public function lead_source($email_number = FALSE)
	{
		if(!$email_number) return FALSE;

		$this->db->where('email_number',$email_number);
		$query = $this->db->get('lead_source',1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
	}

	public function save_lead($data = FALSE)
	{
		if(!$data) return FALSE;

		$this->db->insert('form_leads', $data);
	}

	public function save_order_source($source = FALSE,$id = FALSE)
	{		
		if(!$source) return FALSE;

		if($id)
		{
			$this->db->where('id',$id);
			$this->db->update('lead_source', $source);
			return $id;
		}
		$this->db->insert('lead_source', $source);
		return $this->db->insert_id();
	}

}