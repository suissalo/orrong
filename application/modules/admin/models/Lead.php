<?php 
class Lead extends CI_Model 
{
	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	public function get_lead($lead_id = FALSE)
	{
		if (!$lead_id) return FALSE;

		$this->db->where('id', $lead_id);
		$this->db->where('deleted', 0);
		$query = $this->db->get('form_leads', 1);

		return ($query->num_rows() == 1) ? $query->row() : FALSE;
	}

	// don't display spam & deleted.
	public function get_leads($form_id = FALSE)
	{
		if (!$form_id) return FALSE;

		$this->db->where('form_id', $form_id);
		$this->db->where('deleted', 0);

		// new approach to filter spam 
		$this->db->where('is_spam', 0);
		$query = $this->db->get('form_leads');

		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}

	public function get_form($form_id = FALSE)
	{
		if (!$form_id) return FALSE;

		$this->db->where('id', $form_id);
		$query = $this->db->get('forms', 1);

		return ($query->num_rows() == 1) ? $query->row() : FALSE;
	}

	public function get_forms()
	{
		$query = $this->db->get('forms');

		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}


	public function create_form()
	{
		$data = array(
			'name' => 'New Form',
		);

		$data['notification_message'] = @$this->load->view('admin/leads/default_notification.php', array(), TRUE);

		$this->db->insert('forms', $data);

		return $this->db->insert_id();
	}

	public function update_form($form_id = FALSE, $data = FALSE)
	{
		if (!$form_id || !$data) return FALSE;

		$this->db->where('id', $form_id);
		$this->db->update('forms', $data);

		return TRUE;
	}

	public function remove_form($form_id = FALSE)
	{
		if (!$form_id) return FALSE;

		$this->db->where('id', $form_id);
		$this->db->delete('forms');

		return $this->db->affected_rows();
	}

	public function save_lead_form_attachment($data = FALSE, $return_obj = FALSE)
    {
    	$incoming_attachment = $data;

    	$this->db->where('id', $data['form_id']);
    	$query = $this->db->get('forms', 1);

    	if($query->num_rows()==0) return FALSE;

    	$form = $query->row();
    	if(isset($form->attachments) && $form->attachments != '')
    	{
    		$existing_attachments = json_decode($form->attachments, TRUE);
    		$existing_attachments[] = $incoming_attachment;
    		$attachments = $existing_attachments;
    	}
    	else
    	{
    		$attachments[] = $incoming_attachment;
    	}
    	$form->attachments = json_encode($attachments);

    	$this->update_form($data['form_id'], $form);

    	$form->new_attachment_id = count($attachments)-1;
        
	    return $form;
    }

	public function delete_lead($lead_id = FALSE)
    {
    	if(!$lead_id) return FALSE;

    	$this->db->where('id', $lead_id);
    	$this->db->update('form_leads', array('deleted' => 1));

    	return TRUE;
	}
	
}