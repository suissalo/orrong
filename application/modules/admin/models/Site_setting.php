<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_setting extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function get_all()
	{
		$query = $this->db->get('site_settings');

		return ($query->num_rows() > 0)? $query->result() : FALSE;
	}

	function save_batch($data)
	{
		$this->load->library('upload');
		$path 						= 'assets/media/documents';
		$config['upload_path'] 		= $path;
		$config['allowed_types']	= 'pdf|zip|png|gif|jpg|jpeg|svg';
		$files 						= $_FILES;
		make_folder(ROOT.$path);
		if ($files && count($files) > 0) 
		{

			foreach ($files['setting']['name'] as $key => $file) 
			{
				if (trim($files['setting']['name'][$key])) 
				{
					$_FILES['userfile']['name']		= $files['setting']['name'][$key];
			        $_FILES['userfile']['type']		= $files['setting']['type'][$key];
			        $_FILES['userfile']['tmp_name']	= $files['setting']['tmp_name'][$key];
			        $_FILES['userfile']['error']	= $files['setting']['error'][$key];
			        $_FILES['userfile']['size']		= $files['setting']['size'][$key];

			        $this->upload->initialize($config);
					if ($this->upload->do_upload('userfile'))
					{
						$file_data = $this->upload->data();
						$value = str_replace(ROOT, '', $file_data['full_path']);
						$this->db->where('id', $key);
						$this->db->update('site_settings', array('value' => $value));
					}
					else
					{
						view($this->upload->display_errors());
					}
				}
				
			}			
		}
		foreach ($data['setting'] as $id => $value) 
		{
			$this->db->where('id', $id);
			$this->db->update('site_settings', array('value' => $value));
		}
	}
}