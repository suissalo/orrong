<?php 
class Posts extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all($filter = FALSE)
    {
        if (isset($filter['category']) && $filter['category']) 
        {
            $post_in = array();
            $this->db->select('post_id');
            $this->db->where('category_id', $filter['category']);
            $query = $this->db->get('blog_posts_to_categories');

            if ($query->num_rows() > 0) 
            {
                foreach ($query->result() as $key => $pid) 
                {
                    $post_in[] = $pid->post_id;
                }
            }
            $this->db->where_in('id', $post_in);
        }

        $this->db->order_by('date_added', 'DESC');
        $query = $this->db->get('blog_posts');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function get_page($page_id = FALSE)
    {
        $this->db->where('id', $page_id);
        $query = $this->db->get('blog_posts', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    function images($post_id = FALSE)
    {
        if(!$post_id) return FALSE;
        $this->db->where('post_id', $post_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('blog_images');

        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function image($image_id = FALSE)
    {
        if(!$image_id) return FALSE;
        $this->db->where('id', $image_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('blog_images');

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    function remove_image($image_id)
    {
        if(!$image_id) return FALSE;

        $query = $this->db->where('id', $image_id)->get('blog_images', 1);
        if($query->num_rows() == 0) return FALSE;

        $row = $query->row();

        $this->load->library('thumb');
        $this->thumb->remove_thumbs($row->image, 'gallery_sizes');
        return $this->db->where('id', $image_id)->delete('blog_images');
    }

    public function save_gallery_image($data = FALSE, $return_obj = FALSE)
    {
        $result = $this->db->insert('blog_images', $data);
        return ($return_obj) ? $this->image($this->db->insert_id()) : $this->db->insert_id();
    }

    public function re_order_images($order)
    {
        if (isset($order['sort']) && is_array($order)) {
            foreach ($order['sort'] as $key => $id) {
                $this->db->where('id', $id);
                $this->db->update('blog_images', array('display_order' => $key));
            }
        }
    }

    function re_order_blog_images($order)
    {

        if ($order && is_array($order)) {
            foreach ($order as $key => $id) {
                $this->db->where('id', $id);
                $this->db->update('blog_images', array('display_order' => $key));
            }
        }
    }

    function valid_premalink($page_id = FALSE, $uri = FALSE)
    {
        if($page_id)
        $this->db->where('id <>', $page_id);
    
        $this->db->where('permalink', $uri);
        $query = $this->db->get('blog_posts', 1);

        return ($query->num_rows() == 1)? TRUE : FALSE;
    }

    function re_order($order)
    {
        if ($order && is_array($order)) {
            foreach ($order as $key => $id) {
                $this->db->where('id', $id);
                $this->db->update('blog_posts', array('display_order' => $key));
            }
        }
    }
    
    public function re_order_categories($order)
    {
        if ($order && is_array($order)) 
        {
            foreach ($order as $key => $id) 
            {
                $this->db->where('id', $id);
                $this->db->update('blog_categories', array('display_order' => $key));
            }
        }
    }

    function update_row($page_id = FALSE, $data = FALSE)
    {
        if(!$page_id || !$data || !is_array($data)) return FALSE;

        $post = $this->get_page($page_id);

        if(!$post) return FALSE;

        $categories = FALSE;
        $data['safe_name'] = url_title($data['title'], '-', TRUE);


        if(isset($_FILES['hero']) && $_FILES['hero']['name'] != '' && $data['permalink'])
        {
            $path = config_item('blog_file_path').$data['permalink'];

            make_folder(ROOT.$path);
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('hero'))
            {
                set_message('error', 'Sorry there was an issue uploading Hero. '.$this->upload->display_errors());
            }
            else
            {
                $file           = $this->upload->data();
                $data['hero']  = str_replace(ROOT, '', $file['full_path']);

                $this->load->library('thumb');
                $this->thumb->make_thumbs($file['full_path'], 'hero_sizes');

                if($post->hero)
                {
                    $this->thumb->remove_thumbs($post->hero, 'hero_sizes');
                }
            }
        }

        // fallback gallery image
        if(isset($_FILES['image']) && $_FILES['image']['name'] != '' && $data['permalink'])
        {
            $path = config_item('blog_file_path').$data['permalink'];

            make_folder(ROOT.$path);
            $config['upload_path']      = ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|png|GIF|JPG|PNG';

            $this->load->library('upload');
            $this->upload->initialize($config);
            if (! $this->upload->do_multi_upload('file'))
            {
                set_message('error', 'Sorry there was an issue uploading Hero. '.$this->upload->display_errors());
            }
            else
            {
                $file = $this->upload->data();

                $this->load->library('thumb');
                $this->thumb->make_thumbs($file['full_path'], 'hero_sizes');

                $page_data = array(
                    'image' => $path.$file['file_name'],
                    'post_id' => $post_id,
                    'title' => $data['new_image_title'],
                    'link' => $data['new_image_link'],
                );
                $page_data['image'] = $path.$file['file_name'];
                $page_data['post_id'] = $post_id;

                //view($page_data);

                $image = $this->save_gallery_image($page_data, TRUE);
            }
        }
        unset($data['new_image_title']);
        unset($data['new_image_link']);
        

        if (isset($data['category'])) 
        {
            $this->db->where('post_id', $page_id)->delete('blog_posts_to_categories');
            foreach ($data['category'] as $key => $category_id) 
            {
                $this->db->insert('blog_posts_to_categories', array('post_id' => $page_id, 'category_id' => $category_id));
            }
            unset($data['category']);
        }

        if (isset($data['image_details']))
        {
            foreach ($data['image_details'] as $key => $value)
            {
                $this->db->where('id', $key);
                $this->db->where('post_id', $page_id);
                $this->db->update('blog_images', array('title' => $value['title'], 'link'=> $value['link']));
            } 
            unset($data['image_details']);
        }

        if(isset($data['date_added'])) $data['date_added'] = date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $data['date_added'])));

        // if the link has changed make a new route
        if($post->permalink != $data['permalink'])
        {
            $this->load->model('admin/page');            
            $this->page->make_redirect($post->permalink, $data['permalink']);
        }

        $this->db->where('id', $page_id);
        $result = $this->db->update('blog_posts', $data);
        return $result;
    }

    function new_row($data = FALSE)
    {
        $categories = FALSE;
        if(isset($_FILES['image']) && $_FILES['image']['name'] != '' && $data['permalink'])
        {
            $path = config_item('blog_file_path').$data['permalink'];

            make_folder(ROOT.$path);
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image'))
            {
                set_message('error', 'Sorry there was an issue uploading the image. '.$this->upload->display_errors());
            }
            else
            {
                $file           = $this->upload->data();
                $new_image_path   = str_replace(ROOT, '', $file['full_path']);

                $this->load->library('thumb');
                $this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }

        if (isset($data['category'])) 
        {
            $categories = $data['category'];
            unset($data['category']);
        }
        unset($data['new_category_parent']);

        $new_image_title = $data['new_image_title'];
        $new_image_link = $data['new_image_link'];
        
        unset($data['new_image_title']);
        unset($data['new_image_link']);

        $data['date_added'] = ($data['date_added'])? formate_date(FALSE, strtotime(str_replace('/', '-', $data['date_added']))) : formate_date();
        $data['safe_name'] = url_title($data['title'], '-', TRUE);

        $result = $this->db->insert('blog_posts', $data);
        $post_id = $this->db->insert_id();

        if ($categories) 
        {
            foreach ($categories as $key => $category_id) {
                $this->db->insert('blog_posts_to_categories', array('post_id' => $post_id, 'category_id' => $category_id));
            }
        }

        if (isset($new_image_path))
        {
            $this->db->insert('blog_images', array('post_id' => $post_id, 'title' => $new_image_title, 'link' => $new_image_link, 'image' => $new_image_path) );
        }

        return $post_id;
    }

    
    function remove_row($page_id) 
    {
        $this->db->where('id', $page_id);
        $result = $this->db->delete('blog_posts');

        $this->db->where('post_id', $page_id);
        $result = $this->db->delete('blog_posts_to_categories');

        //now remove images
        if ($images = $this->images($page_id)) 
        {
            foreach ($images as $image) 
            {
                $this->remove_image($image->id);
            }
        }
        return $result;
    }

    function build_list($parent_id = 0) {
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('blog_posts');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function build_category_list($parent_id = 0) {
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('blog_categories');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function categories()
    {
        $this->db->order_by('display_order');
        $query = $this->db->get('blog_categories');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function update_categories($data)
    {
        if (is_array($data['category'])) 
        {
            foreach ($data['category'] as $category_id => $parent_id) 
            {
                $this->db->where('id', $category_id);
                $this->db->update('blog_categories', array('parent_id' => $parent_id));
            }
        }
    }

    function new_category($category, $post_id)
    {
        $categories = explode(',', $category);
        if ($categories) 
        {
            foreach ($categories as $key => $category) 
            {
                $permalink = url_title($category, '-', TRUE);
                $query = $this->db->where('permalink', $permalink)->get('blog_categories', 1);

                if ($query->num_rows() == 1) 
                {   
                    $this->db->where(array('post_id' => $post_id, 'category_id' => $query->row()->id))->delete('blog_posts_to_categories');
                    
                    $this->db->insert('blog_posts_to_categories', array('post_id' => $post_id, 'category_id' => $query->row()->id));
                }
                else
                {
                    $this->db->insert('blog_categories', array('name' => $category, 'permalink' => $permalink, 'published' => 1));
                    $category_id = $this->db->insert_id();
                    $this->db->insert('blog_posts_to_categories', array('post_id' => $post_id, 'category_id' => $category_id));
                }
            }
            return TRUE;
        }
    }

    function remove_category($category_id = FALSE)
    {
        //get the category
        $this->db->where('id', $category_id);
        $query = $this->db->get('blog_categories', 1);

        if ($query->num_rows() == 1)
        {
            $category = $query->row();
        }
        else
        {
            return FALSE;
        }

        // update category children
        $this->db->where('parent_id', $category_id);
        $this->db->update('blog_categories', array('parent_id' => $category->parent_id));

        // remove the category from any posts
        $this->db->where('category_id', $category_id);
        $this->db->delete('blog_posts_to_categories');

        // remove the category its self
        $this->db->where('id', $category_id);
        $this->db->delete('blog_categories');
    }

    public function check_and_add_new_category($newCategory = FALSE, $parent_id = 0)
    {


        if($newCategory === false || ($newCategory = trim($newCategory)) === '')
            return false;

        $parent_id = ($parent_id) ? $parent_id : 0;

        $this->db->from('blog_categories')
                 ->where(array('name' => $newCategory, 'parent_id' => (int)$parent_id))
                 ->limit(1);
        $query = $this->db->get();                 
        if($query->num_rows() === 1)
            return false;

        $safe_name = url_title($newCategory, '-', true);
        $permalink = $safe_name.'/';

        if($parent_id)
        {
            $query = $this->db->query("SELECT permalink FROM blog_categories WHERE id = ?", array($parent_id));
            if($query->num_rows() == 1)
            {
                $row = $query->row();
                $permalink = $row->permalink.$safe_name.'/';
            }
        }

        $data['name']       = $newCategory;
        $data['safe_name']  = $safe_name;
        $data['published']  = 1;
        $data['parent_id']  = $parent_id;
        $data['permalink']  = $permalink;

        $result = $this->db->insert('blog_categories', $data);

        if($result)
        {
            $newCategoryId = $this->db->insert_id();
            $this->db->where(array('id' => $newCategoryId));
            $query = $this->db->get('blog_categories', 1);                        
            return $query->row();
        }
        return false;
    }

    function post_categories($post_id = FALSE)
    {
        if(!$post_id) return FALSE;
        $this->db->select('GROUP_CONCAT(category_id) as selected', FALSE);
        $this->db->where('post_id', $post_id);
        $query = $this->db->get('blog_posts_to_categories');
        return ($query->num_rows() > 0)? explode(',', $query->row()->selected) : FALSE;
    }



    function publish($page_id = FALSE, $data = FALSE)
    {
        if(!$page_id || !$data || !is_array($data)) return FALSE;

        $this->db->where('id', $page_id);
        $result = $this->db->update('blog_posts', $data);
        return $result;
    }


/*
    function categories()
    {
        $this->db->order_by('name');
        $query = $this->db->get('blog_categories');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function new_category($category, $post_id)
    {
        $categories = explode(',', $category);
        if ($categories) 
        {
            foreach ($categories as $key => $category) 
            {
                $permalink = url_title($category, '-', TRUE);
                $query = $this->db->where('permalink', $permalink)->get('blog_categories', 1);

                if ($query->num_rows() == 1) 
                {   
                    $this->db->where(array('post_id' => $post_id, 'category_id' => $query->row()->id))->delete('blog_posts_to_categories');
                    
                    $this->db->insert('blog_posts_to_categories', array('post_id' => $post_id, 'category_id' => $query->row()->id));
                }
                else
                {
                    $this->db->insert('blog_categories', array('name' => $category, 'permalink' => $permalink, 'published' => 1));
                    $category_id = $this->db->insert_id();
                    $this->db->insert('blog_posts_to_categories', array('post_id' => $post_id, 'category_id' => $category_id));
                }
            }
            return TRUE;
        }
    }

    function post_categories($post_id = FALSE)
    {
        if(!$post_id) return FALSE;
        $this->db->select('GROUP_CONCAT(category_id) as selected', FALSE);
        $this->db->where('post_id', $post_id);
        $query = $this->db->get('blog_posts_to_categories');
        return ($query->num_rows() > 0)? explode(',', $query->row()->selected) : FALSE;
    }
    */
}