<?php 
class Page extends MY_Model 
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all($exclude_id = FALSE)
    {
        if($exclude_id) $this->db->where('id <>', $exclude_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('pages');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function get_page($page_id = FALSE)
    {
        $this->db->where('id', $page_id);
        $query = $this->db->get('pages', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    public function valid_premalink($page_id = FALSE, $uri = FALSE)
    {
        if($page_id)
        $this->db->where('id <>', $page_id);
    
        $this->db->where('permalink', $uri);
        $query = $this->db->get('pages', 1);

        return ($query->num_rows() == 1)? FALSE : TRUE;
    }

    public function re_order($order)
    {
        if ($order && is_array($order)) {
            foreach ($order as $key => $id) {
                $this->db->where('id', $id);
                $this->db->update('pages', array('display_order' => $key));
            }
        }
    }

    public function make_redirect($from = FALSE, $to = FALSE, $header = '301')
    {
        if(!$from || !$to) return FALSE;

        $this->db->query("
            INSERT INTO redirects (`id`,`from`,`to`, `header`)
            VALUES (NULL, '".$from."', '".$to."', '".$header."')
            ON DUPLICATE KEY
                UPDATE `to` = '".$to."', `header` = '".$header."';
        ");
    }

    public function update_row($page_id = FALSE, $data = FALSE)
    {
        if(!$page_id || !$data || !is_array($data)) return FALSE;

        if(isset($data['id'])) unset($data['id']);
        
        $this->db->where('id', $page_id);
        $result = $this->db->update('pages', $data);
        // view($this->db->last_query());
        return $result;
    }

    function new_page_gallery($page_data)
    {
        //1st create a Gallery
        $this->db->insert('gallery', array('title' => $page_data['title'].' Gallery'));
        $gallery_id = $this->db->insert_id();

        if(!$gallery_id) return FALSE;

        $path = 'assets/media/'.$page_data['permalink'];
        make_folder(ROOT.$path);
        $this->load->library('upload');
        $this->load->library('thumb');
        $data = array();

        if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['image'] = str_replace(ROOT, '', $file['full_path']);             
                $this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }
        if(isset($_FILES['image_hover']) && $_FILES['image_hover']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image_hover'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['image_hover'] = str_replace(ROOT, '', $file['full_path']);
                $this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }
        if(isset($_FILES['webm']) && $_FILES['webm']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'webm';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('webm'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['webm'] = str_replace(ROOT, '', $file['full_path']);
            }
        }
        if(isset($_FILES['mp4']) && $_FILES['mp4']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'mp4';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('mp4'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['mp4'] = str_replace(ROOT, '', $file['full_path']);
            }
        }

        if (count($data) > 0) 
        {
            $data['gallery_id'] = $gallery_id;
            // $data['width'] = $_POST['new_image_width'];
            // $data['height'] = $_POST['new_image_height'];
            $data['title'] = $_POST['new_image_title'];
            $data['description'] = $_POST['new_image_description'];
            $this->db->insert('gallery_images', $data);
        }

        unset($page_data['new_image_width']);
        unset($page_data['new_image_height']);
        unset($page_data['new_image_title']);
        unset($page_data['new_image_description']);
        unset($page_data['new_image_link']);
        $page_data['gallery_id'] = $gallery_id;

        return $page_data;
    }

    public function new_row($data = FALSE)
    {
        //1st create a Gallery
        if(!isset($data['gallery_id']) || !$data['gallery_id'])
        {
            $data = $this->new_page_gallery($data);
        }
        else
        {
            unset($data['new_image_width']);
            unset($data['new_image_height']);
            unset($data['new_image_title']);
            unset($data['new_image_description']);
            unset($data['new_image_link']);
        }

        /// calcualte the next display order for the newly added page ///
        $parentId = (trim($data['parent_id']) === '0' || $data['parent_id'] === 0 || trim($data['parent_id']) === '') ? 0 : trim($data['parent_id']);
        $criteria = array('parent_id' => $parentId);

        $query = $this->db->select_max('display_order', 'max_display_order')->where($criteria)->get('pages');
        $maxDisplayOrder = ($query->num_rows() > 0) ? $query->row()->max_display_order : 0;    
        $data['display_order'] = ($maxDisplayOrder + 1);
        ////////////////////////////////////////////////////////////////

        $result = $this->db->insert('pages', $data);
        return $this->db->insert_id();
    }

    public function remove_row($page_id, &$pageIdsDeleted = array()) 
    {
        $this->load->model('admin/gallery');
        $pageIdsToDelete = array();
        
        $this->db->where('parent_id', $page_id);
        $query = $this->db->get('pages');

        if($query->num_rows() > 0)
        {
            $rows = array($page_id);
            foreach($query->result() as $row)
            {
                if($row->gallery_id)
                    $this->gallery->remove_row($row->gallery_id);
                
                $rows[] = $row->id;
                $rows = array_merge($rows, $this->_step_get_rows($row->id, $rows));
            }
            $rows = array_unique($rows);
            $pageIdsToDelete = $rows;
            $this->db->where_in('id', $rows);
        }
        else
        {
            $pageIdsToDelete = array($page_id);
            
            //delete the gallery
            $this->db->where('id', $page_id);
            $query = $this->db->get('pages', 1);
            if ($query->num_rows() > 0) 
            {
                $row = $query->row();
                if($row->gallery_id)
                    $this->gallery->remove_row($row->gallery_id);
            }
            $this->db->where('id', $page_id);
        }
        $result = $this->db->delete('pages');
        
        if(is_array($pageIdsToDelete) && count($pageIdsToDelete) > 0)
            $pageIdsDeleted = $pageIdsToDelete;
        
        return $result;
    }
    
    private function _step_get_rows($parent_id, $data)
    {
        $this->load->model('admin/gallery');

        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('pages');
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                if($row->gallery_id)
                {
                    $this->gallery->remove_row($row->gallery_id);
                }

                $data[] = $row->id;
                $data = array_merge($data, $this->_step_get_rows($row->id, $data));
            }
        }
        return $data;
    }

    // public function getAllRowsForPages($pageIds)
    // {
    //     if(!is_array($pageIds))
    //         $pageIds = array($pageIds);
        
    //     $this->db->select('id, page_id')
    //              ->from('page_rows')
    //              ->where_in('page_id', $pageIds);
    //     $query = $this->db->get();
    //     return ($query->num_rows() > 0) ? $query->result() : false; 
    // }
    
    function build_list($parent_id = 0) {
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('pages');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function footer()
    {
        $query = $this->db->get('footer');
        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    function footer_links($column = FALSE)
    {
        $this->db->where('column', $column);
        $this->db->order_by('display_order');
        $query = $this->db->get('footer_links');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function update_footer($data = FALSE)
    {
        if($data['title'] && count($data['title']) > 0)
        {
            foreach ($data['title'] as $id => $title) 
            {   
                if (isset($data['parent'][$id]))
                {
                    $parent = 1;
                }
                else
                {
                    $parent = 0;
                }
                $this->db->where('id', $id);
                $this->db->update('footer_links', array('title' => $title, 'permalink' => $data['link'][$id], 'parent' => $parent));
            }
        }
    }

    function reorder_footer($data = FALSE)
    {
        if($data['sortable'] && count($data['sortable']) > 0)
        {
            foreach ($data['sortable'] as $order => $id) 
            {
                $this->db->where('id', $id);
                $this->db->update('footer_links', array('display_order' => $order));
            }
        }
    }

    function new_footer($column)
    {
        $order = 0;
        $query = $this->db->query("SELECT MAX(display_order) AS current FROM footer_links WHERE `column` = ? LIMIT 1", array($column));
        if($query->num_rows() == 1)
        {
            $order = $query->row()->current + 1;
        }
        $this->db->insert('footer_links', array('column' => $column, 'display_order' => $order));
        return $this->db->insert_id();
    }

    function remove_footer($footer_id = FALSE)
    {
        if (!($footer_id)) return FALSE;

        $this->db->where('id', $footer_id);
        $this->db->delete('footer_links');

        return TRUE;
    }

    // private function _getLastSequenceForPageRow($pageId)
    // {
    //     $pageId = trim($pageId);
    //     $query = $this->db->select('sequence')
    //                        ->from('page_rows')
    //                        ->where('page_id', $pageId)
    //                        ->order_by('sequence', 'DESC')
    //                        ->limit(1)
    //                        ->get();
    //     return ($query->num_rows() > 0) ? $query->row()->sequence : '0';
    // }
    
    /**
     * This function adds a new row for a page. It can add page_row with a number of columns 
     * 
     * @param unknown $pageId
     * @param unknown $noOfCols
     * @throws Exception
     * @return boolean
     */
    // public function addRowWithColumns($pageId, $noOfCols)
    // {
    //     $pageId = trim($pageId);
    //     if($this->get_page($pageId) === false)
    //         throw new Exception('Invalid Page Id provided. Page does not exist');
    //     if(!is_numeric($noOfCols))
    //         throw new Exception('No of columns must be provided to add columns for Page Row');
        
    //     $this->db->trans_start();   
        
    //     $newSeqForPageRow = $this->_getLastSequenceForPageRow($pageId) + 1;
    //     $newRowData = array('page_id' => $pageId, 'sequence' => $newSeqForPageRow);
    //     $newRowId = insertInDbFromArray('page_rows', $newRowData);

    //     $columnArray = array();
    //     $noOfCols = (($noOfCols > 3) ? 3 : $noOfCols);
    //     for($i = 1; $i <= $noOfCols; $i++)
    //     {
    //         $newColumnData = array('page_row_id' => $newRowId, "sequence" => $i);           
    //         $newColId = insertInDbFromArray('page_row_columns', $newColumnData, false);
            
    //         $newModuleData = array('module_type_id' => DEFAULT_MODULE_TYPE_ID, 'module_body' => '', 'page_row_column_id' => $newColId);
    //         $newModuleId = insertInDbFromArray('modules', $newModuleData);
    //     }   
        
    //     $this->db->trans_complete();
        
    //     if ($this->db->trans_status() === false)
    //         throw new Exception($this->db->_error_message());

    //     return true;
    // }

    /**
     * This function will remove all the Rows, Columns and MOdules for a Page 
     * 
     * @param int $pageId
     * @return boolean
     */
    // public function removeAllRowsForPage($pageId)
    // {
    //     $pageId = trim($pageId);
    //     if($this->get_page($pageId) === false)
    //         throw new Exception('Invalid Page Id provided. Page does not exist');
        
    //     is_admin(true);
        
    //     $this->load->model('admin/module');
    //     $info = $this->module->getModulesForPagePageRow($pageId);
    //     if($info !== false)
    //     {
    //         $this->db->trans_start();
            
    //         foreach($info as $key => $value)
    //         {
    //             foreach($value['columns'] as $columnId => $data)
    //             {
    //                 if(trim($data['module']['id']) !== '')
    //                     $this->db->delete('modules', array('id' => $data['module']['id']));
    //                 $this->db->delete('page_row_columns', array('id' => $columnId));
    //             }
    //             $this->db->delete('page_rows', array('id' => $key));
    //         }
            
    //         $this->db->trans_complete();
            
    //         if ($this->db->trans_status() === false)
    //             throw new Exception($this->db->_error_message());
    //     }
    //     return true;
    // }
    
    // public function resequenceRowsForPage($pageId, $lastSequence)
    // {
    //     $this->db->where(array('sequence >' => $lastSequence, 'page_id' => $pageId));
    //     $this->db->set('sequence', 'sequence - 1', false);
    //     $this->db->update('page_rows');
    //     return true;
    // }
    
    // public function removeRowForPage($pageRowId, $pageId, $resequence = true)
    // {
    //     if($this->get_page($pageId) === false)
    //         throw new Exception('Invalid Page Id provided. Page does not exist');
        
    //     $this->load->model('admin/module');
    //     $info = $this->module->getModulesForPagePageRow($pageId, $pageRowId);
    //     if($info !== false)
    //     {
    //         $this->db->trans_start();
    //         $sequence = '';
    //         foreach($info as $key => $value)
    //         {
    //             $sequence = $value['sequence'];
    //             foreach($value['columns'] as $columnId => $data)
    //             {
    //                 if(trim($data['module']['id']) !== '')
    //                     $this->db->delete('modules', array('id' => $data['module']['id']));
    //                 $this->db->delete('page_row_columns', array('id' => $columnId));
    //             }
    //             $this->db->delete('page_rows', array('id' => $key));
    //         }
            
    //         if($resequence === true)
    //             $this->_resequenceRowsForPage($pageId, $sequence);
            
    //         $this->db->trans_complete();
            
    //         if ($this->db->trans_status() === false)
    //             throw new Exception($this->db->_error_message());
    //     }
        
    //     return true;
    // }

    // public function updateSequenceForPageRow($pageRowId, $sequence)
    // {
    //     if(!$pageRowId)
    //         return false;

    //     $this->db->set('sequence', $sequence)
    //              ->where(array('id' => $pageRowId))
    //              ->update('page_rows');
    //     return true;         
    // }

}