<?php 
class Section extends MY_Model 
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function create_section($page_id = FALSE, $section_type = FALSE)
	{
		if (!$page_id || !$section_type) return FALSE;

		$section_types = config_item('section_type');
		$section = $section_types[$section_type];

		$initial_fields = array();
		if (isset($section['main']) && is_array($section['main']))
		{
			foreach ($section['main'] as $key => $value)
			{
				$initial_fields[$key] = '';
			}
		}

		$data = array(
			'page_id'	=> $page_id,
			'type' 		=> $section_type,
			'content' 	=> json_encode($initial_fields),
		);

		$this->db->insert('sections', $data);

		return $this->db->insert_id();

	}

	function delete_section($page_id = FALSE, $section_id = FALSE)
	{
		if (!$page_id || !$section_id) return FALSE;

		$section = $this->get_section($section_id);

		if ($section)
		{
			$all_sections = config_item('section_type');

			if (isset($all_sections[$section->type]))
			{
				$structure = $all_sections[$section->type];

				$path = config_item('section_path').encode($section->id).'/';
				if (isset($structure['file_path']) && $structure['file_path'])
				{
					$path = $section['file_path'].encode($section->id).'/';
				}

				if (file_exists($path)) 
				{
					array_map('unlink', glob($path.'*.*'));
					rmdir($path);
				}
			}
		}

		$this->db->where('id', $section_id);
		$this->db->where('page_id', $page_id);
		
		if ($this->db->delete('sections'))
		{
			$this->db->where('section_id', $section_id);
			$this->db->delete('section_items');
			return TRUE;
		}
		return FALSE;
	}

	function save_section($section_id = FALSE, $post_data = FALSE)
	{
		if (!$section_id || !$post_data) return FALSE;

		$data = array('content' => json_encode($post_data));

		$this->db->where('id', $section_id);
		$this->db->update('sections', $data);

		return $this->db->affected_rows();
	}

	function publish_status($page_id = FALSE, $section_id = FALSE, $published = FALSE)
	{
		if (!$page_id || !$section_id || !is_int($published)) return FALSE;

		$this->db->where('id', $section_id);
		$this->db->where('page_id', $page_id);
		$this->db->update('sections', array('published' => $published));

		return $this->db->affected_rows();
	}

	function get_sections($page_id = FALSE)
	{
		if (!$page_id) return FALSE;

		$this->db->where('page_id', $page_id);
		$this->db->order_by('display_order', 'asc');
		$query = $this->db->get('sections');

		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}

	function get_section($section_id = FALSE)
	{
		if (!$section_id) return FALSE;

		$this->db->where('id', $section_id);
		$query = $this->db->get('sections', 1);

		return ($query->num_rows() == 1) ? $query->row() : FALSE;
	}

	function order_section($sortArray)
    {
        if(is_array($sortArray) && count($sortArray) > 0)
        {
            foreach($sortArray as $key => $value)
            {
                $this->db->update('sections', array('display_order' => $key), array('id' => $value));
            }
        }
        return true;
    }

	function get_section_items($section_id = FALSE)
	{
		if (!$section_id) return FALSE;

		$this->db->where('section_id', $section_id);
		$this->db->order_by('display_order', 'asc');
		$query = $this->db->get('section_items');

		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}

	function create_section_item($section_id = FALSE, $section_type = FALSE)
	{
		if (!$section_id || !$section_type) return FALSE;

		$section_types = config_item('section_type');
		$section = $section_types[$section_type];

		$initial_fields = array();
		if (isset($section['items']) && is_array($section['items']))
		{
			foreach ($section['items'] as $key => $value)
			{
				$initial_fields[$key] = '';
			}
		}

		$data = array(
			'section_id'	=> $section_id,
			'content' 		=> json_encode($initial_fields),
		);

		$this->db->insert('section_items', $data);

		return $this->db->insert_id();
	}

	function delete_section_item($section_id = FALSE, $item_id = FALSE)
	{
		if (!$section_id || !$item_id) return FALSE;

		$this->db->where('id', $item_id);
		$this->db->where('section_id', $section_id);
		$this->db->delete('section_items');

		return $this->db->affected_rows();
	}

	function save_section_items($section_id = FALSE, $item_id = FALSE, $post_data = FALSE)
	{
		if (!$section_id || !$item_id || !$post_data) return FALSE;

		$data = array('content' => json_encode($post_data));

		$this->db->where('id', $item_id);
		$this->db->where('section_id', $section_id);
		$this->db->update('section_items', $data);

		return $this->db->affected_rows();
	}

	function order_section_item($sortArray)
    {
        if(is_array($sortArray) && count($sortArray) > 0)
        {
            foreach($sortArray as $key => $value)
            {
                $this->db->update('section_items', array('display_order' => $key), array('id' => $value));
            }
        }
        return true;
    }

}
