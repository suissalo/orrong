<?php 
class Gallery extends CI_Model 
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    /**
     * get the gallery info from gallery id
     * 
     * @param string/int $galleryId
     * 
     * @return gallery active record object / FALSE if not found
     */
    public function get_gallery_by_id($galleryId = '')
    {
        if(!$galleryId) return false;
        
        $this->db->where(array('id' => $galleryId));
        $query = $this->db->get('gallery', 1);

        return ($query->num_rows() == 1)? $query->row() : false;
    }
    
    public function get_all_images_for_gallery($galleryId = '')
    {
        if(!$galleryId) return false;
                
        $this->db->order_by('display_order');        
        $this->db->where(array('gallery_id' => $galleryId));
        $query = $this->db->get('gallery_images');
        
        return ($query->num_rows() > 0)? $query->result() : false;
    }

    public function get_all()
    {
        $this->db->order_by('display_order');
        $query = $this->db->get('gallery');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function get_gallery($id = FALSE)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('gallery', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    public function get_images($gallery_id = FALSE)
    {
        $this->db->where('gallery_id', $gallery_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('gallery_images');

        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function valid_premalink($page_id = FALSE, $uri = FALSE)
    {
        if($page_id)
        $this->db->where('id <>', $page_id);
    
        $this->db->where('permalink', $uri);
        $query = $this->db->get('gallery', 1);

        return ($query->num_rows() == 1)? FALSE : TRUE;
    }

    public function re_order($order)
    {
        if ($order && is_array($order)) {
            foreach ($order as $key => $id) {
                $this->db->where('id', $id);
                $this->db->update('gallery', array('display_order' => $key));
            }
        }
    }

    public function re_order_images($order)
    {
        if ($order && is_array($order)) {
            foreach ($order as $key => $id) {
                $this->db->where('id', $id);
                $this->db->update('gallery_images', array('display_order' => $key));
            }
        }
    }

    public function update_row($page_id = FALSE, $data = FALSE)
    {
        if(!$page_id || !$data || !is_array($data)) return FALSE;

        $this->db->where('id', $page_id);
        $result = $this->db->update('gallery', $data);

        return $result;
    }

    public function new_row($data = FALSE)
    {
        $result = $this->db->insert('gallery', $data);
        return $this->db->insert_id();
    }

    public function update_image_row($page_id = FALSE, $data = FALSE)
    {
        if(!$page_id || !$data || !is_array($data)) return FALSE;

        $this->db->where('id', $page_id);
        $result = $this->db->update('gallery_images', $data);
        return $result;
    }
    
    public function save_gallery_image($data = FALSE, $return_obj = FALSE)
    {
        $result = $this->db->insert('gallery_images', $data);
        $insert_id = $this->db->insert_id();
        if(!$return_obj) return $insert_id;

        $query = $this->db->query('SELECT * FROM gallery_images WHERE gallery_images.id = ? LIMIT 1 ', array($insert_id));
        return $query->row();
    }
    
    public function make_image($file_data = FALSE) {
        //height, width
        $sizes = config_item('gallery_sizes');
        $this->load->library('thumb');
        
        foreach ($sizes as $name => $values) 
        {
            $new_path = $file_data['file_path'].$file_data['raw_name'].'_'.$name.$file_data['file_ext'];
            $this->thumb->make_thumb($file_data['full_path'], $new_path, $values[0], $values[1]);
        }
    }

    

    public function remove_image($image_id) 
    {
        $this->db->where('id', $image_id);
        $query = $this->db->get('gallery_images', 1);
        if($query->num_rows() == 1) 
        {
            $this->load->library('thumb');

            $row        = $query->row();
            $image      = $row->image;
            // $main       = $row->main;
            // $thumb      = $row->thumb;
            $gallery_id = $row->gallery_id;

            $this->db->where('image', $image);
            $this->db->where('id <>', $image_id);
            $query2 = $this->db->get('gallery_images', 1);
            
            if($query2->num_rows() == 0) 
            {
                if($row->image)
                    $this->thumb->remove_thumbs($row->image, 'hero_sizes');
                if($row->image_hover)
                    $this->thumb->remove_thumbs($row->image_hover, 'hero_sizes');
                if($row->webm)
                    @unlink(ROOT.$row->webm);
                if($row->mp4)
                    @unlink(ROOT.$row->mp4);
            }

            $this->db->where('id', $image_id);
            $result = $this->db->delete('gallery_images');
            return $gallery_id;
        }

    }
    
    public function remove_row($gallery_id = FALSE) 
    {
        if($gallery_id === false || trim($gallery_id) === '')
            return false;
        
        $galleryFolerArray = array();    

        //delete the images
        $this->db->where('gallery_id', $gallery_id);
        $query = $this->db->get('gallery_images');
        if($query->num_rows() > 0) 
        {
            $this->load->library('thumb');

            foreach ($query->result() as $row) 
            {
                $imageArray = explode("/", $row->image);
                array_pop($imageArray);
                $galleryFolder = implode("/", $imageArray);
                $galleryFolerArray[$galleryFolder] = 1;

                if(is_file(ROOT.$row->image)) 
                {
                    //only delete the actual image if its not in use with any other record
                    $this->db->where('image', $row->image);
                    $this->db->where('id <>', $row->id);
                    $query2 = $this->db->get('gallery_images', 1);
                    
                    if($query2->num_rows() == 0) 
                    {
                        $this->remove_image($row->id);
                    }
                }
            }
        }

        //delete the actual gallery
        try
        {
            if(count($galleryFolerArray) === 0)
                $galleryFolerArray[config_item('gallery_path').$gallery_id] = 1;

            $this->db->where('id', $gallery_id);
            $result = $this->db->delete('gallery');
            foreach(array_keys($galleryFolerArray) as $folder)
            {
                //var_dump(ROOT.$folder);
                //var_dump(is_dir(ROOT.$folder));
                if(is_dir(ROOT.$folder))
                {
                    @rmdir(ROOT.$folder);
                }                
            }    
        }
        catch(Exception $ex)
        {
            //die($ex->getMessage());
        }

        return $result;
    }
}