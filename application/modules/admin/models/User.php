<?php 
class User extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function login($username, $password)
    {
        $password = encrypt_password($password);

        $this->db->where('username', $username);
        $this->db->where('pass', $password);
        $query = $this->db->get('admin_users');

        if ($query->num_rows() != 1) return FALSE;

        $admin_user = $query->row();

        $this->session->set_userdata('admin_user_id', md5($admin_user->id));
        $this->session->set_userdata('admin_group_id', $admin_user->group_id);

        /// as the user is already logged in, clear out the hash and has date if any ///
        $this->_reset_hash($admin_user->id);

        return $admin_user;
    }

    private function _reset_hash($userId)
    {
        $result = $this->db->update('admin_users', array('hash' => '', 'hash_date' => ''), array('id' => $userId, 'hash != ' => ''));
        return true;
    }

    function get_user($user_id = FALSE)
    {
        if(!$user_id) {
            $user_id = $this->session->userdata('admin');
        }
        if(!$user_id) {
            $user_id = $this->session->userdata('user');
        }
        $this->db->where('id', $user_id);
        $query = $this->db->get('admin_users');

        if ($query->num_rows() != 1) return FALSE;
        return $query->row();
    }
	
    /**
     * Get all users
     * @param bool $sales = if true then get all users with a salesforce id and not master
     */
	function get_users($sales = FALSE)
    {
        $this->db->select('admin_users.*, admin_groups.name AS group_name', TRUE);
        $this->db->order_by('admin_users.group_id');
        $this->db->join('admin_groups', 'admin_users.group_id = admin_groups.id');
        if($sales) $this->db->where('admin_users.sales_force_id <>', '');  
        $query = $this->db->get('admin_users');
        //view($this->db->last_query());
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    /*
    function get_salers()
    {
        //$this->db->select('admin_users.*, admin_groups.name AS group_name', TRUE);
        //$this->db->order_by('admin_users.group_id');
        //$this->db->join('admin_groups', 'admin_users.group_id = admin_groups.id');
        $this->db->where('group_id <>', 1);
        $this->db->where('owner_id', 0);
        $query = $this->db->get('admin_users');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }
    */

    function reorder($data = FALSE)
    {
        if (!$data)return FALSE;

        foreach ($data as $key => $value) 
        {
            $this->db->where('id', $value);
            $this->db->update('admin_users', $key);
        }
    }

    function update_row($user_id = FALSE, $data = FALSE)
    {
        if(!$user_id || !$data || !is_array($data)) return FALSE;

        if(isset($data['pass']) && trim($data['pass']) != '')
        {
            $data['pass'] = encrypt_password($data['pass']);
        }
        else
        {
            unset($data['pass']);
        }
		
        $this->db->where('id', $user_id);
        $result = $this->db->update('admin_users', $data);
        return $result;
    }
	
	function new_row($data = FALSE)
    {
		if($data['pass']) $data['pass'] = encrypt_password($data['pass']);
        $result = $this->db->insert('admin_users', $data);
        return $this->db->insert_id();
    }
	
	function remove_row($user_id = FALSE) {
		if($user_id) {
			$this->db->where('id', $user_id);
			$result = $this->db->delete('admin_users');
			return $result;
		}
		return FALSE;
	}
	
	function unique_username($username = FALSE, $user_id = FALSE)
    {
		if($username == FALSE) return FALSE;
		if($user_id) $this->db->where('id <>', $user_id);
		$this->db->where('username', $username);
        $result = $this->db->get('admin_users', 1);
        return ($result->num_rows() == 1)? FALSE : TRUE;
    }
    
    /**
     * Get all groups
     */
    function get_groups()
    {
        $query = $this->db->get('admin_groups');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function group($group_id = FALSE)
    {
        if(!$group_id) return FALSE;

        $this->db->where('id', $group_id);
        $query = $this->db->get('admin_groups', 1);
        return ($query->num_rows() > 0)? $query->row() : FALSE;
    }

    function update_group($data, $group_id)
    {
        if(!$group_id) return FALSE;

        $this->db->where('id', $group_id);
        $result = $this->db->update('admin_groups', $data);
        return $result;
    }
    
    /**
     * 
     * @param type $parent_id
     * @return bool | active record
     */
    function get_group_children($parent_id = FALSE)
    {
        if(!$parent_id) return FALSE;
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('admin_groups');
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }
    
    /**
     * get a group permissions
     * 
     * @param type $group_id
     * @return bool | active record
     */
    
    function group_permission($group_id = FALSE)
    {
        if(!$group_id) return FALSE;
        
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('admin_groups', 1);
        
        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }
    
    function get_group_permissions() 
    {
      $permissions = array(
            'blog'      => 'Blog',
            'pages'     => 'Pages',
            'users'     => 'Users',
            'galleries' => 'Galleries'
        ); 
      return $permissions;
    }
    
    function new_group($data) 
    {
        
        $this->db->insert('admin_groups', $data);
        $group_id = $this->db->insert_id();
        
        return $group_id;
    }
    
    function remove_group($group_id = FALSE)
    {
        if(!$group_id) return FALSE;
        
        $this->db->where('id', $group_id);
        $query = $this->db->get_where('admin_groups');
        //die($this->db->last_query());
        if($query->num_rows() < 1 ) return FALSE;
        
        
        $this->db->where('group_id', $group_id);
        $this->db->update('admin_users', array('group_id' => 0));
        
        $this->db->where('id', $group_id);
        $this->db->delete('admin_groups');
        return TRUE;
    }

    public function check_and_generate_hash_for_password_reset($email)
    {
        if($email === false || ($email = trim($email)) === '')
            return false;

        $this->db->from('admin_users')
                 ->where(array('contact_email' => $email)) 
                 ->limit(1)
                 ->order_by('date_added', 'asc');   
        $query = $this->db->get();         

        if($query->num_rows() < 1)
            return false;
                    
        $userDetails = $query->row();
        $hash = md5(time().$userDetails->contact_email);
        $hash_date = new DateTime(date('Y-m-d H:i:s'));
        $hash_date->modify('+1 day');
        $hash_date = $hash_date->format('Y-m-d H:i:s');

        $result = $this->db->update('admin_users', array('hash' => $hash, 'hash_date' => $hash_date), array('id' => $userDetails->id));
        $query = $this->db->where(array('id' => $userDetails->id))->get('admin_users');
        return $query->row();
    }

    public function validate_hash($userId, $hash)
    {
        $output = array();
        $this->db->from('admin_users')
                 ->where(array('id' => $userId, 'hash' => $hash))
                 ->limit(1);
        $query = $this->db->get();         
        if($query->num_rows() === 0)
        {
            $output['success'] = false;
            $output['message'] = "Password Change Request is not valid";
            return $output;
        }

        $userDetails = $query->row();
        $expirationDate = $userDetails->hash_date;

        if(strtotime(date('Y-m-d H:i:s')) >= strtotime($expirationDate))
        {
            $output['success'] = false;
            $output['message'] = "Password Change Request expired";        
        }
        else
        {
            $output['success'] = true;
            $output['message'] = 'all good';
            $output['user'] = $userDetails;
        }

        return $output;   
    }

    public function change_password($userId, $newPassword)
    {
        if($newPassword === false || trim($newPassword) === '')
            return false;
        if($userId === false || trim($userId) === '')
            return false;

        $newPassword = encrypt_password($newPassword);
        $result = $this->db->update('admin_users', array('pass' => $newPassword, 'hash' => '', 'hash_date' => ''), array('id' => $userId));
        return $result;
    }
}