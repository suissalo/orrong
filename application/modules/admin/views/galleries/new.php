<?php 
	$fromModule = false;
	if(isset($info))
	{
		$fromModule = true;
		$moduleId = trim($info['moduleId']);
		$moduleTypeName = trim($info['moduleTypeName']);
	}

	$assignGalleryToService = false;
	if(isset($service) && isset($assign_gallery_to_service))
	{
		$assignGalleryToService = true;
	}
?>

<?php echo form_open_multipart('admin/galleries/save_new'); ?>

<?php if($fromModule === true): ?>
	<input type = "hidden" name = "fromModuleId" value = "<?php echo $moduleId; ?>" />
	<input type = "hidden" name = "fromModuleTypeName" value = "<?php echo $moduleTypeName; ?>" />
<?php endif; ?>	

<?php if($assignGalleryToService === true): ?>
	<input type = "hidden" name = "assign_to_service" value = "1" />
	<input type = "hidden" name = "service_id" value = "<?php echo $service->id; ?>" />
<?php endif; ?>	

<div class="col-xs-60 margin-bottom-large page-heading">
	<div class="row">
		
		<div class="col-xs-60">
		
			<h4 class="margin-top-small">Create a new Gallery</h4>
			
			<button class="btn bg-green pull-right margin-left-medium">Save</button>
			<button class="btn bg-blue pull-right margin-left-medium">Update</button>
			<button class="btn bg-yellow pull-right margin-left-medium">Cancel</button>
			
		</div>
		
	</div><!-- END ROW -->
</div><!-- END 12 COL -->


<div class="col-xs-60">

	<div class="row margin-bottom-large">
		<div class="col-xs-5">
			<label for="prop-status">Active</label>
		</div>
		
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input id="prop-status" type="checkbox" name="status" <?php echo set_radio('status', '1', FALSE); ?> value="1" data-toggle="switch" /></div>
		</div>
        <div class="col-xs-5">
			<label for="prop-arrows">Arrows</label>
		</div>
		
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input type="checkbox" id="prop-arrows" name="arrows" <?php echo set_radio('arrows', '1', FALSE); ?> value="1" data-toggle="switch" /></div>
		</div>

		<div class="col-xs-5">
			<label for="thumbs">Thumbs</label>
		</div>
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input id="thumbs" type="checkbox" name="thumbs" <?php echo set_radio('thumbs', '1', FALSE); ?> value="1" data-toggle="switch" /></div>
		</div>

		<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"></div>
	</div>

	<div class="row margin-bottom-large">
		<div class="col-xs-5">
		    <label for="prop-title">Title</label>
		</div>
		<div class="col-xs-30">
			<input class="form-control" type="text" name="title" id="prop-title" value="<?php echo set_value('title'); ?>"/>
		</div>
	</div>

    <div class="row margin-bottom-large">
    	<label for="content" class="col-xs-5">Content</label>
    	<div class="full-ck col-xs-55"><?php echo $this->ckeditor->editor("content", set_value('content')); ?></div>
	</div>

	<div class="row margin-bottom-large">
		<div class="col-xs-5">
		    <label for="prop-title">Images</label>
		</div>
		<div class="col-xs-10">
			<button type="submit" class="btn bg-green btn-sm" >Images</button>
		</div>
		<div class="col-xs-45"></div>
	</div>
	
</div><!-- END 12 COL -->
	
	
</form>