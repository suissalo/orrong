
<!--
<div class="col-xs-60 margin-bottom-large page-heading">
	<div class="row">
		
		<div class="col-xs-50">
			<h4 class="margin-top-small">Manage Galleries</h4>
		</div>
		<div class="col-xs-5">
			<a class="btn btn-success" href="<?php echo base_url('admin/galleries/new_gallery'); ?>">New Gallery <i class="glyphicon glyphicon-plus"></i></a>
		</div>		
	</div>
</div>

<?php /* if ($galleries): ?>
    <ul class="manage-list listings" data-rel="manage-galleries" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
    <?php foreach ($galleries as $gallery): ?>
	    <li id="sort_<?php echo $gallery->id; ?>">
		<?php echo $gallery->title; ?>
		<div class="btn-group btn-group-sm">
			<a class="confirm btn btn-danger" title="You are about to delete the gallery: <?php echo $gallery->title; ?>, do you wish to continue?" href="<?php echo base_url('admin/galleries/remove/'.encode($gallery->id)); ?>">
				Delete <i class="glyphicon glyphicon-remove"></i>
			</a>
		    <?php if ($gallery->status == 1): ?>
			    <a href="<?php echo base_url('admin/galleries/publish/'.$gallery->id.'/0'); ?>" class="btn btn-info">Unpublish <i class="glyphicon glyphicon-thumbs-down"></i></a>
		    <?php else: ?>
			    <a href="<?php echo base_url('admin/galleries/publish/'.$gallery->id.'/1'); ?>" class="btn btn-warning">Publish <i class="glyphicon glyphicon-thumbs-up"></i></a>
		    <?php endif ?>
                <a href="<?php echo base_url('admin/galleries/edit/'.encode($gallery->id)); ?>" class="btn btn-success">Edit <i class="glyphicon glyphicon-pencil"></i></a>
		</div>
	    </li>
    <?php endforeach ?>
    </ul>
<?php endif */ ?>
-->







	


<div>

	<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">	

			<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a>

			<a href="<?php echo base_url('admin/galleries/new_gallery'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New Gallery
				</span>
			</a>

		</div>
	</div>

	<section>
		<div class="row">
			<div class="col-xs-60 col-sm-60">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		Galleries
			 	</div>
		 	</div>
		</div>

		<?php if ($galleries) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>

		 		<ul class="manage-list" data-rel="manage-galleries" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
		 		
	 				<?php foreach ($galleries as $gallery) : ?>
	 					<li class="item-parent list-none" id="sort_<?php echo $gallery->id; ?>">
	 						<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs pd-l-xxs">
									<!-- <span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
									</span> -->
			 						<?php echo $gallery->title;?>
		 						</div>
		 						<div class="icon-container text-right stop-prop pull-right">
									<a href="<?php echo base_url('admin/galleries/edit/'.encode($gallery->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php if ($gallery->status == 1): ?>
									<a href="<?php echo base_url('admin/galleries/publish/'.$gallery->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/galleries/publish/'.$gallery->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif ?>

									<a href="<?php echo base_url('admin/galleries/remove/'.encode($gallery->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
		 						</div>
	 						</div>

	 					</li>
	 				<?php endforeach; ?>	
		 		</ul>
		 </div>
		<?php else: ?>
			<p>There are no Galleries.</p>
		<?php endif; ?>
	</section>

</div>