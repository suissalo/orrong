<!-- This part is for editing -->
<?php if ($page_mode === 'edit'): ?>

    <?php echo form_open_multipart(); ?>
    
        <div class="row action-buttons">
            <div class="mg-t-md mg-b-md pull-right">
                <a href="<?php echo base_url('/admin/pages/edit/'.$page_id.'#content'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
                    <span class="icon icon-size-150 outline-light icon-crosscircle">
                        <?php echo config_item('icon_image'); ?>
                    </span>
                    <span class="text">
                        Cancel
                    </span>
                </a> 
                <button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
                    <span class="icon icon-size-150 outline-light icon-disk">
                      <?php echo config_item('icon_image'); ?>
                    </span>
                    <span class="text">
                      Save
                    </span>
                </button>
            </div>
        </div>

        <section>
            
            <?php echo Input_helper::heading('Edit Gallery: '. @$article->title); ?>
            
            <div class="col-xs-60 bg-white pd-all-md">

                <div class="row">
                    <div class="col-xs-60">
                        <label for="prop-title">Title *</label>
                    </div>
                    <div class="col-xs-60">
                        <input type="text" class="form-control" name="title" id="prop-title" value="<?php echo set_value('title', @$article->title) ?>" />
                    </div>
                    <div class="col-xs-60 mg-t-md">
                        <label for = "prop_content">Content</label>
                    </div>
                    <div class = "col-xs-60">
                        <?php echo $this->ckeditor->editor("description", set_value('description', @$article->description)); ?>
                    </div>
                </div>

                <?php echo Input_helper::heading('Slides: '); ?>
                <a href="<?php echo base_url('admin/pages/gallery/add_article/'.encode($article->id).'?redirect='.encode(full_url())); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
                    <span class="icon icon-size-150 outline-light icon-plus">
                        <?php echo config_item('icon_image'); ?>
                    </span>
                    <span class="text">
                        Add Slide
                    </span>
                </a>
                <?php foreach ($article->values as $key => $content): ?>
                    <div class="row">
                        <div class="row mg-b-sm">
                            <div class="pull-right">
                              <a href="<?php echo base_url('admin/pages/gallery/remove_article/'.encode($article->id).'/'.encode($key).'?redirect='.encode(full_url())); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete this Image, is this correct?" >
                                <span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
                                  <?php echo config_item('icon_image'); ?>
                                </span>
                              </a>
                            </div>
                        </div>
                        <div class="col-xs-60 mg-t-md">
                            <div class="col-xs-40 pd-r-sm">
                                <div class="col-xs-60">
                                    <label for="prop-values[<?php echo $key; ?>][title">Title</label>
                                </div>
                                <div class="col-xs-60">
                                    <input type="text" class="form-control" name="values[<?php echo $key; ?>][title]" id="prop-values[<?php echo $key; ?>][title" value="<?php echo set_value('values['.$key.'][title]', @$content->title) ?>" />
                                </div>
                                <div class="col-xs-60 mg-t-md">
                                    <div class="col-xs-60 col-md-30 pd-r-md">
                                        <label for="prop-values[<?php echo $key; ?>][link_url">Link</label><br/>
                                        <input type="text" class="form-control" name="values[<?php echo $key; ?>][link_url]" id="prop-values[<?php echo $key; ?>][link_url" value="<?php echo set_value('values['.$key.'][link_url]', @$content->link_url) ?>" />
                                    </div>
                                    <div class="col-xs-60 col-md-30">
                                        <label for="prop-values[<?php echo $key; ?>][link_text">Link Text</label>
                                        <input type="text" class="form-control" name="values[<?php echo $key; ?>][link_text]" id="prop-values[<?php echo $key; ?>][link_text" value="<?php echo set_value('values['.$key.'][link_text]', @$content->link_text) ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-20">
                                <label class="col-xs-60 mg-t-md">Heading Image</label>
                                <div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">
                                    <?php if (@$content->image): ?>
                                        <!-- <div class="row mg-b-sm">
                                            <div class="pull-right">
                                              <a href="<?php echo base_url('admin/pages/gallery/remove_slide_image/'.encode($article->id).'/'.encode($key).'?redirect='.encode(full_url())); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete this Image, is this correct?" >
                                                <span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
                                                  <?php echo config_item('icon_image'); ?>
                                                </span>
                                              </a>
                                            </div>
                                        </div> -->
                                    <?php endif ?>

                                    <div class="clearfix pd-t-xxxs"></div>
                                    
                                    <div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
                                        <?php if (@$content->image): ?>
                                            <img src="<?php echo display_image($content->image); ?>" class="img-responsive" />
                                            <input type="hidden" value="<?php echo $content->image; ?>" name="values[<?php echo $key; ?>][image]" >
                                        <?php else: ?>
                                            <img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">        
                                        <?php endif ?>
                                    </div>

                                    <div class="clearfix pd-t-sm"></div>

                                    <div class="row txt-responsive-xs inherit mg-t-xxs">
                                        <div class="col-xs-60 col-sm-20 inherit">
                                            <label for="imageLocation" class="pd-all-no auto-height">Image</label>
                                        </div>
                                        <div class="col-xs-60 col-sm-40 inherit txt-light">
                                            <input type="file" name="values[<?php echo $key; ?>][image]" id="imageLocation" />
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class = "col-xs-60">
                                <div class="col-xs-60">Content</div>
                                <div class="col-xs-60">
                                    <?php echo $this->ckeditor->editor('values['.$key.'][description]', set_value('values['.$key.'][description]', @$content->description)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                <?php endforeach ?>
                
            </div>

        </section>
    <?php echo form_close(); ?>

<!-- This part is for viewing -->
<?php else : ?>     
    <div class="bg-lighter-grey padding-bottom-large">
        <div class="col-xs-60">
            <span><?php echo (!@$article->title ? 'Title' : $article->title); ?></span>
        </div>
        <div class="col-xs-60">
            <?php echo (!@$article->description ? 'Content' : $article->description); ?>
        </div>
    </div>
<?php endif; ?> 