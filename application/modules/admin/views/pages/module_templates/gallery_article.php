<?php
	if(isset($info) && is_array($info) && count($info) > 0)
	{
		$pageMode = (trim($info['pageMode']) === '' ? 'view' : trim($info['pageMode']));
		$moduleId = (isset($info['moduleId']) ? trim($info['moduleId']) : '');
		$moduleTypeId = (isset($info['moduleTypeId']) ? trim($info['moduleTypeId']) : '');
		$moduleTypeName = (isset($info['moduleTypeName']) ? trim($info['moduleTypeName']) : '');
		$moduleBody = (isset($info['moduleBody']) ? $info['moduleBody'] : '');
		$rowId = (isset($info['rowId']) ? trim($info['rowId']) : '');
		$columnId = (isset($info['columnId']) ? trim($info['columnId']) : '');
		$allModuleTypes = (isset($info['allModuleTypes']) ? $info['allModuleTypes'] : array());
		
		if($moduleId === '')
		{
			echo '<div class = "row margin-bottom-large">
				  	<div class="col-xs-60">SYSTEM ERROR: Module information is missing</div>	
				  </div>';
			return;		
		}	
		
		$title = isset($moduleBody->title) ? trim($moduleBody->title) : '';
		$sub_title = isset($moduleBody->sub_title) ? trim($moduleBody->sub_title) : '';
		$content = isset($moduleBody->content) ? trim($moduleBody->content) : '';
		$module_gallery_id = isset($moduleBody->gallery_id) ? trim($moduleBody->gallery_id) : '';
	}
?>

<!-- This part is for editing -->
<?php if ($pageMode === 'edit'): ?>		
	
	<div class="pd-t-sm pd-l-sm col-xs-60 col-sm-60">
		<?php $this->load->view('/admin/pages/switch_module_type', array('info' => $info)); ?>
	</div>

	<?php echo form_open_multipart('admin/pages/gallery_article/edit/'.encode($moduleId)); ?>
	
	<div class="bg-lighter-grey pd-all-sm mg-t-lg">
	    <div class="col-sm-60">
	    	<div class="col-xs-20">
		  		<?php echo form_submit('edit_module', 'Save', 'class="txt-white txt-responsive-md txt-center txt-medium bg-seagreen"'); ?>
			</div>
		    <div class="mg-t-sm col-xs-60">
		   		<label for="prop-sub_heading">Title</label>
		   	</div>
		   	<div class="mg-t-xs col-xs-60">
			    <input type="text" class="form-control" name="body[title]" id="prop-title" value="<?php echo set_value('body[title]', $title); ?>" />
			    <input type="hidden" class="form-control" name="moduleId" id="prop-module_id" value="<?php echo set_value('moduleId', $moduleId); ?>" />
			    <input type="hidden" class="form-control" name="moduleTypeId" id="prop-module_type_id" value="<?php echo set_value('moduleTypeId', $moduleTypeId); ?>" />
			    <input type="hidden" class="form-control" name="moduleTypeName" id="prop-module_type_name" value="<?php echo set_value('moduleTypeName', $moduleTypeName); ?>" />
		    </div>
		    <div class="mg-t-sm col-xs-60">
		   		<label for="prop-sub_heading">Sub-Title</label>
		   	</div>
		   	<div class="mg-t-xs col-xs-60">
		   		<input type = "text" name= "body[sub_title]" id = "prop_sub_title" value = "<?php echo set_value('body[sub_title]', $sub_title); ?>" />
		   	</div>
	    </div>

	    <div class = "row">
	    	<div class="mg-b-sm mg-t-sm col-xs-60">
	    		<label for = "prop_content">Content</label>
	    	</div>
	    	<div class = "col-xs-60">
	    		<?php echo $this->ckeditor->editor("body[content]", set_value('body[content]', $content)); ?>
	    		<!-- <textarea class = "form-control" name = "body[content]" rows = "4" cols = "100"><?php echo $content; ?></textarea> -->
	    	</div>
		</div>

		<div class = "row">
			<?php if($module_gallery_id == '') : ?>
				<span style = "font-weight:bold; color:red">
					Internal Error, Automatic Gallery creation for Article MOdule failed. Please delete the Entire Row and Try Again.
					Or Try Changing the module type and back to Gallery Article
				</span>
			<?php else : ?>
				<?php
		   			$module_gallery = $this->gallery->get_gallery($module_gallery_id);
		   			$module_gallery_images = $this->gallery->get_images($module_gallery_id);
		   		?>

		   		<input type = "hidden" name = "module_gallery[id]" id = "module_gallery_id" value = "<?php echo $module_gallery->id; ?>" />

				<div class = "col-xs-60 mg-t-xs">
					<div class = "col-xs-10">Gallery Title</div>
					<div class = "col-xs-50">
						<input type = "text" name = "module_gallery[title]" id = "module_gallery_title" value = "<?php echo set_value('module_gallery[title]', $module_gallery->title); ?>" />							
					</div>
				</div>
				<div class = "col-xs-60 mg-t-xs">
					<div class = "col-xs-10">Gallery Content</div>
					<div class = "col-xs-50">
						<input type = "text" name = "module_gallery[content]" id = "module_gallery_content" value = "<?php echo set_value('module_gallery[content]', $module_gallery->content); ?>" />							
					</div>
				</div>

				<div class = "col-xs-60 mg-t-xs">
					<?php if(isset($module_gallery_images) && is_array($module_gallery_images)) : ?>						
						<ul>
						<?php foreach($module_gallery_images as $img) : ?>
							<li id = "<?php echo 'sort_'.$img->id; ?>">
								<?php if($img->image) : ?>									
									<img src = "<?php echo display_image($img->image, 'thumb_'); ?>" alt = "Image Image">
								<?php elseif($img->image_hover) : ?>
									<img src = "<?php echo display_image($img->image_hover, 'thumb_'); ?>" alt = "Hover Image">
								<?php elseif($img->webm) : ?>	
									<video class = "video" loop = "" width = "300"><source src = "<?php echo base_url($img->webm); ?>" type = "video/webm"></video>
								<?php elseif($img->mp4) : ?>									
									<embed width="100%" height="100%" name="plugin" src="<?php echo base_url($img->mp4); ?>" type="video/mp4">
								<?php endif; ?>

								Width
								<select name = "module_gallery_image[<?php echo $img->id; ?>][width]">
									<?php foreach($width_list as $key => $value) : ?>
										<option value = "<?php echo $key; ?>" <?php echo set_select('module_gallery_image['.$img->id.'][width]', $key, ($key == $img->width)); ?> ><?php echo $value; ?></option>
									<?php endforeach; ?>
								</select>

								Height
								<select name = "module_gallery_image[<?php echo $img->id; ?>][height]">
									<?php foreach($height_list as $key => $value) : ?>
										<option value = "<?php echo $key; ?>" <?php echo set_select('module_gallery_image['.$img->id.'][height]', $key, ($key == $img->height)); ?> ><?php echo $value; ?></option>
									<?php endforeach; ?>
								</select>

								<a class="mg-t-xxs remove-parent slide-out confirm btn btn-danger btn-xs" data-rel='#sort_<?php echo $img->id; ?>' title="You are about to delete this Image, is this correct?" href="<?php echo base_url('admin/galleries/remove_image/'.encode($img->id)) ?>">delete</a>									
							</li>
						<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>

				<div class = "col-xs-60 mg-t-xs">
					<div class = "col-xs-60">New Image</div>
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">Image Title</div>							
						<div class = "col-xs-50"><input type = "text" name = "new_image_title" id = "new_image_title" /></div>							
					</div>
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">Image Desc</div>							
						<div class = "col-xs-50"><input type = "text" name = "new_image_description" id = "new_image_description" /></div>							
					</div>
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">Image</div>							
						<div class = "col-xs-50"><input type = "file" name = "new_image"/></div>							
					</div>	
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">Hover Image</div>							
						<div class = "col-xs-50"><input type = "file" name = "new_hover_image"/></div>							
					</div>	
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">WebM</div>							
						<div class = "col-xs-50"><input type = "file" name = "new_webm"/></div>							
					</div>	
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">Mp4</div>							
						<div class = "col-xs-50"><input type = "file" name = "new_webm"/></div>							
					</div>
					<div class = "col-xs-60 mg-t-xs">
						<div class = "col-xs-10">Width</div>							
						<div class = "col-xs-15">
							<select name = "new_image_width">
								<?php foreach($width_list as $key => $value) : ?>
									<option value = "<?php echo $key; ?>" <?php echo set_select('new_width', $key); ?> ><?php echo $value; ?></option>
								<?php endforeach; ?>
							</select>
						</div>							
						<div class = "col-xs-10">Height</div>							
						<div class = "col-xs-15">
							<select name = "new_image_height">
								<?php foreach($height_list as $key => $value) : ?>
									<option value = "<?php echo $key; ?>" <?php echo set_select('new_height', $key); ?> ><?php echo $value; ?></option>
								<?php endforeach; ?>
							</select>
						</div>							
					</div>							
				</div>

			<?php endif; ?>
		</div>
	</div>	
	<?php echo form_close(); ?>

<!-- This part is for viewing -->
<?php else : ?>		
	<div class="bg-lighter-grey padding-bottom-large">
	    <div class="col-xs-60">
	   		<span><?php echo ($title === '' ? 'Title Goes Here' : $title); ?></span>
	   	</div>
	   	<div class="col-xs-60">
	   		<span><?php echo ($sub_title === '' ? 'Sub Title Goes Here' : $sub_title); ?></span>
	   	</div>
	   	<div class="col-xs-60">
	   		<span><?php echo ($content === '' ? 'Content Goes Here' : $content); ?></span>
	   	</div>

	   	<div class = "col-xs-60">
		   	<?php if($module_gallery_id !== '') : ?>
		   		<?php
		   			$module_gallery = $this->gallery->get_gallery($module_gallery_id);
		   			$module_gallery_images = $this->gallery->get_images($module_gallery_id);
		   		?>
		   		<?php if($module_gallery) : ?>
		   			<span class = "col-xs-10">Gallery Title:</span>
		   			<span class = "col-xs-60"><?php echo $module_gallery->title; ?></span>
		   			
		   			<?php
		   				$counter = 0; 
		   				if($module_gallery_images && is_array($module_gallery_images)) :  
		   			?>
		   				<div class = "col-xs-60">
			   				<?php foreach($module_gallery_images as $image) : ?>
			   					<div class = "col-xs-20">
			   						<span class = "col-xs-60"><?php echo $image->title; ?></span>
			   						<span class = "col-xs-60"><?php echo $image->description; ?></span>
			   						<span class = "col-xs-60"><?php echo base_url($image->image); ?></span>
			   						<span class = "col-xs-60"><?php echo base_url($image->image_hover); ?></span>
			   						<span class = "col-xs-60"><?php echo base_url($image->webm); ?></span>
			   						<span class = "col-xs-60"><?php echo base_url($image->mp4); ?></span>
			   					</div>
			   					<?php 
			   						$counter++;
			   						if($counter >= 3)
									{
										$counter = 0;
										echo '</div>';
										echo '<div class = "col-xs-60">';
									}
								?>		
			   				<?php endforeach; ?>
		   				</div>
		   			<?php else : ?>
		   				<div class = "col-xs-60">Gallery Image(s) go here</div>	
		   			<?php endif; ?>

		   		<?php else : ?>
		   			<span>Gallery Content goes here</span>
		   		<?php endif; ?>
		   	<?php else : ?>
				<span>Gallery Content goes here</span>   			
		   	<?php endif; ?>
		</div>		   	
	    
	</div>
<?php endif; ?>	