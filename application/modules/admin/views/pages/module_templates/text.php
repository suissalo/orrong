<?php
	if(isset($info) && is_array($info) && count($info) > 0)
	{
		$pageMode = (trim($info['pageMode']) === '' ? 'view' : trim($info['pageMode']));
		$moduleId = (isset($info['moduleId']) ? trim($info['moduleId']) : '');
		$moduleTypeId = (isset($info['moduleTypeId']) ? trim($info['moduleTypeId']) : '');
		$moduleTypeName = (isset($info['moduleTypeName']) ? trim($info['moduleTypeName']) : '');
		$moduleBody = (isset($info['moduleBody']) ? $info['moduleBody'] : '');
		$rowId = (isset($info['rowId']) ? trim($info['rowId']) : '');
		$columnId = (isset($info['columnId']) ? trim($info['columnId']) : '');
		$allModuleTypes = (isset($info['allModuleTypes']) ? $info['allModuleTypes'] : array());
		
		if($moduleId === '')
		{
			echo '<div class = "row margin-bottom-large">
				  	<div class = "col-xs-60">SYSTEM ERROR: Module information is missing</div>	
				  </div>';
			return;		
		}	
		
		$title = isset($moduleBody->title) ? trim($moduleBody->title) : '';
		$content = isset($moduleBody->content) ? trim($moduleBody->content) : '';
	}
?>

<!-- This part is for editing -->
<?php if ($pageMode === 'edit'): ?>		
	<div class="pd-l-sm pd-t-sm"><?php $this->load->view('/admin/pages/switch_module_type', array('info' => $info)); ?></div>
<!-- 	<hr/> -->
	<?php echo form_open_multipart('admin/pages/text/edit/'.encode($moduleId).'?dy=1'); ?>
	<div class="bg-lighter-grey pd-b-sm pd-l-sm pd-r-sm">
	 	<div class = "col-xs-20">
		  	<?php echo form_submit('edit_module', 'Save', 'class="txt-white txt-responsive-md txt-medium bg-seagreen"'); ?>
		</div>
	    <div class = "row margin-bottom-large">
		    <div class="mg-t-sm mg-b-xs col-xs-60">
		   		<label for="prop-sub_heading" >Title</label>
		   	</div>
		   	<div class="col-xs-60">
			    <input type="text" class="form-control" name="body[title]" id="prop-title" value="<?php echo set_value('body[title]', $title) ?>" />
			    <input type="hidden" class="form-control" name="moduleId" id="prop-module_id" value="<?php echo set_value('moduleId', $moduleId) ?>" />
			    <input type="hidden" class="form-control" name="moduleTypeId" id="prop-module_type_id" value="<?php echo set_value('moduleTypeId', $moduleTypeId) ?>" />
			    <input type="hidden" class="form-control" name="moduleTypeName" id="prop-module_type_name" value="<?php echo set_value('moduleTypeName', $moduleTypeName) ?>" />
		    </div>  
	    </div>
	    <div class = "row margin-bottom-large">
	    	<div class = "mg-t-sm mg-b-xs col-xs-60">
	    		<label for = "prop_content">Content</label>
	    	</div>
	    	<div class = "col-xs-60">
	    		<?php echo $this->ckeditor->editor("body[content]", set_value('body[content]', $content)); ?>

	    		<!-- <textarea class = "form-control" name = "body[content]" rows = "4" cols = "100"><?php echo $content; ?></textarea> -->
	    	</div>
		</div>
	    <!--
	    <div class = "row margin-bottom-large">
	    	<div class = "col-xs-10">
				<?php echo form_submit('edit_module', 'Edit', 'class="btn bg-green"'); ?>
	    	</div>
		</div>
		-->
	</div>	
	<?php echo form_close(); ?>

<!-- This part is for viewing -->
<?php else : ?>		
	<div class="bg-lighter-grey padding-bottom-large">
	    <div class="col-xs-60">
	   		<span><?php echo ($title === '' ? 'Title Goes Here' : $title); ?></span>
	   	</div>
	    <div class="col-xs-60">
		    <div class="row">
		    	<div class = "col-xs-60">
		    		<span><?php echo ($content === '' ? 'Content Goes here' : $content); ?></span>
		    	</div>
		   	</div>
		</div>
	</div>
<?php endif; ?>	
