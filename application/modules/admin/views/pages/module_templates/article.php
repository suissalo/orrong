<!-- This part is for editing -->
<?php if ($page_mode === 'edit'): ?>

	<?php echo form_open_multipart(); ?>
	
		<div class="row action-buttons">
			<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('/admin/pages/edit/'.$page_id.'#content'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a> 
				<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-disk">
					  <?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
					  Save
					</span>
				</button>
			</div>
		</div>

		<section>
			
			<?php echo Input_helper::heading('Edit Article: '. @$article->title); ?>

			<?php //$this->load->view('admin/pages/switch_module_type', array('info' => $info)); ?>
			
			<div class="col-xs-60 bg-white pd-all-md">

				<div class="row">

					<div class="col-xs-60">
						<label for="prop-title">Title *</label>
					</div>
					<div class="col-xs-60">
						<input type="text" class="form-control" name="title" id="prop-title" value="<?php echo set_value('title', @$article->title) ?>" />
					</div>

					<div class="col-xs-60 mg-t-md">
						<div class="col-xs-60 col-md-30 pd-r-md">
							<label for="prop-link_url">Link</label><br/>
							<input type="text" class="form-control" name="link_url" id="prop-link_url" value="<?php echo set_value('link_url', @$article->link_url) ?>" />
						</div>
						<div class="col-xs-60 col-md-30">
							<label for="prop-link_text">Link Text</label>
							<input type="text" class="form-control" name="link_text" id="prop-link_text" value="<?php echo set_value('link_text', @$article->link_text) ?>" />
						</div>
					</div>

					<div class="col-xs-60 mg-t-md">
						<label for = "prop_content">Content</label>
					</div>

					<div class = "col-xs-60">
						<?php echo $this->ckeditor->editor("description", set_value('description', @$article->description)); ?>
					</div>

				</div>

				<div class="row dbl-pd">

					<div class="col-xs-60 col-md-20 dbl-pd">

						<label class="col-xs-60 mg-t-md">Heading Image</label>
						<div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">
							<?php if (@$article->image): ?>
								<div class="row mg-b-sm">
									<div class="pull-right">
									  <a href="<?php echo base_url('admin/pages/article/remove_image/'.encode($article->id).'?redirect='.encode(full_url())); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete this Image, is this correct?" >
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
										  <?php echo config_item('icon_image'); ?>
										</span>
									  </a>
									</div>
								</div>
							<?php endif ?>

							<div class="clearfix pd-t-xxxs"></div>
							
							<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
								<?php if (@$article->image): ?>
									<img src="<?php echo display_image($article->image); ?>" class="img-responsive">		
								<?php else: ?>
									<img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">		
								<?php endif ?>
							</div>

							<div class="clearfix pd-t-sm"></div>

							<div class="row txt-responsive-xs inherit mg-t-xxs">
								<div class="col-xs-60 col-sm-20 inherit">
									<label for="imageLocation" class="pd-all-no auto-height">Image</label>
								</div>
								<div class="col-xs-60 col-sm-40 inherit txt-light">
									<input type="file" name="image" id="imageLocation" />
								</div>
							</div>

							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				
			</div>

		</section>
	<?php echo form_close(); ?>

<!-- This part is for viewing -->
<?php else : ?>		
	<div class="bg-lighter-grey padding-bottom-large">
		<div class="col-xs-60">
			<span><?php echo (!@$article->title ? 'Title' : $article->title); ?></span>
		</div>
		<div class="col-xs-60">
			<?php echo (!@$article->description ? 'Content' : $article->description); ?>
		</div>
	</div>
<?php endif; ?>	