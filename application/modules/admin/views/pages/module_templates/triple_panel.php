<!-- This part is for editing -->
<?php if ($page_mode === 'edit'): //view($article); ?>

	<?php echo form_open_multipart(); ?>
	
		<div class="row action-buttons">
			<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('/admin/pages/edit/'.$page_id.'#content'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a> 
				<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-disk">
					  <?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
					  Save
					</span>
				</button>
			</div>
		</div>

		<section>
			
			<?php echo Input_helper::heading('Edit Article: '. @$article->title); ?>
			
			<div class="col-xs-60 bg-white pd-all-md">

				<div class="row">
					<div class="col-xs-60">
						<label for="prop-title">Title *</label>
					</div>
					<div class="col-xs-60">
						<input type="text" class="form-control" name="title" id="prop-title" value="<?php echo set_value('title', @$article->title) ?>" />
					</div>
				</div>

				<div class="clearfix"></div>
				<label class="col-xs-60 mg-t-md">Sections</label>

				<div class="col-xs-60 col-md-60 dbl-pd">
				<?php for ($key = 0; $key < 3; $key++): ?>
					<div class="col-xs-20 mg-t-md">
						<div class="row pd-all-md bordered mg-r-xs">
							<div class="col-xs-60 mg-t-md">

								<div class="col-xs-60">
									<label for="prop-values[<?php echo $key; ?>][title">Title</label>
								</div>
								<div class="col-xs-60">
									<input type="text" class="form-control" name="values[<?php echo $key; ?>][title]" id="prop-values[<?php echo $key; ?>][title" value="<?php echo set_value('values['.$key.'][title]', @$article->values[$key]->title) ?>" />
								</div>

								<div class="col-xs-60 mg-t-md">
									<?php if (@$article->values[$key]->image): ?>
										<label for="prop-values[<?php echo $key; ?>][image">
											<a href="<?php echo display_image(@$article->values[$key]->image); ?>" class="pop-up">Image</a>
										</label>
										<img src="<?php echo display_image(@$article->values[$key]->image); ?>" class="img-responsive">
										<input type="hidden" name="values[<?php echo $key; ?>][image_url]" value="<?php echo set_value('values['.$key.'][image]', @$article->values[$key]->image); ?>" />
									<?php else: ?>
										<label for="prop-values[<?php echo $key; ?>][image">Image</label>
										<img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
									<?php endif; ?>									
								</div>
								<div class="col-xs-60">
									<input type="file" name="values[<?php echo $key; ?>][image]" id="prop-values[<?php echo $key; ?>][image" />
								</div>

								<div class="col-xs-60 mg-t-md">
									<div class="col-xs-60 col-md-60 pd-r-md">
										<label for="prop-link_url[<?php echo $key; ?>][link_url">Link</label><br/>
										<input type="text" class="form-control" name="values[<?php echo $key; ?>][link_url]" id="prop-values[<?php echo $key; ?>][link_url" value="<?php echo set_value('values['.$key.'][link_url]', @$article->values[$key]->link_url) ?>" />
									</div>
								</div>
								<div class="col-xs-60 mg-t-md">
									<div class="col-xs-60 col-md-60">
										<label for="prop-link_text">Link Text</label>
										<input type="text" class="form-control" name="values[<?php echo $key; ?>][link_text]" id="prop-values[<?php echo $key; ?>][link_text" value="<?php echo set_value('values['.$key.'][link_text]', @$article->values[$key]->link_text) ?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endfor ?>
				</div>
				
			</div>

		</section>
	<?php echo form_close(); ?>

<!-- This part is for viewing -->
<?php else : ?>		
	<div class="bg-lighter-grey padding-bottom-large">
		<div class="col-xs-60">
			<span><?php echo (!@$article->title ? 'Title' : $article->title); ?></span>
		</div>
		<div class="col-xs-60">
			<?php echo (!@$article->description ? 'Content' : $article->description); ?>
		</div>
	</div>
<?php endif; ?>	