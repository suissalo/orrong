<?php echo form_open_multipart('admin/pages/save_edit/'.encode($page->id)); ?>

<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('admin/pages'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a>

				<button  type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
						<span class="icon icon-size-150 outline-light icon-disk">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Save
						</span>
				</button>
		</div>
</div>

<section>
<?php echo Input_helper::heading($page->title); ?>


	<div class="tab-container bg-white pd-all-md">

		<div class="mg-b-md">
			<a href="#details" class="tab-link txt-responsive-sm col-md-9 col-lg-9 pd-all-xs">Details</a>
			<a href="#hero-gallery" class="tab-link txt-responsive-sm col-md-10 col-lg-10 pd-all-xs">Hero Gallery</a>
			<a href="#content" class="tab-link txt-responsive-sm col-md-9 col-lg-9 pd-all-xs">Content</a>
			<div class="clearfix"></div>
			<div class="tab-link-divider"></div>
		</div>

		<div class="tab-content" id="details">

			<div class="row dbl-pd">

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">

					<div class="col-xs-60">
						<label for="title">Title</label>
					</div>
					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="title" id="title" value="<?php echo set_value('title', $page->title); ?>"/>
					</div>

				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd">

					<div class="col-xs-60">
						<label for="parent_id">Parent</label>
					</div>

					<div class="col-xs-60">
						<select name="parent_id" id="parent" class="mg-b-sm">
							<option value="0">Main Navigation</option>
							<?php if ($pages): ?>
									<?php foreach ($pages as $parent): ?>
													<option value="<?php echo $parent->id ?>" <?php if($page->parent_id == $parent->id): ?>selected="selected"<?php endif; ?>> - <?php echo $parent->title; ?></option>
									<?php endforeach; ?>
							<?php endif ?>
							<option value="-1" <?php if($page->parent_id == '-1'): ?>selected="selected"<?php endif; ?>>Orphan</option>
						</select>
					</div>

					<div class="col-xs-60">
						<label for="template">Template</label>
					</div>
					<div class="col-xs-60">
						<select name="template" id="template" class="mg-b-sm">
							<?php if ($templates): ?>
								<?php foreach ($templates as $key => $value): ?>
									<option value="<?php echo $key ?>" <?php echo set_select('template', $key, (($page->template == $key)? TRUE : FALSE)) ?>><?php echo $value ?></option>
								<?php endforeach ?>
							<?php endif ?>
						</select>
					</div>

				</div>

				<div class="col-xs-60 dbl-pd">
					<div class="pull-left">
						<label for="permalink">Link</label>
					</div>
					 <div class="pull-right">
						<label class="custom-checkbox txt-responsive-xs" for="enable_link" >
								<input id="enable_link" type="checkbox" name="enable_link" <?php echo set_radio('enable_link', '1', (bool)($page->enable_link)); ?> value="1" />
								<span class="circle"><i></i></span>
								<span class="text">Clickable</span>
						</label>
					</div>
				</div>

				<div class="col-xs-60 auto-overflow dbl-pd">
					<span class="demo-url input"><?php echo base_url($page->permalink); ?></span>
					<input type="hidden" value="<?php echo $page->permalink ?>" name="permalink" id="permalink" />
				</div>

			</div>
		</div>

		<!-- HERO IMAGES -->
		<div class="tab-content" id="hero-gallery">

			<div class="row action-buttons">

				<?php echo gallery_message(); ?>

				<div class="pull-right">
					<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Order Gallery
						</span>
					</a>
					<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Stop Ordering
						</span>
					</a>
				</div>
			</div>

			<ul class="manage-list manage-galleries row std-pd" data-rel="gallery_images" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">

				<?php if(isset($gallery_images) && is_array($gallery_images)) : ?>
					<?php foreach ($gallery_images as $key => $image): ?>
						<li class="col-xs-60 col-sm-30 col-md-20 std-pd" id="sort_<?php echo $image->id; ?>">
								<?php $this->load->view('admin/_partials/gallery_item_media', array('image' => $image, 'redirect' => full_url().'#hero-gallery')); ?>
						</li>
					<?php endforeach ?>
				<?php endif ?>

				<li class="upload-new-image col-xs-60 col-sm-30 col-md-20 std-pd ordering-hide" data-name="file">
					<div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">

						<div class="clearfix pd-t-xxxs"></div>

						<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
							<div id="galery-uploader" class="dropzone multi-uploader" data-title="Upload Images" data-name="file" data-action="<?php echo base_url('admin/pages/upload_gallery_image/'.encode($page->gallery_id).'/'.encode($page->id)); ?>">
								<div class="fallback">
					                <img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
					            </div>
					            <div class="dz-default dz-message">
					                Drag files here to upload (or click) to upload a new Images

					            </div>
				            </div>
						</div>

						<div class="clearfix pd-t-sm"></div>

						<div class="clearfix"></div>

					</div>
				</li>

			</ul>
		</div>
		<!-- / HERO IMAGES -->


		<div class="tab-content" id="content">

			<div class="row">
				<label for="sub_heading">Page Title</label>
				<input class="mg-b-sm" type="text" name="sub_heading" id="prop-sub_heading" value="<?php echo set_value('sub_heading', $page->sub_heading); ?>"/>
			</div>

			<div class="row">
				<label for="content">Page Content</label>
				<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("detail", set_value('detail', $page->detail)); ?></div>
			</div>

			<!-- SECTIONS -->
			<?php $this->load->view('sections/page_view'); ?>
			<!-- END SECTIONS --> 

			<?php /*
			<!--  MODULES -->
			<div class="row action-buttons mg-b-md mg-t-sm">
				<div class="txt-responsive-sm pull-left mg-t-sm">Row Content</div>

				<div class="pull-right">

						<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
							<span class="icon icon-size-150 icon-reorder">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Order Rows
							</span>
						</a>
						<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
							<span class="icon icon-size-150 icon-reorder">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Stop Ordering
							</span>
						</a>

						<a href="<?php echo base_url('admin/pages/add_row/1/'.encode($page->id).'#content'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
							<span class="icon icon-size-150 outline-light icon-plus">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Add New Row
							</span>
						</a>

						<!-- <div class="pull-right"> -->

						<!-- </div> -->

						<a href="<?php echo base_url('admin/pages/add_row/2/'.encode($page->id).'#content'); ?>" add_row="add_row_1" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
							<span class="icon icon-size-150 outline-light icon-plus">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Add Row With 2 Columns
							</span>
						</a>

						<a href="<?php echo base_url('admin/pages/add_row/3/'.encode($page->id).'#content'); ?>" add_row="add_row_1" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
							<span class="icon icon-size-150 outline-light icon-plus">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Add Row With 3 Columns
							</span>
						</a>

				</div>
			</div>

			<div class="row mg-t-xs">
				<?php if ($page_rows): ?>
					<ul class="manage-list" data-rel="<?php echo $page->id; ?>" data-parent="<?php echo $page->id; ?>" data-url="<?php echo base_url('admin/pages/update_row_order'); ?>">
					<?php foreach ($page_rows as $key => $page_row):
						$columns = $this->module->get_row_colums($page_row->id); ?>
							<?php if ($columns):
								$num_rows = count($columns);?>
								<li class="item-parent list-none bg-light-grey mg-b-md" id="sort_<?php echo $page_row->id; ?>" >
									<div class="pull-left txt-responsive-sm txt-bold mg-t-xs">Row with <?php echo $num_rows; ?> Column/s</div>
									<div class="pull-right ">
										<a href="<?php echo base_url('admin/pages/remove_row/'.encode($page_row->id).'/'.encode($page->id)); ?>" data-rel="#sort_<?php echo $key; ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs confirm slide-out" title="You are about to delete this row, do you wish to continue?">
											<span class="icon icon-size-125 outline-light icon-cross">
												<?php echo config_item('icon_image'); ?>
											</span>
										</a>
									</div>

									<hr/>
									<div class="item-child clearfix pd-all-sm" page_row_id="<?php echo $key; ?>">

									<?php foreach ($columns as $key => $column): //view($column);

										$view_data = array(
											'column_id' => $column->page_row_column_id,
											'row_id' 	=> $page_row->id,
											'article' 	=> json_decode($column->module_body),
											'page_mode' => 'view',
										); ?>
										<div class="col-xs-<?php echo floor(60 / $num_rows); ?> module-box">
											<div class="col-xs-60">
												<div class="pull-right" >
													<?php $this->load->view('admin/pages/switch_module_type', array('current_module' => $column->module_type, 'column_id' => $column->page_row_column_id,  'row_id' => $page_row->id)); ?>
													<a href="<?php echo base_url('admin/pages/'.$column->module_type.'/edit/'.encode($column->page_row_column_id).'/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" title="Edit">
														<span class="icon icon-size-125 outline-light icon-edit">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
													<a href="<?php echo base_url('admin/pages/remove_column/'.encode($column->page_row_column_id).'/'.encode($page->id)); ?>" add_row="add_row_1" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs confirm " title="You are about to delete this row, do you wish to continue?">
														<span class="icon icon-size-125 outline-light icon-cross">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
												</div>
											</div>
											<div class="col-xs-60">
												<?php if (isset($modules[$column->module_type])): ?>
													<?php $this->load->view('pages/module_templates/'.$column->module_type, $view_data); ?>
												<?php endif ?>
											</div>
										</div>

									<?php endforeach ?>
									</div>
								</li>
							<?php endif ?>

					<?php endforeach ?>
					</ul>
				<?php endif ?>
			</div>
			<!--  / MODULES -->
			*/ ?>


			<!-- <div class="row">
				<label for="content">Sidebar Content</label>
				<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("sidebar", set_value('sidebar', $page->sidebar)); ?></div>
			</div> -->

			<div class="row">

				<div class="col-xs-60">
					<label for="prop-meta_title">Meta Title</label>
				</div>
				<div class="col-xs-60">
					<input class="" type="text" name="meta_title" id="prop-meta_title" value="<?php echo set_value('meta_title', $page->meta_title); ?>"/>
				</div>

			</div>

			<div class="row dbl-pd mg-t-md">
				<div class="col-xs-30 dbl-pd">
					<label class="col-xs-60" for="meta_keywords">Meta Keywords <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 8 words separated by a comma)</span></label>
					<textarea name="meta_keywords" id="meta_keywords"><?php echo set_value('meta_keywords', $page->meta_keywords); ?></textarea>
				</div>
				<div class="col-xs-30 dbl-pd">
					<label class="col-xs-60" for="meta_description">Meta Description <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 255 characters)</span></label>
					<textarea name="meta_description" id="meta_description"><?php echo set_value('meta_description', $page->meta_description); ?></textarea>
				</div>
			</div>

		</div>


	</div>
</section>
<input type="hidden" name="gallery_id" value="<?php echo $page->gallery_id; ?>" />
<!-- <input type="hidden" value="1" name="enable_link" id="prop-enable_link" /> -->
</form>
