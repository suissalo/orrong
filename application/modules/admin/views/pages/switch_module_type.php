<?php 
//$this->load->model('caravan');
$all_modules 		= config_item('module_type');
//$caravans 			= $this->caravan->fetch_by(); ?>

<div class="pull-left mg-r-sm">
	<select class="bg-white block txt-responsive-sm" name="module_type[<?php echo $row_id; ?>][<?php echo $column_id; ?>]">
		<?php foreach($all_modules as $module => $values):
			$selected = ($current_module === $module) ? 'selected = selected' : '';	?>
			<option <?php echo $selected; ?> value="<?php echo $module; ?>" <?php echo set_select('module_type', $module, (($current_module == $module)? TRUE : FALSE)) ?>><?php echo ucfirst(str_replace('_', ' ', $module)); ?></option>			
		<?php endforeach; ?>
		<!-- <option value="fixed_module:subscribe" <?php echo set_select('module_type', 'fixed_module:subscribe', (($current_module == 'fixed_module:subscribe')? TRUE : FALSE)) ?>>Subscribe</option> -->
		<?php /*if ($caravans): ?>
			<optgroup label="Caravans">
				<?php foreach ($caravans as $key => $caravan): ?>
					<option value="module:caravans/<?php echo $caravan->id; ?>" <?php echo set_select('module_type', 'module:caravans/'.$caravan->id, (($current_module == 'module:caravans/'.$caravan->id)? TRUE : FALSE)) ?>><?php echo $caravan->name; ?></option>
				<?php endforeach ?>
			</optgroup>
		<?php endif */ ?>
	</select>
</div>
