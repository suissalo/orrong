<?php echo form_open_multipart('admin/pages/save_new'); ?>

<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('admin/pages'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a>     

				<button  type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
						<span class="icon icon-size-150 outline-light icon-disk">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Save
						</span>
				</button>
		</div>
</div>

<section>
<?php echo Input_helper::heading('New Page'); ?>


	 <div class="tab-container bg-white pd-all-md">
			
			<div class="mg-b-md">
				<a href="#details" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Details</a>
				<a href="#hero-gallery" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Hero Gallery</a>
				<a href="#content" class="tab-link txt-responsive-sm col-md-10 pd-all-xs">Content</a>
				<div class="txt-responsive-xxs text-right pull-right col-md-14 pd-all-xxs pd-t-sm">Please complete ALL tabs</div>
				<div class="clearfix"></div>
				<div class="tab-link-divider"></div>
			</div>

			<div class="tab-content" id="details">
			
				<div class="row dbl-pd">

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">

						<div class="col-xs-60">
							<label for="title">Title*</label>
						</div>
						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="title" id="title" value="<?php echo set_value('title'); ?>"/>
						</div>

					</div>

					<div class="col-xs-60 col-sm-30 dbl-pd">
					
						<div class="col-xs-60">
							<label for="parent">Parent</label>
						</div>
						<div class="col-xs-60">
							<select name="parent_id" id="parent" class="mg-b-sm">
								<option value="0" <?php echo set_select('parent_id', '0'); ?>>Main Navigation</option>
								<?php if ($pages): ?>
									<?php foreach ($pages as $parent): ?>
										<option value="<?php echo $parent->id ?>" <?php echo set_select('parent_id', $parent->id); ?> > - <?php echo $parent->title; ?></option>
									<?php endforeach; ?>
								<?php endif ?>
								<option value="-1" <?php echo set_select('parent_id', '-1'); ?>>Orphan</option>
							</select>
						</div>

						<div class="col-xs-60">
							<label for="template">Template</label>
						</div>

						<div class="col-xs-60">
							<select name="template" id="template" class="mg-b-sm">
								<?php if ($templates): ?>
									<?php foreach ($templates as $key => $value): ?>
										<option value="<?php echo $key ?>" <?php echo set_select('template', $key) ?>><?php echo $value ?></option>
									<?php endforeach ?>
								<?php endif ?>
								<option value="-1">Page</option>
							</select>
						</div>

					</div>

					<div class="col-xs-60 dbl-pd">

						<div class="pull-left">
							<label for="permalink">Link</label>
						</div>
						<div class="pull-right">
							<label class="custom-checkbox txt-responsive-xs" for="enable_link" >
									<input id="enable_link" type="checkbox" name="enable_link" <?php echo set_radio('enable_link', '1', TRUE); ?> value="1" />
									<span class="circle"><i></i></span>
									<span class="text">Clickable</span>
							</label>
						</div> 
					 </div>

					<div class="col-xs-60 auto-overflow dbl-pd">
						<span class="demo-url input"><?php echo base_url(set_value('permalink')); ?></span>
					</div>

				</div>
			</div>
			
			<div class="tab-content" id="hero-gallery">

				<div class="row action-buttons">

					<?php echo gallery_message(); ?>
				
					<!-- <div class="pull-right">
						<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
							<span class="icon icon-size-150 icon-reorder">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Order Gallery
							</span>
						</a>
						<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
							<span class="icon icon-size-150 icon-reorder">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								Stop Ordering
							</span>
						</a>
					</div> -->
				</div>
				
				<ul class="manage-list manage-galleries row std-pd" data-rel="gallery_images" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
					
					<li class="col-xs-60 col-sm-30 col-md-20 std-pd" data-name="file">
						<div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">

							<div class="clearfix pd-t-xxxs"></div>
							
							<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
								<img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
							</div>

							<div class="clearfix pd-t-sm"></div>

							<div class="row txt-responsive-xs inherit mg-t-xxs">
								<div class="col-xs-60 col-sm-20 inherit">
									<label for="new-image" class="pd-all-no auto-height">Image</label>
								</div>
								<div class="col-xs-60 col-sm-40 inherit txt-light">
									<input type="file" name="image" id="new-image" />
								</div>
							</div>
							
							<!-- <div class="row txt-responsive-xs inherit mg-t-xxs">
								<div class="col-xs-60 col-sm-20 inherit">
									<label for="prop-new_image_title">Caption</label>
								</div>
								<div class="col-xs-60 col-sm-40 inherit">
									<input type="text" name="new_image_title" id="prop-new_image_title" value="<?php echo set_value('new_image_title'); ?>" /> 
								</div>
							</div> 

							<div class="row txt-responsive-xs inherit mg-t-xs">
								<div class="col-xs-60 col-sm-20 inherit"> 
									<label for="prop-new_image_description">Alt Tag</label>
								</div>
								<div class="col-xs-60 col-sm-40 inherit">
									<input type="text" name="new_image_description" id="prop-new_image_description" value="" />
								</div>
							</div>

							<div class="row txt-responsive-xs inherit mg-t-xs">
								<div class="col-xs-60 col-sm-20 inherit"> 
									<label for="prop-new_image_link">Link</label>
								</div>
								<div class="col-xs-60 col-sm-40 inherit">
									<input type="text" name="new_image_link" id="prop-new_image_link" value="" />
								</div>
							</div>  -->

							<div class="clearfix"></div>

						</div>     
					</li>
					
				</ul>
			</div>

			<div class="tab-content" id="content">

				<div class="row mg-t-md">
					<label for="sub_heading">Page Title</label>
					<input class="mg-b-sm" type="text" name="sub_heading" id="prop-sub_heading" value="<?php echo set_value('sub_heading'); ?>"/> 
				</div>

				<div class="row mg-t-md">
					<label for="content">Page Content</label>
					<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("detail", set_value('detail')); ?></div>
				</div>

				<!-- <div class="row mg-t-md">
					<label for="content">Sidebar Content</label>
					<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("sidebar", set_value('sidebar')); ?></div>
				</div> -->

				<div class="row">

					<div class="col-xs-60">
						<label for="meta-title">Meta Title</label>
					</div>
					<div class="col-xs-60">
						<input class="" type="text" name="meta_title" id="meta-title" <?php echo set_value('meta_title'); ?>/>
					</div>

				</div>

				<div class="row dbl-pd mg-t-md">
					<div class="col-xs-30 dbl-pd">
						<label class="col-xs-60" for="meta_keywords">Meta Keywords <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 8 words separated by a comma)</span></label> 
						<textarea name="meta_keywords" id="meta_keywords" ><?php echo set_value('meta_keywords'); ?></textarea>
					</div>
					<div class="col-xs-30 dbl-pd">
						<label class="col-xs-60" for="meta_description">Meta Description <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 255 characters)</span></label>
						<textarea name="meta_description" id="meta_description"><?php echo set_value('meta_description'); ?></textarea>
					</div>
				</div>
			</div>

		 </div>

	</section>

	<input type="hidden" value="<?php echo set_value('permalink'); ?>" name="permalink" id="permalink" />
	<input type="hidden" value="1" name="enable_link" id="prop-enable_link" />
</form>
