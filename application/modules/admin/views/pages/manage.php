
<div>

	<div class="row action-buttons">

		<div class="mg-t-md mg-b-md pull-right">	

			<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a>

			<a href="<?php echo base_url('admin/pages/new_page'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New Page
				</span>
			</a>

		</div>
	</div>	

	<section>
		<div class="row">
			<div class="col-xs-60 col-sm-60">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		Main Navigation Pages
			 	</div>
		 	</div>
		</div>

		<?php if ($pages) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<!-- <div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">lock</div> -->
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">view</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>

		 		<ul class="manage-list" data-rel="manage-pages" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
		 		
	 				<?php foreach ($pages as $page) : ?>
	 					<?php $sub_pages = $this->page->build_list($page->id); ?>
	 					<li class="item-parent list-none" id="sort_<?php echo $page->id; ?>">
	 						<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
		 							<?php if ($sub_pages) : ?>
			 						<span class="icon icon-size-75 outline-dark icon-rightarrow more sub-pages-show mg-l-xs mg-r-xxs">
										<?php echo config_item('icon_image'); ?>
									</span>
									<?php else: ?>
									<span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
									</span>
									<?php endif; ?>
			 						<?php echo $page->title;?>
		 						</div>
		 						<div class="icon-container text-right stop-prop pull-right">
			 						<!-- <a href="#" class="bg-yellow pd-all-xxs pull-left block pd-l-xs pd-r-xs mg-r-xxxs">
				 						<span class="icon icon-size-125 fill-light icon-lock stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a> -->
									<a href="<?php echo base_url($page->permalink); ?>" class="bg-yellow pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" target="_blank">
										<span class="icon icon-size-125 fill-light icon-preview stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<a href="<?php echo base_url('admin/pages/edit/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php if ($page->status == 1): ?>
									<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif ?>

									<a href="<?php echo base_url('admin/pages/remove/'.encode($page->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete <?php echo $page->title; ?>, do you wish to continue?">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
		 						</div>
	 						</div>

	 					<?php if ($sub_pages) : ?>
	 						<ul class="manage-list sub-pages" data-rel="manage-pages" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
	 						<?php foreach ($sub_pages as $page) : ?>
	 							<?php $sub_sub_pages = $this->page->build_list($page->id); ?>
	 							<li class="item-parent indent-me list-none" id="sort_<?php echo $page->id; ?>">

					 				<div class="item-child row bg-light-grey mg-t-xs pd-all-xxs">
				 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
				 							<?php if ($sub_sub_pages) : ?>
					 						<span class="icon icon-size-75 outline-dark icon-rightarrow more sub-pages-show mg-l-xs mg-r-xxs">
												<?php echo config_item('icon_image'); ?>
											</span>
											<?php else: ?>
											<span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
											</span>
											<?php endif; ?>
					 						<?php echo $page->title;?>
				 						</div>
						 				<div class="icon-container text-right stop-prop pull-right">
					 						<!-- <a href="#" class="bg-yellow pd-all-xxs pull-left block pd-l-xs pd-r-xs mg-r-xxxs">
						 						<span class="icon icon-size-125 fill-light icon-lock stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a> -->
											<a href="<?php echo base_url($page->permalink); ?>" class="bg-yellow pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" target="_blank">
												<span class="icon icon-size-125 fill-light icon-preview stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
											<a href="<?php echo base_url('admin/pages/edit/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
												<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>

											<?php if ($page->status == 1): ?>
											<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
												<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
											<?php else: ?>
											<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
												<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
											<?php endif ?>

											<a href="<?php echo base_url('admin/pages/remove/'.encode($page->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete the page: <?php echo $page->title; ?>, do you wish to continue?">
												<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
				 						</div>
			 						</div>

			 					<?php if ($sub_sub_pages) : ?>
			 						<ul class="manage-list sub-pages" data-rel="manage-pages" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
			 						<?php foreach ($sub_sub_pages as $page) : ?>
			 							<li class="item-parent indent-me list-none" id="sort_<?php echo $page->id; ?>">
											<div class="item-child row bg-light-grey mg-t-xs pd-all-xxs">
						 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
													<span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
													</span>
							 						<?php echo $page->title;?>
						 						</div>
										 		<div class="icon-container text-right stop-prop pull-right">
							 						<!-- <a href="#" class="bg-yellow pd-all-xxs pull-left block pd-l-xs pd-r-xs mg-r-xxxs">
								 						<span class="icon icon-size-125 fill-light icon-lock stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a> -->
													<a href="<?php echo base_url('admin/pages/edit/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
														<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>

													<?php if ($page->status == 1): ?>
													<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
														<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
													<?php else: ?>
													<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
														<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
													<?php endif ?>

													<a href="<?php echo base_url('admin/pages/remove/'.encode($page->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete the page: <?php echo $page->title; ?>, do you wish to continue?">
														<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
						 						</div>
					 						</div>
			 							</li>
			 							<?php endforeach; ?>	
			 						</ul>
			 						<?php endif; ?>
			 					</li>
			 					
	 							<?php endforeach; ?>	
	 						</ul>
	 						<?php endif; ?>
	 					</li>
	 				<?php endforeach; ?>	
		 		</ul>
		 </div>
		<?php else : ?>
		<p>There are no pages</p>
		<?php endif; ?>
	</section>

	<section>
		<div class="row">
		 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white mg-t-md col-sm-60">	
		 		Other Pages
		 	</div>
		</div>

		<?php if ($orphans) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<!-- <div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">lock</div> -->
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>

		 		<ul class="manage-list">
		 		
	 				<?php foreach ($orphans as $page) : ?>
	 					<?php $sub_pages = $this->page->build_list($page->id); ?>
	 					<li class="item-parent list-none">
	 						<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
		 							<?php if ($sub_pages) : ?>
			 						<span class="icon icon-size-75 outline-dark icon-rightarrow more sub-pages-show mg-l-xs mg-r-xxs">
										<?php echo config_item('icon_image'); ?>
									</span>
									<?php else: ?>
									<span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
									</span>
									<?php endif; ?>
			 						<?php echo $page->title;?>
		 						</div>
		 						<div class="icon-container text-right stop-prop pull-right">
			 						<!-- <a href="#" class="bg-yellow pd-all-xxs pull-left block pd-l-xs pd-r-xs mg-r-xxxs">
				 						<span class="icon icon-size-125 fill-light icon-lock stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a> -->
									<a href="<?php echo base_url($page->permalink); ?>" class="bg-yellow pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" target="_blank">
										<span class="icon icon-size-125 fill-light icon-preview stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<a href="<?php echo base_url('admin/pages/edit/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php if ($page->status == 1): ?>
									<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif ?>

									<a href="<?php echo base_url('admin/pages/remove/'.encode($page->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete the page: <?php echo $page->title; ?>, do you wish to continue?">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
		 						</div>
	 						</div>

	 					<?php if ($sub_pages) : ?>
	 						<ul class="manage-list sub-pages">
	 						<?php foreach ($sub_pages as $page) : ?>
	 							<?php $sub_sub_pages = $this->page->build_list($page->id); ?>
	 							<li class="item-parent indent-me list-none">

					 				<div class="item-child row bg-light-grey mg-t-xs pd-all-xxs">
				 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
				 							<?php if ($sub_sub_pages) : ?>
					 						<span class="icon icon-size-75 outline-dark icon-rightarrow more sub-pages-show mg-l-xs mg-r-xxs">
												<?php echo config_item('icon_image'); ?>
											</span>
											<?php else: ?>
											<span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
											</span>
											<?php endif; ?>
					 						<?php echo $page->title;?>
				 						</div>
						 				<div class="icon-container text-right stop-prop pull-right">
					 						<!-- <a href="#" class="bg-yellow pd-all-xxs pull-left block pd-l-xs pd-r-xs mg-r-xxxs">
						 						<span class="icon icon-size-125 fill-light icon-lock stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a> -->
											<a href="<?php echo base_url('admin/pages/edit/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
												<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>

											<?php if ($page->status == 1): ?>
											<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
												<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
											<?php else: ?>
											<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
												<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
											<?php endif ?>

											<a href="<?php echo base_url('admin/pages/remove/'.encode($page->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete the page: <?php echo $page->title; ?>, do you wish to continue?">
												<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>
				 						</div>
			 						</div>

			 					<?php if ($sub_sub_pages) : ?>
			 						<ul class="manage-list sub-pages">
			 						<?php foreach ($sub_sub_pages as $page) : ?>
			 							<li class="item-parent indent-me list-none">
											<div class="item-child row bg-light-grey mg-t-xs pd-all-xxs">
						 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
													<span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
													</span>
							 						<?php echo $page->title;?>
						 						</div>
										 		<div class="icon-container text-right stop-prop pull-right">
							 						<!-- <a href="#" class="bg-yellow pd-all-xxs pull-left block pd-l-xs pd-r-xs mg-r-xxxs">
								 						<span class="icon icon-size-125 fill-light icon-lock stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a> -->
													<a href="<?php echo base_url('admin/pages/edit/'.encode($page->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
														<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>

													<?php if ($page->status == 1): ?>
													<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
														<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
													<?php else: ?>
													<a href="<?php echo base_url('admin/pages/publish/'.$page->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
														<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
													<?php endif ?>

													<a href="<?php echo base_url('admin/pages/remove/'.encode($page->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete the page: <?php echo $page->title; ?>, do you wish to continue?">
														<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
															<?php echo config_item('icon_image'); ?>
														</span>
													</a>
						 						</div>
					 						</div>
			 							</li>
			 							<?php endforeach; ?>	
			 						</ul>
			 						<?php endif; ?>
			 					</li>
			 					

	 							<?php endforeach; ?>	
	 						</ul>
	 						<?php endif; ?>
	 					</li>
	 				<?php endforeach; ?>	
		 		</ul>
		 </div>
		<?php else : ?>
		<p>There are no pages</p>
		<?php endif; ?>
	</section>	

</div>