<?php echo form_open('admin/pages/save_footer/'); ?>

<div class="row action-buttons">
	<div class="mg-t-md mg-b-md pull-right">

		<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
			<span class="icon icon-size-150 icon-reorder">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Order Footer
			</span>
		</a>

		<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
			<span class="icon icon-size-150 icon-reorder">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Stop Ordering
			</span>
		</a>

		<a href="<?php echo base_url('admin/pages'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
		  <span class="icon icon-size-150 outline-light icon-crosscircle">
			<?php echo config_item('icon_image'); ?>
		  </span>
		  <span class="text">
			Cancel
		  </span>
		</a>     

		<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-disk">
			  <?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
			  Save
			</span>
		</button>
	</div>
</div>

<section>
<?php echo Input_helper::heading('Manage Footer'); ?>
<div class="bg-white pd-all-md pd-b-no">


	<!-- <div class="bg-white pd-all-md mg-t-md"> -->
		
		 <div class="row" id="edit-footer">

		<?php if ($footer): ?>
			<?php foreach($footerlinks as $links):?>
					<?php if ($links): ?>
						<?php foreach ($links as $link): ?>
							<div class="edits <?php echo ($new && $new == $link->id)? 'editing' : '' ?> row bg-middle-grey bordered pd-all-sm mg-b-md" id="edit-<?php echo $link->id; ?>">

								<div class="col-xs-60">
									<div class="pull-right">
										<a href="<?php echo base_url('/admin/pages/footer'); ?>" class="bg-red pd-all-xs txt-responsive-xs txt-white block" >
											<span class="icon icon-size-150 outline-light icon-crosscircle stop-prop icon-container">
												<?php echo config_item('icon_image'); ?>
											</span>
											<span class="text txt-white">
												Cancel
											</span>
										</a> 	
									</div>
								</div>
								<div class="col-xs-60">
									<label for="prop-title-<?php echo $link->id; ?>" class="">Title</label>
								</div>
								<div class="col-xs-60">
									<input name="title[<?php echo $link->id ?>]" class="form-control" value="<?php echo set_value('title['.$link->id.']', $link->title); ?>" id="prop-title-<?php echo $link->id; ?>" />
								</div>
								<div class="col-xs-60 pd-t-sm">
									<label for="prop-link-<?php echo $link->id; ?>" class="">Link</label>
								</div>
								<div class="col-xs-60">
									<input name="link[<?php echo $link->id ?>]" class="form-control" value="<?php echo set_value('link['.$link->id.']', $link->permalink); ?>" id="prop-link-<?php echo $link->id; ?>" />
								</div>
									<div class="col-xs-60 pd-t-sm">
									<label for="prop-parent-<?php echo $link->id; ?>" class="">Parent</label>
								</div>
								<div class="col-xs-5">
									<input type="checkbox" name="parent[<?php echo $link->id ?>]" class="form-control" value="<?php echo set_value('parent['.$link->id.']', $link->parent); ?>" <?php echo (isset($link->parent) && $link->parent) ? 'checked' : '';?> id="prop-link-<?php echo $link->id; ?>" />
								</div>
							</div>
						<?php endforeach ?>
					<?php endif ?>
			<?php endforeach; ?>
		<?php endif ?>
		
		</div>
		<!-- </div> -->
	<div class="row dbl-pd">
		<?php if ($footer): ?>
			<?php foreach($footerlinks as $col => $links):
				if($col == 3): ?>
				<div class="clearfix"></div>
			<?php endif; ?>
				<ul class="col-xm-60 col-sm-20 dbl-pd list-none manage-footer listings manage-list mg-b-md" data-url="<?php echo base_url('admin/pages/reorder_footer') ?>">
					<?php if ($links): ?>
						<?php foreach ($links as $link): ?>
							<li id="sortable-<?php echo $link->id; ?>" class="mg-b-xxs ">
								<div class="row item-child row bg-light-grey pd-all-xxs">
									
									<div class="icon-container text-right stop-prop pull-right">
										<a href="" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs mg-r-xxxs block pull-left editable" data-rel="#edit-<?php echo $link->id; ?>">
											<span class="icon icon-size-100 outline-light icon-edit stop-prop icon-container">
												<?php echo config_item('icon_image'); ?>
											</span>
										</a>
										<a href="<?php echo base_url('admin/pages/remove_footer/'.encode($link->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs block pull-left confirm " data-rel="#sortable-<?php echo $link->id; ?>" title="you are about to delete this footer, do you wish to continue?">
											<span class="icon icon-size-100 outline-light icon-cross stop-prop icon-container">
												<?php echo config_item('icon_image'); ?>
											</span>
										</a>
									</div>
									<span class="title txt-responsive-xs"><?php echo $link->title; ?></span>
								</div>
							</li>
						<?php endforeach ?>
						<li id="" class="no-style">
							<a href="<?php echo base_url('admin/pages/new_footer/'.$col); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-xs txt-white pull-left block">
								<span class="icon icon-size-100 outline-light icon-plus txt-responsive-xs">
									<?php echo config_item('icon_image'); ?>
								</span>
								<span class="text txt-white txt-responsive-xs">
									New Footer
								</span>
							</a> 
						</li>
					<?php else: ?>
						<li id="" class="no-style">
							<a href="<?php echo base_url('admin/pages/new_footer/'.$col); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-xs txt-white pull-left block">
								<span class="icon icon-size-100 outline-light icon-plus txt-responsive-xs">
									<?php echo config_item('icon_image'); ?>
								</span>
								<span class="text txt-white txt-responsive-xs">
									New Footer
								</span>
							</a> 
						</li>
					<?php endif ?>
				</ul>
			<?php endforeach; ?>
		<?php endif ?>
		
	</div><!-- END ROW -->

	  </div>

	 <script type="text/javascript">
	$(document).ready(function(){
		if($('.editable').length) {
			$('.edits').not('.editing').hide();
			$('.editable').on('click', function(e){
				e.preventDefault();

				$('.edits').hide();
				var target = $(this).data('rel');
				if($(target).length){
					$(target).show();
					scroll_to('edit-footer');
				}
			});
		}
	});

</script>

</form>
</div>	
  

<!-- <div class="col-xs-60 margin-bottom-large page-heading">
	<div class="row">
		
		<div class="col-xs-60">
		
			<h4 class="margin-top-small">Edit Footer:</h4>
			
			<button class="btn bg-blue pull-right margin-left-medium">Update</button>
			<a href="<?php echo base_url('admin/pages'); ?>" class="btn bg-yellow pull-right margin-left-medium">Cancel</a>
			
		</div>
		
	</div>
</div> -->



<!-- <div class="col-xs-60"> -->
	
	<!--  <div class="row margin-bottom-large">

		<?php if ($footer): ?>
			<?php foreach($footerlinks as $links):?>
					<?php if ($links): ?>
						<?php foreach ($links as $link): ?>
							<div class="edits <?php echo ($new && $new == $link->id)? 'editing' : '' ?> row margin-bottom-large" id="edit-<?php echo $link->id; ?>">
								<div class="col-xs-25"></div>
								<label for="prop-title-<?php echo $link->id; ?>" class="col-xs-5">Title</label>
								<div class="col-xs-20">
									<input name="title[<?php echo $link->id ?>]" class="form-control" value="<?php echo set_value('title['.$link->id.']', $link->title); ?>" />
								</div>

								<label for="prop-link-<?php echo $link->id; ?>" class="col-xs-1">Link</label>
								<div class="col-xs-15">
									<input name="link[<?php echo $link->id ?>]" class="form-control" value="<?php echo set_value('link['.$link->id.']', $link->permalink); ?>" />
								</div>
								<div>
									<a class="slide-out btn btn-danger btn-sm remove-parent" href="<?php echo base_url('admin/pages/remove_footer/'.encode($link->id)); ?>">X</a>
								</div>
							</div>
						<?php endforeach ?>
					<?php endif ?>
			<?php endforeach; ?>
		<?php endif ?>
		
	</div>

	<div class="row margin-bottom-large">

		<?php if ($footer): ?>
			<?php foreach($footerlinks as $col => $links):?>
				<ul class="manage-footer listings manage-list padding-left-small padding-right-small" data-url="<?php echo base_url('admin/pages/reorder_footer') ?>">
					<?php if ($links): ?>
						<?php foreach ($links as $link): ?>
							<li id="sortable-<?php echo $link->id; ?>">
								<i class="handle glyphicon glyphicon-sort pull-right"></i>
								<i class="editable glyphicon glyphicon-pencil" data-rel="#edit-<?php echo $link->id; ?>"></i>
								<?php echo $link->title; ?>
							</li>
						<?php endforeach ?>
						<li id="" class="no-style">
							<a href="<?php echo base_url('admin/pages/new_footer/'.$col); ?>" class="btn bg-green">
								<i class="pull-right glyphicon glyphicon-plus"></i>
								New
							</a>
						</li>
					<?php else: ?>
						<li id="" class="no-style">
							<a href="<?php echo base_url('admin/pages/new_footer/'.$col); ?>" class="btn bg-green">
								<i class="pull-right glyphicon glyphicon-plus"></i>
								New
							</a>
						</li>
					<?php endif ?>
				</ul>
			<?php endforeach; ?>
		<?php endif ?>
		
	</div><!-- END ROW --> 

