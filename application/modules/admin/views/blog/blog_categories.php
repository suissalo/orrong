<?php echo form_open(); ?>

<div class="row action-buttons">

		<div class="mg-t-md mg-b-md pull-right">

			<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a>

			<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-disk">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Save
					</span>
			</button>
		</div>
	</div>	


	<section>
		<div class="row">
			<div class="col-xs-60 col-sm-60">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		Categories
			 	</div>
		 	</div>
		</div>
	
		<?php if ($categories): ?>
		<div class="bg-white row pd-all-md">
			<ul class="manage-list" data-url="<?php echo base_url('admin/blog/update_category_order'); ?>">
		    
		    <?php foreach ($categories as $key => $category): ?>

				<li class="item-parent list-none" id="sort_<?php echo $category->id; ?>">
		 			<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 				<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs pd-l-xxs">
						<?php echo $category->name ?>
						</div>

			            <div class="pull-right">										
							<!-- <a href="<?php echo base_url('admin/blog/edit/'.encode($category->id)); ?>" class="btn btn-success">Edit <i class="glyphicon glyphicon-pencil"></i></a> -->
							<small>Category </small>
							<select style="width:auto;" name="category[<?php echo $category->id; ?>]"  class="btn-group-select mg-r-md" >
								<option value="0">-- Master --</option>
								<?php foreach ($all_categories as $key => $row):
									if ($row->id != $category->id): ?>
										<option value="<?php echo $row->id; ?>" <?php echo set_select('category[' . $category->id . ']', $row->id, (($category->parent_id == $row->id)? TRUE : FALSE)) ?>><?php echo $row->name ?></option>
									<?php endif; ?>							
								<?php endforeach ?>
							</select>

							<a title="You are about to delete the category: <?php echo $category->name; ?>, do you wish to continue?" href="<?php echo base_url('admin/blog/remove_category/'.encode($category->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-right block confirm">
								<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>	
							
							<div class="clearfix"></div>

						</div>

					<div class="clearfix"></div>

					<?php if ($next = $this->posts->build_category_list($category->id)): ?>
						<ul class="manage-list mg-t-sm" data-url="<?php echo base_url('admin/blog/update_category_order'); ?>" >
						    <?php foreach ($next as $key => $sub_category): ?>

						        <li class="item-parent list-none" id="sort_<?php echo $sub_category->id; ?>">
									<div class="item-child row bg-light-grey pd-all-xxs">
						 				<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
										<?php echo $sub_category->name ?>
										</div>

							            <div class="pull-right">

															
											<!-- <a href="<?php echo base_url('admin/blog/edit/'.encode($category->id)); ?>" class="btn btn-success">Edit <i class="glyphicon glyphicon-pencil"></i></a> -->
											<select style="width:auto;" name="category[<?php echo $sub_category->id; ?>]"  class="btn-group-select  mg-r-md" role="group" >
												<option value="0" <?php echo set_select('category[' . $sub_category->id . ']', $row->id, (($sub_category->parent_id == 0)? TRUE : FALSE)); ?>>-- Master --</option>
												<?php foreach ($all_categories as $key => $row):
													if ($row->id != $sub_category->id): ?>
														<option value="<?php echo $row->id; ?>" <?php echo set_select('category[' . $sub_category->id . ']', $row->id, (($sub_category->parent_id == $row->id)? TRUE : FALSE)); ?>><?php echo $row->name ?></option>
													<?php endif; ?>
												<?php endforeach ?>										
											</select>

											<a title="You are about to delete the category: <?php echo $sub_category->name; ?>, do you wish to continue?" href="<?php echo base_url('admin/blog/remove_category/'.encode($sub_category->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-right block confirm">
												<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
													<?php echo config_item('icon_image'); ?>
												</span>
											</a>

											<div class="clearfix"></div>

										</div>

										<div class="clearfix"></div>

										<?php if ($nextnext = $this->posts->build_category_list($sub_category->id)): ?>
											<ul class="manage-list mg-t-sm" data-url="<?php echo base_url('admin/blog/update_category_order'); ?>" >
											    <?php foreach ($nextnext as $key => $sub_category): ?>

											        <li class="item-parent list-none" id="sort_<?php echo $sub_category->id; ?>">
														<div class="item-child row bg-light-grey pd-all-xxs">
											 				<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
															<?php echo $sub_category->name ?>
															</div>

												            <div class="pull-right">

																				
																<!-- <a href="<?php echo base_url('admin/blog/edit/'.encode($category->id)); ?>" class="btn btn-success">Edit <i class="glyphicon glyphicon-pencil"></i></a> -->
																<select style="width:auto;" name="category[<?php echo $sub_category->id; ?>]"  class="btn-group-select  mg-r-md" role="group" >
																	<option value="0" <?php echo set_select('category[' . $sub_category->id . ']', $row->id, (($sub_category->parent_id == 0)? TRUE : FALSE)); ?>>-- Master --</option>
																	<?php foreach ($all_categories as $key => $row):
																		if ($row->id != $sub_category->id): ?>
																			<option value="<?php echo $row->id; ?>" <?php echo set_select('category[' . $sub_category->id . ']', $row->id, (($sub_category->parent_id == $row->id)? TRUE : FALSE)); ?>><?php echo $row->name ?></option>
																		<?php endif; ?>
																	<?php endforeach ?>										
																</select>

																<a title="You are about to delete the category: <?php echo $sub_category->name; ?>, do you wish to continue?" href="<?php echo base_url('admin/blog/remove_category/'.encode($sub_category->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-right block confirm">
																	<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
																		<?php echo config_item('icon_image'); ?>
																	</span>
																</a>

															</div>
														</div>
													</li>
												<?php endforeach ?>
											</ul>
										<?php endif ?>

									</div>
								</li>
							<?php endforeach ?>
						</ul>
					<?php endif ?>
					</div>
		        </li>
		    <?php endforeach ?>
		    </ul>
		</div>
		<?php else: ?>
		<p>There are no Blog Categories.</p>
		<?php endif; ?>

	</section>
</form>