<?php /* if ($categories): ?>
    <?php foreach ($categories as $key => $category): ?>
        <li>
            <input id="category-<?php echo $category->id ?>" type="checkbox" <?php echo set_checkbox('category[]', $category->id, in_array($category->id, $assigned_categories)); ?> value="<?php echo $category->id ?>" name="category[]">
            <label for="category-<?php echo $category->id ?>"><?php echo $category->name ?></label>
        </li>
    <?php endforeach ?>
<?php endif; */ ?>


<?php if ($categories): ?>

    <?php foreach ($categories as $key => $category): ?>
        <li>
            <input id="category-<?php echo $category->id ?>" type="checkbox" <?php echo set_checkbox('category[]', $category->id, in_array($category->id, $assigned_categories)); ?> value="<?php echo $category->id ?>" name="category[]" <?php echo (in_array($category->id, $assigned_categories)) ? 'checked="checked"' : ''; ?> >
            <label for="category-<?php echo $category->id ?>"><?php echo $category->name ?></label>
        </li>
    <?php endforeach ?>

<?php endif ?>