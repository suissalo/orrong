


<div>

	<div class="row action-buttons">

		<div class="mg-t-md mg-b-md pull-right">

			<a href="<?php echo base_url('admin/blog/new_post'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New Post
				</span>
			</a>
		</div>
	</div>	

	<section>
		<?php echo form_open(full_url(), array('id' => 'filter-form')); ?>
			<?php if ($categories): ?>
			<div class="row">
				<div class="col-xs-60 col-sm-60">
				 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">
				 		
				 		<div class="col-sm-20 pull-right txt-light">
							<select name="category" class="bg-white filter-categories">
								<option value="">All Categories</option>
								<?php foreach ($categories as $key => $category): ?>
									<option value="<?php echo $category->id; ?>" <?php echo set_select('category', $category->id); ?>><?php echo $category->name; ?></option>
								<?php endforeach ?>
							</select>	 		 	
						</div>
						<div class="col-sm-20 pull-right txt-white txt-right mg-t-xxs pd-r-sm">Category</div>
						<div class="col-sm-1 pull-right txt-light"></div>
						
				 	</div>
			 	</div>
			</div>
			<?php endif; ?>
		</form>

		<?php if ($articles) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">view</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>

		 		<ul class="manage-list">
		 		
	 				<?php foreach ($articles as $article) : ?>
	 					<li class="item-parent list-none" id="sort_<?php echo $article->id; ?>">
	 						<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs pd-l-xxs">
									<!-- <span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
									</span> -->
			 						<?php echo $article->title;?>
		 						</div>
		 						<div class="icon-container text-right stop-prop pull-right">
		 							<a href="<?php echo base_url(blog_url($article->permalink)); ?>" class="bg-yellow pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" target="_blank">
										<span class="icon icon-size-125 fill-light icon-preview stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<a href="<?php echo base_url('admin/blog/edit/'.encode($article->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php if ($article->published == 1): ?>
									<a href="<?php echo base_url('admin/blog/publish/'.$article->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/blog/publish/'.$article->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif; ?>

									<a href="<?php echo base_url('admin/blog/remove/'.encode($article->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete <?php echo $article->title; ?>, do you wish to continue?">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
		 						</div>
	 						</div>

	 					</li>
	 				<?php endforeach; ?>	
		 		</ul>
		 </div>
		<?php else: ?>
			<p>There are no Blog Posts.</p>
		<?php endif; ?>
	</section>

</div>

