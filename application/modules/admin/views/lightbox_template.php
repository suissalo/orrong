<!DOCTYPE html>
<html lang="en">
    <head>
    <meta name="author" content="Barking Bird" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CMS // Admin</title>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/library.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/light.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/ckfinder/ckfinder.js"></script>
    
    <link href="<?php echo base_url(); ?>assets/admin/styles/light.css" rel="stylesheet" type="text/css" />
    <?php if(isset($map) && $map['js']) { echo $map['js']; } ?>
</head>
<body>
<div class="mainbody" >
	<?php echo $page_content; ?>
</div>
<?php echo get_message(); ?>
<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
</script>
</body>
</html>