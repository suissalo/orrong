<?php echo form_open_multipart('admin/settings/save/', array('class' => '')); ?>

	<div>

		<div class="row action-buttons">
			<div class="mg-t-md mg-b-md pull-right">	

				<button class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-disk">
						<?php echo config_item('icon_image'); ?>
					</span>
					Save
				</button>

			</div>
		</div>	

		<section>
			<div class="row">
				<div class="col-xs-60">
				 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white">	
				 		Settings
				 	</div>
			 	</div>
			</div>
			<div class="row">
				<div class="col-xs-60 pd-all-md bg-white">

				<?php if ($settings) : ?>
					<?php foreach ($settings as $setting): ?>
					<div class="col-xs-60 col-sm-60 pd-b-xs ">
						<div class="">

							<label for="title"><?php echo $setting->title; ?></label>
							<?php if (isset($setting->type) && $setting->type == 'file' && isset($setting->value) && !empty($setting->value) ) : ?>
					
								<a href="<?php echo base_url($setting->value); ?>" class="txt-white bg-seagreen pd-all-xxs  txt-responsive-xxs txt-white" style="display: inline-block" target="_blank">
									<span class="icon icon-size-150 fill-light icon-view">
									<img src="<?php echo base_url('assets/admin/images/icons.png'); ?>" />
									</span>
									View
								</a>
								
							
							<?php endif; ?>

						</div>
						<div class="">
							<input class="" id="prop-settings-<?php echo $setting->id; ?>" type="<?php echo $setting->type; ?>" name="setting[<?php echo $setting->id; ?>]" id="prep-<?php echo $setting->id; ?>" value="<?php echo set_value('setting['.$setting->id.']' , $setting->value); ?>"/>
						</div>	
					</div>	
					<?php endforeach; ?>
				<?php else: ?>
					<p>There are no Settings.</p>
				<?php endif; ?>
				</div>
			</div>
		</section>

	</div>

</form>