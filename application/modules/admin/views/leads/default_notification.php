<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<style type="text/css">
	table { margin: auto; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
	table tr { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
	table td { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
	img { border:none; outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
	#outlook a { padding: 0; }
	html { margin: 0px; padding: 0px; } 
	body { 
		width: 100% !important; -webkit-text-size-adjust: none; -ms-text-size-adjust: 100%; margin: 0; padding: 0;
		-webkit-font-smoothing: antialiased;
		-moz-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-smoothing: antialiased; 
	} 
	.ExternalClass { width:100%; }
	/* stop iphone resizing fonts */
	div, p, a, li, td { -webkit-text-size-adjust: none; }
	</style>

</head>
<body style="margin: 0px; padding: 0px; background-color: #e5e5e5; width: 100%; text-align: center; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222;">
	<!-- BODY TABLE --> 
	<center>
		<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor="#e5e5e5" style="width: 100%; background: #e5e5e5 none; margin: auto; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222; text-align: center; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin: auto;">
			<tbody>
				<tr><td height="20"></td></tr>
				<tr>
					<td align="center" valign="top">
					<!-- BODY TABLE --> 

						<!-- CONTENT TABLE --> 
						<table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="background-color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222; text-align: left; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin: auto;">
							<tbody>
								<tr>
									<td align="center" valign="top" style="font-size: 0px; line-height: 0px; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
									<!-- CONTENT TABLE --> 

										<!-- HEADING --> 
										<table width="560" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 24px; line-height: 1.25; color: #222222; text-align: left; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin: auto;"><tbody>
											<tr><td height="20"></td></tr>
											<tr>
												<td>
													<h1 style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 24px; line-height: 1.25; color: #222222; text-align: left; margin: 0px; padding: 0px;">You have received a new email enquiry from your {subject}</h1>
												</td>
											</tr>
											<tr><td height="20"></td></tr>
										</tbody></table>
										<!-- HEADING --> 

										<table width="560" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222; text-align: left; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin: auto;"><tbody>
											<tr><td height="20"></td></tr>
											<tr>
												<td valign="top" align="left" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222; vertical-align: top; text-align: left;">
													{all}
												</td>
											</tr>
											<tr><td height="40"></td></tr>
										</tbody></table>

										<!-- FOOTER -->
										<table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" style="background-color: #333333; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff; text-align: left; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin: auto;"><tbody>
											<tr><td colspan="3" height="20"></td></tr>
											<tr>
												<td width="20" style="width: 20px;">&nbsp;</td>
												<td>
													<a href="http://www.barkingbird.com.au" target="_blank" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; border: 0; outline: 0;">
														<img src="http://assets.barkingbird.com.au/media/autoresponders/barkingbird/leads/barking-bird-logo-white.png" border="0" height="20" alt="Barking Bird" />
													</a>
												</td>
												<td width="20" style="width: 20px;">&nbsp;</td>
											</tr>
											<tr><td colspan="3" height="10"></td></tr>
											<tr>
												<td width="20" style="width: 20px;">&nbsp;</td>
												<td>
													<strong>Powered by Barking Bird</strong><br/>
													Ground Floor, 10 Grattan St, Prahran, VIC 3181, Australia<br/>
												</td>
												<td width="20" style="width: 20px;">&nbsp;</td>
											</tr>
											<tr><td colspan="3" height="10"></td></tr>
											<tr>
												<td width="20" style="width: 20px;">&nbsp;</td>
												<td>
													<strong>P</strong>&nbsp; <a href="tel:1300660177" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; ">1300 660 177</a><br/>
													<strong>E</strong>&nbsp;&nbsp; <a href="mailto:peck@barkingbird.com.au" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; ">peck@barkingbird.com.au</a><br/>
													<strong>W</strong>&nbsp; <a href="http://www.barkingbird.com.au" target="_blank" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; ">www.barkingbird.com.au</a><br/>
												</td>
												<td width="20" style="width: 20px;">&nbsp;</td>
											</tr>
											<tr><td colspan="3" height="20"></td></tr>
										</tbody></table>
										<!-- FOOTER --> 

									<!-- CONTENT TABLE -->
									</td>
								</tr>
							</tbody>
						</table>
						<!-- CONTENT TABLE -->

					<!-- BODY TABLE --> 
					</td>
				</tr>
				<tr><td height="20"></td></tr>
			</tbody>
		</table>
	</center>
	<!-- BODY TABLE --> 
</body>
</html>