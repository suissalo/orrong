<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<style type="text/css">
	table { margin: auto; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
	table tr { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
	table td { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
	img { border:none; outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
	#outlook a { padding: 0; }
	html { margin: 0px; padding: 0px; } 
	body { 
		width: 100% !important; -webkit-text-size-adjust: none; -ms-text-size-adjust: 100%; margin: 0; padding: 0;
		-webkit-font-smoothing: antialiased;
		-moz-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-smoothing: antialiased; 
	} 
	.ExternalClass { width:100%; }
	/* stop iphone resizing fonts */
	div, p, a, li, td { -webkit-text-size-adjust: none; }
	</style>

</head>
<body style="margin: 0px; padding: 0px; background-color: #e5e5e5; width: 100%; text-align: center; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222;">
<?php echo $message; ?>
</body>
</html>