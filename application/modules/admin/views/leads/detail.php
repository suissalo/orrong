
<div class="row action-buttons">
	<div class="pull-right mg-t-md mg-b-md">
	
		<?php if (access_menu('admin/leads')) : ?>
		
		<a href="<?php echo base_url('admin/leads'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
			<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
			Close
			</span>
		</a>
		<a href="<?php echo base_url('admin/leads/delete_lead/'.encode($lead->id)); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
			<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
			Delete
			</span>
		</a>
		<?php else : ?>
		
		<a href="<?php echo base_url('admin/dashboard'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
			<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
			Close
			</span>
		</a>
		
		<?php endif; ?>
	
	</div>
</div>

<section>
	<div class="row">
		<div class="col-xs-60 col-sm-60">
		 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
		 		Details
		 	</div>
	 	</div>
	</div>

	<?php if ($lead && $lead->content) : ?>
	<div class="bg-white row pd-all-md">
        <div class="tab-content" >
			<div class="row dbl-pd">
                <div class="col-xs-60 col-sm-30 dbl-pd border-right-black mg-b-xs">
					<div class="col-xs-60">
                        <label style="font-weight: bold;">Date</label>
					</div>
					<div class="col-xs-60">
						<span><?php echo $lead->date_created;?></span>
					</div>
				</div>
                <?php foreach($lead->content as $field_name => $field_value):?>
				<div class="col-xs-60 col-sm-60 dbl-pd border-right-black mg-b-xs">
					<div class="col-xs-60">
						<label for="<?php echo $field_name; ?>" style="font-weight: bold;"><?php echo ucfirst($field_name); ?></label>
					</div>
					<div class="col-xs-60">
						<span><?php echo $field_value;?></span>
					</div>
				</div>
                <?php endforeach; ?>
               
			</div>
		</div>
	</div>
	<?php else: ?>
	<p>There are no details.</p>
	<?php endif; ?>	
</section>