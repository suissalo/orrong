<div class="hidden" id="recipient-template">
	<div class="row new-recipient">
		<div class="col-xs-60 std-pd mg-b-sm">
			<a href="#" onClick="$(this).closest('.new-recipient').remove();" class="bg-red pd-all-xxs pd-t-no pd-b-xxxs pull-right block" >
				<span class="icon icon-size-100 outline-light icon-cross stop-prop icon-container">
					<?php echo config_item('icon_image'); ?>
				</span>
			</a>
			<label>Recipient Email</label>
			<input type="text" name="notification_recipients[]" value="" />
		</div>	
	</div>
</div>

<?php echo form_open_multipart(); ?>

	<div class="row action-buttons">
		<div class="pull-right mg-t-md mg-b-md">
			<a href="<?php echo base_url('/admin/leads'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-crosscircle">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Close
				</span>
			</a>

			<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-disk">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Save
				</span>
			</button>
		</div>
	</div>

	<section>

		<div class="row">
			<div class="col-xs-60 col-sm-60 mg-t-md">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		<?php echo $form->name; ?>
			 	</div>
		 	</div>
		</div>	

		<div class="tab-container bg-white pd-all-md">
			
			<div class="mg-b-md">
				<a href="#details" data-form="" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Details</a>
				<a href="#notification" data-form="" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Notification</a>
				<a href="#autoresponder" data-form="" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Autoresponder</a>
				<div class="clearfix"></div>
				<div class="tab-link-divider"></div>
			</div>

			<div class="tab-content row dbl-pd" id="details">
				<div class="col-xs-60 dbl-pd">

					<div class="row">
						<div class="col-xs-60 col-sm-40 std-pd mg-b-sm">
							<label>Name</label>
							<input type="text" name="name" value="<?php echo set_value('name', $form->name); ?>" />
						</div>
						<div class="col-xs-60 col-sm-20 std-pd mg-b-sm">
							<label>Form Secret ID</label>
							<input type="text" class="bg-grey" disabled value="<?php echo $form->secret_id; ?>" />
						</div>
					</div>				

				</div>
				<div class="clearfix"></div>
			</div>

			<div class="tab-content row dbl-pd" id="notification">
				<div class="col-xs-60 dbl-pd">

					<!-- <div class="row">
						<div class="col-xs-60 std-pd">
							<hr />
						</div>
					</div> -->

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<label class="pull-right">
								<input type="checkbox" name="notification_active" value="1" <?php echo set_checkbox ('notification_active', 1, ( (boolean) @$form->notification_active == 1) ); ?> />
								Active
							</label>
							<h2 class="txt-responsive-md">Notification Email</h2>
						</div>	
					</div>

					<div class="row">
						<div class="col-xs-30 std-pd mg-b-sm">
							<label>From Name</label>
							<input type="text" name="notification_from_name" value="<?php echo set_value('notification_from_name', $form->notification_from_name); ?>" />
						</div>	
						<div class="col-xs-30 std-pd mg-b-sm">
							<label>From Email</label>
							<input type="text" name="notification_from_email" value="<?php echo set_value('notification_from_email', $form->notification_from_email); ?>" />
						</div>	
					</div>

					

					<div id="recipients">
						<?php if ($form->notification_recipients) : ?>
						
						<?php $rcount = 0; foreach ($form->notification_recipients as $recipient) : ?>
						
						<div class="row new-recipient">
							<div class="col-xs-60 std-pd mg-b-sm">
								<a href="#" onClick="$(this).closest('.new-recipient').remove();" class="bg-red pd-all-xxs pd-t-no pd-b-xxxs pull-right block" >
									<span class="icon icon-size-100 outline-light icon-cross stop-prop icon-container">
										<?php echo config_item('icon_image'); ?>
									</span>
								</a>
								<label>Recipient Email</label>
								<input type="text" name="notification_recipients[]" value="<?php echo $recipient; ?>" />
							</div>	
						</div>
						
						<?php $rcount++; endforeach; ?>
						
						<?php else : ?>
						
						<div class="row new-recipient">
							<div class="col-xs-60 std-pd mg-b-sm">
								<a href="#" onClick="$(this).closest('.new-recipient').remove();" class="bg-red pd-all-xxs pd-t-no pd-b-xxxs pull-right block" >
									<span class="icon icon-size-100 outline-light icon-cross stop-prop icon-container">
										<?php echo config_item('icon_image'); ?>
									</span>
								</a>
								<label>Recipient Email</label>
								<input type="text" name="notification_recipients[]" value="" />
							</div>	
						</div>
						
						<?php endif; ?>
					</div>

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<a href="#" onClick="$('#recipient-template .new-recipient').clone().appendTo('#recipients');" class="txt-white bg-seagreen pd-all-xxs pd-l-xs pd-r-xs txt-responsive-xs txt-white pull-right block mg-l-sm">
								<span class="icon icon-size-100 outline-light icon-plus">
									<?php echo config_item('icon_image'); ?>
								</span>
								<span class="text">
									Add Recipient
								</span>
							</a>
						</div>	
					</div>

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<label>Subject</label>
							<input type="text" name="notification_subject" value="<?php echo set_value('notification_subject', $form->notification_subject); ?>" />
						</div>	
					</div>

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<label style="height: auto;">Message <small>&nbsp; &nbsp; Available replacements are: {subject}, {lead_date}, {all} and any field name submitted to the form enclosed in '{' and '}'</small></label>
							<textarea class="ckeditor" name="notification_message"><?php echo set_value('notification_message', $form->notification_message); ?></textarea>
							<?php /*<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("notification_message", set_value('notification_message', $form->notification_message)); ?></div>*/?>
						</div>	
					</div>

				</div>
				<div class="clearfix"></div>
			</div>

			<div class="tab-content row dbl-pd" id="autoresponder">
				<div class="col-xs-60 dbl-pd">

					<!-- <div class="row">
						<div class="col-xs-60 std-pd">
							<hr />
						</div>
					</div> -->

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<label class="pull-right">
								<input type="checkbox" name="responder_active" value="1" <?php echo set_checkbox ('responder_active', 1, ( (boolean) @$form->responder_active == 1) ); ?> />
								Active
							</label>
							<h2 class="txt-responsive-md">Autoresponder Email</h2>
						</div>	
					</div>
						
					<div class="row">
						<div class="col-xs-30 std-pd mg-b-sm">
							<label>From Name</label>
							<input type="text" name="responder_from_name" value="<?php echo set_value('responder_from_name', $form->responder_from_name); ?>" />
						</div>	
						<div class="col-xs-30 std-pd mg-b-sm">
							<label>From Email</label>
							<input type="text" name="responder_from_email" value="<?php echo set_value('responder_from_email', $form->responder_from_email); ?>" />
						</div>	
					</div>

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<label>Subject</label>
							<input type="text" name="responder_subject" value="<?php echo set_value('responder_subject', $form->responder_subject); ?>" />
						</div>	
					</div>

					<div class="row">
						<div class="col-xs-60 std-pd mg-b-sm">
							<label  style="height: auto;">Message <small>&nbsp; &nbsp; Available replacements are: {subject}, {lead_date}, {all} and any field name submitted to the form enclosed in '{' and '}'</small></label>
							<textarea class="ckeditor" name="responder_message"><?php echo set_value('responder_message', $form->responder_message); ?></textarea>
						</div>	
					</div>

				</div>
				<div class="clearfix"></div>

				<div class="col-xs-60 dbl-pd">
					<h2 class="txt-responsive-md">Attachments To Send With Autoresponder</h2>
					<ul class="manage-list manage-galleries row std-pd" data-rel="gallery_images" data-url="<?php echo base_url('admin/ajax/add_autoresponder_attachment') ?>">

						<?php if(isset($form->attachments) && $form->attachments != '') : ?>
							<?php $attachments = json_decode($form->attachments, true); //view($attachments); ?>
							<?php foreach ($attachments as $key => $attachment): ?>
								<li class="col-xs-60 col-sm-30 col-md-20 std-pd" id="sort_<?php echo $key; ?>">
										<?php $this->load->view('admin/_partials/autoresponder_attachment_media', array('form_id' => $form->secret_id, 'attachment_id' => $key, 'attachment' => $attachment)); ?>
								</li>
							<?php endforeach ?>
						<?php endif ?>

						<li class="upload-new-image col-xs-60 col-sm-30 col-md-20 std-pd ordering-hide" data-name="file">
							<div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">

								<div class="clearfix pd-t-xxxs"></div>
								
								<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
									<div id="galery-uploader" class="dropzone multi-uploader" data-title="Upload Attachment" data-name="file" data-action="<?php echo base_url('admin/leads/upload_autoresponder_attachment/'.$form->secret_id); ?>">
										<div class="fallback">
							                <img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
							            </div>
							            <div class="dz-default dz-message">
							                Drag files here to upload (or click) to upload a new Attachment
							                
							            </div>
						            </div>
								</div>

								<div class="clearfix pd-t-sm"></div>

								<div class="clearfix"></div>

							</div>
						</li>
						
					</ul>
				</div>
			</div>
	
		</div>

	</section>

</form>    
