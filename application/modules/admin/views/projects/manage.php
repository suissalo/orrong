
<div>

	<div class="row action-buttons">

		<div class="mg-t-md mg-b-md pull-right">	

			<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a>

			<a href="<?php echo base_url('admin/projects/new_project'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New Project
				</span>
			</a>

		</div>
	</div>	

	<section>
		<div class="row">
			<div class="col-xs-60 col-sm-60">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		Projects
			 	</div>
		 	</div>
		</div>

		<?php if ($projects) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<!-- <div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">lock</div> -->
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">view</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>

		 		<ul class="manage-list" data-rel="manage-projects" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
		 		
	 				<?php foreach ($projects as $project) : ?>
	 					<li class="item-parent list-none" id="sort_<?php echo $project->id; ?>">
	 						<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
			 						<?php echo $project->title;?> <small><?php echo $project->width ?'(Full width)':'';?></small>
		 						</div>
		 						<div class="icon-container text-right stop-prop pull-right">
									<a href="<?php echo base_url('projects/'.$project->permalink); ?>" class="bg-yellow pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" target="_blank">
										<span class="icon icon-size-125 fill-light icon-preview stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<a href="<?php echo base_url('admin/projects/edit/'.encode($project->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php if ($project->status == 1): ?>
									<a href="<?php echo base_url('admin/projects/publish/'.$project->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/projects/publish/'.$project->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif ?>

									<a href="<?php echo base_url('admin/projects/remove/'.encode($project->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete <?php echo $project->title; ?>, do you wish to continue?">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
		 						</div>
	 						</div>

	 					
	 					</li>
	 				<?php endforeach; ?>	
		 		</ul>
		 </div>
		<?php else : ?>
		<p>There are no projects</p>
		<?php endif; ?>
	</section>
</div>