<?php echo form_open_multipart('admin/projects/save_new'); ?>

<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('admin/projects'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a>     

				<button  type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
						<span class="icon icon-size-150 outline-light icon-disk">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Save
						</span>
				</button>
		</div>
</div>

<section>
<?php echo Input_helper::heading('New Project'); ?>


	 <div class="tab-container bg-white pd-all-md">
			
			<div class="mg-b-md">
				<a href="#details" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Project Details</a>
				<a href="#hero-gallery" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Hero Gallery</a>
				<div class="txt-responsive-xxs text-right pull-right col-md-14 pd-all-xxs pd-t-sm">Please complete ALL tabs</div>
				<div class="clearfix"></div>
				<div class="tab-link-divider"></div>
			</div>

			<div class="tab-content" id="details">
			
				<div class="row dbl-pd">

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Title*</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="title" id="title" value="<?php echo set_value('title'); ?>"/>
						</div>
					</div>

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="intro">Intro  <?php echo tip('3-5 lines or approx 160 chars');?></label>
						</div>

						<div class="col-xs-60">
							<textarea class="mg-b-sm" name="intro" id="intro" >
								<?php echo set_value('intro'); ?>
							</textarea>
						</div>
						<div class="col-xs-60">
							<label for="subtitle">Subtitle</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="subtitle" id="subtitle" value="<?php echo set_value('subtitle'); ?>"/>
						</div>
					</div>

					<div class="row mg-t-sm">
						<label class="col-xs-60 col-sm-30">Hero Image</label>
					</div>

					<div class="col-xs-60 col-sm-30 row bg-white pd-all-sm dashed-bordered new-image-box">
						<div class="clearfix pd-t-xxxs"></div>

						<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
							<img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
						</div>

						<div class="clearfix pd-t-sm">
						</div>

						<div class="row txt-responsive-xs inherit mg-t-xxs">
							<div class="col-xs-60 col-sm-30 inherit">
								<label for="hero_image" class="pd-all-no auto-height">Hero Image</label>
							</div>

							<div class="col-xs-60 col-sm-30 inherit txt-light">
								<input type="file" name="hero_image" id="hero_image" value="<?php echo set_value('hero_image');?>" />
							</div>
						</div>

						<div class="clearfix">
						</div>
					</div>

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<label for="width" class="col-xs-60"><input type="checkbox" name="width" id="width" value="1" <?php echo set_checkbox('width', '1', 0); ?> />&nbsp; &nbsp;Make this project full width? </label>
					</div>

					<?php if(isset($categories) && is_array($categories)) :?>
						<div class="col-xs-60 col-sm-30 dbl-pd">
							<label for="prop-excerpt">Category</label>

							<select name="category[]">
								<?php foreach ($categories as $key => $category): ?>
								<option value="<?php echo $category->id ?>" <?php echo set_select('category[]', $category->id); ?> ><?php echo $category->name; ?></option>
								<?php endforeach ?>
							</select>
						</div>
					<?php endif ?> 

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Address</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="address" id="address" value="<?php echo set_value('address'); ?>"/>
						</div>
					</div>


					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Type</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="type" id="type" value="<?php echo set_value('type'); ?>"/>
						</div>
					</div>


					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Budget</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="budget" id="budget" value="<?php echo set_value('budget'); ?>"/>
						</div>
					</div>


					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Commenced</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="commenced" id="commenced" value="<?php echo set_value('commenced'); ?>"/>
						</div>
					</div>


					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Completed</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="completed" id="completed" value="<?php echo set_value('completed'); ?>"/>
						</div>
					</div>

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Architect</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="architect" id="architect" value="<?php echo set_value('architect'); ?>"/>
						</div>
					</div>

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Interior Designer</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="interiors" id="interiors" value="<?php echo set_value('interiors'); ?>"/>
						</div>
					</div>

					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="title">Landscaper</label>
						</div>

						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="landscaper" id="landscaper" value="<?php echo set_value('landscaper'); ?>"/>
						</div>
					</div>

				</div>

				<div class="row dbl-pd">	
					<div class="col-xs-60 col-sm-60 dbl-pd ">
						<label for="content">Page Content</label>
						<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("detail", set_value('detail')); ?></div>
					</div>
				</div>

				<div class="row dbl-pd">
					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="author">Testimonial Author</label>
						</div>
						<div class="col-xs-60">
							<input class="mg-b-sm" type="text" name="author" id="author" value="<?php echo set_value('author'); ?>"/>
						</div>
					</div>
					<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
						<div class="col-xs-60">
							<label for="testimonial">Testimonial</label>
						</div>
						<div class="col-xs-60">
							<textarea class="mg-b-sm" name="testimonial" id="testimonial" >
								<?php echo set_value('testimonial'); ?>
							</textarea>
						</div>
						</div>
					</div>
				</div>

				<div class="row dbl-pd">
					<div class="col-xs-60 dbl-pd">
						<label for="meta-title">Meta Title</label>
					</div>

					<div class="col-xs-60 dbl-pd">
						<input class="" type="text" name="meta_title" id="meta-title" <?php echo set_value('meta_title'); ?>/>
					</div>
				</div>

				<div class="row dbl-pd mg-t-md">
					<div class="col-xs-30 dbl-pd">
						<label class="col-xs-60" for="meta_keywords">Meta Keywords <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 8 words separated by a comma)</span></label> 
						<textarea name="meta_keywords" id="meta_keywords" ><?php echo set_value('meta_keywords'); ?></textarea>
					</div>

					<div class="col-xs-30 dbl-pd">
						<label class="col-xs-60" for="meta_description">Meta Description <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 255 characters)</span></label>
						<textarea name="meta_description" id="meta_description"><?php echo set_value('meta_description'); ?></textarea>
					</div>
				</div>

				<div class="row dbl-pd">
					<div class="col-xs-60 dbl-pd">
						<div class="pull-left">
							<label for="permalink">Link</label>
						</div>
					 </div>

					<div class="col-xs-60 auto-overflow dbl-pd">
						<span class="demo-url input"><?php echo base_url(set_value('permalink')); ?></span>
					</div>
				</div>
			</div>
			
			<div class="tab-content" id="hero-gallery">
				<div class="row dbl-pd mg-t-md">
					<div class="col-xs-60">
						Please save the project first before adding images. 
					</div>
				</div>
			</div>

		 </div>

	</section>

	<input type="hidden" value="<?php echo set_value('permalink'); ?>" name="permalink" id="permalink" />
</form>
