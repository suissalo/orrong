<?php echo form_open_multipart('admin/projects/save_edit/'.encode($project->id)); ?>

<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('admin/projects'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a>     

				<button  type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
						<span class="icon icon-size-150 outline-light icon-disk">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Save
						</span>
				</button>
		</div>
</div>

<section>
<?php echo Input_helper::heading($project->title); ?>


	<div class="tab-container bg-white pd-all-md">

		<div class="mg-b-md">
			<a href="#details" class="tab-link txt-responsive-sm col-md-9 col-lg-9 pd-all-xs">Details</a>
			<a href="#hero-gallery" class="tab-link txt-responsive-sm col-md-10 col-lg-10 pd-all-xs">Hero Gallery</a>
			<div class="clearfix"></div>
			<div class="tab-link-divider"></div>
		</div>

		<div class="tab-content" id="details">

			<div class="row dbl-pd">

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="title">Title</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="title" id="title" value="<?php echo set_value('title', $project->title); ?>"/>
					</div>
					<div class="col-xs-60">
						<label for="subtitle">Subtitle</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="subtitle" id="subtitle" value="<?php echo set_value('subtitle', $project->subtitle); ?>"/>
					</div>
					<div class="col-xs-60">
						<label for="intro">Intro <?php echo tip('3-5 lines or approx 160 chars');?></label>
					</div>

					<div class="col-xs-60">
						<textarea class="mg-b-sm" name="intro" id="intro" ><?php echo set_value('intro', $project->intro); ?></textarea>
					</div>
				</div>


				<!-- Hero Image Stuff after this -->
				<?php if ($project->hero_image): ?>
				<div class="col-xs-30 dbl-pd bg-middle-grey pd-all-sm bordered">
					<div class="row mg-b-sm">
						<div class="pull-right">
							<a class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete this Image, is this correct?" href="<?php echo base_url('admin/projects/remove_project_image/'.encode($project->id).'?redirect='.encode('admin/projects/edit/'.encode($project->id))); ?>">
								<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
								<img src="<?php echo base_url('assets/admin/images/icons.svg'); ?>" onerror="this.src='<?php echo base_url('assets/admin/images/icons.png'); ?>'"/>
								</span>
							</a>
						</div>
					</div>

					<div class="col-xs-60 dbl-pd">
						<div class="img-placeholder image-center">
							<img src="<?php echo display_image($project->hero_image,'thumb_'); ?>" class="img-responsive">
						</div>

						<div class="row mg-t-sm txt-responsive-xs txt-light inherit">
							<div class="col-xs-60 col-sm-20 txt-book">
								Image
							</div>

							<div class="col-xs-60 col-sm-40">
								<a href="<?php echo base_url($project->hero_image); ?>" class="pop-up">
								<?php if ($project->hero_image) 
								{
									$hero_image = explode("/", $project->hero_image);
									$hero_image = substr(end($hero_image), 0, 20); 
									echo $hero_image;
								 }
								 else
								 {
									echo 'none';
								} ?>
								</a>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>

				<?php else: ?>

				<div class="row">
					<label class="col-xs-60">Hero Image</label>
				</div>

				<div class="row col-xs-60 col-sm-30 bg-white mg-b-md pd-all-sm dashed-bordered new-image-box">

					<div class="clearfix pd-t-xxxs"></div>

					<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
						<img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
					</div>

					<div class="clearfix pd-t-sm"></div>

					<div class="row txt-responsive-xs inherit mg-t-xxs">
						<div class="col-xs-60 col-sm-20 inherit">
							<label for="hero_image" class="pd-all-no auto-height">Hero Image</label>
						</div>

						<div class="col-xs-60 col-sm-40 inherit txt-light">
							<input type="file" name="hero_image" id="hero_image" />
						</div>
					</div>

					<div class="clearfix"></div>
				</div>

				<?php endif; ?>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<label for="width" class="col-xs-60"><input type="checkbox" name="width" id="width" value="1" <?php echo set_checkbox('width', '1', (bool)($project->width)); ?> />&nbsp; &nbsp;Make this project full width? </label>
				</div>

				<?php if(isset($categories) && is_array($categories)) :?>
					<div class="col-xs-60 col-sm-30 dbl-pd">
						<label for="prop-excerpt">Category</label>

						<select name="category[]">
							<?php foreach ($categories as $key => $category): ?>
							<option value="<?php echo $category->id ?>" <?php echo set_select('category[]', $category->id, ((in_array($category->id, $assigned_categories))? TRUE : FALSE)); ?> ><?php echo $category->name; ?></option>
							<?php endforeach ?>
						</select>
					</div>
				<?php endif ?> 

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="address">Address</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="address" id="address" value="<?php echo set_value('address', $project->address); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="type">Type</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="type" id="type" value="<?php echo set_value('type', $project->type); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="budget">Budget</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="budget" id="budget" value="<?php echo set_value('budget', $project->budget); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="commenced">Commenced</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="commenced" id="commenced" value="<?php echo set_value('commenced', $project->commenced); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="completed">Completed</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="completed" id="completed" value="<?php echo set_value('completed', $project->completed); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="architect">Architect</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="architect" id="architect" value="<?php echo set_value('architect', $project->architect); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="interiors">Interior Designer</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="interiors" id="interiors" value="<?php echo set_value('interiors', $project->interiors); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="landscaper">Landscaper</label>
					</div>

					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="landscaper" id="landscaper" value="<?php echo set_value('landscaper', $project->landscaper); ?>"/>
					</div>
				</div>
			</div>

			<div class="row dbl-pd">
				<div class="col-xs-60 col-sm-60 dbl-pd">
					<label for="content">Project Content</label>
					<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("detail", set_value('detail', $project->detail)); ?></div>
				</div>
			</div>

			<div class="row dbl-pd">
				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="author">Testimonial Author</label>
					</div>
					<div class="col-xs-60">
						<input class="mg-b-sm" type="text" name="author" id="author" value="<?php echo set_value('author', $project->author); ?>"/>
					</div>
				</div>
				<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
					<div class="col-xs-60">
						<label for="testimonial">Testimonial</label>
					</div>
					<div class="col-xs-60">
						<textarea class="mg-b-sm" name="testimonial" id="testimonial" ><?php echo set_value('testimonial', $project->testimonial); ?></textarea>
					</div>
				</div>
			</div>
			
			<div class="row dbl-pd">
				<div class="col-xs-60 col-sm-60 dbl-pd">
					<div class="col-xs-60">
						<label for="prop-meta_title">Meta Title</label>
					</div>

					<div class="col-xs-60">
						<input class="" type="text" name="meta_title" id="prop-meta_title" value="<?php echo set_value('meta_title', $project->meta_title); ?>"/>
					</div>
				</div>

				<div class="col-xs-60 col-sm-60" style="margin-top:10px;">
					<div class="col-xs-30 dbl-pd">
						<label class="col-xs-60" for="meta_keywords">Meta Keywords <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 8 words separated by a comma)</span></label> 
						<textarea name="meta_keywords" id="meta_keywords"><?php echo set_value('meta_keywords', $project->meta_keywords); ?></textarea>
					</div>

					<div class="col-xs-30 dbl-pd">
						<label class="col-xs-60" for="meta_description">Meta Description <span style="vertical-align: bottom; float: right; font-size: 0.75em">(max 255 characters)</span></label>
						<textarea name="meta_description" id="meta_description"><?php echo set_value('meta_description', $project->meta_description); ?></textarea>
					</div>
				</div>

				<div class="col-xs-60 dbl-pd">
					<label for="permalink">Permalink</label>
				</div>

				<div class="col-xs-60 auto-overflow dbl-pd">
					<span class="demo-url input"><?php echo base_url($project->permalink); ?></span>
					<input type="hidden" value="<?php echo $project->permalink ?>" name="permalink" id="permalink" />
				</div>		
			</div>
		</div>

		<!-- HERO IMAGES --> 
		<div class="tab-content" id="hero-gallery">

			<div class="row action-buttons">

				<?php echo gallery_message(); ?>
				
				<div class="pull-right">
					<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Order Gallery
						</span>
					</a>
					<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Stop Ordering
						</span>
					</a>
				</div>
			</div>
			
			<ul class="manage-list manage-galleries row std-pd" data-rel="gallery_images" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">

				<?php if(isset($gallery_images) && is_array($gallery_images)) : ?>
					<?php foreach ($gallery_images as $key => $image): ?>
						<li class="col-xs-60 col-sm-30 col-md-20 std-pd" id="sort_<?php echo $image->id; ?>">
								<?php $this->load->view('admin/_partials/gallery_item_media', array('image' => $image, 'redirect' => full_url().'#hero-gallery')); ?>
						</li>
					<?php endforeach ?>
				<?php endif ?>

				<li class="upload-new-image col-xs-60 col-sm-30 col-md-20 std-pd ordering-hide" data-name="file">
					<div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">

						<div class="clearfix pd-t-xxxs"></div>
						
						<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
							<div id="galery-uploader" class="dropzone multi-uploader" data-title="Upload Images" data-name="file" data-action="<?php echo base_url('admin/projects/upload_gallery_image/'.encode($project->gallery_id).'/'.encode($project->id)); ?>">
								<div class="fallback">
					                <img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
					            </div>
					            <div class="dz-default dz-message">
					                Drag files here to upload (or click) to upload a new Images
					                
					            </div>
				            </div>
						</div>

						<div class="clearfix pd-t-sm"></div>

						<div class="clearfix"></div>

					</div>
				</li>
				
			</ul>
		</div>
		<!-- / HERO IMAGES -->
	</div>
</section>
<input type="hidden" name="gallery_id" value="<?php echo $project->gallery_id; ?>" />
</form>    

