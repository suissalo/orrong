<?php 
	$section_types 		= config_item('section_type');
	$structure 			= (isset($section_types[$section->type])) ? $section_types[$section->type] : FALSE;
	$main_structure 	= (isset($structure['main'])) ? $structure['main'] : FALSE;
	$main_content 		= $section->content;
	$items_structure 	= (isset($structure['items'])) ? $structure['items'] : FALSE;
	$items_content 		= $section->items;
?>

<?php echo form_open_multipart(); ?>
<input type="hidden" name="ref" value="team" />
<div class="row action-buttons">
	<div class="mg-t-md mg-b-md pull-right">
		<a href="<?php echo base_url('/admin/pages/edit/'.encode($section->page_id).'#content'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Cancel
			</span>
		</a> 
		<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-disk">
			  <?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
			  Save
			</span>
		</button>
	</div>
</div>

<?php echo Input_helper::heading('Edit '.$structure['hr_title'].' Section: '); ?>

<?php if (isset($structure['information']) && $structure['information']) : ?>
<div class="row pd-all-sm bg-white">
	<p><?php echo $structure['information']; ?></p>
</div>
<?php endif; ?>

<div class="row pd-all-sm bg-white mg-b-lg">
<div class="row std-pd">
	<?php 
	if ($main_structure) :
		foreach ($main_structure as $field_name => $field_structure) : 
	?>
		<div class="col-xs-60 std-pd mg-t-sm <?php echo (isset($field_structure['class'])) ? $field_structure['class'] : ''; ?> ">
			<label><?php echo str_replace('_', ' ', ucfirst($field_name)); ?></label>
			<?php echo Sections::field_helper($field_structure['field'], $field_name, @$main_content[$field_name], array('options' => @$field_structure['options'])); ?>
		</div>
	<?php 
		endforeach;
	endif; 
	?>
</div>
</div>

<?php if ($items_structure) : ?>

	<div class="row">
		<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
		Items:
		</div>
	</div>

	<div class="row pd-all-sm bg-white">

		<div class="row mg-b-sm">
			<div class="pull-right">
				<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
					<span class="icon icon-size-150 icon-reorder">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Order
					</span>
				</a>
				<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
					<span class="icon icon-size-150 icon-reorder">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Stop Ordering
					</span>
				</a>
			</div>
			<p class="pull-left">Please save first, before adding items.</p>
		</div>

		<div class="manage-list row std-pd" data-url="<?php echo base_url('admin/sections/reorder_item'); ?>">

			<?php
			$item_count = 0;
			if ($items_content) : 
				foreach ($items_content as $item_key => $item_content) : 
					$item_count++;
			?>
			
			<div class="std-pd <?php echo (isset($structure['item_class'])) ? $structure['item_class'] : ''; ?>" id="sort_<?php echo $item_key; ?>">
				<div class="bordered row pd-all-sm bg-white mg-b-sm item-child">

					<a href="<?php echo base_url('admin/sections/remove_item/'.encode($section->page_id).'/'.encode($section->id).'/'.encode($item_key)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-right block mg-r-xxxs confirm " title="You are about to delete this item, do you wish to continue?">
						<span class="icon icon-size-125 outline-light icon-cross">
							<?php echo config_item('icon_image'); ?>
						</span>
					</a>
					<div class="clearfix"></div>

			<?php	
					foreach ($item_content as $content_name => $content_content) : 
					$field_structure = $items_structure[$content_name];
					$field_name = $content_name;
			?>
				<div class="col-xs-60 mg-t-sm <?php echo (isset($field_structure['class'])) ? $field_structure['class'] : ''; ?> ">
					<label><?php echo str_replace('_', ' ', ucfirst($field_name)); ?></label>
					<?php echo Sections::field_helper($field_structure['field'], 'items['.$item_key.']['.$field_name.']', $content_content, array('options' => @$field_structure['options'])); ?>
				</div>
			<?php endforeach; ?>

				</div>
			</div>
			
			<?php 
				endforeach;
			endif; 
			?>

		</div>

	</div>

	<?php if ( !isset($structure['limit']) || !$structure['limit'] || (isset($structure['limit']) && $structure['limit'] && $item_count < $structure['limit']) ) : ?>
	<div class="row pd-all-sm pd-t-no bg-white">
	<a href="<?php echo base_url('admin/sections/add_item/'.encode($section->page_id).'/'.encode($section->id).'/'.$section->type); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block">
		<span class="icon icon-size-150 outline-light icon-plus">
			<?php echo config_item('icon_image'); ?>
		</span>
		<span class="text">
			Add Item
		</span>
	</a>
	</div>
	<?php endif; ?>

<?php endif; ?>

<div class="row action-buttons">
	<div class="mg-t-md mg-b-md pull-right">
		<a href="<?php echo base_url('/admin/pages/edit/'.encode($section->page_id).'#content'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Cancel
			</span>
		</a> 
		<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-disk">
			  <?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
			  Save
			</span>
		</button>
	</div>
</div>

<?php echo form_close(); ?>