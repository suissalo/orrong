	<?php echo Input_helper::heading('Sections'); ?>
<br/>
			<?php
			$this->load->model('admin/section');
			$sections = $this->section->get_sections($page->id);
			$section_types = config_item('section_type');
			//view($section_types);
			?>


			<div class="row mg-b-sm">
				<div class="pull-right">
					<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Order Sections
						</span>
					</a>
					<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Stop Ordering
						</span>
					</a>
				</div>
			</div>


			<?php if (isset($sections) && $sections) : ?>
			<div class="manage-list mg-b-lg row" data-url="<?php echo base_url('admin/sections/reorder_section'); ?>">

				<?php foreach ($sections as $section) : ?>
				<?php $section->content = json_decode($section->content); ?>
				<div id="sort_<?php echo $section->id; ?>">
					<div class="bg-light-grey row mg-b-sm item-child">
						<div class="pull-left">
							<h6 class="pd-all-sm title">
								<?php echo $section_types[$section->type]['hr_title']; ?>
								<?php if (isset($section->content->title) && $section->content->title) : ?>
								<span class="txt-responsive-xxs">(<?php echo $section->content->title; ?>)</span>
								<?php endif; ?>
							</h6>
						</div>
						<div class="pull-right pd-all-xs">
							<a href="<?php echo base_url('admin/sections/edit_section/'.encode($section->page_id).'/'.encode($section->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs" title="Edit">
								<span class="icon icon-size-125 outline-light icon-edit">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>
							<?php if ($section->published == 1): ?>
							<a href="<?php echo base_url('admin/sections/publish_section/'.encode($section->page_id).'/'.encode($section->id).'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
								<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>
							<?php else: ?>
							<a href="<?php echo base_url('admin/sections/publish_section/'.encode($section->page_id).'/'.encode($section->id).'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
								<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>
							<?php endif ?>
							<a href="<?php echo base_url('admin/sections/remove_section/'.encode($section->page_id).'/'.encode($section->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs confirm " title="You are about to delete this section, do you wish to continue?">
								<span class="icon icon-size-125 outline-light icon-cross">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>
						</div>
					</div>
				</div>
				<?php endforeach; ?>

			</div>
			<?php endif; ?>


			<div class="row mg-t-lg">
				<?php if (isset($section_types) && $section_types):?>
				<?php foreach ($section_types as $section_type_key => $section_type) : ?>
					<a class="button bg-seagreen pd-all-xs txt-white txt-responsive-sm mg-l-sm mg-b-sm pull-right" href="<?php echo base_url('admin/sections/add_section/'.encode($page->id).'/'.$section_type_key); ?>">
						<span class="icon icon-size-125 outline-light icon-plus">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text">
								<?php echo $section_type['hr_title']; ?>
							</span>
					</a>
				<?php endforeach; ?>
				<?php endif;?>
			</div>
