<?php 

$main_image = '';
$fallback_image = '';
$delete_path = '';

if (isset($image->image))
{
	$main_image = $image->image;
}
elseif (isset($image->thumb))
{
	$main_image = $image->thumb;
}

if (isset($image->image_hover))
{
	$fallback_image = $image->image_hover;
}
elseif (isset($image->fallback_image))
{
	$fallback_image = $image->fallback_image;
}

if (isset($del_path))
{
	$delete_path = $del_path;
}
else
{
	$delete_path = 'admin/galleries/remove_image/'.encode($image->id);
}
if (isset($redirect))
{
	$delete_path .= '?redirect='.encode($redirect);
}

?>

<div class="item-child row bg-middle-grey mg-t-sm pd-all-sm bordered">
					  <!-- <i class="handle glyphicon glyphicon-sort"></i> -->
<div class="row mg-b-sm">
	<div class="pull-right">
		<?php if (isset($image->status)) : ?>
			<?php if ($image->status == 1) : ?>
			<a title="UNPUBLISH" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xs" href="<?php echo site_url('admin/projects/publish_gallery_image/'.encode($project->id).'/'.encode($image->id).'/0') ?>">
				<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
				<img src="<?php echo base_url('assets/admin/images/icons.png'); ?>" />
				</span>
			</a>
			<?php else: ?>
			<a title="PUBLISH" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xs" href="<?php echo site_url('admin/projects/publish_gallery_image/'.encode($project->id).'/'.encode($image->id).'/1') ?>">
				<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
				<img src="<?php echo base_url('assets/admin/images/icons.png'); ?>" />
				</span>
			</a>
			<?php endif; ?>
	  	<?php endif; ?>
	  <a title="DELETE" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm slide-out" data-rel='#sort_<?php echo $image->id; ?>' title="You are about to delete this Image, is this correct?" href="<?php echo site_url($delete_path) ?>">
		<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
		  <img src="<?php echo base_url('assets/admin/images/icons.png'); ?>" />
		</span>
	  </a>
	</div>
		<?php if (strlen($image->title) > 35) : ?>

	<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold" data-toggle="tooltip" data-placement="top" title="<?php echo $image->title; ?>" style="height: 4.2em; overflow: hidden">
		<?php echo substr($image->title, 0, 32).'...'; ?>
	</div>

	<?php else: ?>

	<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold" style="height: 4.2em; overflow: hidden;">
		<?php echo $image->title; ?>
	</div>

	<?php endif; ?>
</div>

<div class="col-xs-60">
	<div class="img-placeholder image-center" style="background-position: center center; background-repeat: no-repeat; background-size: contain; background-image: url('<?php echo display_image($main_image,'thumb_'); ?>');">
	<?php if($main_image) : ?>
		<img src="<?php echo display_image($main_image,'thumb_'); ?>" class="img-responsive" style="display: none;">	
	<?php endif; ?>
	</div>

	<div class="row mg-t-sm txt-responsive-xs txt-light inherit">
		<div class="col-xs-60 col-sm-20 txt-book">
			Image
		</div>
		<div class="col-xs-60 col-sm-40">
			<a href="<?php echo site_url($main_image); ?>" class="gallery-popup" rel="gallery-<?php echo $image->id; ?>">
			<?php $img_segments = explode("/", $main_image); echo ($main_image) ? substr(end($img_segments), 0, 20) : 'none'; ?>
			</a>
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-title_<?php echo $image->id; ?>">Caption</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="hero_images[<?php echo $image->id; ?>][title]" value="<?php echo set_value('images['.$image->id.'][title]', $image->title); ?>" id="prop-title_<?php echo $image->id; ?>" />
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-description_<?php echo $image->id; ?>">Alt Tag</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="hero_images[<?php echo $image->id; ?>][description]" value="<?php echo set_value('images['.$image->id.'][description]', $image->description); ?>" id="prop-description_<?php echo $image->id; ?>" />
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-description_<?php echo $image->id; ?>">Link</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="hero_images[<?php echo $image->id; ?>][link]" id="prop-new_image_link_<?php echo $image->id; ?>" value="<?php echo set_value('images['.$image->id.'][link]', $image->link); ?>" />
		</div>
	</div>

	
</div>
<div class="clearfix"></div>
</div>
