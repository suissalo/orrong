<?php 
$delete_path = '';

if (isset($del_path))
{
	$delete_path = $del_path;
}
else
{
	$delete_path = 'admin/leads/remove_attachment/'.$form_id.'/'.encode($attachment_id);
}
?>

<div class="item-child row bg-middle-grey mg-t-sm pd-all-sm bordered">
					  <!-- <i class="handle glyphicon glyphicon-sort"></i> -->
<div class="row mg-b-sm">
	<div class="pull-right">

	  <a class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" data-rel='#sort_<?php echo $attachment_id; ?>' title="You are about to delete this Attachment, is this correct?" href="<?php echo base_url($delete_path) ?>">
		<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
		  <?php echo config_item('icon_image'); ?>
		</span>
	  </a>
	</div>
		<?php if (strlen($attachment['file_name']) > 35) : ?>

	<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold" data-toggle="tooltip" data-placement="top" title="<?php echo $attachment['file_name']; ?>" style="height: 4.2em; overflow: hidden">
		<?php echo substr($attachment['file_name'], 0, 32).'...'; ?>
	</div>

	<?php else: ?>

	<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold" style="height: 4.2em; overflow: hidden;">
		<?php echo $attachment['file_name']; ?>
	</div>

	<?php endif; ?>
</div>

<div class="col-xs-60">

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-file_name_<?php echo $attachment_id; ?>">File Name</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<?php echo $attachment['file_name']; ?>
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-file_type_<?php echo $attachment_id; ?>">File Type</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<?php echo strtoupper($attachment['file_type']); ?>
		</div>
	</div>
	
</div>
<div class="clearfix"></div>
</div>
