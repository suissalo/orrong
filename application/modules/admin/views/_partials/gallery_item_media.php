<?php 

$main_image = '';
$fallback_image = '';
$delete_path = '';
$video_file 	= '';
$video_delete_path = '';

if (isset($image->image))
{
	$main_image = $image->image;
}
elseif (isset($image->thumb))
{
	$main_image = $image->thumb;
}

if (isset($image->image_hover))
{
	$fallback_image = $image->image_hover;
}
elseif (isset($image->fallback_image))
{
	$fallback_image = $image->fallback_image;
}

if (isset($image->mp4))
{
	$video_file = $image->mp4;
}

if (isset($del_path))
{
	$delete_path = $del_path;
}
else
{
	$delete_path = 'admin/galleries/remove_image/'.encode($image->id);
}

if (isset($vid_del_path))
{
	$video_delete_path = $vid_del_path;
}
else
{
	$video_delete_path = 'admin/galleries/remove_mp4/'.encode($image->id);
}

if (isset($redirect))
{
	$delete_path .= '?redirect='.encode($redirect);
	$video_delete_path .= '?redirect='.encode($redirect);
}

?>

<div class="item-child row bg-middle-grey mg-t-sm pd-all-sm bordered">
					  <!-- <i class="handle glyphicon glyphicon-sort"></i> -->
<div class="row mg-b-sm">
	<div class="pull-right">

	  <a class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" data-rel='#sort_<?php echo $image->id; ?>' title="You are about to delete this Image, is this correct?" href="<?php echo base_url($delete_path) ?>">
		<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
		  <?php echo config_item('icon_image'); ?>
		</span>
	  </a>
	</div>
		<?php if (strlen($image->title) > 35) : ?>

	<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold" data-toggle="tooltip" data-placement="top" title="<?php echo $image->title; ?>" style="height: 4.2em; overflow: hidden">
		<?php echo substr($image->title, 0, 32).'...'; ?>
	</div>

	<?php else: ?>

	<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold" style="height: 4.2em; overflow: hidden;">
		<?php echo $image->title; ?>
	</div>

	<?php endif; ?>
</div>

<div class="col-xs-60">
	<div class="img-placeholder image-center">
	<?php if($main_image) : ?>
		<img src="<?php echo display_image($main_image,'thumb_'); ?>" class="img-responsive">	
	<?php endif; ?>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label>Image</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit pd-t-xxs">
			<?php if ($main_image): ?>
				<a href="<?php echo site_url($main_image); ?>" rel="gallery-<?php echo $image->id; ?>" target="_blank" class="txt-responsive-xxs mg-r-xxs bg-light-blue pull-left text-center" style="width: 1.625em;" >
					<span class="icon icon-size-125 fill-light icon-view stop-prop icon-container">
						<?php echo config_item('icon_image'); ?>
					</span> 
				</a>
			<?php endif ?>
			<div class="pull-left" style="overflow: hidden; max-width: 85%">	
				<input class="txt-responsive-xxs" type="file" name="hero_images[<?php echo $image->id; ?>][image]" />			
			</div>
			<!-- <a href="<?php echo base_url($main_image); ?>" class="gallery-popup" rel="gallery-<?php echo $image->id; ?>">
				<span class="txt-responsive-xxs mg-r-xxs bg-light-blue pull-left text-center" style="width: 1.625em;">
				<span class="icon icon-size-125 fill-light icon-view stop-prop icon-container">
					<?php echo config_item('icon_image'); ?>
				</span>
				</span>
			<?php 
			if ($main_image) {
				$segments =  explode("/", $main_image); 
				$segments = end($segments);
				echo substr($segments, 0, 20);
			}
			?>
			</a> -->
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xxs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label>Video</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit pd-t-xxs">
			<?php if ($video_file): ?>
				<a href="<?php echo site_url($video_file); ?>" rel="gallery-<?php echo $image->id; ?>" class="pop-up txt-responsive-xxs mg-r-xxs bg-light-blue pull-left text-center" style="width: 1.625em;" >
					<span class="icon icon-size-125 fill-light icon-view stop-prop icon-container">
						<?php echo config_item('icon_image'); ?>
					</span> 
				</a>

				<a href="<?php echo base_url($video_delete_path) ?>" rel="gallery-<?php echo $image->id; ?>" class="txt-responsive-xxs mg-r-xxs bg-red pull-right text-center confirm" title="You are about to delete this Video, is this correct?" style="width: 1.625em; height: 1.375em; position: absolute; top: 2px; bottom: 0px; right: 0px; margin: auto" >
					<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
						<?php echo config_item('icon_image'); ?>
					</span> 
				</a>
			<?php endif ?>
			<div class="pull-left" style="overflow: hidden; max-width: 85%">	
				<input class="txt-responsive-xxs" type="file" name="hero_images[<?php echo $image->id; ?>][video_file]" />			
			</div>
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-title_<?php echo $image->id; ?>">Title</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="images[<?php echo $image->id; ?>][title]" value="<?php echo set_value('images['.$image->id.'][title]', $image->title); ?>" id="prop-title_<?php echo $image->id; ?>" />
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-description_<?php echo $image->id; ?>">Caption</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="images[<?php echo $image->id; ?>][description]" value="<?php echo set_value('images['.$image->id.'][description]', $image->description); ?>" id="prop-description_<?php echo $image->id; ?>" />
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-caption_position_<?php echo $image->id; ?>">Caption Position</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">	
			<select name="images[<?php echo $image->id; ?>][caption_position]" id="prop-caption_position_<?php echo $image->id; ?>" class="mg-b-sm">
				<option value="middle-center" <?php echo set_select('images['.$image->id.'][caption_position]', 'middle-center', (($image->caption_position == 'middle-center') ? TRUE : FALSE)); ?> >Middle Center</option>			
				<option value="top-left" <?php echo set_select('images['.$image->id.'][caption_position]', 'top-left', (($image->caption_position == 'top-left') ? TRUE : FALSE)); ?> >Top Left</option>
				<option value="top-center" <?php echo set_select('images['.$image->id.'][caption_position]', 'top-center', (($image->caption_position == 'top-center') ? TRUE : FALSE)); ?> >Top Center</option>
				<option value="top-right" <?php echo set_select('images['.$image->id.'][caption_position]', 'top-right', (($image->caption_position == 'top-right') ? TRUE : FALSE)); ?> >Top Right</option>
				<option value="middle-left" <?php echo set_select('images['.$image->id.'][caption_position]', 'middle-left', (($image->caption_position == 'middle-left') ? TRUE : FALSE)); ?> >Middle Left</option>
				<option value="middle-right" <?php echo set_select('images['.$image->id.'][caption_position]', 'middle-right', (($image->caption_position == 'middle-right') ? TRUE : FALSE)); ?> >Middle Right</option>
				<option value="bottom-left" <?php echo set_select('images['.$image->id.'][caption_position]', 'bottom-left', (($image->caption_position == 'bottom-left') ? TRUE : FALSE)); ?> >Bottom Left</option>
				<option value="bottom-center" <?php echo set_select('images['.$image->id.'][caption_position]', 'bottom-center', (($image->caption_position == 'bottom-center') ? TRUE : FALSE)); ?> >Bottom Center</option>
				<option value="bottom-right" <?php echo set_select('images['.$image->id.'][caption_position]', 'bottom-right', (($image->caption_position == 'bottom-right') ? TRUE : FALSE)); ?> >Bottom Right</option>
			</select>
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-artists_impression_<?php echo $image->id; ?>">Artists Impression</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<select name="images[<?php echo $image->id; ?>][artists_impression]" id="prop-artists_impression_<?php echo $image->id; ?>" class="mg-b-sm">
				<option value="no" <?php echo set_select('images['.$image->id.'][artists_impression]', 'no', (($image->artists_impression == 'no') ? TRUE : FALSE)); ?> >No</option>
				<option value="yes" <?php echo set_select('images['.$image->id.'][artists_impression]', 'yes', (($image->artists_impression == 'yes') ? TRUE : FALSE)); ?> >Yes</option>
			</select>
		</div>
	</div>		






	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-description_<?php echo $image->id; ?>">Link URL</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="images[<?php echo $image->id; ?>][link]" id="prop-new_image_link_<?php echo $image->id; ?>" value="<?php echo set_value('images['.$image->id.'][link]', $image->link); ?>" />
		</div>
	</div>

	<div class="row txt-responsive-xs inherit mg-t-xs">
		<div class="col-xs-60 col-sm-20 inherit">
			<label for="prop-link_text_<?php echo $image->id; ?>">Link Text</label>
		</div>
		<div class="col-xs-60 col-sm-40 inherit">
			<input type="text" name="images[<?php echo $image->id; ?>][link_text]" id="prop-new_image_link_text_<?php echo $image->id; ?>" value="<?php echo set_value('images['.$image->id.'][link_text]', $image->link_text); ?>" />
		</div>
	</div>

	
</div>
<div class="clearfix"></div>
</div>
