<div class="login-form col-xs-60 col-sm-60 col-md-50 col-lg-40 pd-all-xl mg-t-xl bg-white center-block">
	<div class="col-lg-4 col-md-3 col-sm-2 col-xs-1"></div>


	<div class="reset_password-form">

		<?php echo form_open(base_url('admin/change_password/'.encode($user->id)), array('autocomplete' => 'off')); ?>


		<input type = "hidden" name = "submit_new_password" id = "submit_new_password" value = "1" />

		<div class="txt-center col-xs-60">
			<span class="icon icon-size-400 icon-users fill-dark txt-responsive-xxl">
				<?php echo config_item('icon_image'); ?>
			</span>
		</div>

		<div class="txt-grey txt-center txt-responsive-xxl mg-b-md mg-t-md col-xs-60">
			Password Reset
		</div>

		<div class="clearfix"></div>
		<div class="col-xs-60 col-sm-44 center-block">

			<div class="mg-b-md row std-pd">
		    	<div class="col-xs-60 col-sm-20 txt-grey std-pd">
		    		<label class="txt-grey text-center txt-responsive-sm" for="user_name">User Name</label>	      		
		      	</div>
		      	<div class="col-xs-60 col-sm-40 std-pd">	      		
		      		<input type = "text" readonly name = "user_name" id = "user_name" value = "<?php echo set_value('user_name', $user->name); ?>" />
		      	</div>
		    </div>

		    <div class="mg-b-md row std-pd">
		    	<div class="col-xs-60 col-sm-20 txt-grey std-pd">
		    		<label class="txt-grey text-center txt-responsive-sm" for="new_password">New Password</label>	      		
		      	</div>
		      	<div class="col-xs-60 col-sm-40 std-pd">	      		
		      		<input type="password" class="form-control" autocomplete ="off" value=""  name="new_password" id="new_password" />
		      	</div>
		    </div>

		    <div class="mg-b-md row std-pd">
		    	<div class="col-xs-60 col-sm-20 txt-grey std-pd">
		    		<label class="txt-grey text-center txt-responsive-sm" for="re_new_password">Repeat New Password</label>	      		
		      	</div>
		      	<div class="col-xs-60 col-sm-40 std-pd">	      		
		      		<input type="password" class="form-control" autocomplete ="off" value=""  name="re_new_password" id="re_new_password" />
		      	</div>
		    </div>

		    <div class="mg-b-md row">
				<div class="col-xs-60 col-sm-40 mg-b-md pull-right">
					<button class="border-none pd-all-xxs col-xs-60 button bg-dark-grey txt-white text-center txt-responsive-sm" type="submit">Change Password</button>
				</div>
			</div>
	    </div>	    
	    
		</form>
	    
	</div>

	<div class="col-lg-4 col-md-3 col-sm-2 col-xs-1"></div>

	<div class="clearfix"></div>
	</div>
</div>
