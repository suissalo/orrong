
<?php echo form_open_multipart('admin/users/save_new', array('autocomplete' => 'off')); ?>

<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('admin/users'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
						<span class="icon icon-size-150 outline-light icon-crosscircle">
								<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
								Cancel
						</span>
				</a> 

				<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
						<span class="icon icon-size-150 outline-light icon-disk">
								<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
								Save
						</span>
				</button>
		</div>
</div>

<section>
	<?php echo Input_helper::heading('New'); ?>

	<div class="tab-container row bg-white pd-all-md">

		<div class="mg-b-md">
			<a href="#details" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Details</a>
			<a href="#hero-gallery" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Profile Image</a>
		   <!--  <a href="#content" class="tab-link txt-responsive-sm col-md-10 pd-all-xs">Content</a> -->
			<div class="txt-responsive-xxs text-right pull-right col-md-14 pd-all-xxs pd-t-sm">&nbsp;</div>
			<div class="clearfix"></div>
			<div class="tab-link-divider"></div>
		</div>

		<div class="tab-content row dbl-pd" id="details">

			<div class="col-xs-60 col-sm-30 dbl-pd border-right-black">
				<div class="row">
					<label for="prop-name" class="col-xs-60">Name*</label>
					<div class="col-xs-60">
						<input class="mg-t-xxs form-control" type="text" name="name" id="prop-name" value="<?php echo set_value('name'); ?>"/>
					</div>
				</div>

				<div class="row">
					<label for="prop-contact_email" class="mg-t-sm col-xs-60">Contact Email*</label>
					<div class="col-xs-60">
						<input class="mg-t-xs form-control" type="text" name="contact_email" id="prop-contact_email" value="<?php echo set_value('contact_email'); ?>"/>
					</div>
				</div>

				<!-- <div class="row">
					<label for="prop-phone" class="mg-t-sm col-xs-60">Contact Number</label>
					<div class="mg-t-xxs col-xs-60">
						<input class="form-control" type="text" name="phone" id="prop-phone" value="<?php echo set_value('phone'); ?>"/>
					</div>
				</div> -->
			</div>
	
			<div class="col-xs-60 col-sm-30 dbl-pd">
				<div class="row">
					<label for="prop-username" class="col-xs-60">Username*</label>
					<div class="mg-t-xxs col-xs-60">
						<input class="form-control" autocomplete="off" type="text" name="username" id="prop-username" value="<?php echo set_value('username'); ?>"/>
					</div>
				</div>

				<div class="row">
					<label for="prop-password" class="mg-t-sm col-xs-60">Password*</label>
					<div class="col-xs-60">
						<input class="mg-t-xxs form-control" autocomplete="off" type="password" name="pass" id="prop-password" value=""/>
					</div>
				</div>

				<!-- <div class="row">
					<label for="prop-group" class="mg-t-sm col-xs-60">Group</label>
					<div class="col-xs-60">
						<select class="mg-t-xxs" name="group_id" id="prep-group" >
							<option value="" />// Please Select Group </option>
							<?php if($groups): ?>
								<?php foreach($groups as $group): ?>
									<option value="<?php echo $group->id; ?>" <?php echo set_select('group_id', $group->id); ?> /><?php echo $group->name; ?></option>
								<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
				</div>  -->
			</div>
		</div>

		<div class="tab-content" id="hero-gallery">
			 <div class="row">
					<label for="prop-image" class="mg-t-sm col-xs-60">Profile Image</label>
					<div class="col-xs-60"><input type="file" name="user_image" id="prop-image" /></div>
			</div>
		</div>

<!-- 			<div class="tab-content" id="content">
			 <div class="row">
					<label for="prop-password" class="mg-t-sm col-xs-60">Bio</label>
					<div class="mg-t-xxs col-xs-60">
							<textarea name="bio" class="form-control" id="prop-bio"><?php echo set_value('bio'); ?></textarea>
					</div>
			</div>
		</div>

<div class="col-xs-60">
				
		<div class="clearfix"></div>
</div>    -->   


	</div>

</section>
<input name="group_id" type="hidden" value="2" />
</form>