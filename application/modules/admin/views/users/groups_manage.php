<!--
<div class="col-xs-60 margin-bottom-large page-heading">
	<div class="row">
		<div class="col-xs-60">
		
			<h4 class="margin-top-small">User Groups</h4>
		</div>
	</div>
</div>
<?php /*if($groups): ?>
<ul class="manage-list listings">
	<?php foreach($groups as $group): ?>
	<li >
		<?php echo $group->name; ?>
		
		<div class="btn-group btn-group-sm manage-options">
		<!-- <span class="manage-options"> -->
			<a href="<?php echo base_url('admin/users/edit_group/'.encode($group->id)); ?>" class="btn btn-success">Edit <i class="glyphicon glyphicon-pencil"></i></a>
			<?php if ($group->id != 1): ?>
				<a title="You are about to delete the group: <?php echo $group->name; ?>, and unassign all users assigned to this group. Do you wish to continue?" href="<?php echo base_url('admin/users/remove_group/'.encode($group->id)); ?>" class="confirm btn btn-danger">Delete <i class="glyphicon glyphicon-remove"></i></a>
			<?php endif ?>
		<!-- </span> -->
		</div>
		
	</li>
	<?php endforeach; ?>
</ul>
<?php else: ?>
	<h4>No Groups found</h4>
	<p>There are no Groups to manage, Please <a href="<?php echo base_url('admin/users/new_group') ?>">create a new Group</a> first
<?php endif; */ ?>
-->













<div>

	<div class="row action-buttons">
		<div class="mg-t-md mg-b-md pull-right">    

			<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a>

			<a href="<?php echo base_url('admin/users/new_group'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New User Group
				</span>
			</a>

		</div>
	</div>

	<section>
		<!-- <div class="row">
			<div class="col-xs-60 col-sm-60">
				<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">  
					User Groups
				</div>
			</div>
		</div> -->

		<?php if ($groups) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>


				<ul class="manage-list" data-rel="user_group" data-parent="0" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
				
					<?php foreach ($groups as $group) : ?>
						<li class="item-parent list-none" id="sort_<?php echo $group->id; ?>">
							<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
								<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs pd-l-xxs">
									<!-- <span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
									</span> -->
									<?php echo $group->name;?>
								</div>
								<div class="icon-container text-right stop-prop pull-right">
									<a href="<?php echo base_url('admin/users/edit_group/'.encode($group->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php if ($group->status == 1): ?>
									<a href="<?php echo base_url('admin/users/publish_group/'.$group->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/users/publish_group/'.$group->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif ?>
									<?php if ($group->id == 1): ?>
									<span class="pd-all-xxs pd-l-xs pd-r-xs pull-left block">
										<span class="icon icon-size-125">
										</span>
									</span>
									<?php else: ?>
									<a href="<?php echo base_url('admin/users/remove_group/'.encode($group->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif; ?>
								</div>
							</div>

						</li>
					<?php endforeach; ?>    
				</ul>
		 </div>
		<?php else: ?>
			<p>There are no User Groups.</p>
		<?php endif; ?>
	</section>

</div>