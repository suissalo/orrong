
<?php echo form_open('admin/users/save_new_group'); ?>
<div class="row action-buttons">
	<div class="mg-t-md mg-b-md pull-right">
		<a href="<?php echo base_url('admin/users/groups'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Cancel
			</span>
		</a>

		<button  type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-disk">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Save Changes
			</span>
		</button>
	</div>
</div>

<section>
	
	<?php echo Input_helper::heading('New'); ?>

	<div class="tab-container bg-white pd-all-md">
			
		<div class="mg-b-md">
			<a href="#details" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Details</a><!-- 
			<a href="#hero-gallery" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Hero Gallery</a>
			<a href="#content" class="tab-link txt-responsive-sm col-md-10 pd-all-xs">Content</a>-->
			<div class="txt-responsive-xxs text-right pull-right col-md-14 pd-all-xxs pd-t-sm">&nbsp;</div>
			<div class="clearfix"></div>
			<div class="tab-link-divider"></div>
		</div>  


		<div class="tab-content" id="details">
			<div class="row">
				<label for="prop-name" class="col-xs-60">Name *</label>
				<div class="col-xs-60 col-md-50">
					<input type="text" class="form-control" name="name" id="prop-name" value="<?php echo set_value('name'); ?>"/>
				</div>
				<div class="col-xs-60 col-md-7 pull-right">
					<label class="custom-checkbox txt-responsive-xs" for="prop-active">
						<input type="checkbox" name="status" <?php echo set_checkbox('status', '1'); ?> value="1" id="prop-active" />
						<span class="circle"><i></i></span>
						<span class="text">Active</span>
					</label>
				</div>
			</div>
		</div> 

	</div>

</section>

</form>