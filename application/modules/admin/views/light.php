<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class=""> <!--<![endif]-->
    <head>
    <meta name="author" content="Barking Bird" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CMS // Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=2.0" />
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/templates/default/javascript/html5shiv.js"></script>
    <![endif]-->
    
    <link type="text/css" rel="stylesheet" href="/assets/admin/styles/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/styles/flat-ui.css" />
    <link type="text/css" rel="stylesheet" href="/assets/admin/styles/fonts.css" />
    <link href="<?php echo base_url(); ?>assets/admin/styles/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/styles/dropzone.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/library.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/bootstrap-switch.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/bootstrap-select.js" ></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/dropzone.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/main.js" ></script>

    <?php if(isset($map) && $map['js']) { echo $map['js']; } ?>
</head>
<body>
    <!-- <div class="" style="min-width:940px"> -->
    <div class="">
        <div class="">
            <?php echo get_message(); ?>
            <?php echo $page_content; ?>
        </div> <!-- END CONTAINER -->
    </div> <!-- END MAIN -->

   <script type="text/javascript">
         var base_url = '<?php echo base_url(); ?>';
         <?php if (isset($complete) && ($complete == TRUE || $complete == 1)): ?>
         $(document).ready(function() {
             setTimeout(function() { 
                 <?php if (isset($page_redirect) && $page_redirect):  ?>
                     window.parent.location.href = '<?php echo $page_redirect; ?>';
                     parent.jQuery.fancybox.close(); 
                 <?php elseif (isset($custom_page_redirect) && $custom_page_redirect): ?>
                     window.parent.location.href = '<?php echo $custom_page_redirect; ?>';
                 <?php else: ?>
                     parent.jQuery.fancybox.close(); 
                 <?php endif; ?>
             }, 1000);

         });
         <?php endif ?>
    </script>
</body>
</html>