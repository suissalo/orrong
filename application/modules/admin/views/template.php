
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class=""> <!--<![endif]-->
    <head>
    <meta name="author" content="Barking Bird" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CMS // Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/templates/default/javascript/html5shiv.js"></script>
    <![endif]-->
    
    <link type="text/css" rel="stylesheet" href="/assets/admin/styles/bootstrap.css" />
<!-- 	<link type="text/css" rel="stylesheet" href="/assets/css/fonts.css" /> -->
	<link type="text/css" rel="stylesheet" href="/assets/admin/styles/fonts.css" />
	<link type="text/css" rel="stylesheet" href="/assets/admin/styles/main.css" />
	<link type="text/css" rel="stylesheet" href="/assets/admin/styles/jquery.fancybox.css" />
	<link type="text/css" rel="stylesheet" href="/assets/admin/styles/dropzone.css" />
	<link type="text/css" rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

	<link type="text/css" rel="stylesheet" href="/assets/admin/styles/datatables.min.css" />
	<link type="text/css" rel="stylesheet" href="/assets/admin/styles/buttons.dataTables.min.css" />

	<script type="text/javascript" src="/assets/admin/js/library.js" ></script>  
	<script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/assets/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript" src="/assets/admin/js/dropzone.js" ></script>

	<script type="text/javascript" src="/assets/admin/js/datatables.min.js" ></script>
	<script type="text/javascript" src="/assets/admin/js/dataTables.buttons.min.js" ></script>
	<script type="text/javascript" src="/assets/admin/js/jszip.min.js" ></script>
	<script type="text/javascript" src="/assets/admin/js/buttons.flash.min.js" ></script>
	<!-- <script type="text/javascript" src="/assets/admin/js/pdfmake.min.js" ></script> -->
	<!-- <script type="text/javascript" src="/assets/admin/js/vfs_fonts.js" ></script> -->
	<script type="text/javascript" src="/assets/admin/js/buttons.html5.min.js" ></script>

	<script type="text/javascript" src="/assets/admin/js/main.js?v=1" ></script>

    <?php if(isset($map) && $map['js']) { echo $map['js']; } ?>
</head>

<body class="<?php echo (!logged_in()) ? 'not-logged-in' : '' ?>">
	<div class="loader"></div>
	<div class="sidebar">

		<div class="sidebar-logo client-specific-colour">

			<a href="/admin" style="background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url('<?php echo base_url(Site_settings::get_var('logo')); ?>');">
			</a>

		</div>

		<nav class="sidebar-menu">

			<?php 
			$admin_menu = config_item('admin_menu');
			if (logged_in() && $admin_menu) : 
			?>

			<ul>

				<?php foreach ($admin_menu as $menu) : ?>

				<?php if (access_menu($menu['url'])) : ?>

				<li class="<?php echo (strpos(uri_string(), $menu['url']) !== FALSE) ? 'active open' : ''; ?> has-submenu">
					<a href="<?php echo base_url($menu['url']); ?>">
						<span class="icon icon-size-200 icon-<?php echo url_title($menu['title'], '-', TRUE); ?>">
							<?php echo config_item('icon_image'); ?>
						</span>
						<?php echo $menu['title']; ?>
					</a>

					<?php if (isset($menu['submenu']) && $menu['submenu']) : ?>

					<ul class="submenu">
						<?php foreach ($menu['submenu'] as $submenu) : ?>
						<li class="<?php echo (strpos(uri_string(), $submenu['url']) !== FALSE) ? 'active open' : ''; ?>">
							<a href="<?php echo base_url($submenu['url']); ?>">
								<span class="icon icon-size-200"></span>
								<?php echo $submenu['title']; ?>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>

					<?php endif; ?>

				</li>

				<?php endif; ?>

				<?php endforeach; ?>

			</ul>

			<?php endif; ?>

			<?php //view($_SESSION, FALSE); ?>
			<?php //view($admin_user, FALSE); ?>

		</nav>

		<div class="sidebar-footer">
		</div>

	</div>

	<div class="header client-specific-colour">
		<div class="container container-header">

			<?php echo get_message(); ?>

			<?php if (logged_in()): ?>
			<div class="pull-right">
				<ul id="quick-nav">
					<li>
						<a href="<?php echo base_url(); ?>" target="_blank" class="txt-responsive-sm txt-white">
							<span class="icon icon-size-200 icon-page icon-preview fill-light txt-responsive-xs">
								<?php echo config_item('icon_image'); ?>
							</span>
							View Site
						</a>
					</li>
					<li class="has-submenu">
						<a href="<?php echo base_url('admin/users/edit/'.encode($admin_user->id)); ?>" class="txt-responsive-sm txt-white">
							
								<?php if ($admin_user->image): ?>
									<span class="icon icon-size-200">
										<span>
											<img class="img-responsive" src="<?php echo base_url($admin_user->image); ?>" />
										</span>
									</span>
								<?php else: ?>
									<span class="icon icon-size-200 icon-page icon-users fill-light txt-responsive-xs">
										<?php echo config_item('icon_image'); ?>
									</span>
								<?php endif ?>								
							
							<?php echo (strlen($admin_user->name) > 16) ? substr($admin_user->name, 0, 16).'...' : $admin_user->name; ?>
						</a>
						<div class="submenu profile-menu">
							<ul class="inner txt-responsive-sm txt-white">
								<li class="pd-all-sm bg-white">
									<div class="row spesh-pd">
										<div class="col-xs-23 spesh-pd">
											<?php if ($admin_user->image): ?>

													<div class="col-xs-60">
														<img class="img-responsive" src="<?php echo base_url($admin_user->image); ?>" />
													</div>

											<?php else: ?>
												<div class="txt-center col-xs-60">
													<img class="login-icon img-responsive" src="<?php echo base_url('assets/admin/images/icon-login-large.png'); ?>" />
												</div>
											<?php endif ?>
										</div>
										<div class="col-xs-37 spesh-pd">
											<div class="txt-responsive-sm txt-medium"><?php echo (strlen($admin_user->name) > 12) ? substr($admin_user->name, 0, 12).'...' : $admin_user->name; ?></div>
											<a href="<?php echo base_url('admin/users/edit/'.encode($admin_user->id)); ?>" class="txt-responsive-xs mg-t-xs mg-b-xs">Account Settings</a>
											<a href="<?php echo base_url('admin/logout'); ?>" class="sign-out col-xs-60 txt-responsive-md bg-middle-grey pd-all-xxs">Sign Out</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<?php if (access_menu('admin/settings')) : ?>
					<li class="has-submenu">
						<a href="#" class="txt-responsive-sm txt-white">
							<span class="icon icon-size-200 icon-page icon-cog fill-light txt-responsive-xs">
								<?php echo config_item('icon_image'); ?>
							</span>
						</a>
						<div class="submenu">
							<ul class="inner txt-responsive-sm txt-white">
								<li>
									<a href="<?php echo base_url('admin/settings'); ?>" class="txt-responsive-xs">
										Settings
									</a>
								</li>
							</ul>
						</div>
					</li>
					<?php endif; ?>
					<li>
						<a href="#help-contact" class="inline-popup txt-responsive-sm txt-white">
							<span class="icon icon-size-200 icon-page icon-help fill-light txt-responsive-xs">
								<?php echo config_item('icon_image'); ?>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<?php endif; ?>
		
		</div>
	
	</div>

	<div class="clearfix"></div>

	<div class="main">
		<div class="container">

			<div class="pull-left mg-t-md mg-b-md">
				<div class="page-title txt-responsive-xxxl txt-light">
					<span class="icon icon-size-100 icon-page icon-<?php echo url_title($page_title, '-', TRUE); ?>">
						<?php echo config_item('icon_image'); ?>
					</span>
					<?php echo $page_title; ?>
				</div>
			</div>
			
			<?php echo $page_content; ?>
		</div>
	</div>

	<div id="help-contact">
		<div class="pd-all-xl">
			<h1 class="txt-responsive-md text-center txt-black">If you have an issue with your website and need to speak to someone on our support team, you can contact us by submitting a new ticket request and one of our support representatives will help you out right away!</h1>

			<a href="mailto:support@barkingbird.com.au?subject=CMS Support Request" class="block txt-responsive-md text-center txt-black mg-t-lg inherit">
				<img class="service-icon" style="max-width: 40px;" src="<?php echo base_url('assets/admin/images/icon-email.png'); ?>">
				<span class="block mg-t-sm">Contact support</span>
			</a>
			<div class="clearfix"></div>
		</div>
	</div>

	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
	</script>
	<?php echo $this->form_validation->show_validation_errors(); ?>

</body>
</html>