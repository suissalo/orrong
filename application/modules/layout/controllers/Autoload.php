<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autoload extends MX_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function initiate()
	{
		$this->load->model('gallery');
	}

}