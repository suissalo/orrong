<?php 
if(isset($gallery_images) && $gallery_images) : 
	$hero_image = reset($gallery_images);
	$this->load->view('pages/_partials/hero_gallery', array('images' => array($hero_image)));
else:
	echo '<hr class="header-hr" />';
endif; 
?>

<?php if ($row->sub_heading || $row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php 
if (isset($page_sections) && $page_sections) : 
	foreach ($page_sections as $section) : 
		echo $section->html; 
	endforeach;
endif;
?>

<?php $blog_url = base_url(blog_url());  ?>
<div class="blog-overview">
	
	<div class="container">

		<?php if ($posts) : ?>
		<div class="row dbl-pd">

			<div class="col-xs-60 col-sm-15 dbl-pd">		
				<aside class="blog-categories hidden-xs">
					<h1>Categories</h1>
					<ul>
						<li><a href="<?php echo $blog_url; ?>" class="parent-category">All</a></li>
						<?php
						if (isset($categories) && $categories) : 
							$category_list = array();
							foreach ($categories as $key => $next_category) : 
								$category_url = $blog_url.'/'.$next_category->permalink;
						?>
						<li class="<?php echo ($this->uri->segment(3) == trim($next_category->permalink, '/')) ? 'active open' : ''; ?>">
							<?php $sub_categories = $this->posts->categories($next_category->id); ?>
							<a href="<?php echo $category_url; ?>" class="parent-category"><?php echo $next_category->name; ?> <?php if ($sub_categories): ?><i class="dropdown-arrow active fa fa-chevron-down" data-toggle="#cat_<?php echo $key; ?>" aria-hidden="true"></i><?php endif;?></a>
							<?php $category_list[$next_category->name] = $category_url; ?>
							<?php if ($sub_categories): ?>
							<ul class="open" id="cat_<?php echo $key; ?>">
								<?php foreach ($sub_categories as $key => $sub_category): ?>
								<li class="<?php echo ($this->uri->segment(4) == $sub_category->permalink) ? 'active' : ''; ?>">
									<?php $sub_sub_categories = $this->posts->categories($sub_category->id); ?>
									<a href="<?php echo $blog_url.'/'.$sub_category->permalink; ?>"><?php echo $sub_category->name; ?></a>
									<?php $category_list[$sub_category->name] = $blog_url.'/'.$sub_category->permalink; ?>
									<?php if ($sub_sub_categories): ?>
									<ul class="open">
										<?php foreach ($sub_sub_categories as $key => $sub_sub_category): ?>
										<li class="<?php echo ($this->uri->segment(4) == $sub_sub_category->permalink) ? 'active' : ''; ?>">
											<a href="<?php echo $category_url.$sub_sub_category->permalink; ?>"><?php echo $sub_sub_category->name; ?></a>
											<?php $category_list[$sub_sub_category->name] = $category_url.$sub_sub_category->permalink; ?>
										</li>
										<?php endforeach ?>
									</ul>
									<?php endif ?>

								</li>
								<?php endforeach ?>
							</ul>
							<?php endif ?>
						</li>
						<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</aside>
				<aside class="blog-categories visible-xs">
					<div class="custom-select">
						<select class="js-jump-nav" name="jump">
							<option value="<?php echo $blog_url; ?>">All Categories</option>
							<?php if (count($category_list)) : $full_url = full_url().'/'; ?>
							<?php foreach ($category_list as $key => $value) : ?>
							<option value="<?php echo $value; ?>" <?php echo ($value == $full_url) ? 'selected="selected"' : '' ;?> ><?php echo $key; ?></option>
							<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
				</aside>
			</div>

			<div class="col-xs-60 col-sm-45 dbl-pd">	
				<div class="blog-articles">
					<?php 
						foreach($posts as $post) : 
							$article_link = $blog_url.'/'.$post->permalink; 
					?>
					<article>
						<div class="row dbl-pd">
							<div class="col-xs-60 col-sm-25 dbl-pd">
								<?php if ($post->hero): ?>
								<figure>
									<a href="<?php echo $article_link; ?>" class="image-center" style="background-image: url('<?php echo display_image($post->hero, 'desktop_'); ?>');">
										<img src="<?php echo display_image($post->hero, 'desktop_'); ?>" alt="<?php echo $post->title; ?>">
									</a>
								</figure>
								<?php endif; ?>
							</div>
							<div class="col-xs-60 col-sm-35 dbl-pd">							
								<h1 class="h3"><a href="<?php echo $article_link; ?>"><?php echo $post->title; ?></a></h1>
								<time><?php echo formate_date("d F Y ", strtotime($post->date_added)); ?></time>
								<summary>
									<p><?php echo strip_tags(character_limiter($post->excerpt, 180)); ?></p>
									<a href="<?php echo $article_link; ?>" class="read-more">Read more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
								</summary>
							</div>	
						</div>
					</article>
					<?php endforeach; ?>

					<div class="row">
					<?php echo $paging; ?>
					</div>

				</div>
			</div>

		</div>
		<?php else : ?>
		<p>There are no articles at this time.</p>
		<?php endif; ?>

	</div>

</div>


