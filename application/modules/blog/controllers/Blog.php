<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MX_Controller {
    
    var $data;
    var $permalink;
    function __construct() {
        parent::__construct();
        
        $this->load->model('posts');
        $this->load->model('page');
        $this->permalink = rtrim(uri_string(),'/').'/';
        $this->data['template'] = 'blog';
    }
    
    function index($data = FALSE)
    {
        if($data && is_array($data)) $this->data += $data;

        $blog_url = blog_url();

        //$this->permalink = str_replace($blog_url, '', $this->permalink, '1');
        $this->permalink = preg_replace('%'.$blog_url.'%', '', $this->permalink, 1);
        
        $this->load->model('posts');

        //are we on a article
        $this->data['post'] = $this->posts->premalink($this->permalink);
        if ($this->data['post'])
        {
            $this->data['categories'] = $this->posts->categories();
            return $this->post_detail();
        }

        //new see if its a category
        $category_string        =  $this->permalink;

        $this->data['category'] = $this->posts->category($category_string);

        // if no category_string (blog index page) or there is a category_string and a related category
        if(!$category_string || ($category_string && $this->data['category']))
        {
            $this->data['categories'] = $this->posts->categories();
            return $this->blog_category();
        }
        return FALSE;
        
    }

    function blog_category()
    {

        $per_page            = 10;
        $current             = (int)$this->input->get('page');
        $this->data['posts'] = $this->posts->articles(@$this->data['category']->id, array($current, $per_page));
        //view($this->data['posts']);
        $count               = $this->posts->articles(@$this->data['category']->id, array($current, $per_page), TRUE);
        $this->data['row']   = $this->page->get_permalink(blog_url());

        foreach ($this->data['posts'] as $pkey => $post)
        {
            $hero = reset($this->posts->post_hero($this->data['posts'][$pkey]->id, TRUE)); 
            $this->data['posts'][$pkey]->hero = $hero->image;
        }

        $this->load->library('pagination');

        $config['base_url']             = base_url(uri_string());
        $config['total_rows']           = $count;
        $config['per_page']             = $per_page;
        $config['enable_query_strings'] = TRUE;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['last_link']            = FALSE;
        $config['reuse_query_string']   = TRUE;

        $config['next_link'] = 'next';
        $config['prev_link'] = 'previous';

        $this->data['gallery']  = FALSE;
        //get gallery images
        if (isset($this->data['row']) && $this->data['row']->gallery_id) 
        {
            $this->data['gallery']          = $this->gallery->get_gallery($this->data['row']->gallery_id);
            $this->data['gallery_images']   = $this->gallery->get_images($this->data['row']->gallery_id);
        }
        //view($this->data['posts']);
        $this->pagination->initialize($config);
        $this->data['paging'] = $this->pagination->create_links();

        $html = $this->load->view('posts', $this->data, TRUE);
        $this->data['page_content'] = $html;
        return $this->data;
    }

    function post_detail($url = FALSE)
    {
        //if we didnt come from index method
        if(!$this->data['post'])
        {
            if(!$url) $url = uri_string();
            $this->data['post'] = $this->posts->premalink($url);
        }
        if (!$this->data['post']) 
        {
            return FALSE;
        }

        if ($this->data['post']->published != 1) 
        {
            if(logged_in())
            {
                set_message('info', 'This page is not published');
            }
            else
            {
            	show_404();
            }
        }

        $blog_url = blog_url();
        $this->data['row']    = $this->page->get_permalink($blog_url);

        $this->data['images']         = $this->posts->post_hero($this->data['post']->id, TRUE);
        //$this->data['categories']     = $this->posts->categories(0);
        // $this->data['row']            = (object)array(
        //     'title'             => $this->data['post']->title, 
        //     'meta_keywords'     => $this->data['post']->meta_keywords, 
        //     'meta_description'  => $this->data['post']->meta_description
        // );
        $this->data['row']->title               = $this->data['post']->title;
        $this->data['row']->meta_keywords       = $this->data['post']->meta_keywords;
        $this->data['row']->meta_description    = $this->data['post']->meta_description;

        $last_modified  = new DateTime($this->data['post']->last_modified);
        $date_added     = new DateTime($this->data['post']->date_added);

        if ($this->data['images'] && count($this->data['images']) > 0) 
        {
            $this->data['og']['image']   = display_image($this->data['images'][0]->image);
            
        }
        $this->data['og']['title']                  = $this->data['post']->title;
        $this->data['og']['description']            = $this->data['post']->excerpt;
        $this->data['og']['url']                    = base_url($this->data['post']->permalink);
        $this->data['og']['type']                   = 'article';
        $this->data['og']['article:published_time'] = $date_added->format(DateTime::ISO8601);
        $this->data['og']['article:last_modified']  = $last_modified->format(DateTime::ISO8601);
        $this->data['page_content']                 = $this->load->view('post', $this->data, TRUE);

        return $this->data;
    }

    function featured_news()
    {
        $data['posts'] = $this->posts->featured_posts(2);

        foreach ($data['posts'] as $pkey => $post)
        {
            $data['posts'][$pkey]->hero = FALSE;
            $hero = $this->posts->post_hero($post->id, TRUE); 
            $hero = reset($hero); 
            $data['posts'][$pkey]->hero = $hero->image;

            $data['posts'][$pkey]->categories = FALSE;
            $categories = $this->posts->post_category_names($post->id);
            if ($categories)
            {
                $categories = explode(',', $categories->name);
                $categories = array_slice($categories, 0, 3);
                $categories = implode(', ', $categories);
                $data['posts'][$pkey]->categories = $categories;
            }          
        }

        return $data;
    }
    
}