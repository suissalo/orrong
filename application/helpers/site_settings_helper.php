<?php 

/**
* Site settings
* All settings to do with the site that can be changed
*/
class Site_settings
{
	
	function __construct()
	{
		# code...
	}

	static function get_var($key)
	{
		$ci = &get_instance();

		$ci->db->where('key', $key);
		$query = $ci->db->get('site_settings', 1);

		return ($query->num_rows() == 1)? $query->row()->value : FALSE;
	}

	static function get_all($key = FALSE)
	{
		$ci = &get_instance();
		$query = $ci->db->get('site_settings');

		return ($query->num_rows() > 0)? $query->result() : FALSE;
	}
}
