<?php
class Auspost {
    
    private $post_domain    = 'https://auspost.com.au/api/postcode/search';
    private $api_key        = '56912d4a-23a6-4616-a348-931662dad49b';

    private $document;
    private $method;
    
    function __construct($config = FALSE) {
        
        log_message('debug', "Auspost Class Initialized");

        if($config && isset($config['method']))
        {
            $this->method = $config['method'];
        }
    }
    
    function get_post_code($query, $state = FALSE)
    {

        $data       = ($state)? array('q' => $query, 'state' => $state) : array('q' => $query);
        $payload    = $this->build_payload($data);

        $url        = $this->post_domain.$payload;
        return $this->make_request($url);
    }

    public function get_listings($data)
    {
        //if method is json
        $this->prep_json($data);
        return $this->make_request();
    }

    public function get_listing($data)
    {
        //if method is json
        $this->prep_json($data);
        return $this->make_request();
    }

    private function prep_soap($data)
    {
        //prep the data for $document
        $this->document = '';
    }

    private function prep_json($data)
    {
        //prep the data for $document
        $this->document = '';
    }
    
    private function make_request($url) 
    {
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  'Auth-Key: ' . $this->api_key
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$contents = curl_exec ($ch);
		curl_close ($ch);
		return $contents;
    }
    
    private function build_payload($data = array())
	{
        if(!$data) return FALSE;
        $payload = '?';
		foreach ($data as $key => $value)
		{
			$payload .= "{$key}={$value}".'&';
		}	
		return rtrim($payload, '&');
	}

    public function decode_detailed_response($string = false)
    {
        if($string === false || trim($string) === '')
            return false;

        $lines = explode("\n", $string);
        $output = array();
        if(count($lines) > 0)
        {
            foreach($lines as $row)
            {
                $tmp = array();
                $rowArray = explode("|", $row);
                if(count($rowArray) >= 6)
                {
                    $tmp['post_code'] = $rowArray[1];
                    $tmp['suburb'] = $rowArray[2];
                    $tmp['state'] = $rowArray[3];
                    $tmp['longitude'] = $rowArray[4];
                    $tmp['latitude'] = $rowArray[5];
                    $output[] = $tmp; 
                }
                else
                    continue;
            }
        }
        return $output;
    }
    
    /**
     * This was made to decode a string returned.
     * It is meant to be json... but its not, so this was made.
     * @param bool $prep = set retur to return an array('post code' => 'city state')
     */
    public function decode_suburb_response($string = FALSE, $prep = FALSE)
    {
        if(!$string) return FALSE;
        
        $lines = explode("\n", $string);
        if(count($lines) > 0) 
        {
            foreach($lines as $row) 
            {
                if($prep) 
                {
                    $values = explode('|', $row);
                    if(count($values) > 1) 
                    {
                        $fields[url_title($values[2], '-', TRUE)] = ucwords(strtolower($values[2])).', '.$values[1];
                    }
                }
                else
                {
                    $fields[] = explode('|', $row);
                }
            }
        }
        return $fields;
    }
    
    /**
     * This was made to decode a string returned.
     * It is meant to be json... but its not, so this was made.
     * @param bool $prep = set retur to return an array('post code' => 'city state')
     */
    public function decode_response($string = FALSE, $prep = FALSE)
    {
        if(!$string) return FALSE;
        
        $lines = explode("\n", $string);
        if(count($lines) > 0) 
        {
            foreach($lines as $row) 
            {
                if($prep) 
                {
                    $values = explode('|', $row);
                    if(count($values) > 1) 
                    {
                        $fields[$values[1]] = ucwords(strtolower($values[2])).', '.$values[3];
                    }
                }
                else
                {
                    $fields[] = explode('|', $row);
                }
            }
        }
        return $fields;
    }
    
    
}