<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Lead_reader
{
	//OLD KERIO
	// protected $imap 	= "{114.141.201.156:993/imap/novalidate-cert/ssl}INBOX";	
	// protected $user 	= "freezer@urbanangles.com";
	// protected $password = "urb@fr55bloom";

	/**
	 *	NEW GMAIL
	 *  Had to allow	for less secure auth for this account so we can connect to it.
	 *  https://support.google.com/accounts/answer/6010255
	 */
	protected $imap 	= "{web02.uacorp.com.au:993/imap/ssl}INBOX";	
	protected $user 	= "leads@montalbertplace.com.au";
	protected $password = "_AOEArkHVrgv";
	protected $error 	= "";

	var $message 	= "";
	var $overview	= "";

	var $ci;

	public $providers    = array(
		array('name' => 'REA','email'=> 'realestate.com.au@realestate.com.au'),
		array('name' => 'APT','email'=> 'info@apartmentdevelopments.com.au'),
		);

	function __construct()
	{
		$this->ci = &get_instance();
		$this->ci->load->model('admin/lead_readers');
	}

	function get_email()
	{
		//view(array($this->imap, $this->user, $this->password), FALSE);

		if($inbox = imap_open($this->imap, $this->user, $this->password))
		{
			$orders 		= FALSE;
			$total_msg 		= imap_check($inbox);
			$last_source 	= $this->ci->lead_readers->fetch_last_source();
			
			$max = 100;
			// Start from Min
			if(!$last_source) @$last_source->email_number = "1";
			// Can send email number in URL
			
			if($this->ci->input->get('email_number'))	$last_source->email_number = $this->ci->input->get('email_number');				
			
			$last_msg = ($last_source->email_number+$max > $total_msg->Nmsgs)? $total_msg->Nmsgs : ($last_source->email_number+$max);
			
			//view($last_source->email_number.":".$last_msg);
			
			/* get information specific to this email */
			$overviews 		= imap_fetch_overview($inbox,$last_source->email_number.":".$last_msg,0);
			
			//view(array('overviews' => $overviews, 'total' => $total_msg));

			//see if we can find who made the order
			foreach ($overviews as $key => $overview)
			{

				if($overview->flagged != '1' )
				{	
					$msg      		= FALSE;
					$lead 			= FALSE;
					$message   		= FALSE;

					
					$provider = $this->find_provider($overview->from);

					if ($provider) 
					{						
						if(!$this->ci->lead_readers->lead_source($overview->msgno))
						{
							$structure = imap_fetchstructure($inbox,$overview->msgno);
							//view(array("structure" => $structure), false);

							if ($structure->encoding==3)
							{
								$message 		= base64_decode(imap_fetchbody($inbox,$overview->msgno,2)) . base64_decode(imap_fetchbody($inbox,$overview->msgno,1));
							}
							else
							{
								$message 		= quoted_printable_decode(imap_fetchbody($inbox,$overview->msgno,2)) . quoted_printable_decode(imap_fetchbody($inbox,$overview->msgno,1));
							}

							if(!$message)
							{
								$this->error = 'This order not contains message.';
								return FALSE;
							}

							$this->message 		= $message;
							$this->overview 	= $overview;
							// if this email is added do not repeat it again 
							//view(array("provider" => $provider, "message" => $message), false);
							
							switch($provider)
							{
								case 'REA':
									$this->ci->load->library('Lead_reader_rea');
									$lead = $this->ci->lead_reader_rea->process_lead();
								break;
								case 'APT':
									$this->ci->load->library('Lead_reader_apt');
									$lead = $this->ci->lead_reader_apt->process_lead();
								break;					
							}
							
							if(isset($lead['email']))
							{
								$this->ci->lead_readers->save_lead($lead);

								$source = array();
								$source['source'] = $provider;
								$source['email_number'] 		= $overview->msgno;
								$source['email_date'] 			= date('Y-m-d H:i:s', strtotime($overview->date));
								$source['email_from'] 			= $overview->from;
								$source['email_to'] 			= $overview->to;
								$source['email_subject'] 		= $overview->subject;
								$source['text'] 				= $this->message;
								$this->ci->lead_readers->save_order_source($source);
							}
						}
						else
						{
							$msg[] = "Order is in source";
							$leads[] = array(@$msg,@$overview);
						}
						
					}
					else
					{
						$msg[] = "Provider Not Found";
						$leads[] = array(@$msg,@$overview);		
					}				
				}	
			}
			//view(array("msg" => $msg, "leads" => $leads));
			die('done');
			
		}
		else
		{
			view(array(imap_errors(), imap_alerts()));
		}
	}

	function find_provider($from_address = FALSE)
	{
		if(!$from_address)
		{
			$this->error = 'No From address passed';
			return FALSE;	
		} 
		$old = $from_address;
		$from_address = filter_var(extract_email($from_address),FILTER_VALIDATE_EMAIL);;
		 // view(array($from_address,$old));
		foreach ($this->providers as $key => $email_from) 
		{
			// view(array($from_address,$email_from['email']),FALSE);
			if (trim(strtolower($from_address)) == trim(strtolower($email_from['email']))) 
			{
				return $email_from['name'];
			}
		}

		$this->error = 'Provider not found';
		return FALSE;	
	}

}