 <?php
	/**
	 * GID = group id = 74
	 * CID = Office id = <ClientID>5000</ClientID>
	 */
	class Real_Estate_View_Connector 
	{
		public $defaultTimeOut = 120;
		private $_url = 'http://www.realestateview.com.au/';
		
		private $_company = 'barkingbird';
		private $_code = 'Y2hAYmFya2';

		private $_defaultFetchMethod = "GET";
		private $_methodType = 'XML';

		private $_currentURL = '';

		private $_thridPartyId = 1;

		private $_noOfAgentsToTrack = 10;
		private $_agentData = array('Email', 'MobilePhone', 'Name', 'OfficePhone');
		private $_agentDefaultDesignation = 'Property Agent';

		private $_noOfPhotosToTrack = 9;
		private $_photoData = array('Featured', 'Original', 'Thumb');
		private $_single_entry_photoData = array('FeaturedURL', 'OriginalURL', 'ThumbURL', 'URL');

		private $_noOfISOInspectionToTrack = 20;
		private $_noOfInspectionTimesToTrack = 20;

		private $_house_column_mapping = array('BathroomsCount' => 'no_of_bathrooms', 'BedroomsCount' => 'no_of_bedrooms', 
											'StreetNumber' => 'street_no', 'StreetName~[ ]~StreetType' => 'street',
											'Suburb' => 'city', 'PostCode' => 'post_code', 'CityState' => 'state',
											'TotalParkingCount' => 'no_of_carparks', 'GeocodeLatitude~[,]~GeocodeLongitude' => 'location',
											'Description' => 'description', 'TitleNoHTML' => 'title', 'Price' => 'price',
											'PriceGuideHigh' => 'max_price', 'PriceGuideLow' => 'min_price', 'OrderID' => 'raw_data_order_id',
											'PropertyType' => 'house_type', 'UnitNumber' => 'unit_no', 'PriceText' => 'price_text',
											'Timestamp' => 'rev_timestamp'	 
									  );

		private $fp;
		private $_errorLogOpened = false;

		private $_startPage = 1;
		private $_endPage = '';

		private $_last_updated_time_stamp = '';
		private $_ignoreTimeStamp = false;
		private $_max_timestamp = '';
		private $_no_further_processing = false;

		private $_existing_order_id_array;
		private $_house_ad_status_url_array;
		private $_house_images_file_path;

		const GID = '74';
		const LOG_FILE_LIFE_SPAN = 3;

		const MEDIA_TYPE_IMAGE = '1';
		const MEDIA_TYPE_VIDEO = '2';

		const MEDIA_POSITION_IMAGE = '1';
		const MEDIA_POSITION_VIDEO = '2';
		const MEDIA_POSITION_FLOORPLAN = '3';
		const MEDIA_POSITION_BROCHURE = '4';

		const HOUSE_AD_TYPE_SALE = '1';
		const HOUSE_AD_TYPE_RENT = '2';

		const HOUSE_AD_STATUS_ON_SALE = '1';
		const HOUSE_AD_STATUS_ON_LEASE = '2';
		const HOUSE_AD_STATUS_SOLD = '3';
		const HOUSE_AD_STATUS_LEASED = '4';

		private $_ci;

		public function __construct($data = array())
		{
			$this->fp = null;
			if(is_array($data) && count($data) > 0)
			{
				if(isset($data['default_fetch_method']) && in_array($data['default_fetch_method'], array('GET', 'POST')))
					$this->_defaultFetchMethod = $data['default_fetch_method'];
				if(isset($data['method_type']) && in_array($data['method_type'], array('XML', 'JSON')))
					$this->_methodType = $data['method_type'];
			}

			$this->_ci = &get_instance();
			$this->_ci->load->library('thumb');
			$this->_ci->load->helper('file');

			$this->_existing_order_id_array = array(); /// initialize the existing order id array which by the end of the processing should have all the order id currently on REV ///
			$this->_house_ad_status_url_array = array();
			$this->_house_images_file_path = config_item('house_file_path');

			$this->_last_updated_time_stamp = $this->_get_last_updated_time_for_raw_data();
		}

		private function _create_default_agent_group()
		{
			$result = $this->_ci->db->query("SELECT * FROM agent_groups WHERE name = ?", array('Property Manager'));
			if($result->num_rows() > 0)
				return $result->row();

			return false;
		}

		private function _clean_up_old_logs()
		{
			//fsadfasdafds
			//fsadfdsafds
		}

		private function _get_last_updated_time_for_raw_data()
		{
			$this->_ci->db->from('raw_data_last_updated')
						  ->limit(1);	
			$query = $this->_ci->db->get();						  
			if($query->num_rows() === 0)
			{
				$this->_ignoreTimeStamp = true;
				$this->_max_timestamp = '';
				return '';
			}

			$this->_ignoreTimeStamp = true;	
			$this->_max_timestamp = $query->row()->last_updated_timestamp;
			return $query->row()->last_updated_timestamp;
		}

		private function _generate_authentication_query()
		{
			return array('company' => $this->_company, 'code' => $this->_code);
		}

		private function _reset_current_url()
		{
			$this->_currentURL = '';
		}

		private function _execute()
		{
			$ch = curl_init();
			curl_setopt_array($ch, array(CURLOPT_URL            => $this->_currentURL,
										 CURLOPT_TIMEOUT        => $this->defaultTimeOut,
										 CURLOPT_RETURNTRANSFER => true
									   ));
			$result = curl_exec($ch);
			return $result;
		}

		private function _prepare_error_log()
		{
			try
			{
				$this->fp = fopen(ROOT.'assets/real_esate_view_REST_error'.date('Y-m-d_H:i:s').'.log', 'w');
				$this->_errorLogOpened = true;
			}
			catch(Exception $ex)
			{
				var_dump('Unable to open log file for error reporting');
				$this->_errorLogOpened = true;
			}
		}

		private function _clear_previous_log_files()
		{
			$today = date('Y-m-d');
			foreach(glob(ROOT."assets/*.log") as $file)
			{
				if(strpos($file, 'real_esate_view_REST_error') === false)
					continue;
				
				$fileArray = explode('real_esate_view_REST_error', $file);
				$newFileArray = explode("_", $fileArray[1]);
				$fileDate = trim($newFileArray[0]);
				//echo $fileDate.'<br/>';

				if(strtotime($fileDate) !== false && (strtotime($fileDate) < strtotime($today)))
				{
					$diff = floor((abs(strtotime($today) - strtotime($fileDate))) / (60*60*24));
					//echo  $diff.'<br/>';
					if($diff >= self::LOG_FILE_LIFE_SPAN)
					{
						@unlink($file);
					 	//echo ROOT."assets/".$file."<br/>";
					}	
				}
			}
		}

		private function _check_and_delete_property()
		{
			//var_dump($this->_existing_order_id_array);
			$this->_ci->db->distinct()
						  ->select('raw_data_order_id')	
						  ->from('houses')
						  ->where(array('raw_data_order_id != ' => ''));
			$query = $this->_ci->db->get();	
			
			if($query->num_rows() > 0 && count($this->_existing_order_id_array) > 0) /// just a fail safe that if we cant find any order from REV it could be because of some network problem. So dont delete anything. Try next time ///
			{
				$orderIdArrayFromDB = array_map(create_function('$a', 'return $a->raw_data_order_id;'), $query->result());
				$orderIdArrayToBeDeleted = array_diff($orderIdArrayFromDB, $this->_existing_order_id_array);

				//view(array_diff($this->_existing_order_id_array, $orderIdArrayFromDB));
				//view($orderIdArrayToBeDeleted);

				if(count($orderIdArrayToBeDeleted) > 0)
				{
					// $sql = "select id, safe_name, ad_status from houses where raw_data_order_id IN (".implode(', ', array_fill(0, count($orderIdArrayToBeDeleted), '?')).")";
					// $result = $this->_ci->db->query($sql, $orderIdArrayToBeDeleted);

					$sql = "UPDATE houses SET status = 0 WHERE raw_data_order_id IN (".implode(', ', array_fill(0, count($orderIdArrayToBeDeleted), '?')).")";
					$result = $this->_ci->db->query($sql, $orderIdArrayToBeDeleted);
					
					return;

					//ignore for now.
					$houseIdsToBeDeleted = array_map(create_function('$a', 'return $a->id;') , $result->result());
					$criteria = implode(', ', array_fill(0, count($houseIdsToBeDeleted), '?'));

					$houseSafeNamesToBeDeleted = array();
					foreach($result->result() as $row)
					{
						$extra = $this->_fetch_house_ad_status_text($row->ad_status);
						$houseSafeNamesToBeDeleted[] = trim($row->safe_name).(trim($extra) !== '' ? '-'.trim($extra) : '');
					}
					//$houseSafeNamesToBeDeleted = array_map(create_function('$a', 'return $a->safe_name;') , $result->result());
					
					$this->_ci->db->trans_begin();

					$sql1 = "delete from house_inspection_times where house_id IN (".$criteria.")";
					$result1 = $this->_ci->db->query($sql1, $houseIdsToBeDeleted);

					$sql2 = "delete from agent_house where house_id IN (".$criteria.")";
					$result2 = $this->_ci->db->query($sql2, $houseIdsToBeDeleted);

					$sql3 = "delete from project_house where house_id IN (".$criteria.")";
					$result3 = $this->_ci->db->query($sql3, $houseIdsToBeDeleted);

					$sql4 = "delete from house_images where house_id IN (".$criteria.")";
					$result4 = $this->_ci->db->query($sql4, $houseIdsToBeDeleted);

					$sql5 = "delete from houses where id IN (".$criteria.")";
					$result5 = $this->_ci->db->query($sql5, $houseIdsToBeDeleted);

					if ($this->_ci->db->trans_status() === false)
					{
						$this->_ci->db->trans_rollback();
						if($this->_errorLogOpened === true)
							fwrite($this->fp, trim(time())."Unable to remove. Query executed so far are [".$sql1."]\n[".$sql2."]\n[".$sql3."]\n[".$sql4."]\n[".$sql5."].\nQuery execution failed...\n");
					}   
					else
					{
					    $this->_ci->db->trans_commit();
					    foreach($houseSafeNamesToBeDeleted as $houseSafeName)
						{
							$path = config_item('house_file_path').$houseSafeName.'/';
					        if(file_exists(ROOT.$path) || is_dir(ROOT.$path))
					        {
					            delete_files(ROOT.$path, true);
					            @rmdir(ROOT.$path);
					        }
						}
					}
				}
			}
		}

		public function fetch_and_update()
		{
			$this->_prepare_error_log();

			$urlPart = 'listing_api';
			
			$urlQueryArray = $this->_generate_authentication_query();
			$urlQueryArray['rm'] = 'search';
			$urlQueryArray['GID'] = self::GID;
			$urlQueryArray['sor'] = 'updated';
			//$urlQueryArray['CID'] = '4833';
			$urlQueryPart = http_build_query($urlQueryArray);
			$completeURL = $this->_url.$urlPart.'?'.$urlQueryPart;

			try
			{
				$this->_fetch_by_page($completeURL);
				$this->_ci->db->update('raw_data_last_updated', array('last_updated_timestamp' => $this->_max_timestamp), array('id' => '1'));

				/// check and delete any house info that is not in REV any more ///
				$this->_check_and_delete_property();

				/// check for log files that are created 3 days or earlier, and try to delete them as to keep the directory clear ///
				$this->_clear_previous_log_files();
			}
			catch(Exception $ex)
			{
				var_dump($ex->getMessage());	
			}

			if($this->_errorLogOpened === true)
				fclose($this->fp);
		}

		/*
		 * this function is used once and once only to import all the date from real estate view 
		 * It assumes that the current DB is empty. SO it will by default insert everything and WILL NOT look at the time stamp
		*/
		public function fetch_all()
		{
			$this->_prepare_error_log();

			$urlPart = 'listing_api';
			
			$urlQueryArray = $this->_generate_authentication_query();
			$urlQueryArray['rm'] = 'search';
			$urlQueryArray['GID'] = '74';
			//$urlQueryArray['CID'] = '4833';
			$urlQueryPart = http_build_query($urlQueryArray);
			$completeURL = $this->_url.$urlPart.'?'.$urlQueryPart;
			
			try
			{
				$this->_fetch_by_page($completeURL, $this->_startPage);	

				if($this->_ignoreTimeStamp === true)
					$this->_ci->db->insert('raw_data_last_updated', array('last_updated_timestamp' => $this->_max_timestamp));
				else
					$this->_ci->db->update('raw_data_last_updated', array('last_updated_timestamp' => $this->_max_timestamp), array('id' => '1'));
			}
			catch(Exception $ex)
			{
				var_dump($ex->getMessage());
				//die();
			}

			if($this->_errorLogOpened === true)
				fclose($this->fp);
			
			//die();
		}

		private function _fetch_by_page($url, $currentPage = 1)
		{
			var_dump('Processing Page '.$currentPage);

			$this->_reset_current_url();
			$this->_currentURL = $url;

			$result = $this->_execute();
			if($result === false)
				throw new Exception('Unable to fetch data for url ['.$url.']. Last Page processed so far is ['.($currentPage-1).'] CURL ERROR!!!!');

			$xml = new SimpleXMLElement($result);
			if(trim($xml['ResponseStatus']) !== 'OK')
				throw new Exception('Last Page processed so far is ['.($currentPage-1).'] The XML ERROR IS ['.$xml->error.']');

			$nextPage = $currentPage + 1;
			$nextFound = false;
			foreach($xml->search->Page as $x)
			{	
				if(trim($x['PageNumber']) === trim($nextPage))
				{
					$nextFound = true;
					$nextURL = trim($x->PageURL);
					break;
				}
				else
					continue;
			}

			$this->parse_property_listing_xml($xml);

			if($nextFound === true)
			{
				if(trim($this->_endPage) !== '' && $nextPage > $this->_endPage)
					return true;
				else if($this->_no_further_processing === true)
					return true;

				$this->_fetch_by_page($nextURL, $nextPage);
			}	

			return true;
		}

		private function _insert_raw_data($rawDataArray)
		{
			$queryArray = array();
			foreach($rawDataArray as $rawData)
				$queryArray[] = "(".$this->_ci->db->escape($this->_thridPartyId).", ".$this->_ci->db->escape($rawData).", NOW())";

			if(count($queryArray) > 0)
			{
				$query = "INSERT INTO `raw_data` (`third_party_id`, `data`, `created_date_time`) 
					  VALUES ".implode(', ', $queryArray);
				$result = $this->_ci->db->query($query);
				if($result === false)
				{
					if($this->_errorLogOpened === true)
						fwrite($this->fp, trim(time())."Unable to create raw data. query is[".$query."]. Query execution failed\n");
				}		
			}
			return true;
			//$ci->db->
		}

		private function _format_inspection_data($inspectionTimeData)
		{
			$output = array();

			if(is_array($inspectionTimeData) && count($inspectionTimeData) > 0)
			{
				foreach($inspectionTimeData as $data)
				{
					$tmp  = array();
					$tmp['start'] = $tmp['finish'] = '';

					if(trim($data['start']) === '' && trim($data['finish']) === '')
						continue;
					
					if(($start = strtotime($data['start'])) !== false)
						$tmp['start'] = date('Y-m-d H:i:s', $start);
					if(($finish = strtotime($data['finish'])) !== false)
						$tmp['finish'] = date('Y-m-d H:i:s', $finish);

					$output[] = $tmp;
				}
			}
			return $output;
		}

		private function _check_and_insert_agents($agentData, $officeId)
		{
			$agentIdArray = array();
			$officeId = (trim($officeId) === '' || $officeId === false) ? '' : trim($officeId);
			
			if(is_array($agentData) && count($agentData) > 0)
			{
				foreach($agentData as $agent)
				{
					$insertData = false;

					$queryPart = $paramPart = array();
					$email = (isset($agent['Email']) ? trim($agent['Email']) : '');
					$mobile = (isset($agent['MobilePhone']) ? trim($agent['MobilePhone']) : '');
					$name = (isset($agent['Name']) ? trim($agent['Name']) : '');
					$telephone = (isset($agent['OfficePhone']) ? trim($agent['OfficePhone']) : '');

					if($email !== '')
					{
						$queryPart[] = "email = ?";
						$paramPart[] = $email;
					}	
					if($name !== '')
					{
						$queryPart[] = "`name` like ?";
						$paramPart[] = "%".$name."%";
					}	
					if($email === '' && $name === '' && $mobile !== '')
					{
						$queryPart[] = "mobile = ?";
						$paramPart[] = $mobile;
					}

					if(count($queryPart) > 0)
					{
						/// agent safe name shall be the actual email address before the @ sign ///
						$agentSafeName = '';
						if(trim($email) !== '')
						{
							//$emailArray = explode('@', $email);
							$agentSafeName = url_title($name, '-', true);
						}

						if(trim($agentSafeName) === '')
							$agentSafeName = url_title($name.' '.$email, '-', true);

						$query = 'select * from agents where '.implode(' AND ', $queryPart);
						$result = $this->_ci->db->query($query, $paramPart);
						if($result->num_rows() > 0)
						{
							//$agentSafeName = url_title($name.' '.$email, '-', true);
							$row = $result->row();
							$queryPart = array('`name` = ?', '`mobile` = ?', '`telephone` = ?', '`email` = ?', '`safe_name` = ?', '`office_id` = ?');
							$paramPart = array($name, $mobile, $telephone, $email, $agentSafeName, $officeId, $row->id);

							$updateQuery = "update agents set ".implode(", ", $queryPart)." where id = ?";
							$result = $this->_ci->db->query($updateQuery, $paramPart);				
							$agentIdArray[] = $row->id;
						}	
						else
							$insertData = true;

						if($insertData === true)
						{
							$paramPart = array($email, $name, $telephone, $mobile, $officeId, $agentSafeName, $this->_agentDefaultDesignation);
							$insertQuery = "INSERT INTO `agents` (`email`, `name`, `telephone`, `mobile`, `office_id`, `safe_name`, `designation`) 
											VALUES (".implode(", ", array_fill(0, count($paramPart), '?')).")";
							$result = $this->_ci->db->query($insertQuery, $paramPart);
							if($result !== false)
								$agentIdArray[] = $this->_ci->db->insert_id();
							else
							{
								if($this->_errorLogOpened === true)
									fwrite($this->fp, "Unable to create agent. Query is [".$insertQuery."]. Query Failed...\n");
							}
						}	
					}
					else
					{
						if($this->_errorLogOpened === true)
							fwrite($this->fp, "Unable to locate/create agent [".json_encode($agent)."]. Information missing\n");
					}
				}
			}
			
			return $agentIdArray;
		}

		private function _link_house_with_agents($newHomeId, $agentIdArray)
		{
			/// for an update try to clear any existing agent house link ///
			$this->_ci->db->query("delete from `agent_house` where house_id = ?", $newHomeId);

			if(count($agentIdArray) > 0)
			{
				$agentHouseArray = $paramPart = array();
				$index = 0;

				foreach($agentIdArray as $agentId)
				{
					$agentHouseArray[] = '(?, ?)';
					$paramPart[$index] = $agentId;
					$paramPart[] = $newHomeId;
					$index = $index + 2;
				}	

				$query = 'insert into agent_house (`agent_id`, `house_id`) VALUES '.implode(', ', $agentHouseArray);
				$result = $this->_ci->db->query($query, $paramPart);
				if($result === false)
				{
					if($this->_errorLogOpened === true)
						fwrite($this->fp, "Unable to link agent and house. Tried to execute Query [".$query."]. Execution Failed...\n");
				}
			}
			return true;
		}

		private function _link_house_with_inspection_times($newHomeId, $inspectionTimeData)
		{	
			/// delete any existing house inspection times before inserting ///
			$result = $this->_ci->db->query("delete from `house_inspection_times` where house_id = ?", array($newHomeId));

			if(count($inspectionTimeData) > 0)
			{
				$inspectTimeQueryArray = array();
				$paramPart = array();
				$index = 0;

				foreach($inspectionTimeData as $inspectData)
				{
					$inspectTimeQueryArray[] = array($newHomeId, $inspectData['start'], $inspectData['finish'], 1);
					$paramPart[$index] = $newHomeId;  
					$paramPart[] = $inspectData['start'];  
					$paramPart[] = $inspectData['finish'];  
					$paramPart[] = 1;  
					$index = $index + 4;
				}	
					

				$query = 'insert into house_inspection_times (`house_id`, `start_date_time`, `end_date_time`, `status`) VALUES '
						 .implode(", ", array_fill(0, count($inspectTimeQueryArray), '(?,?,?,?)'));
				
				$result = $this->_ci->db->query($query, $paramPart);
				if($result === false)
				{
					if($this->_errorLogOpened === true)
						fwrite($this->fp, "Unable to add house inspection time. Tried to execute Query [".$query."]. Execution Failed...\n");
				}
			}
			return true;
		}

		private function _check_and_create_office($homeData)
		{
			$title = $description = $homeData['ClientName'].' '.$homeData['ClientSuburb'].' '.$homeData['ClientPostCode'];
			$street = $homeData['ClientAddress'];
			$city = $homeData['ClientSuburb'];
			$postCode = $homeData['ClientPostCode'];
			$officeURL = $homeData['AgentsGroupNameSuburbLink'];
			$safeName = url_title($title, '-', TRUE);

			$result = $this->_ci->db->query("select * from offices where client_id = ? limit 1", array($homeData['ClientID']));
			if($result->num_rows() === 1)
			{
				$queryPart = array('street = ?', 'city = ?', 'post_code = ?',
								   'office_url = ?', 'status = ?');

				$paramPart = array($street, $city, $postCode, $officeURL, 1, $result->row()->id);

				$updateQuery = "update offices set ".implode(", ", $queryPart)." where id = ?";
				$result2 = $this->_ci->db->query($updateQuery, $paramPart);
				return $result->row()->id;
			}
			else
			{
				$queryPart = array($homeData['ClientID'], $title, $description, $street, $city, $postCode, $safeName, $officeURL, '1');

				$insertQuery = "insert into offices (`client_id`, `title`, `description`, `street`, `city`, `post_code`, `safe_name`, `office_url`, `status`)
								values (".implode(", ", array_fill(0, count($queryPart), '?')).")";
				$result = $this->_ci->db->query($insertQuery, $queryPart);
				if($result !== false)
					return $ci->db->insert_id();
			}
			return false;
		}

		private function _copy_house_images_from_old_path_to_new($houseId, $oldPath, $newPath)
		{
			$oldFullPath = ROOT.$this->_house_images_file_path.$oldPath;
			$newFullPath = ROOT.$this->_house_images_file_path.$newPath;

			if(is_dir($oldFullPath) || is_file($oldFullPath))
			{
				make_folder($newFullPath);
	            $previousFiles = get_dir_file_info($oldFullPath, true);
	            if(is_array($previousFiles) && count($previousFiles) > 0)
	            {
	                $dh = opendir($oldFullPath);
	                while(($file = readdir($dh)) !== false)
	                {
	                    if($file != '.' && $file != '..')
	                    {
	                        if(!is_dir($file))
	                            @copy($oldFullPath.'/'.$file, $newFullPath.'/'.$file);
	                    }
	                }
	                closedir($dh);
	                delete_files($oldFullPath, true);  

	                $sql = "select * from house_images where house_id = ?";
	                $query = $this->_ci->db->query($sql, array($houseId));
	                if($query->num_rows() > 0)
	                {
	                	foreach($query->result() as $row)
	                	{
	                		if(($filePath = trim($row->file_path)) !== '')
	                		{
	                			$newFilePath = str_replace($oldPath, $newPath, $filePath);
	                			$sql = 'update house_images set file_path = ? where id = ?';
	                			$this->_ci->db->query($sql, array($newFilePath, $row->id));
	                		}
	                	}
	                }
	            }
	            @rmdir($oldFullPath);   
			}
		}

		/// this function is specifically for youtbe videos being sent from realestataview for a property 
		/// It will convert youtube video URLs from http://youtu.be/9ySXjlmsmaw TO http://www.youtube.com/embed/9ySXjlmsmaw 
		/// It will also convert URL http://www.youtube.com/watch?v=lNdMd8eRwCI&feature=youtu.be to http://www.youtube.com/embed/lNdMd8eRwCI
		private function _rectify_youtube_url($videoURL)
		{
			$urlArray = parse_url($videoURL);
			if(isset($urlArray['host']) && trim($urlArray['host']) !== '' && (stripos($urlArray['host'], 'youtube') !== false || stripos($urlArray['host'], 'youtu.be') !== false))
			{
				$videoId = '';

				if(isset($urlArray['query']) && trim($urlArray['query']) !== '')
				{
					parse_str($urlArray['query']);
					if(isset($v) && trim($v) !== '')
						$videoId = trim($v);		
				}	
				else
				{
					if(isset($urlArray['path']) && ($videoId = trim($urlArray['path'])) !== '')
					{
						if(strpos($videoId, '/') !== false)
							$videoId = substr($videoId, 1);
					}
				}

				if($videoId)
					$videoURL = 'http://www.youtube.com/embed/'.$videoId;
			}
			return $videoURL;
		}

		private function _add_video_link_to_house($newHomeId, $safeName, $videoURL)
		{
			if(($videoURL = trim($videoURL)) === '')
				return true;

			//$videoURL = $this->_rectify_youtube_url($videoURL);

			$sql = "delete from house_images where house_id = ? and media_type = ?";
			$result = $this->_ci->db->query($sql, array($newHomeId, self::MEDIA_TYPE_VIDEO));

			$insertQuery = "insert into house_images (`house_id`, `title`, `description`, `file_path`, `media_type`, `media_position`, `link`)
							values (".$newHomeId.", '', '', '', ".$this->_ci->db->escape(self::MEDIA_TYPE_VIDEO).", 
								    ".$this->_ci->db->escape(self::MEDIA_POSITION_VIDEO).", ".$this->_ci->db->escape($videoURL).")";
			$result = $this->_ci->db->query($insertQuery);
			if(!$result)
			{
				if($this->_errorLogOpened === true)
					fwrite($this->fp, trim(time())."Unable to add house_images (video) for house [".$safeName."(".$newHomeId.")]. Query is [".$insertQuery."]. Query execution failed...\n");	
			}
			return true;
		}

		private function _add_images_to_house($newHomeId, $safeName, $imageDataArray = FALSE, $oldSafeName = false, $oldAdStatus = false)
		{
			if(!$safeName)
				return false;

			/// if it is an existing house and the old safe name is different than the new safe name we have to copy all the images
			/// from the old folder to the new folder before doing anything and update the existing house_images
			if(($oldSafeName !== false && trim($oldSafeName) !== '') && ($oldAdStatus !== false && trim($oldAdStatus) !== ''))
			{
				$oldExtra = $this->_fetch_house_ad_status_text($oldAdStatus);
				$oldSafeName = $oldSafeName.($oldExtra != '' ? '-'.$oldExtra : '');

				if(trim($oldSafeName) !== trim($safeName))
					$this->_copy_house_images_from_old_path_to_new($newHomeId, $oldSafeName, $safeName);
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////

			if(!$imageDataArray || !is_array($imageDataArray) || !count($imageDataArray)) return FALSE;

			/// delete any existing images from the drive as well as from the house_images table ///
			$result = $this->_ci->db->query('delete from house_images where house_id = ? and media_type = ?', array($newHomeId, self::MEDIA_TYPE_IMAGE));
	        $path = $this->_house_images_file_path.$safeName.'/';
	        if($safeName && (trim($path) !== trim($this->_house_images_file_path)) && (file_exists(ROOT.$path) || is_dir(ROOT.$path)))
	        {
	            //$this->_ci->load->helper('file');
	            delete_files(ROOT.$path, true);
	            //@rmdir(ROOT.$path);
	        }
	        /////////////////////////////////////////////////////////////////////////////////

			if(is_array($imageDataArray) && count($imageDataArray) > 0)
			{
				$queryPart = array();
				$displayOrder = 0;
				foreach($imageDataArray as $imageData)
				{
					if((isset($imageData['Original']) && trim($imageData['Original']) !== '') || (isset($imageData['OriginalURL']) && trim($imageData['OriginalURL']) !== ''))
						$imageURL = (isset($imageData['OriginalURL'])) ? trim($imageData['OriginalURL']) : trim($imageData['Original']);

					else if((isset($imageData['Featured']) && trim($imageData['Featured']) !== '') || (isset($imageData['FeaturedURL']) && trim($imageData['FeaturedURL']) !== ''))	
						$imageURL = (isset($imageData['FeaturedURL'])) ? trim($imageData['FeaturedURL']) : trim($imageData['Featured']);

					else
					{
						/// no image data has been fround from the XML, so no point to process. skip it and go the next one ///	
						if($this->_errorLogOpened === true)
							fwrite($this->fp, trim(time())."Unable to add house_images for house [".$safeName."(".$newHomeId.")] as no Original/Featured image has been provided. Data provided is [".json_encode($imageData)."]\n");
						continue;
					}

					/// we have successfully found some image url to download from ///
					if($imageURL !== '')
					{
						/// start downloading image and save it to [assets/media/houses/HOUSE_SAFE_NAME/whatever.jpg] ///
						$imageName = array_pop(explode('/', $imageURL));
						$destFolder = ROOT.$this->_house_images_file_path.$safeName.'/';
						make_folder($destFolder);
						$destLocation = $destFolder.$imageName;
						$imageContent = @file_get_contents($imageURL);
						@file_put_contents($destLocation, $imageContent);

						$dbFilePath = str_replace(ROOT, "", $destLocation);

						/// some how the download failed. no point to process it further. skip it ///
						if(!file_exists($destLocation))
						{
							if($this->_errorLogOpened === true)
								fwrite($this->fp, trim(time())."Unable to donwload image for the new house [".$safeName."(".$newHomeId.")]. downloadable image url is[".$imageURL."].\n");
							continue;
						}
						else
						{
							/// download image from the URL of the XML is successful. Start our version of the image by replicating the image on different sizes ///	
							$newImagesArray = $this->_ci->thumb->make_thumbs($destLocation, 'house_sizes', $destFolder);
							
							/// the image copy was not successful //
							if(($filePath = trim($newImagesArray['main'])) === '')
							{
								if($this->_errorLogOpened === true)
									fwrite($this->fp, trim(time())."Unable to make copy of image for the new house [".$safeName."(".$newHomeId.")]. Image location is [".$destLocation."].\n");
								continue;
							}
							else 	/// the image replication is successful, prepare the query to be inserted to the house_images table ///
							{
								$queryPart[] = "($newHomeId, '', '', ".$this->_ci->db->escape($dbFilePath).", ".$this->_ci->db->escape(self::MEDIA_TYPE_IMAGE).", ".$this->_ci->db->escape(self::MEDIA_POSITION_IMAGE).", ".$this->_ci->db->escape($displayOrder).")";
								$displayOrder++;
							}	
						}	
					}
				}

				/// if we can find any data that can be inserted to the house_images table, start inserting ///
				if(count($queryPart) > 0)
				{
					$query = "insert into house_images (`house_id`, `title`, `description`, `file_path`, `media_type`, `media_position`, `display_order`)
							  values ".implode(", ", $queryPart);
					$result = $this->_ci->db->query($query);		  
					if($result === false)
					{
						if($this->_errorLogOpened === true)
							fwrite($this->fp, trim(time())."Unable to add house_images for the new house. query is[".$query."]. Query execution failed\n");
					}	
				}
			}
			return true;
		}

		private function _add_floorplan_images_to_house($newHomeId, $safeName, $floorplanImageDataArray)
		{
			if(is_array($floorplanImageDataArray) && count($floorplanImageDataArray) > 0)
			{
				$queryPart = array();

				foreach($floorplanImageDataArray as $imageData)
				{
					$displayOrder = 0;
					$imageURL = '';
					if(($imageData = trim($imageData)) !== '')
						$imageURL = $imageData;
					else
						continue;

					/// we have successfully found some image url to download from ///
					if($imageURL !== '')
					{
						/// start downloading image and save it to [assets/media/houses/HOUSE_SAFE_NAME/whatever.jpg] ///
						$imageName = array_pop(explode('/', $imageURL));
						$destFolder = ROOT.config_item('house_file_path').$safeName.'/';
						make_folder($destFolder);
						$destLocation = $destFolder.$imageName;
						$imageContent = file_get_contents($imageURL);
						file_put_contents($destLocation, $imageContent);

						$dbFilePath = str_replace(ROOT, "", $destLocation);

						/// some how the download failed. no point to process it further. skip it ///
						if(!file_exists($destLocation))
						{
							if($this->_errorLogOpened === true)
								fwrite($this->fp, trim(time())."Unable to donwload floorplan image for the new house [".$safeName."(".$newHomeId.")]. downloadable image url is[".$imageURL."].\n");
							continue;
						}
						else
						{
							/// download image from the URL of the XML is successful. Start our version of the image by replicating the image on different sizes ///	
							$newImagesArray = $this->_ci->thumb->make_thumbs($destLocation, 'house_sizes', ROOT.config_item('house_file_path').$safeName.'/');
							
							/// the image copy was not successful //
							if(($filePath = trim($newImagesArray['main'])) === '')
							{
								if($this->_errorLogOpened === true)
									fwrite($this->fp, trim(time())."Unable to make copy of image for the new house [".$safeName."(".$newHomeId.")]. Image location is [".$destLocation."].\n");
								continue;
							}
							else 	/// the image replication is successful, prepare the query to be inserted to the house_images table ///
							{
								$queryPart[] = "($newHomeId, '', '', ".$this->_ci->db->escape($dbFilePath).", ".$this->_ci->db->escape(self::MEDIA_TYPE_IMAGE).", ".$this->_ci->db->escape(self::MEDIA_POSITION_FLOORPLAN).", ".$this->_ci->db->escape($displayOrder).")";
								$displayOrder++;
							}
								
						}	
					}
				}

				/// if we can find any data that can be inserted to the house_images table, start inserting ///
				if(count($queryPart) > 0)
				{
					$query = "insert into house_images (`house_id`, `title`, `description`, `file_path`, `media_type`, `media_position`, `display_order`)
							  values ".implode(", ", $queryPart);
					$result = $this->_ci->db->query($query);		  
					if($result === false)
					{
						if($this->_errorLogOpened === true)
							fwrite($this->fp, trim(time())."Unable to add house_images for the new house. query is[".$query."]. Query execution failed\n");
					}	
				}
			}
			return true;
		}

		private function _fetch_house_ad_status_text($adStatus)
		{
			if(!is_array($this->_house_ad_status_url_array) || count($this->_house_ad_status_url_array) === 0)
				$this->_house_ad_status_url_array = config_item('house_ad_status_url');

			if(trim($adStatus) === '')
				return '';

			if(($value = array_search($adStatus, $this->_house_ad_status_url_array)) === false)
				$value = '';

			return $value;
		}

		private function _chcek_and_update_existing_house($homeData)
		{
			if(isset($homeData['OrderID']))
			{
				$query = "SELECT * FROM houses where raw_data_order_id = ? limit 1";
				$result = $this->_ci->db->query($query, array($homeData['OrderID']));
				if($result->num_rows() === 1)
					return $result->row();
			}
			return false;
		}

		private function _insert_home_related_data($homeData, $agentData, $inspectionTimeData, $imageDataArray, $floorplanImageDataArray)
		{
			$proceed = false;
			if($this->_ignoreTimeStamp === false)
			{
				if(isset($homeData['Timestamp']) && trim($homeData['Timestamp']) !== '')
				{
					if($homeData['Timestamp'] >= $this->_last_updated_time_stamp)
					{
						if($homeData['Timestamp'] >= $this->_max_timestamp)
							$this->_max_timestamp = trim($homeData['Timestamp']);
								
						$proceed = true;
					}	
					else
						$proceed = false;
				}
			}
			else
			{
				$proceed = true;
				if((trim($this->_max_timestamp) === '') || $this->_max_timestamp < $homeData['Timestamp'])
					$this->_max_timestamp = $homeData['Timestamp'];
			}	

			/// we are tyring to update any new data from realestateview. But we did not find any. So don't process ///
			if($proceed === false)
				return;

			$officeId = $this->_check_and_create_office($homeData);
			$inspectionTimeData = $this->_format_inspection_data($inspectionTimeData);
			$agentIdArray = $this->_check_and_insert_agents($agentData, $officeId);
			$agentIdArray = array_unique($agentIdArray);
			//var_dump($agentIdArray);

			$insertData = true;
			$updateData = false;
			$houseId = '';
			$oldSafeName = $oldAdStatus = false;

			/// before inserting check if the house exists on the system or not if exists just update ///
			$existingHouse = $this->_chcek_and_update_existing_house($homeData);
			if($existingHouse !== false && is_object($existingHouse))
			{
				$insertData = false;
				$updateData = false;

				if(trim($existingHouse->rev_timestamp) === '' || (trim($existingHouse->rev_timestamp) !== '' && $existingHouse->rev_timestamp < $homeData['Timestamp']))
				{
					$updateData = true;
					$houseId = $existingHouse->id;
					$oldSafeName = $existingHouse->safe_name;
					$oldAdStatus = $existingHouse->ad_status;
				}
			}

			if($updateData === false && $insertData === false)
				return $insertData;

			///////////////////////////////// otherwise insert ////////////////////////////////////////////

			/// start adding data on the houses table ///
			//$columnArray = $valueArray = array();
			$args = array();

			foreach($this->_house_column_mapping as $key => $value)
			{
				if(strpos($key, '~') !== false)
				{
					list($key1, $connector, $key2) = explode('~', $key);
					if(isset($homeData[$key1]) && isset($homeData[$key2]))
						$args[$value] = $homeData[$key1].rtrim(ltrim($connector, '['), ']').$homeData[$key2];  //$ci->db->escape($homeData[$key1].rtrim(ltrim($connector, '['), ']').$homeData[$key2]);
					else
						$args[$value] = '';	
				}	
				else if(isset($homeData[$key]))
					$args[$value] = $homeData[$key]; // $ci->db->escape($homeData[$key]);
				else
					$args[$value] = '';

				//$columnArray[] = "`".$value."`";
			}

			/// find the ad_type and ad_status for the house before doing insert ///
			if(isset($homeData['AdTypeText']) && ($adTypeText = trim($homeData['AdTypeText'])) !== '')
			{
				if(strpos(strtolower($adTypeText), 'auction') !== false || strpos(strtolower($adTypeText), 'sale') !== false)
					$adType = self::HOUSE_AD_TYPE_SALE;
				else
					$adType = self::HOUSE_AD_TYPE_RENT;

				if(isset($homeData['AvailableOnlyFlag']) && ($isAvailable = trim($homeData['AvailableOnlyFlag'])) !== '')
				{
					if($isAvailable === '1')
					{
						if($adType === self::HOUSE_AD_TYPE_SALE)
							$adStatus = self::HOUSE_AD_STATUS_ON_SALE;
						else
							$adStatus = self::HOUSE_AD_STATUS_ON_LEASE;
					}
					else
					{
						if($adType === self::HOUSE_AD_TYPE_SALE)
							$adStatus = self::HOUSE_AD_STATUS_SOLD;
						else
							$adStatus = self::HOUSE_AD_STATUS_LEASED;
					}	
				}
				else
					$adStatus = '0';
			}
			else
			{
				if($insertData === true)
				{
					$adType = '0';	
					$adStatus = '0';
				}
				else
				{
					$adType = '';	
					$adStatus = '';
				}
			}

			//$columnArray[] = "`ad_type`";
			//$valueArray[] = $ci->db->escape($adType);
			$args['ad_type'] = $adType;

			//$columnArray[] = "`ad_status`";
			//$valueArray[] = $ci->db->escape($adStatus);
			$args['ad_status'] = $adStatus;
			/////////////////////////////////////////////////////////

			$lookupFolder = $this->_fetch_house_ad_status_text($adStatus);

			/// find the Auction date time for the house (IF ANY) before doing insert ///
			if(isset($homeData['AuctionDate']) && ($auctionDate = trim($homeData['AuctionDate'])) !== '')
			{
				if(isset($homeData['AuctionTime']) && ($auctionTime = trim($homeData['AuctionTime'])) !== '')
					$auctionDateTime = $auctionDate.' '.$auctionTime;
				else
					$auctionDateTime = $auctionDate;

				if(($auctionDateTime = strtotime($auctionDateTime)) !== false)
				{
					//$columnArray[] = "`auction_date_time`";
					//$valueArray[] = $ci->db->escape(date('Y-m-d H:i:s', $auctionDateTime));
					$args['auction_date_time'] =  date('Y-m-d H:i:s', $auctionDateTime);
				}
			}	
			/////////////////////////////////////////////////////////

			/// check for is_featured and set the featured_date_time if available ///
			if(isset($homeData['FeaturedProperty']) && trim($homeData['FeaturedProperty']) === '1')
			{
				$args['is_featured'] = '1';
				$args['featured_date_time'] = date('Y-m-d H:i:s');
			}
			else
			{
				/// only set the featured property and date if its a insert. for update dont update the existing entry ///
				if($insertData === true)
				{
					$args['is_featured'] = '0';
					$args['featured_date_time'] = '0000-00-00 00:00:00';
				}
				// else
				// {
				// 	$args['is_featured'] = '0';
				// 	$args['featured_date_time'] = '0000-00-00 00:00:00';
				// }
			}
			//////////////////////////////////////////////////////////////////////////////////

			/// add safe_name to the house ///
			$safeName = url_title($homeData['UnitNumber'].' '.$homeData['StreetNumber'].' '.$homeData['StreetName'].' '.$homeData['StreetType'].' '.$homeData['Suburb'], '-', true);
			$args['safe_name'] = $safeName;
			////////////////////////////////////

			$queryFailed = false;
			$newHomeId = '';

			if($insertData === true)
			{
				//$args['created_date_time'] = date('Y-m-d H:i:s'); 
				$this->_ci->db->set('created_date_time', 'NOW()', FALSE);
				$result = $this->_ci->db->insert('houses', $args);
				//die($this->_ci->last_query());
				if($result === true)
				{
					$houseId = $this->_ci->db->insert_id();
					$newHomeId = $houseId;
				}	
				else
				{
					$queryFailed = true;
					if($this->_errorLogOpened === true)
						fwrite($this->fp, trim(time())."Unable to create new house. query is[".$this->_ci->db->last_query()."]. Query execution failed\n");
				}
			}
			else if(is_numeric($houseId))
			{
				/// any house info that is empty should be discarded while updating house ///
				foreach($args as $key => $value)
				{
					if(trim($value) === '')
						unset($args[$key]);
				}
				///////////////////////////////////////////////////////////////////////

				$updateCriteria = array();
				$updateParam = array();
				
				foreach($args as $key => $value)
				{
					$updateCriteria[] = $key.' = ?';
					$updateParam[] = $value;
				}

				$updateParam[] = $houseId;

				$query = "update houses set ".implode(", ", $updateCriteria)." where id = ?";
				$result = $this->_ci->db->query($query, $updateParam);
				//var_dump($query); var_dump(array_merge($updateParam, array($houseId))); die();
				//$result = $this->_ci->db->update('houses', $args, array('id' => $houseId));
				if($result === false)
				{	
					$queryFailed = true;
					if($this->_errorLogOpened === true)
						fwrite($this->fp, trim(time())."Unable to create new house. query is[".$this->_ci->db->last_query()."]. Query execution failed\n");
				}
				else
					$newHomeId = $houseId;	
			}

			if($queryFailed === false)
			{
				$lookupFolder = $safeName.(($lookupFolder !== '') ? '-'.$lookupFolder : '');

				/// link house with agents if any ///
				$this->_link_house_with_agents($newHomeId, $agentIdArray);

				/// add inspection times to the newly added house ///
				$this->_link_house_with_inspection_times($newHomeId, $inspectionTimeData);

				/// add images to the newly added house ///
				$this->_add_images_to_house($newHomeId, $lookupFolder, $imageDataArray, $oldSafeName, $oldAdStatus);

				/// add video if any to the newly added house ///
				$this->_add_video_link_to_house($newHomeId, $lookupFolder, $homeData['VideoURL']);

				/// add floorplan images to the newly added house ///
				$this->_add_floorplan_images_to_house($newHomeId, $lookupFolder, $floorplanImageDataArray, $oldSafeName, $oldAdStatus);
			}

			return $insertData;
		}

		private function _parse_single_property_listing_xml($property, &$tmp, &$agentDataArray, &$inspectionTimeArray, &$imageDataArray, &$floorplanImageDataArray)
		{
			$tmp['AdType'] = trim($property->AdType);
			$tmp['AdTypeText'] = trim($property->AdTypeText);
			$tmp['AddressText'] = trim($property->AddressText);
			$tmp['AgentsGroupNameSuburbLink'] = trim($property->AgentsGroupNameSuburbLink);
			$tmp['AuctionDate'] = trim($property->AuctionDate);
			$tmp['AuctionProperty'] = trim($property->AuctionProperty);
			$tmp['AuctionTime'] = trim($property->AuctionTime);
			$tmp['AvailableOnlyFlag'] = trim($property->AvailableOnlyFlag);
			$tmp['BathroomsCount'] = trim($property->BathroomsCount);
			$tmp['BedroomsCount'] = trim($property->BedroomsCount);
			$tmp['CarportsCount'] = trim($property->CarportsCount);
			$tmp['CityState'] = trim($property->CityState);
			$tmp['CityStateText'] = trim($property->CityStateText);
			$tmp['ClientAddress'] = trim($property->ClientAddress);
			$tmp['ClientID'] = trim($property->ClientID);
			$tmp['ClientIsREIVMember'] = trim($property->ClientIsREIVMember);
			$tmp['ClientLogoAltText'] = trim($property->ClientLogoAltText);
			$tmp['ClientLogoResultsURL'] = trim($property->ClientLogoResultsURL);
			$tmp['ClientName'] = trim($property->ClientName);
			$tmp['ClientPostCode'] = trim($property->ClientPostCode);
			$tmp['ClientProfileSubUrl'] = trim($property->ClientProfileSubUrl);
			$tmp['ClientState'] = trim($property->ClientState);
			$tmp['ClientStateText'] = trim($property->ClientStateText);
			$tmp['ClientSuburb'] = trim($property->ClientSuburb);
			$tmp['CommercialLeaseText'] = trim($property->CommercialLeaseText);
			
			for($i = 1; $i <= $this->_noOfAgentsToTrack; $i++)
			{
				$singleAgentData = array();
				foreach($this->_agentData as $agentData)
				{
					$elementName = trim('ContactAgent'.$i.$agentData);
					$tmp[$elementName] = trim($property->$elementName);
					$singleAgentData[$agentData] = $tmp[$elementName];
				}
				$agentDataArray[] = $singleAgentData;
			}

			$tmp['CreationDate'] = trim($property->CreationDate);
			$tmp['Description'] = trim($property->Description);
			$tmp['DisplayTrueAddress'] = trim($property->DisplayTrueAddress);
			$tmp['FeaturedProperty'] = trim($property->FeaturedProperty);
			$tmp['FirstPropertyTypeOnly'] = trim($property->FirstPropertyTypeOnly);
			
			/// floorplan calculation below ///

			$tmp['ForSale'] = trim($property->ForSale);
			$tmp['FullDisplayLink'] = trim($property->FullDisplayLink);
			$tmp['GeocodeLatitude'] = trim($property->GeocodeLatitude);
			$tmp['GeocodeLocationType'] = trim($property->GeocodeLocationType);
			$tmp['GeocodeLongitude'] = trim($property->GeocodeLongitude);
			$tmp['GeocodeStatus'] = trim($property->GeocodeStatus);
			$tmp['GeocodeTypes'] = trim($property->GeocodeTypes);
			$tmp['GroupBackgroundColor'] = trim($property->GroupBackgroundColor);
			$tmp['GroupFont'] = trim($property->GroupFont);
			$tmp['GroupFontColor'] = trim($property->GroupFontColor);
			$tmp['GroupID'] = trim($property->GroupID);
			$tmp['HostName'] = trim($property->HostName);
			
			for($i = 1; $i <= $this->_noOfISOInspectionToTrack; $i++)
			{
				$tmpArray = array();

				$element1Name = trim('ISOInspectionStart'.$i);
				$element2Name = trim('ISOInspectionFinish'.$i);

				$tmp[$element1Name] = trim($property->$element1Name);
				$tmp[$element2Name] = trim($property->$element2Name);

				$tmpArray['start'] = $tmp[$element1Name];
				$tmpArray['finish'] = $tmp[$element2Name];

				$inspectionTimeArray[] = $tmpArray;
			}

			for($i = 1; $i <= $this->_noOfInspectionTimesToTrack; $i++)
			{
				$elementName = trim('InspectionTime'.$i);
				$tmp[$elementName] = trim($property->$elementName);
			}

			$tmp['IsResidential'] = trim($property->IsResidential);
			$tmp['IsCommercial'] = trim($property->IsCommercial);
			$tmp['IsSaleOnly'] = trim($property->IsSaleOnly);
			$tmp['LandSize'] = trim($property->LandSize);
			$tmp['LandSizeText'] = trim($property->LandSizeText);
			$tmp['ListingResultsCount'] = trim($property->ListingResultsCount);
			$tmp['LiveServerCheck'] = trim($property->LiveServerCheck);
			$tmp['LockUpGaragesCount'] = trim($property->LockUpGaragesCount);
			$tmp['OrderID'] = trim($property->OrderID);

			$tmp['PhotosCount'] = trim($property->PhotosCount);
			if(is_numeric($tmp['PhotosCount']))
				$this->_noOfPhotosToTrack = $tmp['PhotosCount'];

			$goodImages = array();
			if($this->_noOfPhotosToTrack > 0)
			{	
				$goodImages = $this->_fetch_good_size_image($tmp['OrderID']);
				$imageDataArray = $goodImages['images'];

				/// just a back up plan. If the single fetch fails to give us any image data, rely on the list data ///
				if(!is_array($imageDataArray) || count($imageDataArray) === 0)
				{
					$imageDataArray = array();
					for($i = 1; $i <= $this->_noOfPhotosToTrack; $i++)
					{
						$tmpArray = array();

						foreach($this->_photoData as $photoData)
						{
							$elementName = trim('Photo'.$i.$photoData.'URL');
							$tmp[$elementName] = trim($property->$elementName);
							$tmpArray[$photoData] = $tmp[$elementName];
						}
						$imageDataArray[] = $tmpArray;
					}
				}
				else
					$tmp['all_good_image_data'] = $imageDataArray;
				////////////////////////////////////////////////////////////////////////////////////////////////
			}

			/// data manipulation related to floor plan ///
			/// check if we have already found good quality floorplan images from the single request. if we have use that instead of the list data ///
			if(isset($goodImages['floorplans']) && is_array($goodImages['floorplans']) && count($goodImages['floorplans']) > 0)
			{
				$floorplanImageDataArray = $goodImages['floorplans'];
				$tmp['all_good_floorplan_images'] = $floorplanImageDataArray;
			}
			else // we haven't found any floor plan data from the individual request. Try the normal way ///
			{
				$tmp['FloorplansCount'] = trim($property->FloorplansCount);
				$tmp['FloorplansExist'] = trim($property->FloorplansExist);

				if(($floorPlanCounts = trim($tmp['FloorplansCount'])) !== '' && is_numeric($floorPlanCounts))
				{
					for($i = 1; $i <= $floorPlanCounts; $i++)
					{
						$elementName = 'Floorplan'.$i.'ThumbURL';
						$tmp[$elementName] = trim($property->$elementName);
						$floorplanImageDataArray[] = $tmp[$elementName];
					}	
				}
			}
			//////////////////////////////////////////////

			$tmp['PhotoFloorPlanUpdate'] = trim($property->PhotoFloorPlanUpdate);
			$tmp['PortalSection'] = trim($property->PortalSection);
			$tmp['PortalView'] = trim($property->PortalView);
			$tmp['PostCode'] = trim($property->PostCode);
			$tmp['Price'] = trim($property->Price);
			$tmp['PriceGuideAgentEnquiryOverlay'] = trim($property->PriceGuideAgentEnquiryOverlay);
			$tmp['PriceGuideHigh'] = trim($property->PriceGuideHigh);
			$tmp['PriceGuideHighLease'] = trim($property->PriceGuideHighLease);
			$tmp['PriceGuideLow'] = trim($property->PriceGuideLow);
			$tmp['PriceGuideLowLease'] = trim($property->PriceGuideLowLease);
			$tmp['PriceText'] = trim($property->PriceText);
			$tmp['PropertyType'] = trim($property->PropertyType);
			$tmp['PropertyType2'] = trim($property->PropertyType2);
			$tmp['PropertyType3'] = trim($property->PropertyType3);
			$tmp['PropertyTypeText'] = trim($property->PropertyTypeText);
			$tmp['RentAmount'] = trim($property->RentAmount);
			$tmp['ResidentialDomain'] = trim($property->ResidentialDomain);
			$tmp['Rooms'] = trim($property->Rooms);
			$tmp['SaleLeaseAlertPriceText'] = trim($property->SaleLeaseAlertPriceText);
			$tmp['SaleLeaseAlertText'] = trim($property->SaleLeaseAlertText);
			$tmp['SaleLeaseText'] = trim($property->SaleLeaseText);
			$tmp['SaleLeaseTypeText'] = trim($property->SaleLeaseTypeText);
			$tmp['SoldPriceUndisc'] = trim($property->SoldPriceUndisc);
			$tmp['StreetName'] = trim($property->StreetName);
			$tmp['StreetNumber'] = trim($property->StreetNumber);
			$tmp['StreetOnlyAddressText'] = trim($property->StreetOnlyAddressText);
			$tmp['StreetType'] = trim($property->StreetType);
			$tmp['Suburb'] = trim($property->Suburb);
			
			$tmp['Title'] = trim($property->Title);
			$tmp['TitleNoHTML'] = trim($property->TitleNoHTML);
			$tmp['TotalParkingCount'] = trim($property->TotalParkingCount);
			$tmp['TrueAddressText'] = trim($property->TrueAddressText);
			$tmp['VideoURL'] = trim($property->VideoURL);
			$tmp['UnitNumber'] = trim($property->UnitNumber);
			$tmp['ViewFullDisplayAltTag'] = trim($property->ViewFullDisplayAltTag);
			$tmp['VirtualTourURL'] = trim($property->VirtualTourURL);

			return true;
		}

		public function parse_property_listing_xml(SimpleXMLElement $xml)
		{
			$propertyDataArray = array();
			//var_dump($xml->listing); die();

			foreach($xml->listing as $property)
			{
				$processData = false;
				
				$tmp = array();
				$tmp['OrderID'] = trim($property['OrderID']);
				$tmp['Timestamp'] = trim($property->Timestamp);
				//echo 'Now viewing :'.$tmp['Timestamp'].' comparing with '.$this->_last_updated_time_stamp.'<br/>';

				$this->_existing_order_id_array[] = $tmp['OrderID'];  /// keep track of all the order ids we have found from REV ///

				/// if it is an existing house with the same time stamp as the XML no need to process it. SKIP IT ///
				$eHouse = $this->_chcek_and_update_existing_house(array('OrderID' => $tmp['OrderID']));
				if($eHouse !== false && is_object($eHouse))
				{
					if(trim($eHouse->rev_timestamp) !== '' && $eHouse->rev_timestamp >= $tmp['Timestamp'])
						continue;
				}
				/////////////////////////////////////////////////////////////////////////////////////////////////////////

				if(($this->_ignoreTimeStamp === true) || ($tmp['Timestamp'] >= $this->_last_updated_time_stamp))
					$processData = true;
				else
					$processData = false;  /// we have already processed the data from this point afterwards // so no need to proceed ///

				if($processData === true)
				{
					//echo 'Timestamp processing : '.$tmp['Timestamp'].' which is greater than '.$this->_last_updated_time_stamp.'<br/>';
					$agentDataArray = $inspectionTimeArray = $imageDataArray = $floorplanImageDataArray = array();
					$this->_parse_single_property_listing_xml($property, $tmp, $agentDataArray, $inspectionTimeArray, $imageDataArray, $floorplanImageDataArray);

					$newInsert = $this->_insert_home_related_data($tmp, $agentDataArray, $inspectionTimeArray, $imageDataArray, $floorplanImageDataArray);

					if($newInsert === true)
					{
						$rawData = json_encode($tmp);
						$propertyDataArray[] = $rawData;
					}
				}
			}

			$this->_insert_raw_data($propertyDataArray);
		}

		private function _fetch_good_size_image($orderId)
		{
			$output = array();
			$output['images'] = array();
			$output['floorplans'] = array();

			if (!$orderId) return $output;

			/// reset the current url and set it to fetch individual entry ///
			$this->_reset_current_url();

			$urlPart = 'listing_api';
			$urlQueryArray = $this->_generate_authentication_query();
			$urlQueryArray['rm'] = 'view';
			$urlQueryArray['OID'] = $orderId;
			$urlQueryPart = http_build_query($urlQueryArray);
			$completeURL = $this->_url.$urlPart.'?'.$urlQueryPart;
			$this->_currentURL = $completeURL;
			////////////////////////////////////////////////////////////
			
			/// request a single entry details, get the data and process only the image part ///
			$singleEntry = $this->_execute();
			if($singleEntry === false)
				return $output;

			$singleXML = new SimpleXMLElement($singleEntry);
			if(trim($singleXML['ResponseStatus']) !== 'OK')
				return $output;

			if(trim($orderId) === trim($singleXML->listing['OrderID']))
			{
				/// get the good quality floorplans if any ///	
				$totalFloorplanCount = trim($singleXML->listing->FloorplansCount);
				if($totalFloorplanCount !== '' && is_numeric($totalFloorplanCount) && $totalFloorplanCount > 0)
				{
					for($i = 1; $i <= $totalFloorplanCount; $i++)
					{
						$fElementName = trim('Floorplan'.$i.'URL');
						$output['floorplans'][] = trim($singleXML->listing->$fElementName);
					}
				}
				////////////////////////////////////////////////////

				/// get the good quality image ///
				$totalImageCount = trim($singleXML->listing->PhotosCount);
				if($totalImageCount !== '' && is_numeric($totalImageCount) && $totalImageCount > 0)
				{
					for($i = 1; $i <= $totalImageCount; $i++)
					{
						$tmp = array();
						foreach($this->_single_entry_photoData as $photoData)
						{
							$elementName = trim('Photo'.$i.$photoData);
							$tmp[$photoData] = trim($singleXML->listing->$elementName);
						}	
						$output['images'][] = $tmp;
					}
				}
				//////////////////////////////////////////
			}
			return $output;
		}
	}