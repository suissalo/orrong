<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Lead_reader_apt extends Lead_reader
{
	var $message;
	var $order;
	function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->ci->load->model('admin/lead_readers');
	}

	function process_lead()
	{
		$this->message 							= str_replace(array(PHP_EOL, "\r", "\t"), '', $this->ci->lead_reader->message);
		$this->lead 							= array();
		
		$this->lead['form_id'] = 1001;
		$this->lead['email'] = $this->find_email();
		$names = explode(" ", $this->find_name());
		$name = (isset($names[0]))? $names[0] : '';
		$surname = (isset($names[1]))? $names[1] : '';
		$this->lead['name'] = $name;
		$phone = $this->find_phone();
		$comments = $this->find_comments();
		$this->lead['content'] = json_encode(array(
			"first_name" => $name,
			"last_name" => $surname,
			"email" => $this->lead['email'],
			"phone" => $phone,
			"comments" => $comments
		));
		$this->lead['published'] = 1;
		$this->lead['date_created'] = date('Y-m-d H:i:s');
		$this->lead['deleted'] = 0;
		$this->lead['lead_origin'] = 'APT';

		return $this->lead;
	}

	private function find_email()
	{
		$startDelimiter = '<br>Email: <b> <a href="mailto:';
		$endDelimiter 	= '">';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}
		
		return $text;
	}

	private function find_name()
	{
		$startDelimiter = '<p>Name: <b> ';
		$endDelimiter 	= '<br></b>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}
		

		return $text;
	}

	// private function find_surname()
	// {
	// 	$startDelimiter = '<p><strong>Surname:</strong> ';
	// 	$endDelimiter 	= '</p>';

	// 	if(stripos($this->message, $startDelimiter))
	// 	{
	// 		$text = between_text($this->message, $startDelimiter, $endDelimiter);
	// 		$text 	= str_replace(' ', '', $text);
	// 	}
	// 	else
	// 	{
	// 		$text = '';
	// 	}

	// 	return $text;
	// }

	private function find_phone()
	{
		$startDelimiter = '<br>Phone: <b> ';
		$endDelimiter 	= '<br></b>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}
		
		return $text;
	}

	// private function find_about()
	// {
	// 	$startDelimiter = '<p><strong>About me:</strong> ';
	// 	$endDelimiter 	= '</p>';

	// 	if(stripos($this->message, $startDelimiter))
	// 	{
	// 		$text = between_text($this->message, $startDelimiter, $endDelimiter);
	// 	}
	// 	else
	// 	{
	// 		$text = '';
	// 	}
		
	// 	return $text;
	// }

	private function find_comments()
	{
		$startDelimiter = '<br>Comment:<br><b><br>';
		$endDelimiter 	= '<br></b></p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
		}
		else
		{
			$text = '';
		}

		return $text;
	}
}