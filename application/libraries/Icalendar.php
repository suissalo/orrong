<?php
// Variables used in this script:
// $summary - text title of the event
// $datestart - the starting date (in seconds since unix epoch)
// $dateend - the ending date (in seconds since unix epoch)
// $address - the event's address
// $uri - the URL of the event (add http://)
// $description - text description of the event
// $filename - the name of this file for saving (e.g. my-event-name.ics)
//
// Notes:
// - the UID should be unique to the event, so in this case I'm just using
// uniqid to create a uid, but you could do whatever you'd like.
//
// - iCal requires a date format of "yyyymmddThhiissZ". The "T" and "Z"
// characters are not placeholders, just plain ol' characters. The "T"
// character acts as a delimeter between the date (yyyymmdd) and the time
// (hhiiss), and the "Z" states that the date is in UTC time. Note that if
// you don't want to use UTC time, you must prepend your date-time values
// with a TZID property. See RFC 5545 section 3.3.5
//
// - The Content-Disposition: attachment; header tells the browser to save/open
// the file. The filename param sets the name of the file, so you could set
// it as "my-event-name.ics" or something similar.
//
// - Read up on RFC 5545, the iCalendar specification. There is a lot of helpful
// info in there, such as formatting rules. There are also many more options
// to set, including alarms, invitees, busy status, etc.
//
// https://www.ietf.org/rfc/rfc5545.txt

class ICalendar
{
	private $_cal_file_data;

	public function __construct()
	{
		$this->_cal_file_data = '';
	}

	public function generate_unique_id()
	{

	}	
	 
	// Define helper functions
	// Converts a unix timestamp to an ics-friendly format
	// NOTE: "Z" means that this timestamp is a UTC timestamp. If you need
	// to set a locale, remove the "\Z" and modify DTEND, DTSTAMP and DTSTART
	// with TZID properties (see RFC 5545 section 3.3.5 for info)
	//
	// Also note that we are using "H" instead of "g" because iCalendar's Time format
	// requires 24-hour time (see RFC 5545 section 3.3.12 for info).

	// the Z According to the RFC, there are several accepted forms for the date-time value. The one without 'Z' is referred to as "date with local time", which means that there is no time zone information associated with it. Adding a trailing 'Z' puts the date-time into UTC ("date with UTC time"). Lastly, you can define the time zone explicitly ("date with local time and time zone reference")
	public function dateToCal($timestamp) 
	{
		return date('Ymd\THis', $timestamp);
	}
	 
	// Escapes a string of characters
	public function escapeString($string) 
	{
		return preg_replace('/([\,;])/','\\\$1', $string);
	}

	/// this function generates the calendar file with the given data and should auto prompt for download ///
	public function generate($startTimeStamp, $endTimeStamp, $location, $description, $url, $summary)
	{
		if(trim($startTimeStamp) !== '' && trim($endTimeStamp) !== '')
		{
			$this->_cal_file_data .= "BEGIN:VCALENDAR\r\n";
			$this->_cal_file_data .= "VERSION:2.0\r\n";
			$this->_cal_file_data .= "PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\r\n";
			$this->_cal_file_data .= "CALSCALE:GREGORIAN\r\n";
			$this->_cal_file_data .= "BEGIN:VEVENT\r\n";

			$this->_cal_file_data .= "DTEND:".$this->dateToCal($endTimeStamp)."\r\n";
			$this->_cal_file_data .= "LOCATION:".$this->escapeString($location)."\r\n";
			$this->_cal_file_data .= "DESCRIPTION:".$this->escapeString($description)."\r\n";
			$this->_cal_file_data .= "URL;VALUE=URI:".$this->escapeString($url)."\r\n";
			$this->_cal_file_data .= "SUMMARY:".$this->escapeString($summary)."\r\n";
			$this->_cal_file_data .= "DTSTART:".$this->dateToCal($startTimeStamp)."\r\n";
			$this->_cal_file_data .= "END:VEVENT\r\n";
			$this->_cal_file_data .= "END:VCALENDAR";

			// Set the correct headers for this file
			$filename = str_replace(" ", "_", trim($location.' '.$description)).".ics";								  
			header('Content-type: text/calendar; charset=utf-8');
			header('Content-Disposition: attachment; filename=' . $filename);
			echo $this->_cal_file_data;
		}
	}
}
