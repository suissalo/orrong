<?php
class Emailer
{
	private $email_config;
	
	public function __construct()
	{
		$ci = &get_instance();
		$ci->load->library('email');

		$this->email_config = array(
			// 'smtp_port'		=> 25,
			// 'smtp_host'    	=> "114.141.201.156",
			// 'smtp_user' 	=> 'peck@barkingbird.com.au',
			// 'smtp_pass' 	=> 'urb@pe41touch',
			'protocol'		=> 'smtp',
			'mailtype' 		=> 'html',
			'useragent'		=> 'Barkingbird'			
		);

		$this->email_from_address = 'no-reply@700orrong.com.au';
		$this->email_from_name = '700 Orrong';
	}


	function general_email($address_to)
	{
		$ci     				= &get_instance();
		$ci->load->model('page');

		$message 				= '';
		$config 				= $this->email_config;
		
		$ci->email->initialize($config);
		$ci->email->to($address_to);
		$ci->email->from($this->email_from_address, $this->email_from_name);
		$ci->email->subject('');
		$ci->email->message($message);
		return $ci->email->send();
	}

}