# README #

Based on Whitelabel.

### How do I get set up? ###

Uses gulp version 3 and has issues with npm version > 10 so using package manager, install/use npm 10
```
$ nvm install 10
$ npm install -g gulp
```


Install node dependencies for gulp under /assets
```
$ cd assets
$ npm install
```
Compile (and watch) SCSS
```
$ gulp
```
When ready to upload from a new site, run the build to compress the images as well
```
$ gulp build
```

When ready to upload to production, run through the min
```
$ gulp build:production
```

Add database connection settings to application/config/database.php and use dump.sql as a starting point

Change the site name in web_application.php

Also remember to generate favicons and put them in assets/images/favicon.ico/ folder

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact