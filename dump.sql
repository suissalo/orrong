# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.26)
# Database: 700orrong
# Generation Time: 2020-03-31 21:28:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_groups`;

CREATE TABLE `admin_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0' COMMENT 'owner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL COMMENT '0=inactive, 1=active',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `admin_groups` WRITE;
/*!40000 ALTER TABLE `admin_groups` DISABLE KEYS */;

INSERT INTO `admin_groups` (`id`, `parent_id`, `name`, `status`, `date_created`)
VALUES
  (1,0,'Master Admin',1,'0000-00-00 00:00:00'),
  (2,0,'User',1,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `admin_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '2',
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(255) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `notes` text NOT NULL,
  `bio` text,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT '9999',
  `hash` varchar(255) DEFAULT NULL,
  `hash_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;

INSERT INTO `admin_users` (`id`, `group_id`, `owner_id`, `name`, `pass`, `date_added`, `username`, `contact_email`, `phone`, `notes`, `bio`, `title`, `image`, `display_order`, `hash`, `hash_date`)
VALUES
  (1,1,0,'tech@barkingbird.com.au','1452af4a3651a77ef660ce0a168b179c475d672e','2018-12-04 01:12:30','tech@barkingbird.com.au','tech@barkingbird.com.au','9514 3112','','','',NULL,9999,'','0000-00-00 00:00:00'),
  (2,1,0,'Support','73f11ff31ba5c144e0c06ce607ac907f388d7152','2017-10-25 01:35:35','support@barkingbird.com.au','support@barkingbird.com.au',NULL,'',NULL,NULL,NULL,9999,NULL,NULL);

/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blog_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `permalink` varchar(255) NOT NULL DEFAULT '' COMMENT 'If left blank, the permalink will automatically be created for you.',
  `published` int(1) NOT NULL DEFAULT '1',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `display_order` int(2) DEFAULT '99',
  `safe_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permalink` (`permalink`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_images`;

CREATE TABLE `blog_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `main` varchar(255) NOT NULL DEFAULT '',
  `impressions` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `display_order` int(4) DEFAULT '9999',
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_posts`;

CREATE TABLE `blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `safe_name` varchar(255) DEFAULT NULL,
  `permalink` varchar(255) NOT NULL DEFAULT '' COMMENT 'This is the last part of the url string. If left blank, the permalink will automatically be created for you.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `excerpt` text NOT NULL COMMENT 'A condensed version of the content',
  `hero` varchar(255) NOT NULL DEFAULT '',
  `author_id` int(10) unsigned NOT NULL COMMENT 'If left blank, you will assumed be the author.',
  `allow_comments` int(1) unsigned DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `published` int(1) unsigned NOT NULL DEFAULT '0',
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `date_published` timestamp NULL DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `featured` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permalink` (`permalink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_posts_to_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_posts_to_categories`;

CREATE TABLE `blog_posts_to_categories` (
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `ip_address` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `user_agent` varchar(120) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table footer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `footer`;

CREATE TABLE `footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `columns` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `footer` WRITE;
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;

INSERT INTO `footer` (`id`, `columns`)
VALUES
  (1,5);

/*!40000 ALTER TABLE `footer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table footer_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `footer_links`;

CREATE TABLE `footer_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(2) NOT NULL,
  `column` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`,`column`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table form_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_leads`;

CREATE TABLE `form_leads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NULL DEFAULT NULL,
  `notification_sent` timestamp NULL DEFAULT NULL,
  `responder_sent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `responder_active` tinyint(1) DEFAULT '0',
  `responder_from_name` varchar(255) DEFAULT NULL,
  `responder_from_email` varchar(255) DEFAULT NULL,
  `responder_subject` varchar(255) DEFAULT NULL,
  `responder_message` text,
  `notification_active` tinyint(1) DEFAULT '0',
  `notification_from_name` varchar(255) DEFAULT NULL,
  `notification_from_email` varchar(255) DEFAULT NULL,
  `notification_recipients` text,
  `notification_subject` varchar(255) DEFAULT NULL,
  `notification_message` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `display_order` int(4) DEFAULT '9999',
  `arrows` tinyint(1) DEFAULT NULL,
  `thumbs` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT '1',
  `hover` tinyint(1) DEFAULT NULL,
  `bg_colour` varchar(7) DEFAULT NULL,
  `navigation` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `meta_keywords` (`meta_keywords`,`meta_description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`id`, `title`, `content`, `display_order`, `arrows`, `thumbs`, `status`, `hover`, `bg_colour`, `navigation`, `meta_keywords`, `meta_description`)
VALUES
  (1,'700 Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery_images`;

CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `width` varchar(100) DEFAULT NULL,
  `height` varchar(100) DEFAULT NULL,
  `image` text,
  `thumb` varchar(255) DEFAULT '',
  `main` varchar(255) DEFAULT '',
  `impressions` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `caption` text,
  `caption_position` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT '',
  `display_order` int(4) DEFAULT '9999',
  `image_hover` varchar(255) DEFAULT NULL,
  `webm` varchar(255) DEFAULT NULL,
  `mp4` varchar(255) DEFAULT NULL,
  `link_text` varchar(255) DEFAULT NULL,
  `artists_impression` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table geocoding
# ------------------------------------------------------------

DROP TABLE IF EXISTS `geocoding`;

CREATE TABLE `geocoding` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `display` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  `theme` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_heading` varchar(255) NOT NULL DEFAULT '',
  `detail` text,
  `sidebar` text,
  `permalink` varchar(255) DEFAULT NULL,
  `navigation` varchar(225) DEFAULT '0',
  `template` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(4) DEFAULT '9999',
  `image` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `nav_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nav_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nav_position` int(11) NOT NULL,
  `footer` int(1) DEFAULT NULL,
  `enable_link` int(1) DEFAULT '1',
  `contact_form` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'the url of the contact form to use',
  `contact_embed` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`),
  KEY `enable_link` (`enable_link`),
  FULLTEXT KEY `detail` (`detail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `display`, `timestamp`, `user`, `status`, `theme`, `title`, `sub_heading`, `detail`, `sidebar`, `permalink`, `navigation`, `template`, `meta_title`, `meta_keywords`, `meta_description`, `display_order`, `image`, `gallery_id`, `nav_icon`, `nav_title`, `nav_position`, `footer`, `enable_link`, `contact_form`, `contact_embed`)
VALUES
  (1,0,0,'2020-03-31 17:12:55',NULL,'1',NULL,'700','','',NULL,'700/','0','roi','','','',1,NULL,1,NULL,'',0,0,1,'',0);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories`;

CREATE TABLE `project_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `safe_name` varchar(255) DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '999',
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table project_categories_relation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories_relation`;

CREATE TABLE `project_categories_relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `detail` text,
  `permalink` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(4) DEFAULT '9999',
  `hero_image` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '1',
  `address` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `budget` varchar(255) DEFAULT NULL,
  `commenced` varchar(255) DEFAULT NULL,
  `completed` varchar(255) DEFAULT NULL,
  `architect` varchar(255) DEFAULT NULL,
  `interiors` varchar(255) DEFAULT NULL,
  `landscaper` varchar(255) DEFAULT NULL,
  `safe_name` varchar(255) DEFAULT NULL,
  `intro` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `testimonial` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`),
  FULLTEXT KEY `detail` (`detail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table redirects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redirects`;

CREATE TABLE `redirects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `header` varchar(10) DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `from` (`from`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table section_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `section_items`;

CREATE TABLE `section_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) unsigned NOT NULL,
  `content` text,
  `display_order` int(4) unsigned NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `content` text,
  `display_order` int(4) unsigned NOT NULL DEFAULT '9999',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table site_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_settings`;

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pritty name',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'text',
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `site_settings` WRITE;
/*!40000 ALTER TABLE `site_settings` DISABLE KEYS */;

INSERT INTO `site_settings` (`id`, `key`, `value`, `title`, `type`)
VALUES
  (1,'logo','assets/media/documents/bb.png','Admin Logo','file'),
  (2,'address_1','','Address Line 1','text'),
  (3,'address_2','','Address Line 2','text'),
  (4,'address_3','','Address Line 3','text'),
  (5,'lat_long','','Address Latitude / Longitude (-37.848400, 144.991135)','text'),
  (6,'contact_phone','','Contact Phone','text'),
  (7,'contact_email','','Contact Email','text'),
  (8,'front_end_logo','assets/media/documents/easy8-logo.png','Front End Logo','file'),
  (9,'front_end_logo_scroll',NULL,'Front End Logo on Scroll','file');

/*!40000 ALTER TABLE `site_settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
