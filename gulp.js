"use strict";

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    cssmin = require("gulp-cssmin"),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    minjs = require("gulp-minify"),
    webserver = require("gulp-webserver");

gulp.task("doHtml", function(){
  return gulp.src("./assets/**/*.html");
});

gulp.task("doSass", function(){
  return gulp.src("./assets/scss/styles.scss")
    .pipe(sass())
    .pipe(cssmin())
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest("./assets/css"))
});

gulp.task("watch", function(){
  gulp.watch("./assets/**/*.html", ["doHtml"]);
  gulp.watch("./assets/scss/**/*.scss", ["doSass"]);
});

gulp.task("webserver", function() {
  gulp.src("./assets")
    .pipe(webserver({
      host: "localhost",
      port: "8181",
      open: true
    }));
});

gulp.task("default", [
  "doHtml",
  "doSass",
  "watch",
  "webserver"
]);